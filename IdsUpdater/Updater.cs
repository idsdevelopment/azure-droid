﻿#nullable enable

using System;
using System.Diagnostics;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace IdsUpdater;

public class Updater
{
	public const string BASE_URI          = "https://idsRoute.azureedge.net/downloads",
						VERSION_FILE_NAME = "CurrentVersion.txt",
						DEFAULT_RESELLER  = "Default";

	public const int DEFAULT_POLLING_INTERVAL_IN_MINUTES = 60;

	~Updater()
	{
		KillTimer();
	}

	public Updater( uint                         currentVersionNumber,
					string                       versionTextFileName,
					string                       baseFolder,
					string                       installerExeName,
					Action<string, uint, byte[]> onUpdateAvailable,
					string                       resellerAccountId        = DEFAULT_RESELLER,
					int                          pollingIntervalInMinutes = DEFAULT_POLLING_INTERVAL_IN_MINUTES,
					Action<string>?              installer                = null )
	{
		Installer = installer;

		CurrentVersionNumber = currentVersionNumber;
		InstallFileName      = installerExeName.Trim();
		baseFolder           = baseFolder.TrimEnd( '/' );

		var BaseUri = $"{BASE_URI}/{baseFolder}/{resellerAccountId}/";
		VersionUri    = new Uri( $"{BaseUri}{versionTextFileName}" );
		InstallExeUri = new Uri( $"{BaseUri}{InstallFileName}" );

		void DoUpdate( uint version, byte[] bytes )
		{
			onUpdateAvailable( InstallFileName, version, bytes );
		}

		Task.Run( () =>
				  {
					  CheckForUpdate( DoUpdate );

					  if( pollingIntervalInMinutes > 0 )
					  {
						  CheckForUpdateTimer = new Timer {Interval = pollingIntervalInMinutes * 60 * 1000};

						  CheckForUpdateTimer.Elapsed += ( _, _ ) =>
													     {
														     lock( CheckForUpdateTimer )
														     {
															     CheckForUpdate( ( version, bytes ) =>
																		         {
																			         KillTimer();
																			         DoUpdate( version, bytes );
																		         } );
														     }
													     };
						  CheckForUpdateTimer.Start();
					  }
				  } );
	}

	public Updater( uint                         currentVersionNumber,
					string                       baseFolder,
					string                       installerExeName,
					Action<string, uint, byte[]> onUpdateAvailable,
					string                       resellerAccountId        = DEFAULT_RESELLER,
					int                          pollingIntervalInMinutes = DEFAULT_POLLING_INTERVAL_IN_MINUTES,
					Action<string>?              installer                = null ) : this( currentVersionNumber,
																						   VERSION_FILE_NAME,
																						   baseFolder,
																						   installerExeName,
																						   onUpdateAvailable,
																						   resellerAccountId,
																						   pollingIntervalInMinutes,
																						   installer )
	{
	}

	private Timer? CheckForUpdateTimer;

	private readonly uint CurrentVersionNumber;

	private readonly Action<string>? Installer;

	private readonly string InstallFileName;

	private readonly Uri VersionUri,
						 InstallExeUri;

	private async void CheckForUpdate( Action<uint, byte[]> onUpdateAvailable )
	{
		try
		{
			byte[] Bytes;

			using( var Client = new HttpClient {Timeout = new TimeSpan( 0, 0, 0, 10 )} )
				Bytes = await Client.GetByteArrayAsync( VersionUri );

			var Version = Encoding.UTF8.GetString( Bytes, 3, Bytes.Length - 3 ).Trim();

			if( uint.TryParse( Version, out var ServerVersion ) )
			{
				if( ServerVersion > CurrentVersionNumber )
				{
					byte[] InstallBytes;

					using( var Client = new HttpClient {Timeout = new TimeSpan( 0, 0, 15, 0 )} )
						InstallBytes = await Client.GetByteArrayAsync( InstallExeUri );

					onUpdateAvailable( ServerVersion, InstallBytes );
				}
			}
		}
		catch( Exception E )
		{
			Console.WriteLine( E );
		}
	}

	private void KillTimer()
	{
		// ReSharper disable once InconsistentlySynchronizedField
		if( CheckForUpdateTimer is not null )
		{
			lock( CheckForUpdateTimer )
			{
				var T = CheckForUpdateTimer;
				CheckForUpdateTimer = null;
				T.Stop();
				T.Dispose();
			}
		}
	}

	private static void DoInstall( string filename )
	{
		Process.Start( filename )?.WaitForInputIdle( 10000 );
	}

	public void RunInstall()
	{
		( Installer ?? DoInstall )( InstallFileName );
	}
}