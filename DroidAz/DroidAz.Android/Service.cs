﻿using DroidAz.Droid;
using Application = Android.App.Application;
using Object = Java.Lang.Object;

namespace DroidAz;

[Service]
public class IdsDroidService : Service
{
	public IBinder Binder { get; private set; }

	public bool ServiceStarted { get; protected internal set; }

	private Intent Intent;

	private LocationProvider Locations;


	public override StartCommandResult OnStartCommand( Intent intent, StartCommandFlags flags, int startId )
	{
		Intent    = intent;
		Locations = new LocationProvider( this );
		return StartCommandResult.Sticky;
	}

	public void StopService()
	{
		StopService( Intent );
		ServiceStarted = false;
	}

	public override void OnDestroy()
	{
		StopService();
		Locations?.Dispose();
		Locations = null;
		base.OnDestroy();
	}

	public override IBinder OnBind( Intent intent )
	{
		return Binder = new IdsDroidServiceBinder( this );
	}
}

public class IdsDroidServiceBinder : Binder
{
	public IdsDroidService Service { get; }

	public IdsDroidServiceBinder( IdsDroidService service )
	{
		Service = service;
	}
}

public class IdsDroidServiceConnection : Object, IServiceConnection
{
	private readonly MainActivity Activity;

	public Action<IdsDroidService> OnConnected;

	public IdsDroidServiceConnection( MainActivity activity )
	{
		Activity = activity;
	}

	public void OnServiceConnected( ComponentName name, IBinder service )
	{
		if( service is IdsDroidServiceBinder ServiceBinder )
		{
			Activity.Binder  = ServiceBinder;
			Activity.IsBound = true;

			if( !ServiceBinder.Service.ServiceStarted )
			{
				ServiceBinder.Service.ServiceStarted = true;
				Application.Context.StartService( AndroidGlobals.Background.ServiceIntent );
				OnConnected?.Invoke( ServiceBinder.Service );
			}
		}
	}

	public void OnServiceDisconnected( ComponentName name )
	{
		Activity.IsBound = false;
	}
}