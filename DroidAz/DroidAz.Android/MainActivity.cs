﻿#nullable enable

#pragma warning disable CS0618 // Type or member is obsolete

using Android.Content.PM;
using DroidAz.Annotations;
using DroidAz.Droid.BarcodeScanners;
using DynamsoftBarcodeScanner;
using Globals;
using Environment = Android.OS.Environment;
using File = Java.IO.File;
using FileProvider = Android.Support.V4.Content.FileProvider;
using Keyboard = Globals.Keyboard;
using Platform = Xamarin.Essentials.Platform;
using Settings = Android.Provider.Settings;
using Sounds = DroidAz.Droid.Globals.Sounds;
using Uri = Android.Net.Uri;

// ReSharper disable InconsistentNaming

namespace DroidAz.Droid;

[Activity( Label = "IDS Mobile",
		   Icon = "@mipmap/icon",
		   Theme = "@style/SplashScreen",
		   MainLauncher = true,
		   AlwaysRetainTaskState = true,
		   LaunchMode = LaunchMode.SingleTask,
//		   ClearTaskOnLaunch = true,
		   ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation | ConfigChanges.Keyboard | ConfigChanges.KeyboardHidden /*,
		ScreenOrientation = ScreenOrientation.Portrait */ )]
public partial class MainActivity : FormsAppCompatActivity
{
	public static MainActivity? Instance { get; private set; }

	[UsedImplicitly]
	private static IdsDroidService? Service;

	private static App? AppInstance;
	private        bool Paused;

	private enum PERMISSIONS
	{
		LOCATION_PERMISSION_REQUEST,
		STORAGE_REQUEST,
		INSTALL_REQUEST,
		NO_DOZE,
		CAMERA
	}

	private Action? NextPermissionCallback;

	public IdsDroidServiceBinder? Binder;
	public bool                   IsBound;


	protected override void OnPause()
	{
		Paused = true;
		base.OnPause();
	}

	protected override void OnResume()
	{
		Paused = false;
		base.OnResume();
	}

	protected override void OnCreate( Bundle savedInstanceState )
	{
		Instance                      = this;
		AndroidGlobals.Debug.Activity = this;

		base.OnCreate( savedInstanceState );

		Platform.Init( this, savedInstanceState ); // add this line to your code, it may also be called: bundle

		InitOrientation();

		Sounds.InitSounds();

		Keyboard.ShowHide = show =>
							{
								Globals.Keyboard.Show = show;
							};

	#region Install
		Updates.OnSaveUpdate = ( filename, _, bytes ) =>
							   {
								   try
								   {
									   var FileName = MainActivity.FileName( filename ).AbsolutePath;

									   System.IO.File.Delete( FileName );
									   System.IO.File.WriteAllBytes( FileName, bytes );
								   }
								   catch( Exception E )
								   {
									   Console.WriteLine( E );
								   }
							   };

		Updates.OnRunUpdate = filename =>
							  {
								  try
								  {
									  var F = FileName( filename );

									  if( F.Exists() )
									  {
										  Intent InstallIntent;
										  Uri    U;
										  var    MajorVersion = AndroidGlobals.Android.MajorVersion;

										  if( MajorVersion >= 7 )
										  {
											  InstallIntent = new Intent( Intent.ActionInstallPackage );
											  U             = FileProvider.GetUriForFile( this, "com.idsapp.fileprovider", F );
										  }
										  else
										  {
											  InstallIntent = new Intent( Intent.ActionView );
											  U             = Uri.FromFile( F )!;
										  }

										  InstallIntent.SetDataAndType( U, "application/vnd.android.package-archive" );
										  InstallIntent.SetFlags( ActivityFlags.NewTask | ActivityFlags.GrantReadUriPermission | ActivityFlags.GrantWriteUriPermission );
										  StartActivity( InstallIntent );
									  }
								  }
								  catch( Exception Exception )
								  {
									  Console.WriteLine( Exception );
								  }
							  };
	#endregion

		TabLayoutResource = Resource.Layout.Tabbar;
		ToolbarResource   = Resource.Layout.Toolbar;

		GetPermissions();

		global::Globals.BarcodeScanners.InitDynamSoftScanner = testing => new DynamsoftBarcodeScannerInterfaced( new BarcodeScanner( this, testing ) );

		Forms.Init( this, savedInstanceState );
		LoadApplication( AppInstance ??= new App() );
	}

	public override void OnRequestPermissionsResult( int requestCode, string[]? permissions, [GeneratedEnum] Permission[]? grantResults )
	{
		if( grantResults is not null && ( ( grantResults.Length <= 0 ) || ( grantResults[ 0 ] == Permission.Denied ) ) )
		{
			string Text;

			switch( (PERMISSIONS)requestCode )
			{
			case PERMISSIONS.CAMERA:
				Text = "Camera";
				break;

			case PERMISSIONS.LOCATION_PERMISSION_REQUEST:
				Text = "GPS";
				break;

			case PERMISSIONS.STORAGE_REQUEST:
				Text = "Storage";
				break;

			case PERMISSIONS.INSTALL_REQUEST:
				Text = "Install";
				break;

			case PERMISSIONS.NO_DOZE:
				Text = "Disable Battery Optimisation";
				break;

			default:
				return;
			}

			ShowErrorDialogue( Text );
		}
		else
			NextPermissionCallback?.Invoke();
	}

	private static File FileName( string filename ) => new( Environment.GetExternalStoragePublicDirectory( Environment.DirectoryDownloads ), filename );

	private void AskPermission( string[] permission, PERMISSIONS requestCode, Action completedCallback )
	{
		NextPermissionCallback = completedCallback;

		// Here, thisActivity is the current activity
		if( CheckSelfPermission( permission[ 0 ] ) != Permission.Granted )
		{
			if( !ShouldShowRequestPermissionRationale( permission[ 0 ] ) )
			{
				RequestPermissions( permission, (int)requestCode );
				return;
			}
		}

		completedCallback.Invoke();
	}

	private void DoService()
	{
		AndroidGlobals.Background.ServiceIntent = new Intent( this, typeof( IdsDroidService ) );

		AndroidGlobals.Background.Connection = new IdsDroidServiceConnection( this )
											   {
												   OnConnected = service =>
																 {
																	 Service = service;
																	 StartService( AndroidGlobals.Background.ServiceIntent );
																 }
											   };

		// Keep active across configuration changes use ApplicationContext
		ApplicationContext?.BindService( AndroidGlobals.Background.ServiceIntent, AndroidGlobals.Background.Connection, Bind.AutoCreate );
	}

	private void GetPermissions()
	{
		var MajorVersion = AndroidGlobals.Android.MajorVersion;

		void Android_6_Permissions()
		{
			AskPermission( new[] {Manifest.Permission.Camera, Manifest.Permission.CaptureVideoOutput},
						   PERMISSIONS.CAMERA,
						   () =>
						   {
							   void AskLoc()
							   {
								   AskPermission( new[] {Manifest.Permission.AccessFineLocation, Manifest.Permission.AccessCoarseLocation}, PERMISSIONS.LOCATION_PERMISSION_REQUEST, DoService );
							   }

							   if( MajorVersion < 13 )
							   {
								   AskPermission( new[] {Manifest.Permission.WriteExternalStorage, Manifest.Permission.ReadExternalStorage},
												  PERMISSIONS.STORAGE_REQUEST,
												  AskLoc );
							   }
							   else
							   {
								   AskLoc();
							   }
						   } );
		}

		void Android_7_Permissions()
		{
			if( MajorVersion == 7 )
			{
				AskPermission( new[] {Manifest.Permission.RequestInstallPackages},
							   PERMISSIONS.INSTALL_REQUEST,
							   Android_6_Permissions );
			}
			else
				Android_6_Permissions();
		}

		async void Android_8_Permissions()
		{
			if( MajorVersion >= 8 )
			{
				if( PackageManager is not null && !PackageManager.CanRequestPackageInstalls() )
				{
					var SettingsIntent = new Intent( Settings.ActionManageUnknownAppSources );
					SettingsIntent.SetData( Uri.Parse( "package:" + PackageName ) );
					StartActivity( SettingsIntent );

					var Ok = await Task.Run( () =>
											 {
												 while( !Paused )
													 Thread.Sleep( 1000 );

												 while( Paused ) // Wait for Settings Dialog To Close
													 Thread.Sleep( 1000 );

												 return PackageManager.CanRequestPackageInstalls();
											 } );

					if( !Ok )
					{
						ShowErrorDialogue( "Install" );
						return;
					}
				}
			}

			Android_7_Permissions();
		}

		void Android_9_Permissions()
		{
			if( MajorVersion >= 6 )
			{
				AskPermission( new[] {Manifest.Permission.RequestIgnoreBatteryOptimizations},
							   PERMISSIONS.NO_DOZE, Android_8_Permissions );
			}
			else
				Android_8_Permissions();
		}

		// Initial kick off
		Android_9_Permissions();
	}

	private void ShowErrorDialogue( string text )
	{
		var Alert = new AlertDialog.Builder( this );
		Alert.SetTitle( "Cannot continue" );
		Alert.SetMessage( $"Unable to continue without {text} permission." );

		Alert.SetPositiveButton( "Ok", ( _, _ ) =>
									   {
										   Finish();
									   } );

		var Dialog = Alert.Create();
		Dialog?.Show();
	}
}