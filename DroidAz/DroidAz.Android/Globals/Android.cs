﻿#nullable enable

namespace DroidAz.Droid;

public static partial class AndroidGlobals
{
	public static class Background
	{
		public static Intent?                    ServiceIntent;
		public static IdsDroidServiceConnection? Connection;
	}

	public static class Android
	{
		// ReSharper disable once MemberHidesStaticFromOuterClass
		public static string Version
		{
			get
			{
				var Vsn = global::Android.OS.Build.VERSION.Release;

				if( Vsn is not null )
				{
					var Temp = Vsn.Split( new[] { '.' }, StringSplitOptions.RemoveEmptyEntries );
					_MajorVersion = int.Parse( Temp[ 0 ] );
					_MinorVersion = Temp.Length > 1 ? int.Parse( Temp[ 1 ] ) : 0;
					_Build        = _MajorVersion;

					return Vsn;
				}
				return "";
			}
		}

		public static int MajorVersion
		{
			get
			{
				if( _MajorVersion == -1 )
				{
					var _ = Version;
				}

				return _MajorVersion;
			}
		}

		public static int MinorVersion
		{
			get
			{
				if( _MinorVersion == -1 )
				{
					var _ = Version;
				}

				return _MinorVersion;
			}
		}

		public static int Build
		{
			get
			{
				if( _Build == -1 )
				{
					var _ = Version;
				}

				return _Build;
			}
		}

		public static int VersionAsInt
		{
			get
			{
				if( AsInt == -1 ) // Needs {} for scope
					AsInt = int.Parse( Version.Replace( ".", "" ).Trim() );

				return AsInt;
			}
		}

		private static int _MajorVersion = -1,
		                   _MinorVersion = -1,
		                   _Build        = -1,
		                   AsInt         = -1;

		public static int ConvertPixelsToDp( float pixelValue )
		{
			var D = MainActivity.Instance?.Resources?.DisplayMetrics;
			return D is not null ? (int)( pixelValue / D.Density ) : 1;
		}

		public static int ConvertDpToPixels( float pixelValue )
		{
			var D = MainActivity.Instance?.Resources?.DisplayMetrics;
			return D is not null ? (int)( pixelValue * D.Density ) : 1;
		}
	}
}