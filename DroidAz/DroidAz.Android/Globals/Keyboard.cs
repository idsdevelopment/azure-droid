﻿#nullable enable

using Globals;
using Keyboard = Globals.Keyboard;
using Timer = System.Timers.Timer;

namespace DroidAz.Droid
{
	public partial class MainActivity : FormsAppCompatActivity
	{
		private static Timer? BackDebounceTimer;

		public override bool DispatchKeyEvent( KeyEvent? e )
		{
			if( e is not null )
			{
				if( e.KeyCode == Keycode.Back )
				{
					// Sometimes gives multiple events
					if( BackButton.AllowBackButton && ( BackDebounceTimer == null ) )
					{
						BackDebounceTimer = new Timer( 500 );

						BackDebounceTimer.Elapsed += ( _, _ ) =>
													 {
														 BackDebounceTimer.Dispose();
														 BackDebounceTimer = null;
													 };
						BackDebounceTimer.Start();
						BackButton.OnBackKey?.Invoke();
					}

					return true;
				}

				if( e.Action == KeyEventActions.Down )
				{
					Keyboard.BUTTON Button;

					switch( e.KeyCode )
					{
					case Keycode.ButtonL1:
						Button = Keyboard.BUTTON.LEFT_BUTTON_1;
						goto DoButton;

					case Keycode.ButtonR1:
						Button = Keyboard.BUTTON.RIGHT_BUTTON_1;
					DoButton:

						var Bu = new Keyboard.OnButtonPressData
								 {
									 Button = Button
								 };

						Keyboard._OnButtonPress.Raise( this, Bu );
						return !Bu.Handled && base.DispatchKeyEvent( e );
					}

					var Key = (char)e.GetUnicodeChar( e.MetaState );

					if( Key is >= ' ' and <= '~' or (char)0x9 or (char)0xd )
					{
						var Kd = new Keyboard.OnKeyPressData {Key = Key};

						Keyboard._OnKeyPress.Raise( this, Kd );

						if( Kd.Handled )
							return false;
					}
				}
			}
			return base.DispatchKeyEvent( e );
		}
	}
}

namespace DroidAz.Droid.Globals
{
	public static class Keyboard
	{
		public static bool Show
		{
			set
			{
				ShowValue = value;

				if( KbdTimer is null )
				{
					KbdTimer = new Timer( 20 ) {AutoReset = false};

					KbdTimer.Elapsed += ( _, _ ) =>
										{
											DoKbd( ShowValue );
										};
				}
				KbdTimer.Stop();
				KbdTimer.Start();
			}
		}

		public static bool Hide
		{
			set => Show = !value;
		}

		private static Timer? KbdTimer;

		private static volatile bool ShowValue;

		private static void DoKbd( bool show )
		{
			var Activity = MainActivity.Instance;

			Activity?.RunOnUiThread( () =>
								     {
									     var Focus = Activity.Window?.CurrentFocus;

									     if( Focus is not null )
									     {
										     var InputManager = (InputMethodManager?)Activity.GetSystemService( Context.InputMethodService );
										     InputManager?.HideSoftInputFromWindow( Focus.WindowToken, show ? HideSoftInputFlags.ImplicitOnly : HideSoftInputFlags.None );
									     }
								     } );
		}
	}
}