﻿namespace DroidAz.Droid.Globals;

// ReSharper disable once UnusedMember.Global
public static class UnhandledExceptions
{
	static UnhandledExceptions()
	{
		AppDomain.CurrentDomain.UnhandledException += ( _, exception ) =>
		                                              {
			                                              if( exception.ExceptionObject is Exception E )
				                                              global::Globals.UnhandledExceptions.OnHandledException?.Invoke( E );
		                                              };

		TaskScheduler.UnobservedTaskException += ( _, args ) =>
		                                         {
			                                         global::Globals.UnhandledExceptions.OnHandledException?.Invoke( args.Exception );
		                                         };

		AndroidEnvironment.UnhandledExceptionRaiser += ( _, args ) =>
		                                               {
			                                               global::Globals.UnhandledExceptions.OnHandledException?.Invoke( args.Exception );
		                                               };
	}
}