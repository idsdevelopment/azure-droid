﻿using Android.Content.PM;
using Orientation = Globals.Orientation;

namespace DroidAz.Droid;

public partial class MainActivity : FormsAppCompatActivity
{
	private void InitOrientation()
	{
		Orientation.ChangeOrientation = orientation =>
		                                {
			                                RequestedOrientation = orientation switch
			                                                       {
				                                                       Orientation.ORIENTATION.LANDSCAPE => ScreenOrientation.Landscape,
				                                                       Orientation.ORIENTATION.PORTRAIT  => ScreenOrientation.Portrait,
				                                                       _                                 => ScreenOrientation.Unspecified
			                                                       };
		                                };
	}
}