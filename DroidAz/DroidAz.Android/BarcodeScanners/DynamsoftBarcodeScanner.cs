﻿#nullable enable

#pragma warning disable 618

using Android.Graphics;
using Com.Dynamsoft.Dbr;
using static Android.Hardware.Camera;
using Camera = Android.Hardware.Camera;
using Exception = Java.Lang.Exception;
using ImageButton = Android.Widget.ImageButton;
using Resource = DroidAz.Droid.Resource;
using Timer = System.Timers.Timer;


namespace DynamsoftBarcodeScanner;

public class BarcodeScanner : LinearLayout, IDisposable, IPreviewCallback, ISurfaceHolderCallback, IDBRDLSLicenseVerificationListener
{
	private Activity Activity = null!;

	private readonly bool Testing;

	public Action<bool>? OnScanChange;

#region Screen Changes
	public override void OnScreenStateChanged( ScreenState screenState )
	{
		base.OnScreenStateChanged( screenState );

		if( screenState == ScreenState.Off )
			SurfaceDestroyed( null );
		else
			Initialize();
	}
#endregion

	public BarcodeScanner( Context context, bool testing ) : base( context )
	{
		Testing = testing;
		Initialize();
	}

	private void Initialize()
	{
		InitScanner( Testing );

		var View = ( Activity = (Activity)Context! ).LayoutInflater.Inflate( Resource.Layout.barcodeScanner, this )!;
		View.RemoveFromParent();

		Activity.AddContentView( View, new ViewGroup.LayoutParams( -1, -1 ) );
		Surface = View.FindViewById<SurfaceView>( Resource.Id.surface )!;

		StopBtn       =  View.FindViewById<ImageButton>( Resource.Id.stopBtn )!;
		StopBtn.Click += StopBtnOnClick;

		FlashBtn = View.FindViewById<ImageButton>( Resource.Id.flashBtn )!;

		FlashBtn.Click += OnFlashBtnClick;

		ZoomInBtn = View.FindViewById<ImageButton>( Resource.Id.zoomInBtn )!;

		ZoomInBtn.Click += ( _, _ ) =>
		                   {
			                   var Parameters = Camera?.GetParameters();

			                   if( Parameters is not null )
			                   {
				                   var CurZoom = Parameters.Zoom;

				                   if( ++CurZoom <= Parameters.MaxZoom )
				                   {
					                   Parameters.Zoom = CurZoom;
					                   Camera?.SetParameters( Parameters );
					                   Zoom = CurZoom;
				                   }
			                   }
		                   };

		ZoomOutBtn = View.FindViewById<ImageButton>( Resource.Id.zoomOutBtn )!;

		ZoomOutBtn.Click += ( _, _ ) =>
		                    {
			                    var Parameters = Camera?.GetParameters();

			                    if( Parameters != null )
			                    {
				                    var CurZoom = Parameters.Zoom;

				                    if( --CurZoom >= 0 )
				                    {
					                    Parameters.Zoom = CurZoom;
					                    Camera?.SetParameters( Parameters );
					                    Zoom = CurZoom;
				                    }
			                    }
		                    };
		Scanning = false;
	}

	private void StopBtnOnClick( object sender, EventArgs e )
	{
		Scanning = false;
	}

#region Barcodes
	private readonly List<string> BufferedBarcodes = new();

	private bool _UseBufferedBarcodes;

	public bool UseBufferedBarcodes
	{
		get => _UseBufferedBarcodes;
		set
		{
			if( value )
			{
				if( !_UseBufferedBarcodes )
					OnBarcodes += OnBarcodesEvent;
			}
			else
			{
				if( _UseBufferedBarcodes )
					OnBarcodes -= OnBarcodesEvent;
			}
			_UseBufferedBarcodes = value;
		}
	}

	public           List<string> BarcodeList { get; } = new();
	private readonly List<string> TempBarcodeList = new();

	private enum READ_STATE : byte
	{
		READ_0,
		READ_1
	}

	private READ_STATE ReadState;
#endregion


#region Flash
	private ImageButton FlashBtn = null!,
	                    StopBtn  = null!;


	private Timer? FlashOffTimer;

	private void OnFlashOffTimerOnElapsed( object o, ElapsedEventArgs? elapsedEventArgs )
	{
		var Cam = Camera;

		var Parameters = Cam?.GetParameters();

		if( Parameters is not null )
		{
			Parameters.FlashMode = Parameters.FlashModeOff;
			Cam!.SetParameters( Parameters );
		}
		_Flash = false;
	}


	private void EnableFlash( bool value )
	{
		var Cam = Camera;

		if( Cam is not null )
		{
			if( !value )
			{
				if( FlashOffTimer is null )
				{
					FlashOffTimer = new Timer( 100 )
					                {
						                AutoReset = false
					                };

					FlashOffTimer.Elapsed += OnFlashOffTimerOnElapsed;
				}
				FlashOffTimer.Start();
			}
			else
			{
				FlashOffTimer?.Stop();

				Cam.StartPreview();
				var Parameters = Cam.GetParameters();

				if( Parameters is not null )
				{
					Parameters.FlashMode = Parameters.FlashModeTorch;
					Cam.SetParameters( Parameters );
				}
			}
		}
	}

	private bool _Flash,
	             FlashSetting;

	private void InitFlash()
	{
		FlashSetting = Preferences.Get( FLASH, false );
		SetFlashImage();
	}

	private void SetFlashImage()
	{
		// ReSharper disable once AssignmentInConditionalExpression
		FlashBtn.SetImageResource( _Flash ? Resource.Drawable.flashon : Resource.Drawable.flashoff );
	}

	private bool Flash
	{
		get => _Flash;
		set
		{
			if( ( _Flash != value ) || !value )
				EnableFlash( value );
			_Flash = value;

			SetFlashImage();
		}
	}

	private void OnFlashBtnClick( object? o, EventArgs? _ )
	{
		var F = !Flash;
		Flash        = F;
		FlashSetting = F;
		Preferences.Set( FLASH, F );
	}
#endregion

#region Events
	private volatile bool BarcodeRunning;

	private void OnBarcodesEvent( object sender, List<string> barcodes )
	{
		lock( BufferedBarcodes )
		{
			BufferedBarcodes.AddRange( barcodes );

			if( !BarcodeRunning )
			{
				BarcodeRunning = true;

				Tasks.RunVoid( () =>
				               {
					               while( true )
					               {
						               string BCode;

						               lock( BufferedBarcodes )
						               {
							               if( BufferedBarcodes.Count <= 0 )
							               {
								               BarcodeRunning = false;
								               break;
							               }

							               BCode = BufferedBarcodes[ 0 ];
							               BufferedBarcodes.RemoveAt( 0 );
						               }

						               Activity.RunOnUiThread( () =>
						                                       {
							                                       RaiseBarcodeEvent( BCode );
						                                       } );
					               }
				               } );
			}
		}
	}

	private readonly WeakEventSource<List<string>> _BarcodesEventSource = new();

	public event EventHandler<List<string>> OnBarcodes
	{
		add => _BarcodesEventSource.Subscribe( value );
		remove => _BarcodesEventSource.Unsubscribe( value );
	}

	private void RaiseBarcodesEvent( List<string> e )
	{
		_BarcodesEventSource.Raise( this, e );
	}


	private readonly WeakEventSource<string> _BarcodeEventSource = new();

	public event EventHandler<string> OnBarcode
	{
		add => _BarcodeEventSource.Subscribe( value );
		remove => _BarcodeEventSource.Unsubscribe( value );
	}

	private void RaiseBarcodeEvent( string e )
	{
		_BarcodeEventSource.Raise( this, e );
	}
#endregion

#region Settings
	private const string ZOOM  = "Barcode_Zoom",
	                     FLASH = "Barcode_Flash";

	private static int Zoom
	{
		get => Preferences.Get( ZOOM, 5 );
		set => Preferences.Set( ZOOM, value );
	}

	private const    int  NEXT_SCAN_DELAY = 2000;
	private volatile bool InScanDelay;
#endregion

#region Camera
	private ImageButton ZoomInBtn  = null!,
	                    ZoomOutBtn = null!;

	private Camera? Camera;

	private int PreviewWidth,
	            PreviewHeight;

	private bool Processing;

	public void OnPreviewFrame( byte[]? data, Camera? _ )
	{
		if( BarcodeReader is not null && Scanning && !Processing )
		{
			Processing = true;

			try
			{
				var YuvImage   = new YuvImage( data, ImageFormatType.Nv21, PreviewWidth, PreviewHeight, null );
				var StrideList = YuvImage.GetStrides() ?? Array.Empty<int>();

				var Text = BarcodeReader.DecodeBuffer( YuvImage.GetYuvData(), PreviewWidth, PreviewHeight, StrideList[ 0 ], EnumImagePixelFormat.IpfNv21, "" );

				var Length = Text.Length;

				if( Length > 0 )
				{
					var Barcodes = ( from T in Text
					                 orderby T.BarcodeText
					                 select T.BarcodeText ).ToList();

					bool ReadOk()
					{
						return ( TempBarcodeList.Count == Length ) && ( Barcodes.Intersect( TempBarcodeList ).Count() == Length );
					}

					switch( ReadState )
					{
					case READ_STATE.READ_0:
						++ReadState;
						TempBarcodeList.AddRange( Barcodes );
						ReadState = READ_STATE.READ_1;
						break;

					case READ_STATE.READ_1 when ReadOk():
						BarcodeList.AddRange( Barcodes );
						Vibration.Vibrate( TimeSpan.FromMilliseconds( 250 ) );
						Scanning = false; // Must be before event in case event restarts the scanning
						TempBarcodeList.Clear();
						ReadState = READ_STATE.READ_0;
						RaiseBarcodesEvent( Barcodes );
						break;

					default:
						TempBarcodeList.Clear();
						ReadState = READ_STATE.READ_0;
						goto case READ_STATE.READ_0;
					}
				}
			}
			catch
			{
			}
			finally
			{
				Processing = false;
			}
		}
	}

	private Camera? OpenCamera()
	{
		Visibility = ViewStates.Visible;
		BringToFront();

		if( Camera is null || Holder is null )
		{
			try
			{
				if( Holder is null )
				{
					Holder = Surface.Holder;
					Holder?.AddCallback( this );
				}

				Camera = Open();

				if( Camera is { } Cam )
				{
					var Parameters = Cam.GetParameters();

					if( Parameters is not null )
					{
						Parameters.AutoExposureLock     = false;
						Parameters.ExposureCompensation = 0;

						if( Parameters.IsVideoStabilizationSupported )
							Parameters.VideoStabilization = true;

						if( Parameters.SupportedFocusModes is not null && Parameters.SupportedFocusModes.Contains( Parameters.FocusModeContinuousVideo ) )
							Parameters.FocusMode = Parameters.FocusModeContinuousVideo;

						//Parameters.ColorEffect = Camera.Parameters.EffectMono;

						Parameters.PreviewFrameRate = 30;
						Parameters.PictureFormat    = ImageFormatType.Jpeg;
						Parameters.PreviewFormat    = ImageFormatType.Nv21;

						Parameters.Zoom = Zoom;

						var SupportedPreviewSizes = Parameters.SupportedPreviewSizes;

						if( SupportedPreviewSizes is not null )
						{
							var PreviewSize = ( from S in SupportedPreviewSizes
							                    orderby S.Width descending
							                    select S ).First();

							Parameters.SetPreviewSize( PreviewSize.Width, PreviewSize.Height );
						}

						Cam.SetParameters( Parameters );

						var ConfigurationOrientation = Resources?.Configuration?.Orientation ?? Android.Content.Res.Orientation.Portrait;

						Cam.SetDisplayOrientation( ConfigurationOrientation == Android.Content.Res.Orientation.Portrait ? 90 : 0 );
						Cam.SetPreviewDisplay( Holder );

						//Get camera width
						if( Parameters.PreviewSize is not null )
						{
							PreviewWidth  = Parameters.PreviewSize.Width;
							PreviewHeight = Parameters.PreviewSize.Height;
						}
						Surface.DrawingCacheEnabled = true;
					}
				}
			}
			catch( Exception Exception )
			{
				Console.WriteLine( $"Couldn't open camera: {Exception.Message}" );
				Camera = null;
			}
		}
		return Camera;
	}
#endregion

#region Scanner
	public void DLSLicenseVerificationCallback( bool ok, Exception error )
	{
	}

	private const string ORGANIZATION_ID        = "100241000",
	                     HANDSHAKE_CODE         = "100241000-100791455",
	                     TESTING_HANDSHAKE_CODE = "100241000-Testing";

	private BarcodeReader? BarcodeReader;

	private bool _Scanning;

	private const int FORMATS = EnumBarcodeFormat.BfCode128
	                            | EnumBarcodeFormat.BfCode39
	                            | EnumBarcodeFormat.BfUpcA
	                            | EnumBarcodeFormat.BfUpcE
	                            | EnumBarcodeFormat.BfEan13;

	private void InitScanner( bool testing )
	{
		var Br = new BarcodeReader();

		Br.InitLicenseFromDLS( new DMDLSConnectionParameters
		                       {
			                       OrganizationID = ORGANIZATION_ID,
			                       HandshakeCode  = testing ? TESTING_HANDSHAKE_CODE : HANDSHAKE_CODE
		                       }, this );

		var Settings = Br.RuntimeSettings;
		Settings.BarcodeFormatIds    = FORMATS;
		Settings.BarcodeFormatIds2   = FORMATS;
		Settings.MinResultConfidence = 1;

		Settings.LocalizationModes = new[]
		                             {
			                             EnumLocalizationMode.LmConnectedBlocks, 0, 0, 0, 0, 0, 0, 0
		                             };

		Settings.FurtherModes.ImagePreprocessingModes = new[]
		                                                {
			                                                EnumImagePreprocessingMode.IpmGraySmooth, EnumImagePreprocessingMode.IpmGeneral, 0, 0, 0, 0, 0, 0
		                                                };

		Br.UpdateRuntimeSettings( Settings );
		BarcodeReader = Br;
	}

#region Scanning
	private bool _ContinuousScanning;

	public bool ContinuousScanning
	{
		get => _ContinuousScanning;
		set
		{
			_ContinuousScanning = value;
			Scanning            = value;
		}
	}

	private bool First = true;

	public bool Scanning
	{
		get => _Scanning;

		set
		{
			if( ( _Scanning != value ) || First )
			{
				First = false;

				Activity.RunOnUiThread( () =>
				                        {
					                        if( OpenCamera() is { } Cam )
					                        {
						                        if( value )
						                        {
							                        Cam.SetPreviewCallback( this );
							                        Cam.StartPreview();

							                        if( !Flash && FlashSetting )
								                        Flash = true;

							                        Tasks.RunVoid( () =>
							                                       {
								                                       Processing = false;

								                                       BarcodeList.Clear();

								                                       while( InScanDelay )
									                                       Thread.Sleep( 10 );

								                                       _Scanning = true;
							                                       } );
						                        }
						                        else
						                        {
							                        _Scanning = false;

							                        OnFlashOffTimerOnElapsed( this, null );

							                        Cam.StopPreview();
							                        Cam.SetPreviewCallback( null );

							                        Visibility  = ViewStates.Invisible;
							                        InScanDelay = true;

							                        Tasks.RunVoid( NEXT_SCAN_DELAY, () =>
							                                                        {
								                                                        InScanDelay = false;
							                                                        } );
						                        }
					                        }
					                        OnScanChange?.Invoke( value );
				                        } );
			}
		}
	}
#endregion
#endregion

#region Sureface
	private SurfaceView     Surface = null!;
	private ISurfaceHolder? Holder;

	public void SurfaceChanged( ISurfaceHolder? holder, Format format, int width, int height )
	{
		if( Camera is { } Cam )
		{
			Cam.SetPreviewCallback( null );
			Cam.SetPreviewDisplay( Holder );
			Cam.SetPreviewCallback( this );
			InitFlash();
		}
	}

	public void SurfaceCreated( ISurfaceHolder? holder )
	{
		Camera?.Reconnect(); // Generates SurfaceChanges
	}

	public void SurfaceDestroyed( ISurfaceHolder? holder )
	{
		if( Holder is not null )
		{
			Holder.RemoveCallback( this );
			Holder = null;
		}

		if( Camera is not null )
		{
			Camera.SetPreviewCallback( null );
			Camera.StopPreview();
			Camera.Release();
			Camera = null;
		}

		if( FlashOffTimer is not null )
		{
			FlashOffTimer.Dispose();
			FlashOffTimer = null;
		}
	}
#endregion
}