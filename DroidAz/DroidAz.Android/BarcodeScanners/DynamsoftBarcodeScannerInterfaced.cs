﻿#nullable enable

using BarcodeInterface;
using DynamsoftBarcodeScanner;

namespace DroidAz.Droid.BarcodeScanners;

public class DynamsoftBarcodeScannerInterfaced : IBarcodeScanner
{
	public bool Scanning
	{
		get => Scanner.Scanning;
		set => Scanner.Scanning = value;
	}

	public bool IsEnabled { get; set; }

	public string          Placeholder          { get; set; } = "";
	public Color           PlaceholderTextColor { get; set; }
	public Action<string>? OnBarcode            { get; set; }
	public ContentView     ScannerView          { get; set; } = null!;

	private readonly BarcodeScanner Scanner;

	private readonly object LockObject = new();

	public DynamsoftBarcodeScannerInterfaced( BarcodeScanner scanner )
	{
		Scanner                     = scanner;
		Scanner.UseBufferedBarcodes = true;

		Scanner.OnBarcode += ( _, barcode ) =>
		                     {
			                     lock( LockObject )
				                     _Barcode = barcode;

			                     OnBarcode?.Invoke( barcode );
		                     };
	}

	public bool Focus() => true;

#region Barcode
	private string _Barcode = "";

	public string Barcode
	{
		get
		{
			string Result;

			lock( LockObject )
			{
				Result   = _Barcode;
				_Barcode = "";
			}
			return Result;
		}
	}
#endregion
}