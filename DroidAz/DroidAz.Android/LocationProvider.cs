﻿using Android.Locations;
using DroidAz.Droid;
using Protocol.Data;
using Location = Android.Locations.Location;
using Object = Java.Lang.Object;
using Settings = Android.Provider.Settings;
using Timer = System.Timers.Timer;

namespace DroidAz;

public class LocationProvider : Object, ILocationListener
{
	private const int DEFAULT_LOCATION_TICK_IN_SECONDS = 30;

	private readonly Criteria CriteriaForLocationService = new()
	                                                       {
		                                                       Accuracy = Accuracy.Fine
	                                                       };

	private double LastLat,
	               LastLong;

	private readonly Timer LocationTimer;

	private LocationManager Manager;

	private volatile string Provider = "";

	protected override void Dispose( bool disposing )
	{
		if( disposing && Manager is not null )
		{
			Manager.RemoveUpdates( this );
			Manager.Dispose();
			Manager = null;
		}

		base.Dispose( disposing );
	}

	public LocationProvider( Context context )
	{
		Manager = (LocationManager)context.GetSystemService( Context.LocationService );

		GetProvider();

		// If GPS is off open up GPS settings
		if( string.IsNullOrEmpty( Provider ) || !Manager.IsProviderEnabled( Provider ) )
		{
			AndroidGlobals.Debug.Activity.RunOnUiThread( () =>
			                                             {
				                                             var GpsSettingsIntent = new Intent( Settings.ActionLocationSourceSettings );
				                                             GpsSettingsIntent.SetFlags( ActivityFlags.NewTask );
				                                             context.StartActivity( GpsSettingsIntent );
			                                             } );
		}

		WaitProvider();
		LocationTimer = new Timer( DEFAULT_LOCATION_TICK_IN_SECONDS * 1000 );

		LocationTimer.Elapsed += ( _, _ ) =>
		                         {
			                         if( ( LastLat != 0 ) && ( LastLong != 0 ) )
				                         RaiseNewPosition();
		                         };
		LocationTimer.Start();
	}

	private void RaiseNewPosition()
	{
		if( Globals.Settings.GpsEnabled )
		{
			Globals.Gps.CurrentGpsPoint = new GpsPoint
			                              {
				                              LocalDateTime = DateTimeOffset.Now,
				                              Latitude      = LastLat,
				                              Longitude     = LastLong
			                              };
		}
	}

	private void GetProvider()
	{
		if( AndroidGlobals.Android.VersionAsInt <= 412 )
			Provider = LocationManager.GpsProvider;
		else
		{
			var AcceptableLocationProviders = Manager.GetProviders( CriteriaForLocationService, true );
			Provider = AcceptableLocationProviders!.Any() ? AcceptableLocationProviders.First() : string.Empty;
		}
	}

	private void WaitProvider()
	{
		Task.Run( () =>
		          {
			          while( true )
			          {
				          GetProvider();

				          if( string.IsNullOrWhiteSpace( Provider ) || !Manager.IsProviderEnabled( Provider ) )
					          Thread.Sleep( 100 );
				          else
					          break;
			          }

			          var Act = AndroidGlobals.Debug.Activity;

			          while( Act == null )
			          {
				          Thread.Sleep( 100 );
				          Act = AndroidGlobals.Debug.Activity;
			          }

			          Act.RunOnUiThread( () =>
			                             {
				                             Manager.RequestLocationUpdates( Provider, 1500, 1, this );
			                             } );
		          } );
	}

	// ILocationListener Interface routines
	public void OnLocationChanged( Location location )
	{
		LastLat  = location.Latitude;
		LastLong = location.Longitude;

		LocationTimer.Stop();
		LocationTimer.Start();

		RaiseNewPosition();
	}

	public void OnProviderDisabled( string provider )
	{
	}

	public void OnProviderEnabled( string provider )
	{
	}

	public void OnStatusChanged( string provider, Availability status, Bundle extras )
	{
	}
}