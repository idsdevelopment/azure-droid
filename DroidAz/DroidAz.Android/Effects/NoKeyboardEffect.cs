﻿using Debug = System.Diagnostics.Debug;

namespace DroidAz.Droid.Effects
{
	namespace ControlSamples.Droid.Effects
	{
		// ReSharper disable once InconsistentNaming
		public class NoKeyboardEffect_Droid : PlatformEffect
		{
			protected override void OnAttached()
			{
				try
				{
					if( Control is EditText EditText )
					{
						// cursor shown, but keyboard will not be displayed
						if( Build.VERSION.SdkInt >= BuildVersionCodes.Lollipop )
							EditText.ShowSoftInputOnFocus = false;
						else
							EditText.SetTextIsSelectable( true );
					}
				}
				catch( Exception Ex )
				{
					Debug.WriteLine( $"{nameof( NoKeyboardEffect_Droid )} failed to attached: {Ex.Message}" );
				}
			}

			protected override void OnDetached()
			{
			}
		}
	}
}