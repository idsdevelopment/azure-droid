using DroidAz.Droid.Effects.ControlSamples.Droid.Effects;
using DroidAz.Effects;

[assembly: ResolutionGroupName( "Ids.Effects" )]
[assembly: ExportEffect( typeof( NoKeyboardEffect_Droid ), nameof( NoKeyboardEffect ) )]
[assembly: UsesFeature( "android.hardware.camera.autofocus", Required = true )]