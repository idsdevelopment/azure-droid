#nullable enable


// ReSharper disable SuggestBaseTypeForParameter

[assembly: ExportRenderer( typeof( ExtendedEntry ), typeof( ExtendedEntryRenderer ) )]

namespace XamarinAndroidEntry.Droid;

/// <summary>
///     Class ExtendedEntryRenderer.
/// </summary>
public class ExtendedEntryRenderer : EntryRenderer, IVirtualKeyboard
{
	/// <summary>
	///     Called when [element changed].
	/// </summary>
	/// <param
	///     name="e">
	///     The e.
	/// </param>
	protected override void OnElementChanged( ElementChangedEventArgs<Entry> e )
	{
		base.OnElementChanged( e );

		if( e.OldElement is null && Control is not null )
		{
			var View = (ExtendedEntry)Element;
			var Text = Control;

			Text.Click += ( _, _ ) =>
						  {
							  if( !View.ShowVirtualKeyboardOnFocus )
								  HideKeyboard();
						  };

			var (L, T, R, B) = View.Padding;

			Text.SetPadding( (int)L, (int)T, (int)R, (int)B );
			Text.SetTextIsSelectable( true );
			Text.SetSelectAllOnFocus( true );

			SetFont( View );
			SetTextAlignment( View );

			SetPlaceholderTextColor( View );
			SetMaxLength( View );

			View.VirtualKeyboardHandler = this;
		}
	}

	/// <summary>
	///     Handles the
	///     <see
	///         cref="E:ElementPropertyChanged" />
	///     event.
	/// </summary>
	/// <param
	///     name="sender">
	///     The sender.
	/// </param>
	/// <param
	///     name="e">
	///     The
	///     <see
	///         cref="System.ComponentModel.PropertyChangedEventArgs" />
	///     instance containing the event data.
	/// </param>
	protected override void OnElementPropertyChanged( object sender, PropertyChangedEventArgs e )
	{
		base.OnElementPropertyChanged( sender, e );

		var View = (ExtendedEntry)Element;

		if( e.PropertyName == Entry.TextColorProperty.PropertyName )
			SetTextColor( View );

		else if( e.PropertyName == ExtendedEntry.PlaceholderTextColorProperty.PropertyName )
			SetPlaceholderTextColor( View );

		else if( e.PropertyName == InputView.MaxLengthProperty.PropertyName )
			SetMaxLength( View );

		else if( e.PropertyName == ExtendedEntry.FontProperty.PropertyName )
			SetFont( View );

		else if( e.PropertyName == ExtendedEntry.XAlignProperty.PropertyName )
			SetTextAlignment( View );
	}

	public ExtendedEntryRenderer( Context context ) : base( context )
	{
	}

	/// <summary>
	///     Sets the text alignment.
	/// </summary>
	/// <param
	///     name="view">
	///     The view.
	/// </param>
	private void SetTextAlignment( ExtendedEntry view )
	{
		Control.Gravity = view.XAlign switch
						  {
							  Xamarin.Forms.TextAlignment.Center => GravityFlags.CenterHorizontal,
							  Xamarin.Forms.TextAlignment.End    => GravityFlags.End,
							  Xamarin.Forms.TextAlignment.Start  => GravityFlags.Start,
							  _                                  => Control.Gravity
						  };
	}

	/// <summary>
	///     Sets the font.
	/// </summary>
	/// <param
	///     name="view">
	///     The view.
	/// </param>
	private void SetFont( ExtendedEntry view )
	{
		if( view.Font != Font.Default )
		{
			Control.TextSize = view.Font.ToScaledPixel();
			Control.Typeface = view.Font.ToTypeface();
		}
	}

	/// <summary>
	///     Sets the color of the placeholder text.
	/// </summary>
	/// <param
	///     name="view">
	///     The view.
	/// </param>
	private void SetPlaceholderTextColor( ExtendedEntry view )
	{
		if( view.PlaceholderTextColor != Color.Default )
			Control.SetHintTextColor( view.PlaceholderTextColor.ToAndroid() );
	}

	/// <summary>
	///     Sets the color of the  text.
	/// </summary>
	/// <param
	///     name="view">
	///     The view.
	/// </param>
	private void SetTextColor( ExtendedEntry view )
	{
		if( view.TextColor != Color.Default )
			Control.SetTextColor( view.TextColor.ToAndroid() );
	}

	/// <summary>
	///     Sets the MaxLength Characters.
	/// </summary>
	/// <param
	///     name="view">
	///     The view.
	/// </param>
	private void SetMaxLength( ExtendedEntry view )
	{
		Control.SetFilters( new IInputFilter[] {new InputFilterLengthFilter( view.MaxLength )} );
	}

	public void ShowKeyboard()
	{
		try
		{
			Control.RequestFocus();

			if( Control.Context?.GetSystemService( Context.InputMethodService ) is InputMethodManager InputMethodManager )
			{
				InputMethodManager.ShowSoftInput( Control, ShowFlags.Forced );
 #pragma warning disable CS0618
				InputMethodManager.ToggleSoftInput( ShowFlags.Forced, HideSoftInputFlags.ImplicitOnly );
 #pragma warning restore CS0618
			}
		}
		catch // Probably Disposed
		{
		}
	}

	public void HideKeyboard()
	{
		try
		{
			Control.RequestFocus();

			if( Control.Context?.GetSystemService( Context.InputMethodService ) is InputMethodManager InputMethodManager )
			{
				InputMethodManager.ShowSoftInput( Control, ShowFlags.Implicit );
				InputMethodManager.HideSoftInputFromWindow( Control.WindowToken, HideSoftInputFlags.None ); // this probably needs to be set to ToogleSoftInput, forced.
			}
		}
		catch // Probably Disposed
		{
		}
	}
}