﻿#nullable enable

using DroidAz.Views._Standard.Menu;

namespace DroidAz;

public static class Applications
{
	public const string SIGN_IN           = "SignIn",
						BLANK_PAGE        = "BlankPage",
						DELIVERIES_LOADER = "Pickups & Deliveries";

	public class MenuEntry
	{
		public string Text { get; set; } = "";
		public string Id = "";
	}

	public class Program
	{
		public bool                    ConfirmTripRemove;
		public bool                    Default;
		public Orientation.ORIENTATION Orientation = Globals.Orientation.ORIENTATION.PORTRAIT;
		public Type?                   PageType;
	}

	public static string CurrentProgram = "";

	public static ContentView? LoadArea;
	public static ContentPage? MainContentPage;

	public static bool ShowMenu
	{
		get => MainPageViewModel.Instance is not null && MainPageViewModel.Instance.MenuVisible;

		set
		{
			if( MainPageViewModel.Instance is not null )
				MainPageViewModel.Instance.MenuVisible = value;
		}
	}

	public static Dictionary<string, Program>? Programs
	{
		get => _Programs;
		set
		{
			_Programs = value;

			if( value is not null )
			{
				var MenuEntries = new ObservableCollection<MenuEntry>();

				foreach( var Pgm in value )
				{
					var Text = Pgm.Key;

					if( ( Text != BLANK_PAGE ) && ( Text != SIGN_IN ) )
					{
						var Entry = new MenuEntry
									{
										Id   = Text,
										Text = Globals.Resources.GetStringResource( Text )
									};

						MenuEntries.Add( Entry );
					}
				}

				if( MainPageViewModel.Instance is not null )
					MainPageViewModel.Instance.MenuItemsSource = MenuEntries;
			}
		}
	}

	private static Dictionary<string, Program>? _Programs;

	private static ContentView RunProgram( ContentView loadArea, ContentView program )
	{
		try
		{
			if( MainPageViewModel.Instance is not null )
				MainPageViewModel.Instance.ShowTripCount = false;

			var Content = loadArea.Content;

			if( Content is not null )
				Content.IsVisible = false;

			loadArea.Content          = program;
			program.HorizontalOptions = LayoutOptions.FillAndExpand;
			program.VerticalOptions   = LayoutOptions.FillAndExpand;
		}
		catch( Exception Exception )
		{
			Console.WriteLine( Exception );
		}

		return program;
	}

	private static string GetDefaultProgram()
	{
		if( Programs is not null )
		{
			foreach( var P in Programs )
			{
				var Program = P.Value;

				if( Program.Default )
					return P.Key;
			}
		}
		return BLANK_PAGE;
	}

	public static void Kill()
	{
		Process.GetCurrentProcess().Kill();
	}

	public static ContentView? RunProgram( ContentView? loadArea, Type? pgm )
	{
		if( loadArea is not null && pgm is not null )
		{
			try
			{
				return RunProgram( loadArea, (ContentView)Activator.CreateInstance( pgm ) );
			}
			catch( Exception Exception )
			{
				Console.WriteLine( Exception );
			}
		}

		return null;
	}

	public static ContentView? RunProgram( ContentView? loadArea, string pgmName )
	{
		if( loadArea is not null )
		{
			try
			{
				if( pgmName.IsNullOrWhiteSpace() )
					pgmName = GetDefaultProgram();

				if( Programs is not null && Programs.TryGetValue( pgmName, out var Pgm ) && Pgm is not null )
				{
					CurrentProgram = pgmName;

					if( pgmName != SIGN_IN )
					{
						new SaveSettingsViewModel
						{
							LastProgram = pgmName
						};

						if( MainPageViewModel.Instance is not null )
							MainPageViewModel.Instance.CurrentProgramDescription = Globals.Resources.GetStringResource( pgmName );
					}

					Orientation.LayoutDirection   = Pgm.Orientation;
					TripOptions.ConfirmTripRemove = Pgm.ConfirmTripRemove;

					return RunProgram( loadArea, Pgm.PageType );
				}

				return RunProgram( loadArea, GetDefaultProgram() );
			}
			catch( Exception Exception )
			{
				Console.WriteLine( Exception );
			}
		}

		return null;
	}

	public static ContentView? RunProgram( string pgmName ) => RunProgram( LoadArea, pgmName );
}