﻿namespace DroidAz;

public static class ViewExtensions
{
	public static async Task<VisualElement> FadeTo( this VisualElement from, VisualElement to )
	{
		to.IsVisible = true;

		await Task.WhenAll( from.FadeTo( 0, 500, Easing.CubicOut ),
		                    to.FadeTo( 1, 500, Easing.CubicIn ) );

		from.IsVisible = false;
		return to;
	}

	public static async Task<VisualElement> FadeTo( this Layout layout, View from, View to )
	{
		layout.RaiseChild( to );
		return await from.FadeTo( to );
	}
}