﻿namespace DroidAz;

public class CurrentVersion
{
	public const uint CURRENT_VERSION = 2175;

	public static readonly string VERSION = $"{(decimal)CURRENT_VERSION / 1000:N3}";

	public static readonly string APP_VERSION = $"DROID Vsn {VERSION}";
}