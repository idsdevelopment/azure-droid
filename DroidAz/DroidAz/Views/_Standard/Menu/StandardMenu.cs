﻿#nullable enable

using DroidAz.Views._Standard.Deliveries;

namespace DroidAz.Views._Standard.Menu;

public static class StandardMenu
{
	public static Dictionary<string, Applications.Program> Programs = new()
	                                                                  {
		                                                                  {
			                                                                  Applications.SIGN_IN,
			                                                                  new Applications.Program
			                                                                  {
				                                                                  PageType = typeof( SignIn )
			                                                                  }
		                                                                  },
		                                                                  {
			                                                                  Applications.BLANK_PAGE,
			                                                                  new Applications.Program
			                                                                  {
				                                                                  PageType = typeof( BlankPage.BlankPage )
			                                                                  }
		                                                                  },
		                                                                  {
			                                                                  Applications.DELIVERIES_LOADER,
			                                                                  new Applications.Program
			                                                                  {
				                                                                  PageType = typeof( Loader ),
				                                                                  Default  = true
			                                                                  }
		                                                                  }
	                                                                  };
}