﻿namespace DroidAz.Views._Standard.Menu;

public class SaveSettingsViewModel : ViewModelBase
{
	[Setting]
	public string LastProgram
	{
		get { return Get( () => LastProgram, "" ); }
		set { Set( () => LastProgram, value ); }
	}

	[DependsUpon( nameof( LastProgram ) )]
	public void WhenSettingChanges()
	{
		SaveSettings();
	}
}