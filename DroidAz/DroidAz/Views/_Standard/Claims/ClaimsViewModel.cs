﻿#nullable enable

using System.ComponentModel;
using System.Runtime.CompilerServices;
using DroidAz.Annotations;

namespace DroidAz.Views._Standard.Claims;

public class TripDisplay : INotifyPropertyChanged
{
	private static string? OfflineText;

	public string TripId { get; set; }

	public string DeliveryAddress
	{
		get => _DeliveryAddress ?? ( OfflineText ??= Globals.Resources.GetStringResource( "Offline" ) );
		set
		{
			_DeliveryAddress = value;
			OnPropertyChanged();
		}
	}

	public bool IsOffline => _DeliveryAddress is null;

	public TripDisplay( string tripId )
	{
		TripId = tripId;
	}

	private string? _DeliveryAddress;

	[NotifyPropertyChangedInvocator]
	protected virtual void OnPropertyChanged( [CallerMemberName] string? propertyName = null )
	{
		PropertyChanged?.Invoke( this, new PropertyChangedEventArgs( propertyName ) );
	}

	public event PropertyChangedEventHandler? PropertyChanged;
}

public class ClaimsViewModel : TripsBaseViewModel
{
	public string TripId
	{
		get { return Get( () => TripId, "" ); }
		set { Set( () => TripId, value ); }
	}

	public bool IsScanning
	{
		get { return Get( () => IsScanning, false ); }
		set { Set( () => IsScanning, value ); }
	}


	public int TotalScanned
	{
		get { return Get( () => TotalScanned, 0 ); }
		set { Set( () => TotalScanned, value ); }
	}


	public ObservableCollection<TripDisplay> ClaimedTrips
	{
		get { return Get( () => ClaimedTrips, [] ); }
		set { Set( () => ClaimedTrips, value ); }
	}

	public ClaimsViewModel() : base( false )
	{
	}
}