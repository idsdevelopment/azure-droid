﻿#nullable enable

using TripUpdate = DroidAz.Views.BaseViewModels.TripUpdate;

namespace DroidAz.Views._Standard.Deliveries.NoLocation.Batch;

public class NoPiecesViewModel : TripsBaseViewModel
{
#region Enables
	public bool EnablePickupDelivery
	{
		get { return Get( () => EnablePickupDelivery, false ); }
		set { Set( () => EnablePickupDelivery, value ); }
	}
#endregion

#region Selected Item
	public TripDisplayEntry? SelectedItem
	{
		get { return Get( () => SelectedItem, (TripDisplayEntry?)null ); }
		set { Set( () => SelectedItem, value ); }
	}
#endregion

#region Barcode
	public bool OnBarcode( string barcode )
	{
		if( barcode != "" )
		{
			switch( CurrentShow )
			{
			case NoPieces.SHOW.TRIP_LIST:
				var Rec = ( from T in TripDisplayEntries
				            where T.TripId == barcode
				            select T ).FirstOrDefault();

				if( Rec is not null )
				{
					LocationTrip = null;
					SelectedItem = Rec;
					Execute_OnItemDoubleTapped();
					return true;
				}
				return false;

			case NoPieces.SHOW.LOCATION_TRIP_LIST:
				foreach( var Entry in LocationTrips )
				{
					if( Entry.TripId == barcode )
					{
						if( !Entry.IsVisible )
							return false;

						LocationTrip = Entry;
						break;
					}
				}
				break;
			}
		}
		return true;
	}
#endregion


#region BuildLocation
	private void ShowTrips()
	{
		NoPieces.SHOW Page;

		switch( BuildLocationTrips() )
		{
		case 0:
			return;

		case 1:
			Page = NoPieces.SHOW.DETAILS;
			break;

		default:
			Page = NoPieces.SHOW.LOCATION_TRIP_LIST;
			break;
		}

		EnablePickupDelivery = true;
		Show( Page );
	}
#endregion

#region Actions
	public Func<NoPieces.SHOW, Task>? OnShow;

	private NoPieces.SHOW CurrentShow = NoPieces.SHOW.TRIP_LIST;

	private async void Show( NoPieces.SHOW show )
	{
		CurrentShow = show;

		DetailsVisible = show == NoPieces.SHOW.DETAILS;

		if( OnShow is not null )
			await OnShow( show );
	}
#endregion

#region Arrive Time
	public bool CompulsoryArriveTime
	{
		get { return Get( () => CompulsoryArriveTime, false ); }
		set { Set( () => CompulsoryArriveTime, value ); }
	}


	public DateTimeOffset ArriveTime
	{
		get { return Get( () => ArriveTime, DateTimeOffset.MinValue ); }
		set { Set( () => ArriveTime, value ); }
	}


	public int WaitTimeInSeconds
	{
		get { return Get( () => WaitTimeInSeconds, 0 ); }
		set { Set( () => WaitTimeInSeconds, value ); }
	}

	public ICommand OnArriveTimeCancel => Commands[ nameof( OnArriveTimeCancel ) ];

	public void Execute_OnArriveTimeCancel()
	{
		Execute_OnCancel();
	}

	public ICommand OnArriveTime => Commands[ nameof( OnArriveTime ) ];


	public void Execute_OnArriveTime()
	{
		ShowTrips();
	}
#endregion

#region Location
	public TripDisplayEntry? LocationTrip
	{
		get { return Get( () => LocationTrip, (TripDisplayEntry?)null ); }
		set { Set( () => LocationTrip, value ); }
	}

	[DependsUpon( nameof( LocationTrip ) )]
	public void WhenLocationTripChanges()
	{
		if( LocationTrip is { } LTrip )
		{
			LTrip.IsVisible = false;

			var LTrips = LocationTrips;

			if( LTrips.All( entry => !entry.IsVisible ) )
			{
				foreach( var Trip in LTrips )
					Trip.IsVisible = true;

				EnablePickupDelivery = true;
				SelectedItem         = LocationTrip;
				Show( NoPieces.SHOW.DETAILS );
			}
		}
	}

	private static readonly ObservableCollection<TripDisplayEntry> EmptyLocationTrips = [];

	public ObservableCollection<TripDisplayEntry> LocationTrips
	{
		get { return Get( () => LocationTrips, EmptyLocationTrips ); }
		set { Set( () => LocationTrips, value ); }
	}

	private int BuildLocationTrips()
	{
		ObservableCollection<TripDisplayEntry> Result;

		var SelectedTrip = SelectedItem;

		if( SelectedTrip is not null )
		{
			bool TripOk( TripDisplayEntry t )
			{
				static bool CmpCo( string co1Name, string co2Name, string coAddress1, string coAddress2 )
				{
					return ( co1Name == co2Name ) && ( coAddress1 == coAddress2 ) /* || ( ( co1Barcode != "" ) && ( co2Barcode != "" ) && ( co1Barcode == co2Barcode ) ) */;
				}

				return SelectedTrip.Status switch
				       {
					       STATUS.DISPATCHED when ( t.Status == STATUS.DISPATCHED ) && CmpCo( SelectedTrip.PickupCompany,
					                                                                          t.PickupCompany,
					                                                                          SelectedTrip.PickupAddress,
					                                                                          t.PickupAddress ) => true,

					       STATUS.PICKED_UP when ( t.Status == STATUS.PICKED_UP ) && CmpCo( SelectedTrip.DeliveryCompany,
					                                                                        t.DeliveryCompany,
					                                                                        SelectedTrip.DeliveryAddress,
					                                                                        t.DeliveryAddress ) => true,
					       _ => false
				       };
			}

			Result = new ObservableCollection<TripDisplayEntry>( from T in TripDisplayEntries
			                                                     where TripOk( T )
			                                                     orderby T.Status
			                                                     select T );
		}
		else
			Result = EmptyLocationTrips;

		LocationTrips = Result;
		return Result.Count;
	}
#endregion

#region Update
	public TripUpdate? UpdateItem
	{
		get { return Get( () => UpdateItem, (TripUpdate?)null ); }
		set { Set( () => UpdateItem, value ); }
	}

	[DependsUpon( nameof( UpdateItem ) )]
	public void WhenUpdateItemChanges()
	{
		if( UpdateItem is { } Ui )
		{
			var ATime = ArriveTime;

			if( Ui.ArriveTime > ATime )
				ATime = Ui.ArriveTime;

			Tasks.RunVoid( () =>
			               {
				               foreach( var Trip in LocationTrips )
				               {
					               var Update = new TripUpdate
					                            {
						                            Trip              = Trip,
						                            ArriveTime        = ATime,
						                            DriverNotes       = Ui.DriverNotes.Trim(),
						                            ImageBytes        = Ui.ImageBytes,
						                            ImageExtension    = Ui.ImageExtension,
						                            IsUndeliverable   = Ui.IsUndeliverable,
						                            PopPod            = Ui.PopPod,
						                            Signature         = Ui.Signature,
						                            WaitTimeInSeconds = WaitTimeInSeconds,
						                            WaitTimeStart     = ArriveTime
					                            };

					               PickupDeliverTrip( Update );
				               }
			               } );
		}
		Execute_FinaliseTrip();
	}
#endregion

#region Item Tapped
	public bool IsCancelButtonVisible
	{
		get { return Get( () => IsCancelButtonVisible, false ); }
		set { Set( () => IsCancelButtonVisible, value ); }
	}


	public ICommand OnItemTapped => Commands[ nameof( OnItemTapped ) ];

	public void Execute_OnItemTapped()
	{
		if( SelectedItem is { } Si )
		{
			if( !Si.ReadByDriver )
			{
				Si.ReadByDriver = true;
				UpdateFromObject( new ReadByDriver { TripId = Si.TripId } );
			}

			Show( NoPieces.SHOW.DETAILS );
		}
	}

	public ICommand OnItemDoubleTapped => Commands[ nameof( OnItemDoubleTapped ) ];

	public void Execute_OnItemDoubleTapped()
	{
		if( CompulsoryArriveTime )
		{
			IsCancelButtonVisible = true;
			NoPieces.SHOW Page;

			if( ArriveTime == DateTimeOffset.MinValue )
				Page = NoPieces.SHOW.ARRIVE_TIME;
			else
			{
				EnablePickupDelivery = true;
				Page                 = LocationTrips.Any( entry => entry.IsVisible ) ? NoPieces.SHOW.LOCATION_TRIP_LIST : NoPieces.SHOW.DETAILS;
			}
			Show( Page );
		}
		else
			ShowTrips();
	}

#region Back Button
	public Action? OnBackButton,
	               OnCancelButton;

	public ICommand OnBack => Commands[ nameof( OnBack ) ];

	private void DoBack()
	{
		SelectedItem          = null;
		LocationTrip          = null;
		EnablePickupDelivery  = false;
		IsCancelButtonVisible = ArriveTime != DateTimeOffset.MinValue;
		Show( NoPieces.SHOW.TRIP_LIST );
	}

	public void Execute_OnBack()
	{
		DoBack();
		OnBackButton?.Invoke();
	}

	public ICommand OnCancel => Commands[ nameof( OnCancel ) ];

	public void Execute_OnCancel()
	{
		ArriveTime = DateTimeOffset.MinValue;
		DoBack();
		OnCancelButton?.Invoke();
	}
#endregion
#endregion

#region Finalise Trip
	public ICommand FinaliseTrip => Commands[ nameof( FinaliseTrip ) ];

	public void Execute_FinaliseTrip()
	{
		Execute_OnCancel();
	}
#endregion

#region Waiting For Trips
	public Action<bool>? OnWaitingForTrips;

	public bool WaitingForTrips
	{
		get { return Get( () => WaitingForTrips, false ); }
		set { Set( () => WaitingForTrips, value ); }
	}


	[DependsUpon( nameof( WaitingForTrips ) )]
	public void WhenWaitingForTripsChanges()
	{
		OnWaitingForTrips?.Invoke( WaitingForTrips );
	}
#endregion

#region Waiting For Trips
	public Action<bool>? OnDetailsVisible;


	public bool DetailsVisible
	{
		get { return Get( () => DetailsVisible, false ); }
		set { Set( () => DetailsVisible, value ); }
	}


	[DependsUpon( nameof( DetailsVisible ) )]
	public void WhenDetailsVisibleChanges()
	{
		OnDetailsVisible?.Invoke( DetailsVisible );
	}
#endregion
}