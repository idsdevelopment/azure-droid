﻿#nullable enable

namespace DroidAz.Views._Standard.Deliveries.NoLocation.Batch;

[XamlCompilation( XamlCompilationOptions.Compile )]
public partial class NoPieces : ContentView
{
	public enum SHOW
	{
		TRIP_LIST,
		LOCATION_TRIP_LIST,
		DETAILS,
		ARRIVE_TIME
	}

	public NoPieces()
	{
		InitializeComponent();
		var M = Model = (NoPiecesViewModel)Root.BindingContext;

		void DoCommand( ICommand? command )
		{
			if( command is not null && command.CanExecute( this ) )
				command.Execute( this );
		}

		M.OnBackButton = () =>
						 {
							 DoCommand( OnBackButton );
						 };

		M.OnCancelButton = () =>
						   {
							   DoCommand( OnCancelButton );
						   };

		M.OnWaitingForTrips = waiting =>
							  {
								  WaitingForTrips = waiting;
							  };

		M.OnDetailsVisible = visible =>
							 {
								 DetailsVisible = visible;
							 };

		M.OnShow = async show =>
				   {
					   ArriveWaitTime.IsVisible = false;
					   Barcode                  = "";

					   switch( show )
					   {
					   case SHOW.TRIP_LIST:
						   await Root.FadeIn( TripList, Details );
						   break;

					   case SHOW.LOCATION_TRIP_LIST:
						   await Root.FadeIn( BasicList, TripList );
						   break;

					   case SHOW.DETAILS:
						   Details.SelectedItem = null; // Force pieces update
						   Details.SelectedItem = M.SelectedItem;
						   await Root.FadeIn( Details, BasicList );
						   break;

					   case SHOW.ARRIVE_TIME:
						   ArriveWaitTime.IsVisible = true;
						   await Root.FadeIn( ArriveWaitTime, TripList );
						   break;
					   }
				   };

		M.OnShow( SHOW.TRIP_LIST );
	}

	private readonly NoPiecesViewModel Model;

#region Waiting For Trips
	public static readonly BindableProperty WaitingForTripsProperty = BindableProperty.Create( nameof( WaitingForTrips ),
																							   typeof( bool ),
																							   typeof( NoPieces ),
																							   false,
																							   BindingMode.OneWayToSource );

	public bool WaitingForTrips
	{
		get => (bool)GetValue( WaitingForTripsProperty );
		set => SetValue( WaitingForTripsProperty, value );
	}
#endregion


#region Are Details Visible
	public static readonly BindableProperty DetailsVisibleProperty = BindableProperty.Create( nameof( DetailsVisible ),
																							  typeof( bool ),
																							  typeof( NoPieces ),
																							  false,
																							  BindingMode.OneWayToSource );

	public bool DetailsVisible
	{
		get => (bool)GetValue( DetailsVisibleProperty );
		set => SetValue( DetailsVisibleProperty, value );
	}
#endregion

#region Barcode
	public static readonly BindableProperty BarcodeProperty = BindableProperty.Create( nameof( Barcode ),
																					   typeof( string ),
																					   typeof( NoPieces ),
																					   "",
																					   BindingMode.TwoWay,
																					   propertyChanged: BarcodePropertyChanged
																					 );

	private static void BarcodePropertyChanged( BindableObject bindable, object oldValue, object newValue )
	{
		if( bindable is NoPieces Nl && newValue is string BCode && BCode.IsNotNullOrWhiteSpace() )
		{
			if( !Nl.Model.OnBarcode( BCode ) )
				Sounds.PlayBadScanSound();
		}
	}

	public string Barcode
	{
		get => (string)GetValue( BarcodeProperty );
		set => SetValue( BarcodeProperty, value );
	}
#endregion

#region BackButton
	public ICommand? OnBackButton
	{
		get => (ICommand?)GetValue( OnBackButtonProperty );
		set => SetValue( OnBackButtonProperty, value );
	}

	public static readonly BindableProperty OnBackButtonProperty = BindableProperty.Create( nameof( OnBackButton ),
																							typeof( ICommand ),
																							typeof( NoPieces ),
																							defaultBindingMode: BindingMode.OneWayToSource
																						  );
#endregion

#region CancelButton
	public ICommand? OnCancelButton
	{
		get => (ICommand?)GetValue( OnCancelButtonProperty );
		set => SetValue( OnCancelButtonProperty, value );
	}

	public static readonly BindableProperty OnCancelButtonProperty = BindableProperty.Create( nameof( OnCancelButton ),
																							  typeof( ICommand ),
																							  typeof( NoPieces ),
																							  defaultBindingMode: BindingMode.OneWayToSource
																							);
#endregion
}