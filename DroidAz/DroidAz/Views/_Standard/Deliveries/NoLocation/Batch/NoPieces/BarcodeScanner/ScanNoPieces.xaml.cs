﻿using DroidAz.Views._Standard.Deliveries.NoLocation.BatchNoPieces.BarcodeScanner;

namespace DroidAz.Views._Standard.Deliveries.NoLocation.Batch.NoPiecesBarcodeScanner;

[XamlCompilation( XamlCompilationOptions.Compile )]
public partial class ScanNoPieces : ContentView
{
	public ScanNoPieces()
	{
		InitializeComponent();
	}
}