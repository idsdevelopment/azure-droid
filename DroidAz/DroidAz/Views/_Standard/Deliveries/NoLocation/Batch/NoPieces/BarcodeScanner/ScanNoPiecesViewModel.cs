﻿#nullable enable

namespace DroidAz.Views._Standard.Deliveries.NoLocation.BatchNoPieces.BarcodeScanner;

internal sealed class ScanNoPiecesViewModel : ViewModelBase
{
	public bool WaitingForTrips
	{
		get { return Get( () => WaitingForTrips, false ); }
		set { Set( () => WaitingForTrips, value ); }
	}

#region Back Button
	public ICommand OnBackButton => Commands[ nameof( OnBackButton ) ];

	public void Execute_OnBackButton()
	{
		Barcode  = "";
		Scanning = false;
		Scanning = true;
	}
#endregion

#region Scanner
	[DependsUpon( nameof( DetailsVisible ) )]
	[DependsUpon( nameof( WaitingForTrips ) )]
	public void WhenScannerVisibilityChanges()
	{
		IsScannerVisible = !WaitingForTrips && !DetailsVisible;
		Scanning         = IsScannerVisible;
	}

	public bool IsScannerVisible
	{
		get { return Get( () => IsScannerVisible, false ); }
		set { Set( () => IsScannerVisible, value ); }
	}


	public bool Scanning
	{
		get { return Get( () => Scanning, false ); }
		set { Set( () => Scanning, value ); }
	}
#endregion

	public bool DetailsVisible
	{
		get { return Get( () => DetailsVisible, false ); }
		set { Set( () => DetailsVisible, value ); }
	}

#region Barcode
	public string Barcode
	{
		get { return Get( () => Barcode, "" ); }
		set { Set( () => Barcode, value ); }
	}
#endregion
}