﻿#nullable enable

namespace DroidAz.Views._Standard.Deliveries.NoLocation;

[XamlCompilation( XamlCompilationOptions.Compile )]
public partial class NoPieces : ContentView
{
	protected readonly NoPiecesViewModel? Model;

	public NoPieces()
	{
		InitializeComponent();

		if( BindingContext is NoPiecesViewModel M )
		{
			M.OnShowTripList = async () =>
			                   {
				                   TripList.IsVisible    = true;
				                   TripList.SelectedItem = null;
				                   await this.FadeTo( Details, TripList );
				                   Details.IsVisible = false;
			                   };

			M.OnShowDetails = async () =>
			                  {
				                  Details.IsVisible = true;
				                  await this.FadeTo( TripList, Details );
				                  TripList.IsVisible = false;
			                  };

			Model = M;

			// Start on the Trip List
			M.OnShowTripList();
		}
	}
}