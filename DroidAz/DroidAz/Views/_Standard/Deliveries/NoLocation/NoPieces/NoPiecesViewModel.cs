﻿#nullable enable

using TripUpdate = DroidAz.Views.BaseViewModels.TripUpdate;

namespace DroidAz.Views._Standard.Deliveries.NoLocation;

public class NoPiecesViewModel : TripsBaseViewModel
{
	protected override void OnInitialised()
	{
		TripCount     = 0;
		ShowTripCount = true;
	}

	public NoPiecesViewModel() : base( false )
	{
	}

#region Events
	public Action? OnShowTripList;
	public Action? OnShowDetails;
#endregion

#region Selected Trip
	public TripUpdate? UpdateItem
	{
		get { return Get( () => UpdateItem, (TripUpdate?)null ); }
		set { Set( () => UpdateItem, value ); }
	}

	[DependsUpon( nameof( UpdateItem ) )]
	public void WhenUpdateItemChanges()
	{
		var Item = UpdateItem;

		if( Item is not null )
		{
			UpdateItem = null;
			PickupDeliverTrip( Item );
		}
		SelectedItem = null;
	}

#region Selected Item
	public TripDisplayEntry? SelectedItem
	{
		get { return Get( () => SelectedItem, (TripDisplayEntry?)null ); }
		set { Set( () => SelectedItem, value ); }
	}

	private bool InSelectedItem;

	[DependsUpon( nameof( SelectedItem ) )]
	public void WhenTripListSelectedItemChanges()
	{
		if( !InSelectedItem )
		{
			InSelectedItem = true;

			try
			{
				var Selected = SelectedItem;

				if( Selected is not null )
				{
					Selected.RecentlyUpdated = false;
					var TripId = Selected.TripId;

					if( Trips.TryGetValue( TripId, out var SelectedTrip ) )
						SelectedTrip.Modified = false;

					if( !Selected.ReadByDriver )
					{
						Selected.ReadByDriver = true;

						if( SelectedTrip is not null )
							SelectedTrip.ReadByDriver = true;

						UpdateFromObject( new ReadByDriver {TripId = TripId} );
					}
					OnShowDetails?.Invoke();
				}
			}
			finally
			{
				InSelectedItem = false;
			}
		}
	}
#endregion

	public ICommand ItemTapped => Commands[ nameof( ItemTapped ) ];

	public void Execute_ItemTapped()
	{
		WhenTripListSelectedItemChanges();
	}
#endregion

#region Buttons
	public ICommand OnBackButton => Commands[ nameof( OnBackButton ) ];

	public void Execute_OnBackButton()
	{
		OnShowTripList?.Invoke();
		SelectedItem = null;
	}
#endregion
}