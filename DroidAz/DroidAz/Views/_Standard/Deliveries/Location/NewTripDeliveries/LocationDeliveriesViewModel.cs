﻿#nullable enable

using System.ComponentModel;
using System.Runtime.CompilerServices;
using CommandViewModel.Annotations;
using DroidAz.Modifications;

namespace DroidAz.Views._Standard.Deliveries.Location.NewTripDeliveries;

public class LocationDeliveriesItem : INotifyPropertyChanged
{
	public string Barcode
	{
		get => _Barcode;
		set
		{
			_Barcode = value;
			OnPropertyChanged();
		}
	}

	public int Count
	{
		get => _Count;
		set
		{
			_Count = value;
			OnPropertyChanged();
		}
	}

	private string _Barcode = "";

	private int _Count;

	[NotifyPropertyChangedInvocator]
	protected virtual void OnPropertyChanged( [CallerMemberName] string? propertyName = null )
	{
		PropertyChanged?.Invoke( this, new PropertyChangedEventArgs( propertyName ) );
	}

	public event PropertyChangedEventHandler? PropertyChanged;
}

public class LocationDeliveriesViewModel : UpdateViewModel
{
	public const string LOCATION = "Location",
	                    BARCODE  = "Barcode";


	public string Placeholder
	{
		get { return Get( () => Placeholder, LOCATION ); }
		set { Set( () => Placeholder, value ); }
	}

	public string ScannedCode
	{
		get { return Get( () => ScannedCode, "" ); }
		set { Set( () => ScannedCode, value ); }
	}

	public bool IsScanning
	{
		get { return Get( () => IsScanning, false ); }
		set { Set( () => IsScanning, value ); }
	}

	public string Location
	{
		get { return Get( () => Location, "" ); }
		set { Set( () => Location, value ); }
	}


	public bool LocationVisible
	{
		get { return Get( () => LocationVisible, IsInDesignMode ); }
		set { Set( () => LocationVisible, value ); }
	}

	public bool CancelEnabled
	{
		get { return Get( () => CancelEnabled, IsInDesignMode ); }
		set { Set( () => CancelEnabled, value ); }
	}


	public bool DeliverVisible
	{
		get { return Get( () => DeliverVisible, IsInDesignMode ); }
		set { Set( () => DeliverVisible, value ); }
	}


	public bool DeliverEnabled
	{
		get { return Get( () => DeliverEnabled, false ); }
		set { Set( () => DeliverEnabled, value ); }
	}


	public ObservableCollection<LocationDeliveriesItem> Barcodes
	{
		get { return Get( () => Barcodes, [] ); }
		set { Set( () => Barcodes, value ); }
	}

	public ICommand Cancel => Commands[ nameof( Cancel ) ];

	public ICommand Deliver => Commands[ nameof( Deliver ) ];


	private bool DoingBarcodes;

	protected override void OnInitialised()
	{
		base.OnInitialised();
			
	}


	[DependsUpon( nameof( ScannedCode ) )]
	public void WhenScannedCodeChanges()

	{
		var Code = ScannedCode.Trim();

		if( Code.IsNotNullOrWhiteSpace() )
		{
			if( !DoingBarcodes )
			{
				DoingBarcodes   = true;
				Location        = Code;
				LocationVisible = true;
				CancelEnabled   = true;
				Placeholder     = BARCODE;
			}
			else
			{
				DeliverVisible = true;
				DeliverEnabled = true;

				var Rec = ( from B in Barcodes
				            where B.Barcode == Code
				            select B ).FirstOrDefault();

				if( Rec is null )
				{
					Rec = new LocationDeliveriesItem
					      {
						      Barcode = Code
					      };
					Barcodes.Add( Rec );
				}

				Rec.Count++;
			}
		}
	}

	public void Execute_Cancel()
	{
		Location        = "";
		CancelEnabled   = false;
		Placeholder     = LOCATION;
		LocationVisible = false;
		DoingBarcodes   = false;
		DeliverVisible  = false;
		Barcodes        = [];
	}

	public void Execute_Deliver()
	{
		DeliverEnabled = false;

		var G = Globals.Gps.CurrentGpsPoint;

		UpdateFromObject( new LocationDelivery
		                  {
			                  Deliveries = new Protocol.Data.LocationDeliveries
			                               {
				                               Location     = Location,
				                               DeliveryTime = DateTimeOffset.Now,
				                               Driver       = Users.UserName,
				                               Latitude     = G.Latitude,
				                               Longitude    = G.Longitude,
				                               Items = ( from B in Barcodes
				                                         select new LocationDeliveryItem
				                                                {
					                                                Barcode  = B.Barcode,
					                                                Quantity = B.Count
				                                                } ).ToList()
			                               }
		                  } );
		Execute_Cancel();
	}
}