﻿#nullable enable

using DroidAz.Dialogs;

namespace DroidAz.Views._Standard.Deliveries.Location.Pieces;

[XamlCompilation( XamlCompilationOptions.Compile )]
public partial class Pieces : ContentView
{
	public enum SHOW
	{
		TRIP_LIST,
		PIECES_LIST,
		FINALISE_DETAILS,
		DETAILS
	}

	public Pieces()
	{
		InitializeComponent();

		PiecesList.Opacity = 0;
		var R = Root;
		R.LowerChild( PiecesList );
		R.LowerChild( Details );
		R.RaiseChild( LocationList );

		void StartScanning()
		{
			LocationList.StartScanning();
		}

		var M = Model = (PiecesViewModel)Root.BindingContext;

		M.OnShow = async show =>
				   {
					   switch( show )
					   {
					   case SHOW.TRIP_LIST:
						   LocationList.Cancel();
						   await Root.FadeIn( LocationList, Details );
						   StartScanning();
						   break;

					   case SHOW.PIECES_LIST:
						   await Root.FadeIn( PiecesList, LocationList );
						   break;

					   case SHOW.FINALISE_DETAILS:
					   case SHOW.DETAILS:
						   var D = Details;
						   D.SelectedItem         = null; // Force pieces update
						   D.SelectedItem         = M.SelectedItem;
						   D.EnablePickupDelivery = show != SHOW.DETAILS;
						   await Root.FadeIn( Details, PiecesList );
						   break;
					   }
				   };

		M.OnWarnSingle = async tripId =>
						 {
							 await WarningDialog.Execute( $"{Globals.Resources.GetStringResource( "OnlySingle" )}\r\n{Globals.Resources.GetStringResource( "ShipmentId" )}: {tripId}" );
						 };

		void DoCommand( ICommand? cmd )
		{
			if( cmd is not null && cmd.CanExecute( this ) )
				cmd.Execute( this );
		}

		M.OnSingleTapAction = () =>
							  {
								  DoCommand( OnSingleTap );
							  };

		M.OnSingleTapAction = () =>
							  {
								  DoCommand( OnDoubleTap );
							  };

		M.OnCancelAction = () =>
						   {
							   DoCommand( OnCancel );
						   };

		StartScanning();
	}

	private readonly PiecesViewModel Model;

#region Show
	public static readonly BindableProperty ShowDetailsProperty = BindableProperty.Create( nameof( ShowDetails ),
																						   typeof( bool ),
																						   typeof( Pieces ),
																						   false,
																						   propertyChanged: ShowDetailsPropertyChanged );

	private static async void ShowDetailsPropertyChanged( BindableObject bindable, object oldValue, object newValue )
	{
		if( bindable is Pieces {Model.OnShow: { } Func} P && newValue is bool Show )
		{
			var SelectedItem = P.SelectedItem;

			P.Details.SelectedItem = SelectedItem is null ? null : ( from T in P.LocationList.TripDisplayEntries
																	 where T.TripId == SelectedItem
																	 select T ).FirstOrDefault();

			await Func( Show ? SHOW.DETAILS : SHOW.TRIP_LIST );
		}
	}

	public bool ShowDetails
	{
		get => (bool)GetValue( ShowDetailsProperty );
		set => SetValue( ShowDetailsProperty, value );
	}

	public static readonly BindableProperty SelectedItemProperty = BindableProperty.Create( nameof( SelectedItem ),
																							typeof( string ),
																							typeof( Pieces )
																						  );

	public string? SelectedItem
	{
		get => (string?)GetValue( SelectedItemProperty );
		set => SetValue( SelectedItemProperty, value );
	}
#endregion

#region Actions
	public static readonly BindableProperty OnSingleTapProperty = BindableProperty.Create( nameof( OnSingleTap ),
																						   typeof( ICommand ),
																						   typeof( Pieces ) );

	public ICommand? OnSingleTap
	{
		get => (ICommand?)GetValue( OnSingleTapProperty );
		set => SetValue( OnSingleTapProperty, value );
	}

	public static readonly BindableProperty OnDoubleTapProperty = BindableProperty.Create( nameof( OnDoubleTap ),
																						   typeof( ICommand ),
																						   typeof( Pieces ) );

	public ICommand? OnDoubleTap
	{
		get => (ICommand?)GetValue( OnDoubleTapProperty );
		set => SetValue( OnDoubleTapProperty, value );
	}

	public static readonly BindableProperty OnCancelProperty = BindableProperty.Create( nameof( OnCancel ),
																						typeof( ICommand ),
																						typeof( Pieces ) );

	public ICommand? OnCancel
	{
		get => (ICommand?)GetValue( OnCancelProperty );
		set => SetValue( OnCancelProperty, value );
	}
#endregion
}