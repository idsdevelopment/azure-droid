﻿#nullable enable

using DroidAz.Controls.Default.PiecesList;
using TripUpdate = DroidAz.Views.BaseViewModels.TripUpdate;

namespace DroidAz.Views._Standard.Deliveries.Location.Pieces;

public class PiecesViewModel : TripsBaseViewModel
{
#region Counts
	public decimal TotalScanned
	{
		get { return Get( () => TotalScanned, 0 ); }
		set { Set( () => TotalScanned, value ); }
	}
#endregion

	public PiecesViewModel() : base( false )
	{
	}

#region Selected Trips
	public TripUpdate? UpdateItem
	{
		get { return Get( () => UpdateItem, (TripUpdate?)null ); }
		set { Set( () => UpdateItem, value ); }
	}

	public bool IsPickup
	{
		get { return Get( () => IsPickup, false ); }
		set { Set( () => IsPickup, value ); }
	}

	private static bool CompareLocation( bool isPickup, TripDisplayEntry master, TripDisplayEntry trip )
	{
		// ReSharper disable once LocalFunctionHidesMethod
		static bool Compare( string masterLocation, string masterCompany, string masterAddress,
							 string location,       string company,       string address )
		{
			// Check Location First
			return ( masterLocation == location )
				   || ( ( string.Compare( masterCompany, company, StringComparison.OrdinalIgnoreCase ) == 0 )
						&& ( string.Compare( masterAddress, address, StringComparison.OrdinalIgnoreCase ) == 0 ) );
		}

		if( isPickup )
		{
			return Compare( master.PickupBarcode, master.PickupCompany, master.PickupAddress,
							trip.PickupBarcode, trip.PickupCompany, trip.PickupAddress );
		}

		return Compare( master.DeliveryBarcode, master.DeliveryCompany, master.DeliveryAddress,
						trip.DeliveryBarcode, trip.DeliveryCompany, trip.DeliveryAddress );
	}

	private static bool ValidTrip( TripDisplayEntry master, TripDisplayEntry trip ) => CompareLocation( master.Status < STATUS.PICKED_UP, master, trip );

#region Location Items
	public IEnumerable<TripDisplayEntry>? AllTrips
	{
		get { return Get( () => AllTrips, new List<TripDisplayEntry>() ); }
		set { Set( () => AllTrips, value ); }
	}

	public IEnumerable<TripDisplayEntry>? LocationTrips
	{
		get { return Get( () => LocationTrips, new List<TripDisplayEntry>() ); }
		set { Set( () => LocationTrips, value ); }
	}

	[DependsUpon( nameof( LocationTrips ) )]
	public void WhenLocationTripsChange()
	{
		if( LocationTrips is { } DisplayEntries )
		{
			var Item = DisplayEntries.FirstOrDefault();
			SelectedItem = Item;

			if( Item is not null )
				Execute_OnItemDoubleTapped();
		}
	}
#endregion

#region Selected Item
	public TripDisplayEntry? SelectedItem
	{
		get { return Get( () => SelectedItem, (TripDisplayEntry?)null ); }
		set { Set( () => SelectedItem, value ); }
	}

	public Action? OnSingleTapAction,
				   OnDoubleTapAction;

	public ICommand OnItemTapped => Commands[ nameof( OnItemTapped ) ];

	public async void Execute_OnItemTapped()
	{
		OnSingleTapAction?.Invoke();
		await Show( Pieces.SHOW.DETAILS );
	}

	public ICommand OnItemDoubleTapped => Commands[ nameof( OnItemDoubleTapped ) ];

	public async void Execute_OnItemDoubleTapped()
	{
		OnDoubleTapAction?.Invoke();
		var Master = SelectedItem;

		if( Master is not null )
		{
			var Pu = Master.IsPickup;
			IsPickup            = Pu;
			ShowPickupCompany   = Pu;
			ShowDeliveryCompany = !Pu;

			// All Trips at the same pickup or delivery location
			// Delivery could be multiple pickup companies
			var DisplayEntries = ( from T in AllTrips
								   where ValidTrip( Master, T )
								   select T ).ToList();

			List<TripDisplayEntry> ValidEntries;

			var ProcessOnlyMaster = false;

			if( Master.IsDelivery )
			{
				ValidEntries = [];

				// Add Items from the same source location
				for( var I = DisplayEntries.Count; --I >= 0; )
				{
					var Trip = DisplayEntries[ I ];

					if( CompareLocation( true, Master, Trip ) )
					{
						ValidEntries.Add( Trip );
						DisplayEntries.RemoveAt( I );
					}
				}

				// DisplayEntries should now be only trips different to master

				// Get all master items
				var MasterItems = new HashSet<string>();

				foreach( var Entry in ValidEntries )
				{
					MasterItems.Add( Entry.TripId );

					if( Entry.HasItems )
					{
						foreach( var EntryItem in Entry.Items )
							MasterItems.Add( PiecesListViewModel.GetCode( EntryItem ) );
					}
				}

				// if duplicate only process master
				foreach( var Entry in DisplayEntries )
				{
					bool HasItem( TripDisplayEntry entry )
					{
						if( entry.HasItems )
						{
							foreach( var E in entry.Items )
							{
								if( MasterItems.Contains( PiecesListViewModel.GetCode( E ) ) )
									return true;
							}
						}
						return false;
					}

					if( MasterItems.Contains( Entry.TripId ) || HasItem( Entry ) )
					{
						ValidEntries      = [Master];
						ProcessOnlyMaster = true;
						break;
					}
				}
			}
			else
				ValidEntries = DisplayEntries;

			LocationTrips = ValidEntries;

			foreach( var Trip in ValidEntries )
			{
				if( !Trip.ReadByDriver )
					UpdateFromObject( new ReadByDriver {TripId = Trip.TripId} );
			}

			await Show( Pieces.SHOW.PIECES_LIST );

			if( ProcessOnlyMaster )
				await WarnSingle( Master.TripId );
		}
	}
#endregion
#endregion

#region Show
	public Func<string, Task>? OnWarnSingle;

	public async Task WarnSingle( string tripId )
	{
		if( OnWarnSingle is not null )
			await OnWarnSingle( tripId );
	}

	public Func<Pieces.SHOW, Task>? OnShow;

	public async Task Show( Pieces.SHOW show )
	{
		if( OnShow is not null )
			await OnShow( show );
	}
#endregion

#region Events
	public Action? OnCancelAction;

	public ICommand OnCancel => Commands[ nameof( OnCancel ) ];

	public async void Execute_OnCancel()
	{
		SelectedItem = null;
		await Show( Pieces.SHOW.TRIP_LIST );
		OnCancelAction?.Invoke();
	}

	public ICommand OnFinalise => Commands[ nameof( OnFinalise ) ];

	public async void Execute_OnFinalise()
	{
		var Item = SelectedItem;

		if( Item is not null )
		{
			Item.Pieces = TotalScanned;
			await Show( Pieces.SHOW.FINALISE_DETAILS );
		}
	}

	public ICommand OnBackButton => Commands[ nameof( OnBackButton ) ];

	public async void Execute_OnBackButton()
	{
		SelectedItem = null;
		await Show( Pieces.SHOW.TRIP_LIST );
	}

	public ICommand OnFinalised => Commands[ nameof( OnFinalised ) ];

	public void Execute_OnFinalised()
	{
		var Upd = UpdateItem;

		if( Upd is not null && LocationTrips is not null )
		{
			foreach( var Trip in LocationTrips )
			{
				Upd.Trip = Trip;
				PickupDeliverTrip( Upd );
			}
		}

		Execute_OnCancel();
	}
#endregion

#region Hide/Show
	public bool ShowPickupCompany
	{
		get { return Get( () => ShowPickupCompany, true ); }
		set { Set( () => ShowPickupCompany, value ); }
	}

	public bool ShowDeliveryCompany
	{
		get { return Get( () => ShowDeliveryCompany, true ); }
		set { Set( () => ShowDeliveryCompany, value ); }
	}
#endregion
}