﻿#nullable enable

using Preferences = Globals.Preferences;
using TripUpdate = DroidAz.Views.BaseViewModels.TripUpdate;

namespace DroidAz.Views._Standard.Deliveries.Location;

public class NoPiecesViewModel : TripsBaseViewModel
{
	public NoPieces.NoPieces Owner = null!;

#region Batch Mode
	private bool ProcessingBatchMode;

	private void BatchMode( STATUS status )
	{
		ProcessingBatchMode = true;

		var Li = ( from L in LocationItems
				   where L.Status == status
				   select L ).ToList();

		AllowUndeliverable = Preferences.AllowUndeliverable && ( Li.Count > 0 ) && ( ( Li.Count == 1 ) || Preferences.BatchUndeliverableAtLocation );

		decimal TotalPieces = 0,
				TotalWeight = 0;

		foreach( var Entry in Li )
		{
			TotalPieces += Entry.Pieces;
			TotalWeight += Entry.Weight;
		}

		SelectedItem = new TripDisplayEntry( Li.First() )
					   {
						   TripId = Globals.Resources.GetStringResource( "MultipleItems" ),
						   Pieces = TotalPieces,
						   Weight = TotalWeight
					   };
	}

	public ICommand OnBatchPickupMode => Commands[ nameof( OnBatchPickupMode ) ];

	public void Execute_OnBatchPickupMode()
	{
		BatchMode( STATUS.DISPATCHED );
	}

	public ICommand OnBatchDeliveryMode => Commands[ nameof( OnBatchDeliveryMode ) ];

	public void Execute_OnBatchDeliveryMode()
	{
		BatchMode( STATUS.PICKED_UP );
	}

	public ICollection<TripDisplayEntry>? LocationItems
	{
		get { return Get( () => LocationItems, (ICollection<TripDisplayEntry>?)null ); }
		set { Set( () => LocationItems, value ); }
	}

	[DependsUpon( nameof( LocationItems ) )]
	public void WhenLocationItemsChanges()
	{
		ProcessingBatchMode = false;
		AllowUndeliverable  = false;

		var Trps = LocationItems;

		if( Trps is not null )
		{
			foreach( var Trip in Trps )
			{
				Trip.RecentlyUpdated = false;

				if( !Trip.ReadByDriver )
				{
					Trip.ReadByDriver = true;
					UpdateFromObject( new ReadByDriver {TripId = Trip.TripId} );
				}
			}
		}
	}
#endregion

#region Back Button
	public ICommand OnBackButton => Commands[ nameof( OnBackButton ) ];

	public async void Execute_OnBackButton()
	{
		SelectedItem        = null;
		ProcessingBatchMode = false;
		await Owner.Show( NoPieces.NoPieces.SHOW.LOCATION );
	}
#endregion


#region Selected Item
	public IList<string> TripIdFilter
	{
		get { return Get( () => TripIdFilter, new List<string>() ); }
		set { Set( () => TripIdFilter, value ); }
	}

	public bool AllowUndeliverable
	{
		get { return Get( () => AllowUndeliverable, false ); }
		set { Set( () => AllowUndeliverable, value ); }
	}


	public TripDisplayEntry? SelectedItem
	{
		get { return Get( () => SelectedItem, (TripDisplayEntry?)null ); }
		set { Set( () => SelectedItem, value ); }
	}

	[DependsUpon( nameof( SelectedItem ) )]
	public async void WhenSelectedItemChanges()
	{
		if( SelectedItem is not null )
		{
			if( !ProcessingBatchMode )
				AllowUndeliverable = Preferences.AllowUndeliverable;

			await Owner.Show( NoPieces.NoPieces.SHOW.DETAILS );
		}
	}
#endregion

#region Update  Item
	public ICommand OnFinalised => Commands[ nameof( OnFinalised ) ];

	public void Execute_OnFinalised()
	{
		if( LocationItems is not null && ( LocationItems.Count > 0 ) )
			Execute_OnBackButton();
		else
			Owner.Cancel();
	}

	public TripUpdate? UpdateItem
	{
		get { return Get( () => UpdateItem, (TripUpdate?)null ); }
		set { Set( () => UpdateItem, value ); }
	}

	[DependsUpon( nameof( UpdateItem ) )]
	public void WhenUpdateItemChanges()
	{
		var Item = UpdateItem;
		UpdateItem = null;

		if( Item is not null )
		{
			if( !ProcessingBatchMode )
				PickupDeliverTrip( Item );

			else if( LocationItems is not null )
			{
				if( !Item.IsUndeliverable || ( Item.IsUndeliverable && Preferences.BatchUndeliverableAtLocation ) )
				{
					foreach( var Entry in LocationItems )
					{
						Item.Trip = Entry;
						PickupDeliverTrip( Item );
					}
				}
				LocationItems = new List<TripDisplayEntry>();
				TripIdFilter  = new List<string>();
			}

			var Id = Item.Trip?.TripId;

			if( Id is not null )
			{
				var LocItems = LocationItems;

				LocItems?.Remove( ( from Entry in LocItems
									where Entry.TripId == Id
									select Entry ).FirstOrDefault() );

				TripIdFilter = ( from Tid in TripIdFilter
								 where Tid != Id
								 select Tid ).ToList();
			}
		}
		ProcessingBatchMode = false;
		SelectedItem        = null;
	}
#endregion
}