﻿namespace DroidAz.Views._Standard.Deliveries.Location.NoPieces;

[XamlCompilation( XamlCompilationOptions.Compile )]
public partial class NoPieces : ContentView
{
	public enum SHOW
	{
		LOCATION,
		DETAILS
	}

	public async Task Show( SHOW show )
	{
		switch( show )
		{
		case SHOW.LOCATION:
			await Root.FadeIn( LocationList, Details );
			break;

		case SHOW.DETAILS:
			await Root.FadeIn( Details, LocationList );
			break;
		}
	}

	public async void Cancel()
	{
		LocationList.Cancel();
		await Show( SHOW.LOCATION );
	}

	public NoPieces()
	{
		InitializeComponent();

		Root.LowerChild( Details );
		Root.RaiseChild( LocationList );

		if( Root.BindingContext is NoPiecesViewModel Model )
		{
			Model.Owner = this;
		}
	}
}