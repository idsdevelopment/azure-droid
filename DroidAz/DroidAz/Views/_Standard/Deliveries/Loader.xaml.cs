﻿#nullable enable

using DroidAz.Views._Standard.Deliveries.Location.NoPieces;
using DroidAz.Views._Standard.Deliveries.Location.Pieces;
using DroidAz.Views._Standard.Deliveries.NoLocation.Batch.NoPiecesBarcodeScanner;
using Preferences = Globals.Preferences;

namespace DroidAz.Views._Standard.Deliveries;

[XamlCompilation( XamlCompilationOptions.Compile )]
public partial class Loader : ContentView
{
	public Loader()
	{
		InitializeComponent();

		var ByPieces      = Preferences.DevicePreferences.ByPiece;
		var BatchMode     = Preferences.DevicePreferences.BatchLocation;
		var ScanShipments = Preferences.DevicePreferences.ScanShipments;

		if( Preferences.DevicePreferences.ByLocation )
			Applications.RunProgram( this, ByPieces ? typeof( Pieces ) : typeof( NoPieces ) );
		else // No Location
		{
			if( BatchMode )
				Applications.RunProgram( this, ScanShipments ? typeof( ScanNoPieces ) : typeof( NoLocation.Batch.NoPieces ) );
			else
				Applications.RunProgram( this, ByPieces ? typeof( NoLocation.Pieces.Pieces ) : typeof( NoLocation.NoPieces ) );
		}
	}
}