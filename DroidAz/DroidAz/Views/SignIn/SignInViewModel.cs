﻿#nullable enable

using DroidAz.Modifications;
using DroidAz.Views._Standard.Menu;
using Preferences = Globals.Preferences;

namespace DroidAz.Views;

public class SignInViewModel : PingViewModel
{
	public bool OnLine
	{
		get { return Get( () => OnLine, false ); }
		set { Set( () => OnLine, value ); }
	}

	[Setting]
	public string Account
	{
		get { return Get( () => Account, "" ); }
		set { Set( () => Account, value ); }
	}

	[Setting]
	public string UserName
	{
		get { return Get( () => UserName, "" ); }
		set { Set( () => UserName, value ); }
	}

	public string Password
	{
		get { return Get( () => Password, "" ); }
		set { Set( () => Password, value ); }
	}

	public bool SignInEnabled
	{
		get { return Get( () => SignInEnabled, false ); }
		set { Set( () => SignInEnabled, value ); }
	}

	public ICommand Login => Commands[ nameof( Login ) ];

	public Action? DisplayTimeError,
	               DisplayInvalidSignIn,
	               SignInComplete;

	public SignInViewModel()
	{
		OnPing += ( _, online ) =>
		          {
			          OnLine = online;
		          };

		OnLine = MainPageViewModel.IsOnline;
	}

	[DependsUpon( nameof( OnLine ) )]
	[DependsUpon( nameof( Account ) )]
	[DependsUpon( nameof( UserName ) )]
	[DependsUpon( nameof( Password ) )]
	public void EnableSignIn()
	{
		SignInEnabled = OnLine && !string.IsNullOrEmpty( Account ) && !string.IsNullOrEmpty( UserName ) && !string.IsNullOrEmpty( Password );
	}

	public async Task Execute_Login()
	{
		if( !IsInDesignMode )
		{
			SignInEnabled = false;

			var UName = UserName.Trim();

			try
			{
				var (LIn, _) = await Azure.LogIn( CurrentVersion.APP_VERSION, Account, UName, Password, true );

				switch( LIn )
				{
				case Azure.AZURE_CONNECTION_STATUS.TIME_ERROR:
					DisplayTimeError?.Invoke();
					break;

				case Azure.AZURE_CONNECTION_STATUS.ERROR:
					DisplayInvalidSignIn?.Invoke();
					break;

				default:
					var DebugSlot = Azure.DebugServer;

					static Task<bool> Init()
					{
						return Task.Run( () =>
						                 {
							                 Preferences.ResetPreferences();

							                 var Main = MainPageViewModel.Instance;

							                 if( Main is not null )
							                 {
								                 Main.AllowManualBarcode     = Preferences.AllowManualBarcode;
								                 Main.AllowThirdPartyScanner = Preferences.AllowDynamsoftBarcodeReader;
							                 }
							                 return true;
						                 } );
					}

					if( await Init() )
					{
						Users.CarrierId = Account;
						Users.UserName  = UName;

						if( Preferences.IsPml )
						{
							async Task<Azure.InternalClient?> DoLogin( Azure.SLOT slot )
							{
								try
								{
									var Client = new Azure.InternalClient( slot, CurrentVersion.APP_VERSION, Account, UName );

									switch( await Client.LogIn( Password, true ) )
									{
									case Azure.AZURE_CONNECTION_STATUS.TIME_ERROR:
										DisplayTimeError?.Invoke();
										return null;

									case Azure.AZURE_CONNECTION_STATUS.ERROR:
										DisplayInvalidSignIn?.Invoke();
										return null;
									}
									return Client;
								}
								catch
								{
								}
								return null;
							}

							var Client = await DoLogin( Azure.SLOT.FORCE_PRODUCTION );

							if( Client is null )
								goto Error;

							Azure.Client = Client;

							Client = await DoLogin( Azure.SLOT.SCHEDULES );

							if( Client is null )
								goto Error;

							Azure.SecondaryClient = Client;
						}

						Applications.Programs = Users.Programs;

						if( MainPageViewModel.Instance is not null )
						{
							var M = MainPageViewModel.Instance;
							M.DebugServer = DebugSlot;
							M.DriverName  = UName.Capitalise();
						}

						UpdateViewModel.InitialiseUpdates();

						SignInComplete?.Invoke();

						var LastPgm = new SaveSettingsViewModel().LastProgram;
						Applications.RunProgram( LastPgm );
						Applications.ShowMenu = true;
						SaveSettings();
					}
					break;
				}
			Error: ;
			}
			catch( Exception E )
			{
				Console.WriteLine( E );
				return;
			}
			SignInEnabled = true;
		}
	}
}