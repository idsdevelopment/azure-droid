﻿#nullable enable

using Timer = System.Timers.Timer;

namespace DroidAz.Views;

[XamlCompilation( XamlCompilationOptions.Compile )]
public partial class SignIn : ContentView
{
	public SignIn()
	{
		InitializeComponent();
		Orientation.LayoutDirection = Orientation.ORIENTATION.BOTH;
		Applications.ShowMenu       = false;

		if( Root.BindingContext is SignInViewModel Model )
		{
			Model.DisplayTimeError = () =>
			                         {
				                         MainPage.Instance.DisplayAlert( Globals.Resources.GetStringResource( "Error" ),
				                                                         Globals.Resources.GetStringResource( "DeviceTimeError" ),
				                                                         Globals.Resources.GetStringResource( "Cancel" ) );
			                         };

			Model.DisplayInvalidSignIn = () =>
			                             {
				                             MainPage.Instance.DisplayAlert( Globals.Resources.GetStringResource( "Error" ),
				                                                             Globals.Resources.GetStringResource( "InvalidCredentials" ),
				                                                             Globals.Resources.GetStringResource( "Cancel" ) );
			                             };

			Model.SignInComplete = () => { Orientation.LayoutDirection = Orientation.ORIENTATION.PORTRAIT; };

		}

		var Timer = new Timer( 150 )
		            {
			            AutoReset = false
		            };

		Timer.Elapsed += ( _, _ ) =>
		                 {
			                 ViewModelBase.Dispatcher( () =>
			                                           {
				                                           Password.Focus();
				                                           Timer.Dispose();
			                                           } );
		                 };

		Timer.Start();
	}
}