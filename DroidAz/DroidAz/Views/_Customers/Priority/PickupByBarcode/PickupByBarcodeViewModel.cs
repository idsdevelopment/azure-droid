﻿#nullable enable

using System.ComponentModel;
using System.Runtime.CompilerServices;
using DroidAz.Annotations;
using DroidAz.Modifications;

namespace DroidAz.Views._Customers.Priority;

public static class PriorityExtensions
{
	public static bool IsPackageBarcode( this string str )
	{
		return str.IndexOfAny( ['+', '-', '(', ')'] ) != -1;
	}
}

public class PickupByBarcodeViewModel : ViewModelBase
{
	public class DisplayEntry : INotifyPropertyChanged
	{
		public string TripId { get; set; }

		public bool Unknown
		{
			get => _Unknown;
			set
			{
				_Unknown = value;
				InvokeOnPropertyChanged();
			}
		}

		public bool Selected
		{
			get => _Selected;
			set
			{
				_Selected = value;
				InvokeOnPropertyChanged();
			}
		}

		private bool _Unknown,
		             _Selected;

		[NotifyPropertyChangedInvocator]
		protected virtual void InvokeOnPropertyChanged( [CallerMemberName] string? propertyName = null )
		{
			PropertyChanged?.Invoke( this, new PropertyChangedEventArgs( propertyName ) );
		}

		public DisplayEntry( string tripId )
		{
			TripId = tripId;
		}

		public event PropertyChangedEventHandler? PropertyChanged;
	}

#region Processing
	public async Task ProcessPickups()
	{
		Scanning = false;

		DisplayEntries.Clear();

		var TripIds = ( from T in await Azure.Client.RequestGetTripIdsByStatusAndLocationAndServiceLevel( new StatusAndLocationAndServiceLevel( new List<STATUS> {STATUS.DISPATCHED}, PickupPharmacyLocation, PickupServiceLevel ) )
		                orderby T
		                select T ).ToList();

		if( TripIds.Count > 0 )
		{
			foreach( var TripId in TripIds )
				DisplayEntries.Add( new DisplayEntry( TripId ) );

			Scanning = true;
		}
	}
#endregion

#region Scanning
	// ReSharper disable once InconsistentNaming
	public Action? OnNotCX,
	               OnNotPackage,
	               OnAlreadyScanned,
	               OnWarning;

	public Action<DisplayEntry>? OnScrollIntoView;

	public Func<Task<bool>>? OnCancel;

	public bool Scanning
	{
		get { return Get( () => Scanning, false ); }
		set { Set( () => Scanning, value ); }
	}


	public string Barcode
	{
		get { return Get( () => Barcode, "" ); }
		set { Set( () => Barcode, value ); }
	}


	[DependsUpon( nameof( Barcode ) )]
	public void WhenBarcodeChanges()
	{
		var BCode = Barcode.Trim();

		if( BCode.IsNotNullOrWhiteSpace() )
		{
			DisplayEntry? ScrollEntry = null;

			try
			{
				if( BCode.StartsWith( "CX" ) )
				{
					if( BCode.IsPackageBarcode() )
					{
						var De = DisplayEntries;

						for( var Ndx = De.Count; --Ndx >= 0; )
						{
							var Entry = De[ Ndx ];

							if( Entry.TripId == BCode )
							{
								if( !Entry.Selected )
								{
									De.RemoveAt( Ndx );
									Entry.Selected = true;
									De.Insert( 0, Entry );
									EnableClaimNow = true;
									ScrollEntry    = Entry;
								}
								else
									OnAlreadyScanned?.Invoke();
								return;
							}
						}
						De.Insert( 0, ScrollEntry = new DisplayEntry( BCode ) {Selected = true, Unknown = true} );
						EnableClaimNow = true;
						OnWarning?.Invoke();
					}
					else
						OnNotPackage?.Invoke();
				}
				else
					OnNotCX?.Invoke();
			}
			finally
			{
				Barcode = "";

				if( ScrollEntry is not null )
					OnScrollIntoView?.Invoke( ScrollEntry );
			}
		}
	}
#endregion

#region Items
	public ObservableCollection<DisplayEntry> DisplayEntries
	{
		get { return Get( () => DisplayEntries, [] ); }
		set { Set( () => DisplayEntries, value ); }
	}


	public DisplayEntry? SelectedItem
	{
		get { return Get( () => SelectedItem, (DisplayEntry?)null ); }
		set { Set( () => SelectedItem, value ); }
	}

	[DependsUpon100( nameof( SelectedItem ) )]
	public void WhenSelectedItemChanges()
	{
		if( SelectedItem is not null )
			SelectedItem = null;
	}
#endregion

#region Location
	public Action? OnShowLocation;

	public string PickupServiceLevel = "",
	              PickupRoute        = "";

	public string PickupPharmacy
	{
		get { return Get( () => PickupPharmacy, "" ); }
		set { Set( () => PickupPharmacy, value ); }
	}


	public string PickupPharmacyLocation
	{
		get { return Get( () => PickupPharmacyLocation, "" ); }
		set { Set( () => PickupPharmacyLocation, value ); }
	}


	public string PickupPharmacyText
	{
		get { return Get( () => PickupPharmacyText, "" ); }
		set { Set( () => PickupPharmacyText, value ); }
	}

	[DependsUpon( nameof( PickupPharmacy ) )]
	[DependsUpon( nameof( PickupPharmacyLocation ) )]
	public void WhenLocationChanges()
	{
		PickupPharmacyText = $"{PickupPharmacy} ({PickupPharmacyLocation})";
	}
#endregion

#region Buttons
	private bool IgnoreCancel;

	public ICommand Back => Commands[ nameof( Back ) ];

	public async void Execute_Back()
	{
		Scanning = false;

		if( IgnoreCancel || !DisplayEntries.Any( entry => entry.Selected ) || OnCancel is null || await OnCancel() )
		{
			DisplayEntries.Clear();
			PickupPharmacy = "";
			PickupRoute    = "";
			OnShowLocation?.Invoke();
		}
		IgnoreCancel = false;
	}

	public bool EnableClaimNow
	{
		get { return Get( () => EnableClaimNow, false ); }
		set { Set( () => EnableClaimNow, value ); }
	}

	public ICommand ClaimNow => Commands[ nameof( ClaimNow ) ];

	public bool CanExecute_ClaimNow() => EnableClaimNow;

	public async void Execute_ClaimNow()
	{
		await Azure.Client.RequestPickupByBarcode( new Protocol.Data.Customers.Priority.PickupByBarcode
		                                           {
			                                           Program      = CurrentVersion.APP_VERSION,
			                                           Driver       = Users.UserName,
			                                           AccountId    = Globals.Priority.Location.GetAccountFromPharmacy( PickupPharmacy ),
			                                           Route        = PickupRoute,
			                                           ServiceLevel = PickupServiceLevel,
			                                           TripIds = ( from D in DisplayEntries
			                                                       where D.Selected
			                                                       select D.TripId ).ToList()
		                                           } );
		IgnoreCancel = true;
		Execute_Back();
	}
#endregion
}