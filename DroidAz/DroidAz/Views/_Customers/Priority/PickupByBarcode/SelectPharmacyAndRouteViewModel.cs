﻿#nullable enable

namespace DroidAz.Views._Customers.Priority;

public class SelectPharmacyAndRouteViewModel : ViewModelBase
{
	private const string ROUTE = "ROUTE";

	private Dictionary<string, List<string>> PharmacyRoutes   = null!;
	private Dictionary<string, string>       PharmacyLocation = null!;

	private bool Initialialised;


	protected override async void OnInitialised()
	{
		base.OnInitialised();

		Execute_Clear();

		List<Globals.Priority.Location.Pharmacy>? Pharmacies1;
		( Pharmacies1, PharmacyRoutes ) = await Globals.Priority.Location.GetPharmacyRouteNames();

		Pharmacies = ( from P in Pharmacies1
		               orderby P.Name
		               select P.Name ).ToList();

		PharmacyLocation = ( from P in Pharmacies1
		                     select P ).ToDictionary( pharmacy => pharmacy.Name, pharmacy => pharmacy.LocationCode );

		var Serv = await Azure.Client.RequestGetServiceLevels();

		if( !Serv.Contains( ROUTE ) )
			Serv.Add( ROUTE );

		ServiceLevels = ( from S in Serv
		                  orderby S
		                  select S ).ToList();

		Initialialised = true;
	}

#region Next
	public bool NextEnable
	{
		get { return Get( () => NextEnable, false ); }
		set { Set( () => NextEnable, value ); }
	}

	private void EnableNext()
	{
		if( Initialialised && SelectedPharmacy.IsNotNullOrWhiteSpace() )
		{
			NextEnable = SelectedServiceLevel switch
			             {
				             null  => false,
				             ""    => false,
				             ROUTE => SelectedRoute.IsNotNullOrWhiteSpace(),
				             _     => true
			             };
		}
		else
			NextEnable = false;
	}
#endregion

#region Service Levels
	public List<string> ServiceLevels
	{
		get { return Get( () => ServiceLevels, [] ); }
		set { Set( () => ServiceLevels, value ); }
	}


	public string SelectedServiceLevel
	{
		get { return Get( () => SelectedServiceLevel, "" ); }
		set { Set( () => SelectedServiceLevel, value ); }
	}

	[DependsUpon10( nameof( SelectedPharmacy ) )]
	[DependsUpon10( nameof( SelectedServiceLevel ) )]
	public void WhenServiceLevelChanges()
	{
		var Pharmacy = SelectedPharmacy;
		var Enable   = ( SelectedServiceLevel == ROUTE ) && ( Pharmacy != "" );
		EnableRoutes = Enable;

		if( Enable )
			Routes = PharmacyRoutes.TryGetValue( Pharmacy, out var R ) ? R : [];

		SelectedRoute = "";
		EnableNext();
	}
#endregion

#region Routes
	public List<string> Routes
	{
		get { return Get( () => Routes, [] ); }
		set { Set( () => Routes, value ); }
	}


	public string SelectedRoute
	{
		get { return Get( () => SelectedRoute, "" ); }
		set { Set( () => SelectedRoute, value ); }
	}


	public bool EnableRoutes
	{
		get { return Get( () => EnableRoutes, false ); }
		set { Set( () => EnableRoutes, value ); }
	}

	[DependsUpon10( nameof( SelectedRoute ) )]
	public void WhenSelectedRouteChanges()
	{
		EnableNext();
	}
#endregion

#region Pharmacies
	public List<string> Pharmacies
	{
		get { return Get( () => Pharmacies, [] ); }
		set { Set( () => Pharmacies, value ); }
	}


	public string SelectedPharmacy
	{
		get { return Get( () => SelectedPharmacy, "" ); }
		set { Set( () => SelectedPharmacy, value ); }
	}
#endregion

#region Actions
	public ICommand Clear => Commands[ nameof( Clear ) ];

	public void Execute_Clear()
	{
		SelectedRoute        = "";
		SelectedPharmacy     = "";
		SelectedServiceLevel = "";
		NextEnable           = false;
	}

	public delegate void OnCloseEvent();

	public delegate void OnNextEvent( string pharmacy, string pharmacyLocation, string serviceLevel, string route );

	public OnCloseEvent? OnClose;
	public OnNextEvent?  OnNext;

	public ICommand Next => Commands[ nameof( Next ) ];

	private bool CanExecNext;

	public bool CanExecute_Next()
	{
		var Retval = CanExecNext;
		CanExecNext = true;
		return Retval;
	}


	public void Execute_Next()
	{
		var Pharm = SelectedPharmacy;
		OnNext?.Invoke( Pharm, PharmacyLocation[ Pharm ], SelectedServiceLevel, SelectedRoute );
	}


	public ICommand Close => Commands[ nameof( Close ) ];

	public void Execute_Close()
	{
		OnClose?.Invoke();
	}
#endregion
}