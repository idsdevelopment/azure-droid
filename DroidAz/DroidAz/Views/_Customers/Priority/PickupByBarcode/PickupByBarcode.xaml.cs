﻿#nullable enable

using DroidAz.Dialogs;

namespace DroidAz.Views._Customers.Priority;

[XamlCompilation( XamlCompilationOptions.Compile )]
public partial class PickupByBarcode : ContentView
{
	public PickupByBarcode()
	{
		InitializeComponent();

		if( Root.BindingContext is PickupByBarcodeViewModel Pm
		    && SelectPharmacyAndRoute.BindingContext is SelectPharmacyAndRouteViewModel SelectModel )
		{
			Pm.OnNotCX   = Sounds.PlayBadScanSound;
			Pm.OnWarning = Sounds.PlayWarningSound;

			Pm.OnNotPackage = async () =>
			                  {
				                  await ErrorDialog.Execute( "Please scan a package barcode." );
			                  };

			Pm.OnAlreadyScanned = async () =>
			                      {
				                      await ErrorDialog.Execute( "Package already scanned" );
			                      };

			Pm.OnScrollIntoView = entry =>
			                      {
				                      CollectionView.ScrollTo( entry );
			                      };

			Pm.OnCancel = async () => await ConfirmDialog.Execute( "Cancel pickup?" );

			async void SelectModelOnNext( string pharmacy, string pharmacyLocation, string level, string route )
			{
				Pm.PickupPharmacy         = pharmacy;
				Pm.PickupPharmacyLocation = pharmacyLocation;
				Pm.PickupServiceLevel     = level;
				Pm.PickupRoute            = route;

				BarcodeScanner.IsVisible = true;
				await this.FadeTo( SelectPharmacyAndRoute, ProcessPickups );
				BarcodeScanner.Focus();

				await Pm.ProcessPickups();
			}

			SelectModel.OnNext = SelectModelOnNext;

			async void PickupModelOnShowLocation()
			{
				BarcodeScanner.IsVisible = false;
				await this.FadeTo( ProcessPickups, SelectPharmacyAndRoute );
			}

			Pm.OnShowLocation = PickupModelOnShowLocation;

			PickupModelOnShowLocation();
		}
	}
}