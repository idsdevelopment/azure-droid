﻿#nullable enable

using Protocol.Data.Customers.Priority;

namespace DroidAz.Views._Customers.Priority;

public static class Globals
{
	public static class Priority
	{
		public static class Location
		{
			private static readonly PriorityLocations[] PLocations =
			[
				new( "Livonia", "793", "Livonia" ),
				new( "Cincinnati", "495", "Cincinnati" ),
				new( "Ashland", "496", "Ashland" ),
				new( "Perrysburg", "743", "Perrysburg" ),
				new( "Hilliard", "742", "Hilliard" ),
				new( "Wadsworth", "623", "Wadsworth" ),
				new( "Grandrapids", "788", "Grandrapids" ),
				new( "Beattyville", "653", "Beattyville" ),
				new( "Dayton", "606", "Dayton" ),
				new( "Escanaba", "735", "Escanaba" ),
				new( "Lexington", "677", "Lexington" ),
				new( "West Branch", "790", "WestBranch" ),
				new( "Heartland (Stats Only)", "766", "Toledo" ),
				new( "Akron", "864", "Akron" ),

				new( "Griffith", "754", "griffith" ),
				new( "South Bend", "764", "southbend" ),
				new( "Fort Wayne", "760", "fortwayne" ),
				new( "Indianapolis", "705", "indianapolis" ),
				new( "Henderson", "780", "henderson" ),
				new( "Louisville", "854", "louisville" ),

				new( "Des Plaines", "717", "DesPlaines" ),
				new( "Peoria", "797", "Peoria" ),
				new( "Decatur", "798", "Decatur" ),
				new( "Quad Cities", "859", "QuadCities" ),

				new( "Infusion", "888", "Infusion" ),

				new( "Minnesota", "464", "Minnesota" )
			];

			private static readonly List<Pharmacy>                   Pharmacies     = [];
			private static readonly Dictionary<string, List<string>> PharmacyRoutes = new();

			private record PriorityLocations( string Code, string Location, string AccountId );

			public record Pharmacy( string Name, string LocationCode );

			public static async Task<(List<Pharmacy> Pharmacies, Dictionary<string, List<string>> PharmacyRoutes)> GetPharmacyRouteNames() => AddPharmacyRouteNames( await Azure.Client.RequestPriorityGetRouteNamesForDevice() );


			public static string GetAccountFromPharmacy( string pharmacy )
			{
				pharmacy = pharmacy.TrimToUpper();

				foreach( var (Code, _, AccountId) in PLocations )
				{
					if( Code.ToUpper() == pharmacy )
						return AccountId;
				}
				return "";
			}

			private static string DoLocation( string name )
			{
				name = name.TrimToUpper();

				foreach( var (Code, Location, _) in PLocations )
				{
					if( Code.ToUpper() == name )
						return Location.ToUpper();
				}
				return "";
			}


			private static (List<Pharmacy> Pharmacies, Dictionary<string, List<string>> PharmacyRoutes) AddPharmacyRouteNames( PriorityDeviceRoutes? pharmacyRouteNames )
			{
				Pharmacies.Clear();
				PharmacyRoutes.Clear();

				if( pharmacyRouteNames is not null )
				{
					var Missing = new HashSet<string>();

					foreach( var Location in PLocations )
						Missing.Add( Location.Code.ToUpper() );

					static void DoLoc( string name, IEnumerable<string> routeNames )
					{
						var Location = DoLocation( name );

						var PName = name.ToUpper();

						Pharmacies.Add( new Pharmacy( PName, Location ) );

						PharmacyRoutes.Add( PName, ( from R in routeNames
						                             orderby R
						                             select R.ToUpper() ).ToList() );
					}

					foreach( var PharmacyRouteName in pharmacyRouteNames )
					{
						var PName    = "";
						var Pharmacy = PharmacyRouteName.Pharmacy;

						foreach( var (Code, _, XlateName) in PLocations )
						{
							if( Pharmacy.Compare( XlateName, StringComparison.OrdinalIgnoreCase ) == 0 )
							{
								PName = Code;
								break;
							}
						}

						if( PName.IsNotNullOrWhiteSpace() )
						{
							Missing.Remove( PName.ToUpper() );
							DoLoc( PName, PharmacyRouteName.RouteNames );
						}
					}

					var DontUse = new List<string> { "Don't use routes" };

					foreach( var M in Missing )
						DoLoc( M, DontUse );
				}
				return ( Pharmacies, PharmacyRoutes );
			}
		}
	}
}