﻿#nullable enable

using DroidAz.Controls.PodAndSign;

namespace DroidAz.Views._Customers.Priority;

public class DeliveriesViewModel : ViewModelBase
{
	public enum SHOW_STATE
	{
		NONE,
		LOCATION,
		DETAILS,
		SELECT_STATIONS,
		SCAN_TRIPS,
		POD_AND_SIGN
	}

#region Filters
	public STATUS[] StatusFilter
	{
		get { return Get( () => StatusFilter, [STATUS.PICKED_UP] ); }
		set { Set( () => StatusFilter, value ); }
	}
#endregion

	public Action<SHOW_STATE>? Show;

#region Scanning
	public TripDisplayEntry? ScanningSelectedItem
	{
		get { return Get( () => ScanningSelectedItem, (TripDisplayEntry?)null ); }
		set { Set( () => ScanningSelectedItem, value ); }
	}

	[DependsUpon( nameof( ScanningSelectedItem ) )]
	public void WhenScanningSelectedItemChanges()
	{
		if( ScanningSelectedItem is not null )
			ScanningSelectedItem = null;
	}
#endregion

#region Trips
	public ObservableCollection<TripDisplayEntry> LocationTripDisplayEntries
	{
		get { return Get( () => LocationTripDisplayEntries, [] ); }
		set { Set( () => LocationTripDisplayEntries, value ); }
	}

#region Selected Trip Ids
	public List<string> SelectedTripIds
	{
		get { return Get( () => SelectedTripIds, [] ); }
		set { Set( () => SelectedTripIds, value ); }
	}

	[DependsUpon( nameof( SelectedTripIds ) )]
	public void WhenSelectedTripIdChanges()
	{
		var TripIds = SelectedTripIds;

		if( TripIds.Count > 0 )
		{
			foreach( var Entry in LocationTripDisplayEntries )
				Entry.IsVisible = true;

			var Trips = new ObservableCollection<TripDisplayEntry>( from L in LocationTripDisplayEntries
			                                                        where TripIds.Contains( L.TripId )
			                                                        select L );
			TripsToBeDelivered = Trips;

			if( Trips.Count > 0 )
				Show?.Invoke( SHOW_STATE.SCAN_TRIPS );
		}
	}

	public ObservableCollection<TripDisplayEntry> TripsToBeDelivered
	{
		get { return Get( () => TripsToBeDelivered, [] ); }
		set { Set( () => TripsToBeDelivered, value ); }
	}
#endregion

	public TripDisplayEntry? SelectedTrip
	{
		get { return Get( () => SelectedTrip, (TripDisplayEntry?)null ); }
		set { Set( () => SelectedTrip, value ); }
	}

	public ICommand LocationItemTapped => Commands[ nameof( LocationItemTapped ) ];

	public void Execute_LocationItemTapped()
	{
		Show?.Invoke( SHOW_STATE.DETAILS );
	}


	public string LocationName
	{
		get { return Get( () => LocationName, "" ); }
		set { Set( () => LocationName, value ); }
	}


	public IList<string> LocationTripIds
	{
		get { return Get( () => LocationTripIds, new List<string>() ); }
		set { Set( () => LocationTripIds, value ); }
	}

	[DependsUpon( nameof( LocationTripIds ) )]
	public void WhenLocationTripIdsChanges()
	{
		if( LocationTripIds.Count > 0 )
			Show?.Invoke( SHOW_STATE.SELECT_STATIONS );
	}
#endregion

#region Buttons
	public Func<Task<bool>>? OnCancelDialogue;

	public ICommand OnDetailsBackButton => Commands[ nameof( OnDetailsBackButton ) ];

	public void Execute_OnDetailsBackButton()
	{
		SelectedTrip = null;
		Show?.Invoke( SHOW_STATE.LOCATION );
	}


	public ICommand OnCancel => Commands[ nameof( OnCancel ) ];

	public async void Execute_OnCancel()
	{
		if( OnCancelDialogue is null || await OnCancelDialogue() )
		{
			SelectedTrip = null;
			Show?.Invoke( SHOW_STATE.LOCATION );
		}
	}

	public ICommand OnScanNext => Commands[ nameof( OnScanNext ) ];

	public void Execute_OnScanNext()
	{
		Show?.Invoke( SHOW_STATE.POD_AND_SIGN );
	}

#region Finalise
	public PodAndSign.Result Values
	{
		get { return Get( () => Values, new PodAndSign.Result() ); }
		set { Set( () => Values, value ); }
	}

	public delegate void FinaliseTripsEvent( List<TripDisplayEntry> trips, PodAndSign.Result values );

	public FinaliseTripsEvent FinaliseTrips = null!;
	public ICommand           OnFinaliseButtonClick => Commands[ nameof( OnFinaliseButtonClick ) ];

	public void Execute_OnFinaliseButtonClick()
	{
		FinaliseTrips( [..TripsToBeDelivered], Values );
		Execute_OnCancel();
	}
#endregion
#endregion
}