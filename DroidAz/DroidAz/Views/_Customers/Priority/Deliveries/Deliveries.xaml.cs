﻿using DroidAz.Dialogs;

namespace DroidAz.Views._Customers.Priority;

[XamlCompilation( XamlCompilationOptions.Compile )]
public partial class Deliveries : ContentView
{
	private DeliveriesViewModel.SHOW_STATE CurrentState = DeliveriesViewModel.SHOW_STATE.NONE;

	public Deliveries()
	{
		InitializeComponent();

		var LocationView   = Location;
		var TripsViewModel = Location.Model;

		var DetailsView    = Details;
		var StationsView   = Stations;
		var ScanView       = ScanTrips;
		var PodAndSignView = PodAndSign;

		var M = (DeliveriesViewModel)Root.BindingContext;

		var TripsJustFinalised = false;

		M.OnCancelDialogue = async () =>
		                     {
			                     if( !TripsJustFinalised )
			                     {
				                     var Cancel = await CancelDialog.Execute();

				                     if( Cancel )
					                     ScanView.StopScanning();

				                     return Cancel;
			                     }
			                     TripsJustFinalised = false;
			                     return true;
		                     };

		M.FinaliseTrips = ( trips, values ) =>
		                  {
			                  TripsJustFinalised = true;

			                  Tasks.RunVoid( () =>
			                                 {
				                                 foreach( var Trip in trips )
					                                 TripsViewModel.PickupDeliverTrip( Trip, values.PopPod, values.Signature, values.ImageBytes, values.ImageExtension );
			                                 } );
		                  };

		M.Show = async state =>
		         {
			         if( state != CurrentState )
			         {
				         switch( CurrentState )
				         {
				         case DeliveriesViewModel.SHOW_STATE.SELECT_STATIONS:
				         case DeliveriesViewModel.SHOW_STATE.LOCATION when state != DeliveriesViewModel.SHOW_STATE.SELECT_STATIONS:
					         LocationView.Cancel();
					         break;
				         }

				         CurrentState = state;

				         bool Loc   = false,
				              Det   = false,
				              Stats = false,
				              Scan  = false,
				              Pod   = false;

				         switch( state )
				         {
				         case DeliveriesViewModel.SHOW_STATE.LOCATION:
					         Loc = true;
					         break;

				         case DeliveriesViewModel.SHOW_STATE.DETAILS:
					         Det = true;
					         break;

				         case DeliveriesViewModel.SHOW_STATE.SELECT_STATIONS:
					         Stats = true;
					         break;

				         case DeliveriesViewModel.SHOW_STATE.SCAN_TRIPS:
					         Scan = true;
					         break;

				         case DeliveriesViewModel.SHOW_STATE.POD_AND_SIGN:
					         Pod = true;
					         break;
				         }

				         await Task.WhenAll(
				                            this.RaiseAndFade( LocationView, Loc ),
				                            this.RaiseAndFade( DetailsView, Det ),
				                            this.RaiseAndFade( StationsView, Stats ),
				                            this.RaiseAndFade( ScanView, Scan ),
				                            this.RaiseAndFade( PodAndSignView, Pod )
				                           );

				         switch( state )
				         {
				         case DeliveriesViewModel.SHOW_STATE.LOCATION:
					         LocationView.StartScanning();
					         break;

				         case DeliveriesViewModel.SHOW_STATE.SCAN_TRIPS:
					         ScanView.StartScanning();
					         break;
				         }
			         }
		         };
	}
}