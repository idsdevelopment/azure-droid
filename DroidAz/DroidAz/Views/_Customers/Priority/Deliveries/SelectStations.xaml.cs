﻿#nullable enable

namespace DroidAz.Views._Customers.Priority;

[XamlCompilation( XamlCompilationOptions.Compile )]
public partial class SelectStations : ContentView
{
	private readonly SelectStationsViewModel Model;

	public SelectStations()
	{
		InitializeComponent();

		var M = Model = (SelectStationsViewModel)Root.BindingContext;
		M.Owner = this;

		M.OnCancel = () =>
		             {
			             var C = OnCancel;

			             if( C is not null )
			             {
				             if( C.CanExecute( this ) )
					             C.Execute( this );
			             }
		             };
	}

#region Buttons
	public static readonly BindableProperty OnCancelProperty = BindableProperty.Create( nameof( OnCancel ),
	                                                                                    typeof( ICommand ),
	                                                                                    typeof( SelectStations ) );

	public ICommand? OnCancel
	{
		get => (ICommand?)GetValue( OnCancelProperty );
		set => SetValue( OnCancelProperty, value );
	}
#endregion

#region Location Name
	public static readonly BindableProperty LocationNameProperty = BindableProperty.Create( nameof( LocationName ),
	                                                                                        typeof( string ),
	                                                                                        typeof( SelectStations ),
	                                                                                        "",
	                                                                                        BindingMode.Default,
	                                                                                        propertyChanged: LocationNamePropertyChanged
	                                                                                      );

	private static void LocationNamePropertyChanged( BindableObject bindable, object oldValue, object newValue )
	{
		if( bindable is SelectStations S && newValue is string LocationName )
			S.Model.LocationName = LocationName;
	}

	public string LocationName
	{
		get => (string)GetValue( LocationNameProperty );
		set => SetValue( LocationNameProperty, value );
	}


	public static readonly BindableProperty LocationTripIdsProperty = BindableProperty.Create( nameof( LocationTripIds ),
	                                                                                           typeof( IList<string> ),
	                                                                                           typeof( SelectStations ),
	                                                                                           new List<string>(),
	                                                                                           BindingMode.OneWay,
	                                                                                           propertyChanged: LocationTripIdsPropertyChanged );

	private static void LocationTripIdsPropertyChanged( BindableObject bindable, object oldValue, object newValue )
	{
		if( bindable is SelectStations S && newValue is IList<string> LocTrips )
			S.Model.LocationTripIds = LocTrips;
	}

	public IList<string> LocationTripIds
	{
		get => (IList<string>)GetValue( LocationTripIdsProperty );
		set => SetValue( LocationTripIdsProperty, value );
	}
#endregion

#region Selected Trip Id's
	public static readonly BindableProperty SelectedTripIdsProperty = BindableProperty.Create( nameof( SelectedTripIds ),
	                                                                                           typeof( IList<string> ),
	                                                                                           typeof( SelectStations ),
	                                                                                           new List<string>(),
	                                                                                           BindingMode.OneWayToSource
	                                                                                         );


	public IList<string> SelectedTripIds
	{
		get => (IList<string>)GetValue( SelectedTripIdsProperty );
		set => SetValue( SelectedTripIdsProperty, value );
	}
#endregion
}