﻿#nullable enable

using System.ComponentModel;
using System.Runtime.CompilerServices;
using DroidAz.Annotations;
using DroidAz.Views._Customers.Priority.Controls;

namespace DroidAz.Views._Customers.Priority;

public class StationEntry : INotifyPropertyChanged
{
	public bool IsChecked
	{
		get => _IsChecked;
		set
		{
			_IsChecked = value;
			CheckedCallback( this );
			OnPropertyChanged();
		}
	}

	public string Station
	{
		get => _Station;
		set
		{
			_Station = value;
			OnPropertyChanged();
		}
	}

	private readonly Action<StationEntry> CheckedCallback;

	private bool _IsChecked;

	private         string _Station = "";
	public readonly bool   IsAllElement;

	public readonly SelectStationsViewModel Owner;

	public readonly IList<string> TripIds;

	[NotifyPropertyChangedInvocator]
	protected virtual void OnPropertyChanged( [CallerMemberName] string? propertyName = null )
	{
		PropertyChanged?.Invoke( this, new PropertyChangedEventArgs( propertyName ) );
	}

	public StationEntry( SelectStationsViewModel owner, string station, bool isAll, IList<string> tripIds, Action<StationEntry> checkedCallback )
	{
		Owner           = owner;
		Station         = station;
		IsAllElement    = isAll;
		TripIds         = tripIds;
		CheckedCallback = checkedCallback;
	}

	public event PropertyChangedEventHandler? PropertyChanged;
}

public class SelectStationsViewModel : ViewModelBase
{
	private const string ALL_STATIONS = "(All Stations)";

	public string LocationName
	{
		get { return Get( () => LocationName, "" ); }
		set { Set( () => LocationName, value ); }
	}

	public List<StationEntry> Stations
	{
		get { return Get( () => Stations, [] ); }
		set { Set( () => Stations, value ); }
	}


	public bool OkEnabled
	{
		get { return Get( () => OkEnabled, false ); }
		set { Set( () => OkEnabled, value ); }
	}

	public SelectStations Owner = null!;

	private void DoStations()
	{
		var StationList = ( from T in LocationTripIds
		                    let Split = T.SplitCxBarcode()
		                    where Split.Ok
		                    group ( Split.DestinationLocation, Split.NursingStation, TripId: T ) by Split.NursingStation
		                    into S
		                    orderby S.Key
		                    let Station = S.First()
		                    let TripIds = ( from St in S
		                                    where St.NursingStation == Station.NursingStation
		                                    select St.TripId ).ToList()
		                    select ( Station.DestinationLocation, Station.NursingStation, TripIds ) ).ToList();

		var StationEntries = new List<StationEntry>();

		var StationListCount = StationList.Count;

		if( StationListCount > 0 )
		{
			if( StationListCount > 1 )
			{
				StationEntries.Add( new StationEntry( this,
				                                      $"D{StationList[ 0 ].DestinationLocation}  {ALL_STATIONS}",
				                                      true,
				                                      LocationTripIds,
				                                      CheckedCallback ) );
			}

			StationEntries.AddRange( from S in StationList
			                         select new StationEntry(
			                                                 this,
			                                                 $"D{S.DestinationLocation}-{S.NursingStation}",
			                                                 false,
			                                                 S.TripIds,
			                                                 CheckedCallback ) );

			if( StationListCount == 1 )
				StationEntries[ 0 ].IsChecked = true;
		}
		Stations = StationEntries;
		EnableOk();
	}

#region Station Checked Callabck
	private bool InStationCheckedCallback;

	internal void CheckedCallback( StationEntry sender )
	{
		if( !InStationCheckedCallback )
		{
			InStationCheckedCallback = true;

			try
			{
				var OwnerStations = sender.Owner.Stations;
				var IsAll         = sender is {IsAllElement: true, IsChecked: true};

				foreach( var Station in OwnerStations )
				{
					if( Station != sender )
					{
						if( IsAll )
							Station.IsChecked = false;
						else if( Station.IsAllElement )
							Station.IsChecked = false;
					}
				}
				EnableOk();
			}
			finally
			{
				InStationCheckedCallback = false;
			}
		}
	}
#endregion

#region Location Trip Ids
	public IList<string> LocationTripIds
	{
		get { return Get( () => LocationTripIds, new List<string>() ); }
		set { Set( () => LocationTripIds, value ); }
	}

	[DependsUpon( nameof( LocationTripIds ) )]
	public void WhenLocationTripIdsChange()
	{
		Owner.SelectedTripIds = new List<string>();
		DoStations();
	}
#endregion

#region Buttons
	public Action?  OnCancel;
	public ICommand CancelButtonClick => Commands[ nameof( CancelButtonClick ) ];

	public void Execute_CancelButtonClick()
	{
		OnCancel?.Invoke();
	}

	private void EnableOk()
	{
		OkEnabled = ( from S in Stations
		              where S.IsChecked
		              select S ).Any();
	}


	public ICommand OkButtonClick => Commands[ nameof( OkButtonClick ) ];

	public void Execute_OkButtonClick()
	{
		List<string> SelectedTripIds;

		var St = Stations;

		if( St.Count > 0 )
		{
			// If All, select everything that's unchecked
			var S0              = St[ 0 ];
			var SelectUnchecked = S0.Station.Contains( ALL_STATIONS ) && S0.IsChecked;

			SelectedTripIds = ( from S in St
			                    where SelectUnchecked ? !S.IsChecked : S.IsChecked
			                    select S ).SelectMany( s => s.TripIds ).ToList();
		}
		else
			SelectedTripIds = [];

		Owner.SelectedTripIds = SelectedTripIds;
	}
#endregion
}