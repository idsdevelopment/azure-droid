﻿namespace DroidAz.Views._Customers.Priority.Controls;

public static class Extensions
{
	public static string PriorityBarcodeFilter( this string str )
	{
		var Temp = new StringBuilder();

		foreach( var C in str )
		{
			if( C is >= 'A' and <= 'Z' or >= 'a' and <= 'z' or >= '0' and <= '9' or '+' or '-' )
				Temp.Append( C );
		}

		return Temp.ToString();
	}

	//
	// Structure (I think)
	//
	// CX-S[Source Location[-Heartland Location]]+D[Destination Location[-Nursing Location]]+I[Invoice Number]
	// 

	public static (bool Ok, bool IsHeartland, string SourceLocation, string DestinationLocation, string NursingStation, string InvoiceNumber) SplitCxBarcode(this string barcode)
	{
		(bool Ok, bool IsHeartland, string SourceLocation, string DestinationLocation, string NursingStation, string InvoiceNumber) Result = (false, false, "", "", "", "");

		var Barcode = barcode.Trim();

		if (Barcode.StartsWith("CX-S", StringComparison.OrdinalIgnoreCase))
		{
			Barcode = Barcode.TrimStart("CX-S", StringComparison.OrdinalIgnoreCase);
			var P = Barcode.IndexOf('+'); // Start of destination

			if (P > 0)
			{
				var SourceLocation      = Barcode.Substring(0, P);
				var DestinationLocation = Barcode.Substring(P + 1);

				if (DestinationLocation.StartsWith("D", StringComparison.OrdinalIgnoreCase))
				{
					DestinationLocation = DestinationLocation.TrimStart("D", StringComparison.OrdinalIgnoreCase);
					P                   = DestinationLocation.IndexOf('+');

					if (P > 0)
					{
						Result.InvoiceNumber = DestinationLocation.Substring(P + 1);
						DestinationLocation  = DestinationLocation.Substring(0, P);

						var P1 = SourceLocation.IndexOf('-');

						if (P1 > 0)
						{
							Result.IsHeartland    = true;
							Result.SourceLocation = SourceLocation.Substring(0, P1);
						}
						else
							Result.SourceLocation = SourceLocation;

						var DestAndNursing = DestinationLocation.Substring(0, P);
						P = DestAndNursing.IndexOf('-');

						if (P > 0)
						{
							Result.DestinationLocation = DestAndNursing.Substring(0, P);
							Result.NursingStation      = DestAndNursing.Substring(P + 1);
						}
						else
							Result.DestinationLocation = DestAndNursing;

						Result.Ok = true;
					}
				}
			}
		}
		return Result;
	}

}