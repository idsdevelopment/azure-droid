﻿using DroidAz.Controls.Default.Location;

namespace DroidAz.Views._Customers.Priority.Controls;

public class PriorityLocation : Location
{
	public override string OnFormatBarcode( string barcode, ObservableCollection<TripDisplayEntry> displayTrips ) => barcode.PriorityBarcodeFilter();

	public override ObservableCollection<TripDisplayEntry> OnFilterByBarcode( ObservableCollection<TripDisplayEntry> displayTrips, string barcode )
	{
		barcode = barcode.Trim();

		if( barcode.Contains( '-' ) && !barcode.Contains( '+' ) )
		{
			var Trip = ( from T in displayTrips
			             where T.Reference == barcode
			             select T.TripId ).FirstOrDefault();

			if( Trip is not null )
				barcode = Trip;
		}

		var (Ok, _, _, DestinationLocation, _, _) = barcode.SplitCxBarcode();

		if( Ok )
		{
			var NursingStationLocation = $"{DestinationLocation}-";

			return new ObservableCollection<TripDisplayEntry>( from T in displayTrips
			                                                   where ( T.DisplayLocation == DestinationLocation ) || T.DisplayLocation.StartsWith( NursingStationLocation )
			                                                   select T );
		}

		return [];
	}
}