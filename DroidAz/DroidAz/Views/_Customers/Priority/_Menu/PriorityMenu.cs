﻿#nullable enable

namespace DroidAz.Views._Customers.Priority.Menu;

public static class PriorityMenu
{
	private const string PICKUP_BY_BARCODE = "PriorityPickupByBarcode",
	                     DELIVERIES        = "PriorityDeliveries";


	public static Dictionary<string, Applications.Program> Programs = new()
	                                                                  {
		                                                                  {
			                                                                  PICKUP_BY_BARCODE,
			                                                                  new Applications.Program
			                                                                  {
				                                                                  PageType = typeof( PickupByBarcode ),
				                                                                  Default  = true
			                                                                  }
		                                                                  },
		                                                                  {
			                                                                  DELIVERIES,
			                                                                  new Applications.Program
			                                                                  {
				                                                                  PageType = typeof( Deliveries )
			                                                                  }
		                                                                  }
	                                                                  };
}