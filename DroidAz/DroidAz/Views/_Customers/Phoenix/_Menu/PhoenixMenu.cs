﻿using DroidAz.Views._Standard.Deliveries;

namespace DroidAz.Views._Customers.Phoenix.Menu;

public static class PhoenixMenu
{
	public static Dictionary<string, Applications.Program> Programs = new()
																	  {
																		  {
																			  Applications.DELIVERIES_LOADER,
																			  new Applications.Program
																			  {
																				  PageType          = typeof( Loader ),
																				  Default           = true,
																				  ConfirmTripRemove = true
																			  }
																		  }
																	  };
}