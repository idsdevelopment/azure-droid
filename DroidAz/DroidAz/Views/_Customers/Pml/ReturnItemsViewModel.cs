﻿#nullable enable

using System.ComponentModel;
using Preferences = Globals.Preferences;

namespace DroidAz.Views._Customers.Pml;

public class PmlTripItemsDisplayEntry : TripItemsDisplayEntry, INotifyPropertyChanged
{
	public const string UP      = "UP",
	                    UPLIFT  = "UPLIFT",
	                    UPPACK  = "UPPACK",
	                    UPPC    = "UPPC",
	                    UPTOTAL = "UPTOTAL",
	                    RETURN  = "RETURN",
	                    CTN     = "(CT)",
	                    PK      = "(PK)";

	public bool IsUplift   => string.Compare( ItemCode, UPLIFT, StringComparison.OrdinalIgnoreCase ) == 0;
	public bool IsUpPack   => string.Compare( ItemCode, UPPACK, StringComparison.OrdinalIgnoreCase ) == 0;
	public bool IsUpPc     => string.Compare( ItemCode, UPPC, StringComparison.OrdinalIgnoreCase ) == 0;
	public bool IsUpTotal  => string.Compare( ItemCode, UPTOTAL, StringComparison.OrdinalIgnoreCase ) == 0;
	public bool IsReturn   => string.Compare( ItemCode, RETURN, StringComparison.OrdinalIgnoreCase ) == 0;
	public bool IsUp       => ItemCode.StartsWith( UP, StringComparison.OrdinalIgnoreCase ) && !IsUpTotal;
	public bool MoreToDo   => DisplayBalance > 0;
	public bool HasScanned => DisplayScanned > 0;

	public bool InError => MoreToDo;

	public string BaseDescription = "";

	public List<string> OriginalTripIds = [];

	public PmlTripItemsDisplayEntry( TripItemsDisplayEntry e ) : base( e )
	{
		AllowedCartonQuantity = e.CartonQuantity;
	}


#region Values
	private decimal _Balance;

	public override decimal Balance
	{
		get => _Balance;
		set
		{
			if( SetField( ref _Balance, value ) )
			{
				OnPropertyChanged( nameof( DisplayBalance ) );
				OnPropertyChanged( nameof( IsVisible ) );
			}
		}
	}

	private decimal _ImaginaryScanned;

	public decimal ImaginaryScanned
	{
		get => _ImaginaryScanned;
		set
		{
			if( SetField( ref _ImaginaryScanned, value ) )
			{
				OnPropertyChanged( nameof( DisplayScanned ) );
				OnPropertyChanged( nameof( DisplayBalance ) );
				OnPropertyChanged( nameof( IsVisible ) );
			}
		}
	}

	public decimal AllowedCartonQuantity;
#endregion


#region Display
	public override decimal DisplayScanned => Scanned + ImaginaryScanned;

	public override decimal DisplayBalance => Balance - ImaginaryScanned;
#endregion

#region Visibility
	private bool _DontHide;

	public bool DontHide
	{
		get => _DontHide;

		set
		{
			if( SetField( ref _DontHide, value ) )
				OnPropertyChanged( nameof( IsVisible ) );
		}
	}

	public override bool IsVisible
	{
		get => ( ( ( MoreToDo && IsReturn ) || DontHide ) && !IsUpTotal ) || ( HasScanned && IsUp );
		set { }
	}
#endregion
}

public class ReturnItemsViewModel : ViewModelBase
{
	public bool IsScanning
	{
		get { return Get( () => IsScanning, false ); }
		set { Set( () => IsScanning, value ); }
	}


	public bool BarcodeScannerEnabled
	{
		get { return Get( () => BarcodeScannerEnabled, false ); }
		set { Set( () => BarcodeScannerEnabled, value ); }
	}


	public int TotalScanned
	{
		get { return Get( () => TotalScanned, 0 ); }
		set { Set( () => TotalScanned, value ); }
	}


	public int TotalOfAllScanned
	{
		get { return Get( () => TotalOfAllScanned, 0 ); }
		set { Set( () => TotalOfAllScanned, value ); }
	}

	public bool EnableNext
	{
		get { return Get( () => EnableNext, false ); }
		set { Set( () => EnableNext, value ); }
	}

	private bool    AllowUnderScan;
	public  Action? NotAuthorized;

	public Action? OnBackButton;

	private decimal ScanLimit;

	protected override void OnInitialised()
	{
		base.OnInitialised();
		AllowUnderScan = Preferences.AllowUnderScans;
	}

	[DependsUpon( nameof( TotalOfAllScanned ) )]
	public void WhenTotalScannedChanges()
	{
		Dispatcher( () =>
		            {
			            if( MainPageViewModel.Instance is not null )
				            MainPageViewModel.Instance.ScannedCount = TotalOfAllScanned;
		            } );
	}

	public void StartScanning( bool resetScan )
	{
		if( resetScan )
		{
			TotalScanned      = 0;
			TotalOfAllScanned = 0;
		}

		var Limit = decimal.MaxValue;

		foreach( var Item in Items )
		{
			Item.DontHide = false;

			if( Item.IsUpTotal )
				Limit = Math.Min( Limit, Item.Pieces );
		}

		ScanLimit = Limit;
		DoEnableNext();
		IsScanning = true;
	}

	private void DoEnableNext()
	{
		var Itms = Items;

		var Enable = Itms.Count > 0;

		if( Enable )
		{
			if( AllowUnderScan )
			{
				Enable = ( from I in Itms
				           where I.Scanned > 0
				           select I ).Any();
			}
			else
			{
				Enable = !( from I in Itms
				            where I.IsReturn && I.MoreToDo
				            select I ).Any();
			}
		}
		EnableNext = Enable;
	}

#region Items
#region Cartons
	private Dictionary<string, (string Description, string BaseDescription, decimal Quantity, PmlTripItemsDisplayEntry Entry)> Cartons = [];

	private Dictionary<string, PmlTripItemsDisplayEntry> ItemToCarton = [],
	                                                     CartonToItem = [];
#endregion

	private Dictionary<string, PmlTripItemsDisplayEntry> CurrentDisplayedItems = [];
	private List<PmlTripItemsDisplayEntry>               OriginalItems         = [];

	public ObservableCollection<PmlTripItemsDisplayEntry> Items
	{
		get { return Get( () => Items, [] ); }
		set
		{
			Set( () => Items, value, items =>
			                         {
				                         OriginalItems = [..items];

				                         var Filtered = new ObservableCollection<PmlTripItemsDisplayEntry>( from Item in items
				                                                                                            where Item.IsVisible
				                                                                                            orderby Item.Barcode
				                                                                                            select Item );

				                         CurrentDisplayedItems = [];

				                         foreach( var Item in Filtered )
					                         CurrentDisplayedItems.TryAdd( Item.Barcode, Item );

				                         return Filtered;
			                         } );
		}
	}

	[DependsUpon( nameof( Items ) )]
	public void WhenItemsChange()
	{
		Cartons      = [];
		ItemToCarton = [];
		CartonToItem = [];

		// Find all cartons
		foreach( var Item in Items )
		{
			string Desc;

			string Strip( string suffix )
			{
				return Item.Description.Replace( suffix, "", StringComparison.OrdinalIgnoreCase ).Trim().ToUpper();
			}

			if( Item.IsCarton )
			{
				Desc = Strip( PmlTripItemsDisplayEntry.CTN );
				Cartons.Add( Item.Barcode, ( Item.Description, Desc, Item.CartonQuantity, Item ) );
			}
			else
				Desc = Strip( PmlTripItemsDisplayEntry.PK );

			Item.BaseDescription = Desc;
		}

		// Link cartons to items
		foreach( var Item in Items )
		{
			if( !Item.IsCarton )
			{
				var Desc = Item.BaseDescription;

				foreach( var Carton in Cartons )
				{
					if( Desc == Carton.Value.BaseDescription )
					{
						Item.CartonQuantity = Carton.Value.Quantity;
						ItemToCarton.Add( Item.Barcode, Carton.Value.Entry );
						CartonToItem.Add( Carton.Key, Item );
						break;
					}
				}
			}
		}
	}
#endregion

#region Selected Item
	public PmlTripItemsDisplayEntry? SelectedItem
	{
		get { return Get( () => SelectedItem, (PmlTripItemsDisplayEntry?)null ); }
		set { Set( () => SelectedItem, value ); }
	}

	[DependsUpon( nameof( SelectedItem ), Delay = 1000 )]
	public void WhenSelectedItemChanges()
	{
		SelectedItem = null; // Remove highlight
	}
#endregion

#region Back Button
	public ICommand BackButton => Commands[ nameof( BackButton ) ];

	public void Execute_BackButton()
	{
		OnBackButton?.Invoke();
	}
#endregion

#region ItemCode
	public string ItemCode
	{
		get { return Get( () => ItemCode, "" ); }
		set { Set( () => ItemCode, value ); }
	}

	[DependsUpon( nameof( ItemCode ) )]
	public void WenItemCodeChanges()
	{
		var Barcode = ItemCode.Trim();

		if( Barcode != "" )
		{
			ItemCode = "";

			var Ok = true;

			try
			{
				var CurrentItems = Items;

				if( Barcode.IsNotNullOrEmpty() )
				{
					if( !CurrentDisplayedItems.ContainsKey( Barcode ) )
					{
						var Found      = false;
						var ItemsCount = CurrentItems.Count;

						for( var Index = 0; Index < ItemsCount; Index++ )
						{
							var CurrentItem = CurrentItems[ Index ];

							if( ( CurrentItem.Barcode == Barcode ) && CurrentItem.IsVisible )
							{
								Found = true;
								break;
							}
						}

						if( !Found )
						{
							ItemsCount = OriginalItems.Count;

							for( var Index = 0; Index < ItemsCount; Index++ )
							{
								var CurrentItem = OriginalItems[ Index ];

								if( CurrentItem.Barcode == Barcode )
								{
									OriginalItems.RemoveAt( Index );
									CurrentItems.Add( CurrentItem );
									CurrentDisplayedItems.TryAdd( Barcode, CurrentItem );
									break;
								}
							}
						}
					}

					// Do returns first
					foreach( var Item in CurrentItems )
					{
						if( ( Item.Barcode == Barcode ) && Item is { IsReturn: true, MoreToDo: true } )
						{
							try
							{
								if( Item.IsCarton )
								{
									if( Item.DisplayBalance > 0 ) // Has a cartons remaining
									{
										if( CartonToItem.TryGetValue( Item.Barcode, out var CartonItem ) )
										{
											var CQty = Item.CartonQuantity;

											if( CartonItem.DisplayBalance >= CQty )
												CartonItem.Balance -= CQty;
											else
											{
												Ok = false;
												return;
											}
										}

										Item.Scanned += 1;
										Item.Balance -= 1;
									}
									else
									{
										Ok = false;
										return;
									}
								}
								else
								{
									if( Item.DisplayBalance > 0 )
									{
										Item.Scanned++;
										Item.Balance--;

										if( ItemToCarton.TryGetValue( Item.Barcode, out var Carton ) )
										{
											var CQty    = Carton.CartonQuantity;
											var Balance = Math.Truncate( Item.Balance / CQty );
											Carton.Balance = Balance;
										}
									}
									else
									{
										Ok = false;
										return;
									}
								}
							}
							finally
							{
								if( Ok )
									TotalOfAllScanned++;
							}
							return;
						}
					}
				}

				// Do UpPacks
				foreach( var Item in CurrentItems )
				{
					if( ( Item.Barcode == Barcode ) && Item is { IsUpPack: true, MoreToDo: true } )
					{
						Item.Scanned++;
						Item.Balance--;
						TotalOfAllScanned++;
						return;
					}
				}

				if( TotalScanned >= ScanLimit )
					Ok = false;
				else
				{
					//Check all pieces before uplifts
					foreach( var Item in CurrentItems )
					{
						if( ( Item.Barcode == Barcode ) && Item is { IsUpPc: true, MoreToDo: true } )
						{
							Item.Scanned++;
							Item.Balance--;

							TotalScanned++;
							TotalOfAllScanned++;
							return;
						}
					}

					// Do Uplifts
					foreach( var Item in CurrentItems )
					{
						if( ( Item.Barcode == Barcode ) && Item is { IsUplift: true, MoreToDo: true } )
						{
							var Len = Barcode.Length;

							var Prefix = Len switch
							             {
								             8  => Barcode[ ..3 ],
								             13 => Barcode[ ..7 ],
								             _  => ""
							             };

							switch( Prefix )
							{
						#if DEBUG
							case "":
						#endif
							case "9310704":
							case "8710632":
							case "932":
							case "933":
							case "934":
								Item.Scanned++;
								Item.Balance--;
								TotalScanned++;
								TotalOfAllScanned++;
								return;
							}
						}
					}
					Ok = false;
				}
			}
			finally
			{
				if( !Ok )
					NotAuthorized?.Invoke();
				DoEnableNext();
			}
		}
	}
#endregion

#region Next Button
	public delegate void NextButtonEvent( int totalScanned, ObservableCollection<PmlTripItemsDisplayEntry> items );

	public NextButtonEvent OnNextButton = null!;
	public ICommand        Next => Commands[ nameof( Next ) ];

	public bool CanExecute_Next() => EnableNext;

	public void Execute_Next()
	{
		OnNextButton( TotalOfAllScanned, Items );
	}
#endregion

#region Futile Button
	public Action   OnFutile = null!;
	public ICommand Futile => Commands[ nameof( Futile ) ];

	public void Execute_Futile()
	{
		OnFutile();
	}
#endregion

#region Location Items
	public ObservableCollection<TripDisplayEntry>? LocationItems

	{
		get { return Get( () => LocationItems, [] ); }
		set { Set( () => LocationItems, value ); }
	}

	[DependsUpon( nameof( LocationItems ) )]
	public void WhenLocationItemsChange()
	{
		if( LocationItems is not null && ( LocationItems.Count > 0 ) )
		{
			var TripIds = new List<string>( from I in LocationItems
			                                select I.TripId );

			// Sum duplicate items
			var ItemsDict = new Dictionary<string, PmlTripItemsDisplayEntry>();

			var TempItems = from Entry in LocationItems
			                where Entry.Status < STATUS.PICKED_UP
			                from Item in Entry.Items
			                orderby Item.Barcode
			                select new PmlTripItemsDisplayEntry( Item );

			foreach( var Entry in TempItems )
			{
				var Barcode = Entry.Barcode;

				if( ItemsDict.TryGetValue( Barcode, out var ItemsDisplayEntry ) )
				{
					ItemsDisplayEntry.Pieces   += Entry.Pieces;
					ItemsDisplayEntry.Balance  += Entry.Balance;
					ItemsDisplayEntry.Original += Entry.Original;
				}
				else
				{
					Entry.OriginalTripIds = TripIds;
					ItemsDict[ Barcode ]  = Entry;
				}
			}

			Items = new ObservableCollection<PmlTripItemsDisplayEntry>( ( from I in ItemsDict.Values
			                                                              orderby I.Barcode
			                                                              select I ) );
		}
		else
			Items = [];

		DoEnableNext();
	}
#endregion
}