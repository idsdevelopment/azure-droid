﻿#nullable enable

namespace DroidAz.Views._Customers.Pml;

public class FutileReasonsViewModel : ViewModelBase
{
	public bool EnableOk
	{
		get { return Get( () => EnableOk, false ); }
		set { Set( () => EnableOk, value ); }
	}


	public string Reason
	{
		get { return Get( () => Reason, "" ); }
		set { Set( () => Reason, value ); }
	}


	public bool DeleteItems
	{
		get { return Get( () => DeleteItems, false ); }
		set { Set( () => DeleteItems, value ); }
	}

	[DependsUpon( nameof( Reason ) )]
	public void WhenReasonChanges()
	{
		var R    = Reason;
		var IsOk = R.IsNotNullOrEmpty();
		EnableOk = IsOk;

		if( IsOk )
		{
			DeleteItems = R.Trim() switch
			              {
				              "Stock Not Ready"               => false,
				              "Other"                         => false,
				              "Stock Not Identical To Return" => true,
				              "No Stock To Collect"           => true,
				              "Australian Stock"              => true,
				              "Open Stock"                    => true,
				              _                               => false
			              };
		}
	}

#region Buttons
	public Action OnCancel = null!;

	public delegate void OkEvent( string reason, bool delete );

	public OkEvent OnOk = null!;

	public ICommand Ok => Commands[ nameof( Ok ) ];

	public bool CanExecute_Ok() => false; // Weird, Xamarin forms takes this as the first value for IsEnabled on a Button

	public void Execute_Ok()
	{
		OnOk( Reason.Trim(), DeleteItems );
	}


	public ICommand Cancel => Commands[ nameof( Cancel ) ];

	public void Execute_Cancel()
	{
		OnCancel();
	}
#endregion
}