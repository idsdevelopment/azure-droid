﻿#nullable enable

namespace DroidAz.Views._Customers.Pml;

public class ReturnDetailsViewModel : ViewModelBase
{
	public const string SATCHEL = "Satchel";

	public TripDisplayEntry Trip
	{
		get { return Get( () => Trip, new TripDisplayEntry() ); }
		set { Set( () => Trip, value ); }
	}

	[DependsUpon( nameof( Trip ) )]
	public void WhenTripChanges()
	{
		EnablePickupButton = string.Compare( Trip.PackageType, SATCHEL, StringComparison.OrdinalIgnoreCase ) != 0;
	}

#region PickupButton
	public bool EnablePickupButton
	{
		get { return Get( () => EnablePickupButton, true ); }
		set { Set( () => EnablePickupButton, value ); }
	}


	public Action<TripDisplayEntry>? OnPickupTrip;

	public ICommand OnPickup => Commands[ nameof( OnPickup ) ];

	public void Execute_OnPickup()
	{
		OnPickupTrip?.Invoke( Trip );
	}
#endregion

#region Back Button
	public ICommand OnBack => Commands[ nameof( OnBack ) ];

	public void Execute_OnBack()
	{
		Dispatcher( async () =>
		            {
			            if( OnBackButton is not null )
				            await OnBackButton.Invoke();
		            } );
	}

	public delegate Task OnBackButtonEvent();


	public OnBackButtonEvent? OnBackButton;
#endregion
}