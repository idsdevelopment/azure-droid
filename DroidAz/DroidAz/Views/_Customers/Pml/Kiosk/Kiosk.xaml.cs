﻿using Keyboard = Globals.Keyboard;

namespace DroidAz.Views._Customers.Pml.Kiosk;

[XamlCompilation( XamlCompilationOptions.Compile )]
public partial class Kiosk : ContentView
{
	public Kiosk()
	{
		InitializeComponent();

		void Close()
		{
			ViewModelBase.Dispatcher( async () =>
			                          {
				                          MenuButtonsArea.IsVisible = true;
				                          await this.FadeTo( ProgramArea, MenuButtonsArea );
				                          ProgramArea.IsVisible = false;
				                          ProgramArea.Content   = null;
			                          } );
		}

		void Show( View view )
		{
			ViewModelBase.Dispatcher( async () =>
			                          {
				                          Keyboard.Show          = false;
				                          ProgramArea.Content    = view;
				                          view.HorizontalOptions = LayoutOptions.FillAndExpand;
				                          view.VerticalOptions   = LayoutOptions.FillAndExpand;

				                          if( view.BindingContext is TripsBaseViewModel VModel )
					                          VModel.OnClose = Close;

				                          ProgramArea.IsVisible = true;
				                          await this.FadeTo( MenuButtonsArea, ProgramArea );
				                          MenuButtonsArea.IsVisible = false;
			                          } );
		}

		if( Root.BindingContext is KioskViewModel M )
		{
			M.RunVerify = () =>
			              {
				              Show( new Verify() );
			              };

			M.RunClaim = () =>
			             {
				             Show( new Claim() );
			             };
		}
	}
}