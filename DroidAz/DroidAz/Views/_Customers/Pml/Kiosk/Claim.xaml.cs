﻿#nullable enable

namespace DroidAz.Views._Customers.Pml.Kiosk;

[XamlCompilation( XamlCompilationOptions.Compile )]
public partial class Claim : ContentView
{
	public Claim()
	{
		InitializeComponent();

		if( BindingContext is ClaimViewModel Model )
		{
			Model.OnTripNotFound = async tripId =>
			                       {
				                       var Text = Globals.Resources.GetStringResource( "PmlShipmentNotFound" );

				                       await MainPage.Instance.DisplayAlert( Globals.Resources.GetStringResource( "Error" ),
				                                                             $"{Text}: {tripId}",
				                                                             Globals.Resources.GetStringResource( "Cancel" ) );
			                       };

			Model.OnAlreadyScanned = async tripId =>
			                         {
				                         var Text = Globals.Resources.GetStringResource( "PmlAlreadyScanned" );

				                         await MainPage.Instance.DisplayAlert( Globals.Resources.GetStringResource( "Error" ),
				                                                               $"{tripId} {Text}",
				                                                               Globals.Resources.GetStringResource( "Cancel" ) );
			                         };
			Model.Scanning = true;
		}
	}
}