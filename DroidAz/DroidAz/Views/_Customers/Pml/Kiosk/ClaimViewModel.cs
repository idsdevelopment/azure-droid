﻿using DroidAz.Modifications;
using Protocol.Data._Customers.Pml;

namespace DroidAz.Views._Customers.Pml.Kiosk;

public class ClaimViewModel : TripsBaseViewModel
{
	public string Caption
	{
		get { return Get( () => Caption, "Caption" ); }
		set { Set( () => Caption, value ); }
	}

	private string ScanCage,
	               ScanShipments,
	               CageBarcode,
	               BarcodeText;

	public Action<string> OnTripNotFound,
	                      OnAlreadyScanned;

	protected override void OnInitialised()
	{
		base.OnInitialised();

		ScanCage      = Globals.Resources.GetStringResource( "PmlScanCage" );
		ScanShipments = Globals.Resources.GetStringResource( "PmlScanShipments" );
		CageBarcode   = Globals.Resources.GetStringResource( "PmlCageBarcode" );
		BarcodeText   = Globals.Resources.GetStringResource( "Barcode" );

		OTrips             = [];
		CloseButtonVisible = true;
		ScanningCage       = true;
		Scanning           = true;
	}

#region Cage
	public string Cage
	{
		get { return Get( () => Cage, "123456" ); }
		set { Set( () => Cage, value ); }
	}


	public bool ShowCage
	{
		get { return Get( () => ShowCage, IsInDesignMode ); }
		set { Set( () => ShowCage, value ); }
	}
#endregion


#region Scanner
	public string PlaceholderText
	{
		get { return Get( () => PlaceholderText, "" ); }
		set { Set( () => PlaceholderText, value ); }
	}

	public bool ScanningCage
	{
		get { return Get( () => ScanningCage, false ); }
		set { Set( () => ScanningCage, value ); }
	}


	public bool NotScanningCage
	{
		get { return Get( () => NotScanningCage, false ); }
		set { Set( () => NotScanningCage, value ); }
	}


	[DependsUpon( nameof( ScanningCage ) )]
	public void WhenScanningCageChanges()
	{
		var Sc = ScanningCage;
		NotScanningCage = !Sc;

		if( Sc )
		{
			ShowCage           = false;
			Caption            = ScanCage;
			PlaceholderText    = CageBarcode;
			CloseButtonVisible = true;
		}
		else
		{
			ShowCage              = true;
			Cage                  = Barcode;
			Caption               = ScanShipments;
			PlaceholderText       = BarcodeText;
			CloseButtonVisible    = false;
			CompleteButtonVisible = false;
		}
	}

	public bool Scanning
	{
		get { return Get( () => Scanning, false ); }
		set { Set( () => Scanning, value ); }
	}


	public string Barcode
	{
		get { return Get( () => Barcode, "" ); }
		set { Set( () => Barcode, value ); }
	}

	[DependsUpon10( nameof( Barcode ) )]
	public void WhenBarcodeChanges()
	{
		var BCode = Barcode.Trim();

		Barcode = "";

		if( BCode.IsNotNullOrWhiteSpace() )
		{
			if( ScanningCage )
				ScanningCage = false;
			else
			{
				if( OTrips.Any( T => T.TripId == BCode ) )
				{
					Dispatcher( () =>
					            {
						            Sounds.PlayBadScanSound();
						            OnAlreadyScanned?.Invoke( BCode );
					            } );

					Scanning = true;

					return;
				}

				Task.Run( async () =>
				          {
					          var Trip = await Azure.Client.RequestGetTrip( new GetTrip { TripId = BCode, Signatures = false } );

					          Dispatcher( () =>
					                      {
						                      if( Trip is not null && Trip.Ok )
						                      {
							                      OTrips.Add( new TripDisplayEntry( Trip, ServiceLevelColours ) );
							                      RaisePropertyChangedDelayed( nameof( OTrips ) );
							                      CloseButtonVisible    = false;
							                      CompleteButtonVisible = true;
						                      }
						                      else
						                      {
							                      Sounds.PlayBadScanSound();
							                      OnTripNotFound?.Invoke( BCode );
						                      }
					                      } );
				          } );
			}
		}
	}
#endregion

#region Trips
	public ObservableCollection<TripDisplayEntry> OTrips
	{
		get { return Get( () => OTrips, [] ); }
		set { Set( () => OTrips, value ); }
	}

	public TripDisplayEntry SelectedItem
	{
		get { return Get( () => SelectedItem, (TripDisplayEntry)null ); }
		set { Set( () => SelectedItem, value ); }
	}
#endregion


#region Buttons
	public bool CloseButtonVisible
	{
		get { return Get( () => CloseButtonVisible, IsInDesignMode ); }
		set { Set( () => CloseButtonVisible, value ); }
	}

	[DependsUpon( nameof( CloseButtonVisible ) )]
	public void WhenCloseButtonVisibleChanges()
	{
		var Vis = !CloseButtonVisible;
		CancelButtonVisible   = Vis;
		CompleteButtonVisible = Vis;
	}

	public ICommand Close => Commands[ nameof( Close ) ];

	public void Execute_Close()
	{
		OnClose?.Invoke();
	}


	public bool CancelButtonVisible
	{
		get { return Get( () => CancelButtonVisible, IsInDesignMode ); }
		set { Set( () => CancelButtonVisible, value ); }
	}


	public ICommand Cancel => Commands[ nameof( Cancel ) ];

	public void Execute_Cancel()
	{
		CloseButtonVisible = true;
		OTrips             = [];
		ScanningCage       = true;
	}

	public bool CompleteButtonVisible
	{
		get { return Get( () => CompleteButtonVisible, IsInDesignMode ); }
		set { Set( () => CompleteButtonVisible, value ); }
	}


	public ICommand Complete => Commands[ nameof( Complete ) ];

	public void Execute_Complete()
	{
		var Trps = OTrips;

		var ClaimTrips = new ClaimTrips
		                 {
			                 ProgramName = $"Kiosk Claim - {CurrentVersion.APP_VERSION}",
			                 UserName    = Users.UserName,
			                 Cage        = Cage,
			                 TripIds = ( from T in Trps
			                             select T.TripId ).ToList()
		                 };

		UpdateFromObject( new PmlClaimTrips { Trips = ClaimTrips } );

		Execute_Cancel();
	}
#endregion
}