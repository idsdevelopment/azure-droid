﻿#nullable enable

namespace DroidAz.Views._Customers.Pml.Kiosk;

[XamlCompilation( XamlCompilationOptions.Compile )]
public partial class Verify : ContentView
{
	public Verify()
	{
		InitializeComponent();

		if( BindingContext is VerifyViewModel Model )
		{
			void ShowError( string errorIId )
			{
				Model.OnItemAlert = () =>
									{
										Sounds.PlayAlert1Sound();
									};

				ViewModelBase.Dispatcher( async () =>
										  {
											  Sounds.PlayBadScanSound.Invoke();

											  await MainPage.Instance.DisplayAlert( Globals.Resources.GetStringResource( "Error" ),
																				    Globals.Resources.GetStringResource( errorIId ),
																				    Globals.Resources.GetStringResource( "Cancel" ) );

											  Model.Scanning = true;
										  } );
			}

			Model.OnInvalidAudrNumber = () =>
										{
											ShowError( "PmlNotSatchel" );
										};

			Model.OnNoSatchelsFound = () =>
									  {
										  ShowError( "PmlNoSatchel" );
									  };

			Model.OnItemNotFound = () =>
								   {
									   ShowError( "PmlShipmentNotFound" );
								   };

			Model.OnResetScan = async _ =>
								{
									var Ok = Globals.Resources.GetStringResource( "Ok" );

									var Result = await MainPage.Instance.DisplayActionSheet( Globals.Resources.GetStringResource( "PmlResetScan" ),
																						     Globals.Resources.GetStringResource( "Cancel" ),
																						     Globals.Resources.GetStringResource( Ok ) );

									return Result == Ok;
								};

			Model.OnVerifyOk = async () =>
							   {
								   await MainPage.Instance.DisplayActionSheet( Globals.Resources.GetStringResource( "PmlVerifyOk" ),
																		       null,
																		       Globals.Resources.GetStringResource( "Ok" ) );
							   };

			Model.OnScanningChange = scanning =>
									 {
										 if( scanning )
											 Scanner.Focus();
									 };

			Model.Scanning = true;
		}
	}
}