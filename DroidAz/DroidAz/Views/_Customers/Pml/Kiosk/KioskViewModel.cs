﻿#nullable enable

namespace DroidAz.Views._Customers.Pml.Kiosk;

public class KioskViewModel : ViewModelBase
{
	protected override void OnInitialised()
	{
		base.OnInitialised();
		Globals.Orientation.LayoutDirection = Orientation;
	}

#region Orientation
	[Setting]
	public Orientation.ORIENTATION Orientation
	{
		get { return Get( () => Orientation, Globals.Orientation.ORIENTATION.LANDSCAPE ); }
		set { Set( () => Orientation, value ); }
	}

	[DependsUpon( nameof( Orientation ) )]
	public void WhenOrientationChanges()
	{
		SaveSettings();
		Globals.Orientation.LayoutDirection = Orientation;
	}
#endregion

	public Action? RunVerify,
	               RunClaim;

#region Verify
	public ICommand Verify => Commands[ nameof( Verify ) ];

	public void Execute_Verify()
	{
		RunVerify?.Invoke();
	}
#endregion

#region Claim
	public ICommand Claim => Commands[ nameof( Claim ) ];

	public void Execute_Claim()
	{
		RunClaim?.Invoke();
	}
#endregion

#region Exit
	public ICommand Exit => Commands[ nameof( Exit ) ];

	public void Execute_Exit()
	{
		MainPageViewModel.Instance?.Execute_SignOut();
	}
#endregion
}