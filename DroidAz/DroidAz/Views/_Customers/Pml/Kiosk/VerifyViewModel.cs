﻿#nullable enable

using System.ComponentModel;
using DroidAz.Modifications;

namespace DroidAz.Views._Customers.Pml.Kiosk;

public class KioskTripItemsDisplayEntry : TripItemsDisplayEntry, INotifyPropertyChanged
{
	public override decimal Balance
	{
		get => base.Balance;
		set
		{
			base.Balance = value;
			OnPropertyChanged();
			OverScanned = value < 0;
			IsVisible   = value != 0;
		}
	}

	public override decimal Scanned
	{
		get => base.Scanned;
		set
		{
			base.Scanned = value;
			OnPropertyChanged();
		}
	}

	public override bool OverScanned
	{
		get => base.OverScanned;
		set
		{
			base.OverScanned = value;
			OnPropertyChanged();
		}
	}

	public override bool IsVisible
	{
		get => base.IsVisible;
		set
		{
			base.IsVisible = value;
			OnPropertyChanged();
		}
	}

	public KioskTripItemsDisplayEntry( TripItemsDisplayEntry items ) : base( items )
	{
	}
}

public class VerifyViewModel : TripsBaseViewModel
{
	private const string PML_SATCHEL_BARCODE = "PmlSatchelBarcode",
						 BARCODE             = "Barcode";

	public string Satchel
	{
		get { return Get( () => Satchel, "" ); }
		set { Set( () => Satchel, value ); }
	}

	protected override void OnInitialised()
	{
		base.OnInitialised();
		CancelButtonVisible   = false;
		CompleteButtonVisible = false;
		CloseButtonVisible    = true;
		Scanning              = true;
	}

	public Action OnInvalidAudrNumber = null!,
				  OnNoSatchelsFound   = null!,
				  OnItemAlert         = null!;

	public Func<KioskTripItemsDisplayEntry, Task<bool>>? OnResetScan;

	public Action<bool>? OnScanningChange;
	public Func<Task>?   OnVerifyOk;

#region Barcode
	public Action? OnItemNotFound;

	public string Barcode
	{
		get { return Get( () => Barcode, "" ); }
		set { Set( () => Barcode, value ); }
	}

	[DependsUpon10( nameof( Barcode ) )]
	public async void WhenBarcodeChanges()
	{
		var Code = Barcode;
		Barcode = "";

		try
		{
			if( Code.IsNotNullOrWhiteSpace() )
			{
				if( ScanningItems )
				{
					var Found = false;

					foreach( var I in Items )
					{
						if( I.Barcode == Code )
						{
							I.Scanned++;
							I.Balance--;
							Found = true;
							break;
						}
					}

					if( !Found )
					{
						OnItemNotFound?.Invoke();
						return;
					}

					foreach( var I in Items )
					{
						if( I.Balance != 0 )
						{
							CompleteButtonVisible = false;
							return;
						}
					}
					CompleteButtonVisible = true;
				}
				else if( !Code.StartsWith( "AUDR" ) )
					OnInvalidAudrNumber();
				else
				{
					var Satchels = await Azure.Client.RequestGetTripsStartingWith( new SearchTripsByStatus
																			       {
																				       StartStatus = STATUS.PICKED_UP,
																				       EndStatus   = STATUS.PICKED_UP,
																				       TripId      = Code
																			       } );

					if( Satchels is {Count: > 0} )
					{
						var Trp = Satchels[ 0 ];

						if( Trp.PackageType.Compare( "SATCHEL", StringComparison.OrdinalIgnoreCase ) != 0 )
							OnNoSatchelsFound();
						else
						{
							Satchel = Trp.TripId;

							var T = new TripDisplayEntry( Trp, ServiceLevelColours );

							Items = ( from I in T.Items
									  where I.Pieces > 0
									  select new KioskTripItemsDisplayEntry( I ) ).ToList();

							Trip = T;

							CancelButtonVisible = true;
							ScanningItems       = true;
						}
					}
					else
						OnNoSatchelsFound();
				}
			}
		}
		finally
		{
			Scanning = true;
		}
	}
#endregion

#region BarcodeScaner
	public string PlaceholderText
	{
		get { return Get( () => PlaceholderText, "" ); }
		set { Set( () => PlaceholderText, value ); }
	}

	public bool Scanning
	{
		get { return Get( () => Scanning, false ); }
		set { Set( () => Scanning, value ); }
	}

	[DependsUpon100( nameof( Scanning ) )]
	public void WhenScanningChanges()
	{
		OnScanningChange?.Invoke( Scanning );
	}
#endregion

#region ListView
	public TripDisplayEntry Trip
	{
		get { return Get( () => Trip, new TripDisplayEntry() ); }
		set { Set( () => Trip, value ); }
	}

	public KioskTripItemsDisplayEntry? SelectedItem
	{
		get { return Get( () => SelectedItem, (KioskTripItemsDisplayEntry?)null ); }
		set { Set( () => SelectedItem, value ); }
	}

	public List<KioskTripItemsDisplayEntry> Items
	{
		get { return Get( () => Items, [] ); }
		set { Set( () => Items, value ); }
	}

	[DependsUpon( nameof( SelectedItem ) )]
	public async void WhenSelectedItemChanges()
	{
		var Item = SelectedItem;

		if( Item is {OverScanned: true} )
		{
			if( OnResetScan is not null )
			{
				if( await OnResetScan( Item ) )
				{
					Item.Balance = Item.Pieces;
					Item.Scanned = 0;
				}
				Scanning = false;
				Scanning = true;
			}
			SelectedItem = null;
		}
	}
#endregion

#region Buttons
	public ICommand Close => Commands[ nameof( Close ) ];

	public void Execute_Close()
	{
		OnClose?.Invoke();
	}

	public ICommand Cancel => Commands[ nameof( Cancel ) ];

	public void Execute_Cancel()
	{
		CompleteButtonVisible = false;
		CancelButtonVisible   = false;
		SelectedItem          = null;
		ScanningItems         = false;
		Scanning              = true;
	}

	public bool CancelButtonVisible
	{
		get { return Get( () => CancelButtonVisible, IsInDesignMode ); }
		set { Set( () => CancelButtonVisible, value ); }
	}

	public bool CloseButtonVisible
	{
		get { return Get( () => CloseButtonVisible, IsInDesignMode ); }
		set { Set( () => CloseButtonVisible, value ); }
	}

	[DependsUpon100( nameof( CancelButtonVisible ) )]
	[DependsUpon100( nameof( CompleteButtonVisible ) )]
	public void EnableClose()
	{
		CloseButtonVisible = !CancelButtonVisible && !CompleteButtonVisible;
	}

	[InvokeChangeOnInitialise]
	public bool ScanningItems
	{
		get { return Get( () => ScanningItems, false ); }
		set { Set( () => ScanningItems, value ); }
	}

	public bool ScanningSatchel
	{
		get { return Get( () => ScanningSatchel, true ); }
		set { Set( () => ScanningSatchel, value ); }
	}

	[DependsUpon( nameof( ScanningItems ) )]
	public void WhenScanningItemsChanges()
	{
		var Si = ScanningItems;

		CancelButtonVisible = Si;
		ScanningSatchel     = !Si;
		PlaceholderText     = Globals.Resources.GetStringResource( !Si ? PML_SATCHEL_BARCODE : BARCODE );
	}

	[DependsUpon( nameof( CancelButtonVisible ) )]
	public void WhenCancelButtonVisibleChanges()
	{
		CloseButtonVisible = !CancelButtonVisible;
	}

	public bool CompleteButtonVisible
	{
		get { return Get( () => CompleteButtonVisible, IsInDesignMode ); }
		set { Set( () => CompleteButtonVisible, value ); }
	}

	public ICommand Complete => Commands[ nameof( Complete ) ];

	public async void Execute_Complete()
	{
		var Point = Globals.Gps.CurrentGpsPoint;

		UpdateFromObject( new PmlVerifySatchel
						  {
							  ProgramName = $"Kiosk Verify - {CurrentVersion.APP_VERSION}",
							  TripId      = Trip.TripId,
							  UserName    = Users.UserName,

							  DateTime  = DateTimeOffset.Now,
							  Latitude  = Point.Latitude,
							  Longitude = Point.Longitude
						  } );

		if( OnVerifyOk is not null )
			await OnVerifyOk();

		Execute_Cancel();
	}
#endregion
}