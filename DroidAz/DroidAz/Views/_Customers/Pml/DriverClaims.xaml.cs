﻿using Signature = DroidAz.Controls.Default.Signature;

namespace DroidAz.Views._Customers.Pml;

[XamlCompilation( XamlCompilationOptions.Compile )]
public partial class DriverClaims : ContentView
{
	public DriverClaims()
	{
		InitializeComponent();

		var Ok          = Globals.Resources.GetStringResource( "Ok" );
		var Cancel      = Globals.Resources.GetStringResource( "Cancel" );
		var CancelClaim = Globals.Resources.GetStringResource( "PmlCancelClaim" );
		var NoASatchel  = Globals.Resources.GetStringResource( "PmlNotSatchel" );
		var Satchel     = Globals.Resources.GetStringResource( "PmlSatchel" );

		var Error          = Globals.Resources.GetStringResource( "Error" );
		var AlreadyScanned = Globals.Resources.GetStringResource( "PmlAlreadyScanned" );

		if( BindingContext is DriverClaimsViewModel Model )
		{
			Model.OnCancel = async () =>
			                 {
				                 var Result = await MainPage.Instance.DisplayActionSheet( CancelClaim, Cancel, Ok );

				                 return Result == Ok;
			                 };

			Model.OnNotASatchel = () =>
			                      {
				                      ViewModelBase.Dispatcher( async () =>
				                                                {
					                                                Sounds.PlayBadScanSound();
					                                                await MainPage.Instance.DisplayAlert( Error, NoASatchel, Cancel );
				                                                } );
			                      };

			Model.OnAlreadyScanned = satchel =>
			                         {
				                         ViewModelBase.Dispatcher( async () =>
				                                                   {
					                                                   Sounds.PlayBadScanSound();
					                                                   await MainPage.Instance.DisplayAlert( Error, $"{Satchel}: {satchel}\r\n{AlreadyScanned}", Cancel );
				                                                   } );
			                         };

			var Sig = new Signature();
			SignatureView.Content = Sig;

			Model.ClearSignature = () =>
			                       {
				                       Sig.Clear();
			                       };

			Sig.OnCancel = () =>
			               {
				               Model.OnSignatureCancel();
			               };

			Sig.OnOk = signature =>
			           {
				           Model.OnSignatureOk( signature );
			           };
		}
	}
}