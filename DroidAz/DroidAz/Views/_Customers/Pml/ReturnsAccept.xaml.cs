﻿#nullable enable

using Keyboard = Globals.Keyboard;

namespace DroidAz.Views._Customers.Pml;

[XamlCompilation( XamlCompilationOptions.Compile )]
public partial class ReturnsAccept : ContentView
{
	public int TotalScanned
	{
		set => Model.TotalScanned = value;
		get => Model.TotalScanned;
	}

	public ObservableCollection<PmlTripItemsDisplayEntry> Items
	{
		set
		{
			foreach( var Entry in value )
				Entry.DontHide = true;

			Model.Items = value;
		}
	}

	private readonly ReturnsAcceptViewModel Model;

	public Action? OnBack;

	public Action? OnFutile,
	               OnAccept;

	public ReturnsAccept()
	{
		InitializeComponent();

		var M = (ReturnsAcceptViewModel)Root.BindingContext;
		Model = M;

		M.OnFutile = () => { OnFutile?.Invoke(); };

		M.OnAccept = () => { OnAccept?.Invoke(); };

		M.OnBack = () => { OnBack?.Invoke(); };

		M.OnResetItem = async () => await MainPage.Instance.DisplayAlert( Globals.Resources.GetStringResource( "Confirm" ),
		                                                                  Globals.Resources.GetStringResource( "PmlResetScan" ),
		                                                                  Globals.Resources.GetStringResource( "Ok" ),
		                                                                  Globals.Resources.GetStringResource( "Cancel" ) );
	}

#region Double Tap
	// Could not get Gesture Recogniser to work,
	private void Cell_OnTapped( object sender, EventArgs e )
	{
		Keyboard.DoubleTap( () => { Model.Execute_ResetItem(); } );
	}
#endregion
}