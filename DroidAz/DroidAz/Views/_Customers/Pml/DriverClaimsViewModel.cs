﻿#nullable enable

using DroidAz.Controls.Default;
using DroidAz.Modifications;
using Protocol.Data._Customers.Pml;
using Signature = Protocol.Data.Signature;

namespace DroidAz.Views._Customers.Pml;

public class DriverClaimsViewModel : TripsBaseViewModel
{
#region ListView
	public ObservableCollection<TripDisplayEntry> Satchels
	{
		get { return Get( () => Satchels, [] ); }
		set { Set( () => Satchels, value ); }
	}
#endregion

	private string ScanCage       = "",
	               ScanSatchels   = "",
	               CageBarcode    = "",
	               SatchelBarcode = "";

	public Action<string>? OnAlreadyScanned;

	public Func<Task<bool>>? OnCancel;
	public Action?           OnNotASatchel;

	protected override void OnInitialised()
	{
		base.OnInitialised();

		ScanCage       = Globals.Resources.GetStringResource( "PmlScanCage" );
		ScanSatchels   = Globals.Resources.GetStringResource( "PmlScanSatchels" );
		CageBarcode    = Globals.Resources.GetStringResource( "PmlCageBarcode" );
		SatchelBarcode = Globals.Resources.GetStringResource( "PmlSatchelBarcode" );

		Satchels = [];

		ScanningCage = true;
		Scanning     = true;
		IsEnabled    = true;
	}

#region PopAndSign
	public Action? ClearSignature;

	private Orientation.ORIENTATION SaveOrientation;

	public string Pop
	{
		get { return Get( () => Pop, "" ); }
		set { Set( () => Pop, value ); }
	}

	[DependsUpon( nameof( Signature ) )]
	[DependsUpon250( nameof( Pop ) )]
	public void WhenPopChanges()
	{
		CompleteEnable = Pop.IsNotNullOrWhiteSpace() && Signature is not null;
	}


	public bool PopAndSign
	{
		get { return Get( () => PopAndSign, false ); }
		set { Set( () => PopAndSign, value ); }
	}

	public bool NotPopAndSign
	{
		get { return Get( () => NotPopAndSign, true ); }
		set { Set( () => NotPopAndSign, value ); }
	}

	[DependsUpon( nameof( PopAndSign ) )]
	public void WhenPopAndSignChanges()
	{
		NotPopAndSign = !PopAndSign;
	}

	public bool CompleteEnable
	{
		get { return Get( () => CompleteEnable, false ); }
		set { Set( () => CompleteEnable, value ); }
	}

	public ICommand Complete => Commands[ nameof( Complete ) ];

	public void Execute_Complete()
	{
		var Now = DateTimeOffset.Now;
		var Gps = Globals.Gps.CurrentGpsPoint;
		var Sig = Signature ?? new SignatureResult();

		var ClaimSatchels = new ClaimSatchels
		                    {
			                    ProgramName = $"Driver Claim Satchels - {CurrentVersion.APP_VERSION}",
			                    UserName    = Users.UserName,
			                    ClaimTime   = Now,
			                    Latitude    = Gps.Latitude,
			                    Longitude   = Gps.Longitude,
			                    Cage        = Cage,
			                    POP         = Pop,
			                    Signature = new Signature
			                                {
				                                Date    = Now,
				                                Height  = Sig.Height,
				                                Width   = Sig.Width,
				                                Points  = Sig.Points,
				                                Status  = STATUS.PICKED_UP,
				                                Status1 = STATUS1.CLAIMED
			                                },

			                    SatchelIds = ( from S in Satchels
			                                   select S.TripId ).ToList()
		                    };

		UpdateFromObject( new PmlClaimSatchel { Satchels = ClaimSatchels } );

		ScanningCage = true;
	}


	public ICommand Back => Commands[ nameof( Back ) ];

	public void Execute_Back()
	{
		PopAndSign = false;
		Scanning   = true;
	}

	public SignatureResult? Signature
	{
		get { return Get( () => Signature, (SignatureResult?)null ); }
		set { Set( () => Signature, value ); }
	}


	public bool SignatureNotVisible
	{
		get { return Get( () => SignatureNotVisible, true ); }
		set { Set( () => SignatureNotVisible, value ); }
	}

	public bool SignatureVisible
	{
		get { return Get( () => SignatureVisible, false ); }
		set { Set( () => SignatureVisible, value ); }
	}

	[DependsUpon( nameof( SignatureVisible ) )]
	public void WhenSignatureVisibleChanges()
	{
		SignatureNotVisible = !SignatureVisible;
	}


	public ICommand Sign => Commands[ nameof( Sign ) ];

	public void Execute_Sign()
	{
		SaveOrientation             = Orientation.LayoutDirection;
		Orientation.LayoutDirection = Orientation.ORIENTATION.LANDSCAPE;
		ClearSignature?.Invoke();
		SignatureVisible = true;
	}


	public void OnSignatureOk( SignatureResult? signature )
	{
		SignatureVisible            = false;
		Orientation.LayoutDirection = SaveOrientation;
		Signature                   = signature;
	}

	public void OnSignatureCancel()
	{
		OnSignatureOk( null );
	}
#endregion

#region Cage
	public bool ShowCage
	{
		get { return Get( () => ShowCage, IsInDesignMode ); }
		set { Set( () => ShowCage, value ); }
	}

	public string Cage
	{
		get { return Get( () => Cage, "" ); }
		set { Set( () => Cage, value ); }
	}


	public bool ScanningCage
	{
		get { return Get( () => ScanningCage, false ); }
		set { Set( () => ScanningCage, value ); }
	}

	[DependsUpon( nameof( ScanningCage ) )]
	public void WhenScanningCageChanges()
	{
		if( ScanningCage )
		{
			Caption             = ScanCage;
			CancelButtonVisible = false;
			NextButtonVisible   = false;
			PopAndSign          = false;
			PlaceholderText     = CageBarcode;
			ShowCage            = false;
			Satchels            = [];
		}
		else
		{
			Caption             = ScanSatchels;
			CancelButtonVisible = true;
			PlaceholderText     = SatchelBarcode;
			ShowCage            = true;
		}
	}
#endregion

#region Scanner
	public bool IsEnabled
	{
		get { return Get( () => IsEnabled, false ); }
		set { Set( () => IsEnabled, value ); }
	}


	public string Caption
	{
		get { return Get( () => Caption, "Caption" ); }
		set { Set( () => Caption, value ); }
	}


	public string PlaceholderText
	{
		get { return Get( () => PlaceholderText, "" ); }
		set { Set( () => PlaceholderText, value ); }
	}


	public string Barcode
	{
		get { return Get( () => Barcode, "" ); }
		set { Set( () => Barcode, value ); }
	}


	[DependsUpon( nameof( Barcode ) )]
	public void WhenBarcodeChanges()
	{
		var BCode = Barcode.Trim();

		Globals.Keyboard.Show = false;

		if ( BCode.IsNotNullOrWhiteSpace() )
		{
			if( ScanningCage )
			{
				Cage         = BCode;
				ScanningCage = false;
			}
			else
			{
				if( !BCode.StartsWith( "AUDR" ) )
					OnNotASatchel?.Invoke();
				else
				{
					if( Satchels.Any( s => s.TripId == BCode ) )
					{
						OnAlreadyScanned?.Invoke( BCode );
						return;
					}

					Task.Run( async () =>
					          {
						          var Satchel = await Azure.Client.RequestGetTrip( new GetTrip
						                                                           {
							                                                           Signatures = false,
							                                                           TripId     = BCode
						                                                           } );
						          var Sc = ServiceLevelColours;

						          Dispatcher( () =>
						                      {
							                      if( Satchel is not null && Satchel.Ok )
							                      {
								                      Satchels.Add( new TripDisplayEntry( Satchel, Sc ) );
								                      RaisePropertyChangedDelayed( nameof( Satchels ) );
								                      NextButtonVisible = true;
							                      }
							                      else
								                      OnNotASatchel?.Invoke();
						                      } );
					          } );
				}
			}
		}
	}

	public bool Scanning
	{
		get { return Get( () => Scanning, false ); }
		set { Set( () => Scanning, value ); }
	}
#endregion


#region Buttons
	public bool CancelButtonVisible
	{
		get { return Get( () => CancelButtonVisible, IsInDesignMode ); }
		set { Set( () => CancelButtonVisible, value ); }
	}

	public ICommand Cancel => Commands[ nameof( Cancel ) ];

	public async void Execute_Cancel()
	{
		if( OnCancel is not null && await OnCancel() )
			ScanningCage = true;
	}


	public bool NextButtonVisible
	{
		get { return Get( () => NextButtonVisible, IsInDesignMode ); }
		set { Set( () => NextButtonVisible, value ); }
	}


	public ICommand Next => Commands[ nameof( Next ) ];

	public void Execute_Next()
	{
		PopAndSign       = true;
		SignatureVisible = false;
	}
#endregion
}