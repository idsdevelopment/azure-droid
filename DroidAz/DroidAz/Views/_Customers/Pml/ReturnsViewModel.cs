﻿#nullable enable

using Protocol.Data._Customers.Pml;

namespace DroidAz.Views._Customers.Pml;

public class ReturnsViewModel : TripsBaseViewModel
{
	private bool   Initialised;
	public  Action MixedSystemError = null!;


	public ShowDelegate Show = null!;

	public ReturnsViewModel() : base( false )
	{
	}

	public enum SHOW_STATE
	{
		NONE,
		LOCATION,
		ITEMS,
		ACCEPT,
		FUTILE,
		SIGN,
		SATCHEL,
		DETAILS
	}

	protected override void OnInitialised()
	{
		base.OnInitialised();
		Initialised = true;
	}

#region Pickup Complete
	public void PickupComplete( List<string> tripIds, Protocol.Data._Customers.Pml.Satchel satchel )
	{
		DoShow( SHOW_STATE.LOCATION, true );
		UpdateFromObject( new SatchelUpdate { Satchel = satchel } );
		RemoveTrips( tripIds );
	}
#endregion

	public delegate Task ShowDelegate( SHOW_STATE state, bool resetScan );

#region Futile
	private ObservableCollection<TripDisplayEntry>? FutileItems;

	public void ProcessFutile( string reason, bool delete )
	{
		if( FutileItems is { Count: > 0 } Items )
		{
			var Status = delete ? STATUS.DELETED : STATUS.ACTIVE;

			var TripIds = ( from E in Items
			                select E.TripId ).ToList();

			UpdateFromObject( new FutileUpdate
			                  {
				                  Futile = new Futile
				                           {
					                           ProgramName = CurrentVersion.APP_VERSION,
					                           TripIds     = TripIds,
					                           Reason      = reason,
					                           Status      = Status,
					                           DateTime    = DateTimeOffset.Now
				                           }
			                  } );

			foreach( var TripId in TripIds )
				RemoveTrip( TripId );
		}
		FutileItems = [];
		DoShow( SHOW_STATE.LOCATION, true );
	}
#endregion


#region Location Items
	public ICommand ItemTapped => Commands[ nameof( ItemTapped ) ];

	public void Execute_ItemTapped()
	{
		var Selected = SelectedDisplayEntry;

		if( Selected is not null )
		{
			SelectedDisplayEntry = null;
			// Details view later
		}
	}

	public ObservableCollection<TripDisplayEntry>? LocationItems
	{
		get { return Get( () => LocationItems, [] ); }
		set { Set( () => LocationItems, value ); }
	}


	[DependsUpon( nameof( LocationItems ) )]
	public void WhenLocationItemsChange()
	{
		if( Initialised && LocationItems is { Count: > 0 } Items )
		{
			SHOW_STATE NextShow;

			if( Common.CheckMixedMode( Items, MixedSystemError ) == Common.MIXED_MODE_RESULT.ERROR )
				NextShow = SHOW_STATE.LOCATION;
			else
			{
				FutileItems = Items;
				NextShow    = SHOW_STATE.ITEMS;
			}
			DoShow( NextShow, true );
		}
	}

	public async void DoShow( SHOW_STATE show, bool resetItemCount )
	{
		ShowTripCount = show switch
		                {
			                SHOW_STATE.LOCATION => true,
			                SHOW_STATE.ITEMS    => false,
			                _                   => ShowTripCount
		                };

		await Show( show, resetItemCount );
	}
#endregion

#region Trips
	public TripDisplayEntry? SelectedDisplayEntry
	{
		get { return Get( () => SelectedDisplayEntry, new TripDisplayEntry() ); }
		set { Set( () => SelectedDisplayEntry, value ); }
	}


	public List<string> TripIdFilter
	{
		get { return Get( () => TripIdFilter, [] ); }
		set { Set( () => TripIdFilter, value ); }
	}


	public new ObservableCollection<TripDisplayEntry>? TripDisplayEntries
	{
		get { return Get( () => TripDisplayEntries, (ObservableCollection<TripDisplayEntry>?)null ); }
		set { Set( () => TripDisplayEntries, value ); }
	}

	// Remove Satchels from the display
	[DependsUpon( nameof( TripDisplayEntries ) )]
	public void WhenTripDisplayEntriesChange()
	{
		TripIdFilter = ( from E in TripDisplayEntries
		                 select E.TripId ).ToList();

		if( Initialised && ( Common.CheckMixedMode( TripDisplayEntries, MixedSystemError ) == Common.MIXED_MODE_RESULT.ERROR ) )
			Show( SHOW_STATE.LOCATION, true );
	}
}
#endregion