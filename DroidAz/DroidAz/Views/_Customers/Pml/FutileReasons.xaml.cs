﻿#nullable enable

namespace DroidAz.Views._Customers.Pml
{
	[XamlCompilation( XamlCompilationOptions.Compile )]
	public partial class FutileReasons : ContentView
	{
		public Action                         OnCancel = null!;
		public FutileReasonsViewModel.OkEvent OnOk     = null!;

		public FutileReasons()
		{
			InitializeComponent();

			if( Root.BindingContext is FutileReasonsViewModel M )
			{
				M.OnOk = ( reason, delete ) =>
				         {
					         OnOk( reason, delete );
				         };

				M.OnCancel = () => { OnCancel(); };
			}
		}
	}
}