﻿#nullable enable

using DroidAz.Dialogs;

namespace DroidAz.Views._Customers.Pml;

[XamlCompilation( XamlCompilationOptions.Compile )]
public partial class Satchel : ContentView
{
	public string SatchelId
	{
		get => Model.Satchel;
		set => Model.Satchel = value;
	}

	private readonly SatchelViewModel Model;

	public SatchelViewModel.OnAcceptEvent? OnAccept;
	public Action?                         OnCancel;

	public void Cancel()
	{
		Scanner.Scanning  = false;
		Scanner.IsEnabled = false;
	}

	public void StartScanning()
	{
		Scanner.Scanning = true;
	}

	public Satchel()
	{
		InitializeComponent();

		var M = (SatchelViewModel)Root.BindingContext;
		Model = M;

		M.OnBadSatchel = () =>
		                 {
			                 ViewModelBase.Dispatcher( async () =>
			                                           {
				                                           Sounds.PlayBadScanSound?.Invoke();
				                                           await ErrorDialog.Execute( "PmlNotSatchel" );
				                                           Scanner.Scanning = true;
			                                           } );
		                 };

		M.OnAccept = satchel => { OnAccept?.Invoke( satchel ); };

		M.OnCancel = () => { OnCancel?.Invoke(); };
	}
}