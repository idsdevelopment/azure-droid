﻿#nullable enable

using Keyboard = Globals.Keyboard;

// ReSharper disable PrivateFieldCanBeConvertedToLocalVariable

namespace DroidAz.Views._Customers.Pml;

[XamlCompilation( XamlCompilationOptions.Compile )]
public partial class Returns : ContentView
{
	private readonly ReturnDetails    DetailsPage;
	private readonly FutileReasons    FutileReasonsPage;
	private readonly ReturnsViewModel Model;
	private readonly ReturnItems      ReturnItemsPage;
	private readonly ReturnsAccept    ReturnsAcceptPage;
	private readonly PodAndSign       ReturnsSignPage;
	private readonly Satchel          SatchelPage;

	public Returns()
	{
		InitializeComponent();

		var M = (ReturnsViewModel)Root.BindingContext;
		Model = M;

		Keyboard.Show = false;

		ReturnsViewModel.SHOW_STATE CurrentShowState  = ReturnsViewModel.SHOW_STATE.NONE,
		                            PreviousShowState = ReturnsViewModel.SHOW_STATE.NONE;

		RaiseChild( Location );

	#region Page Init
		ReturnsSignPage   = (PodAndSign)SignArea.Content;
		ReturnItemsPage   = (ReturnItems)ItemsArea.Content;
		ReturnsAcceptPage = (ReturnsAccept)AcceptArea.Content;
		FutileReasonsPage = (FutileReasons)FutileArea.Content;
		DetailsPage       = (ReturnDetails)DetailsArea.Content;
		SatchelPage       = (Satchel)SatchelArea.Content;
	#endregion

	#region Pod and Sign
		ReturnsSignPage.OnBack = async () =>
		                         {
			                         await Model.Show( ReturnsViewModel.SHOW_STATE.SATCHEL, false );
		                         };

		ReturnsSignPage.OnPickupComplete = ( tripIds, satchel ) =>
		                                   {
			                                   Model.PickupComplete( tripIds, satchel );
		                                   };
	#endregion

	#region Return Items
		ReturnItemsPage.OnFutile = async () =>
		                           {
			                           await Model.Show( ReturnsViewModel.SHOW_STATE.FUTILE, false );
		                           };
		ReturnItemsPage.OnBackButton = OnBackButton;

		ReturnItemsPage.OnNextButton = async ( scanned, items ) =>
		                               {
			                               ReturnsAcceptPage.TotalScanned = scanned;
			                               ReturnsAcceptPage.Items        = items;
			                               ReturnsSignPage.Items          = items;
			                               await Model.Show( ReturnsViewModel.SHOW_STATE.ACCEPT, false );
		                               };
	#endregion

	#region Accept
		ReturnsAcceptPage.OnFutile = async () =>
		                             {
			                             await Model.Show( ReturnsViewModel.SHOW_STATE.FUTILE, false );
		                             };

		ReturnsAcceptPage.OnAccept = async () =>
		                             {
			                             ReturnsSignPage.Trip         = Model.SelectedDisplayEntry;
			                             ReturnsSignPage.TotalScanned = ReturnsAcceptPage.TotalScanned;
			                             await Model.Show( ReturnsViewModel.SHOW_STATE.SATCHEL, false );
		                             };

		ReturnsAcceptPage.OnBack = async () =>
		                           {
			                           await Model.Show( ReturnsViewModel.SHOW_STATE.ITEMS, false );
		                           };
	#endregion

	#region Futile
		FutileReasonsPage.OnCancel = () =>
		                             {
			                             Model.Show( PreviousShowState, false );
		                             };

		FutileReasonsPage.OnOk = ( reason, delete ) =>
		                         {
			                         Model.ProcessFutile( reason, delete );
		                         };
	#endregion

	#region Details
		var DetailsViewModel = (ReturnDetailsViewModel)DetailsPage.BindingContext;

		DetailsViewModel.OnPickupTrip = async _ =>
		                                {
			                                await Model.Show( ReturnsViewModel.SHOW_STATE.ITEMS, false );
		                                };
	#endregion

	#region Satchel
		SatchelPage.OnCancel = () =>
		                       {
			                       ReturnItems.CancelReturnDialog( async () =>
			                                                       {
				                                                       await Model.Show( ReturnsViewModel.SHOW_STATE.LOCATION, false );
			                                                       } );
		                       };

		SatchelPage.OnAccept = async satchel =>
		                       {
			                       ReturnsSignPage.Satchel = satchel;
			                       await Model.Show( ReturnsViewModel.SHOW_STATE.SIGN, false );
		                       };
	#endregion

		M.MixedSystemError = Common.ShowMixedModeError;

		M.Show = async ( showState, resetScan ) =>
		         {
			         if( showState != CurrentShowState )
			         {
				         PreviousShowState = CurrentShowState;
				         CurrentShowState  = showState;

				         bool Loc     = false,
				              Items   = false,
				              Accept  = false,
				              Futile  = false,
				              Sign    = false,
				              Satchel = false,
				              Details = false;

				         switch( PreviousShowState )
				         {
				         case ReturnsViewModel.SHOW_STATE.LOCATION:
					         Location.Cancel();
					         break;

				         case ReturnsViewModel.SHOW_STATE.ITEMS:
					         ReturnItemsPage.Cancel();
					         break;

				         case ReturnsViewModel.SHOW_STATE.SATCHEL:
					         SatchelPage.Cancel();
					         break;
				         }

				         switch( showState )
				         {
				         case ReturnsViewModel.SHOW_STATE.LOCATION:
					         Location.StartScanning();
					         break;

				         case ReturnsViewModel.SHOW_STATE.ITEMS:
					         ReturnItemsPage.StartScanning( resetScan );
					         break;

				         case ReturnsViewModel.SHOW_STATE.SATCHEL:
					         SatchelPage.StartScanning();
					         break;
				         }

				         switch( showState )
				         {
				         case ReturnsViewModel.SHOW_STATE.LOCATION:
					         Loc = true;
					         break;

				         case ReturnsViewModel.SHOW_STATE.ITEMS:
					         Items = true;
					         break;

				         case ReturnsViewModel.SHOW_STATE.ACCEPT:
					         Accept = true;
					         break;

				         case ReturnsViewModel.SHOW_STATE.FUTILE:
					         Futile = true;
					         break;

				         case ReturnsViewModel.SHOW_STATE.SIGN:
					         Sign = true;
					         break;

				         case ReturnsViewModel.SHOW_STATE.SATCHEL:
					         Satchel = true;
					         break;

				         case ReturnsViewModel.SHOW_STATE.DETAILS:
					         Details = true;
					         break;
				         }

				         await Task.WhenAll(
				                            this.RaiseAndFade( Location, Loc ),
				                            this.RaiseAndFade( ItemsArea, Items ),
				                            this.RaiseAndFade( AcceptArea, Accept ),
				                            this.RaiseAndFade( FutileArea, Futile ),
				                            this.RaiseAndFade( SignArea, Sign ),
				                            this.RaiseAndFade( SatchelArea, Satchel ),
				                            this.RaiseAndFade( DetailsArea, Details )
				                           );
			         }
		         };

		Task OnBackButton()
		{
			return Model.Show( ReturnsViewModel.SHOW_STATE.LOCATION, false );
		}

		DetailsViewModel.OnBackButton = OnBackButton;
		BackButton.OnBackKey          = OnBackButton;
		OnBackButton();
	}
}