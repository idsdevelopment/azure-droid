﻿#nullable enable

namespace DroidAz.Views._Customers.Pml;

[XamlCompilation( XamlCompilationOptions.Compile )]
public partial class ReturnItems : ContentView
{
	private readonly ReturnItemsViewModel Model;

	public Func<Task>? OnBackButton,
	                   OnFutile;


	public ReturnItemsViewModel.NextButtonEvent? OnNextButton = null;


	public ReturnItems()
	{
		InitializeComponent();

		var M = Model = (ReturnItemsViewModel)Root.BindingContext;

		M.OnNextButton = ( scanned, items ) =>
		                 {
			                 OnNextButton?.Invoke( scanned, items );
		                 };

		M.NotAuthorized = () =>
		                  {
			                  ViewModelBase.Dispatcher( async () =>
			                                            {
				                                            Sounds.PlayBadScanSound.Invoke();

				                                            await MainPage.Instance.DisplayAlert( Globals.Resources.GetStringResource( "Error" ),
				                                                                                  Globals.Resources.GetStringResource( "PmlNotAuthorized" ),
				                                                                                  Globals.Resources.GetStringResource( "Cancel" ) );
			                                            } );
		                  };

		M.OnBackButton = () =>
		                 {
			                 if( OnBackButton is not null )
			                 {
				                 ViewModelBase.Dispatcher( () =>
				                                           {
					                                           CancelReturnDialog( async () =>
					                                                               {
						                                                               M.Items = [];
						                                                               await OnBackButton();
					                                                               } );
				                                           } );
			                 }
		                 };

		M.OnFutile = () =>
		             {
			             OnFutile?.Invoke();
		             };
	}

	public void StartScanning( bool resetScan )
	{
		Model.StartScanning( resetScan );
	}

	public void Cancel()
	{
		Model.IsScanning            = false;
		Model.BarcodeScannerEnabled = false;
	}


	public static void CancelReturnDialog( Action onReturnCanceled )
	{
		ViewModelBase.Dispatcher( async () =>
		                          {
			                          var Ok = await MainPage.Instance.DisplayAlert( Globals.Resources.GetStringResource( "Confirm" ),
			                                                                         Globals.Resources.GetStringResource( "PmlCancelReturn" ),
			                                                                         Globals.Resources.GetStringResource( "Ok" ),
			                                                                         Globals.Resources.GetStringResource( "Cancel" ) );

			                          if( Ok )
				                          onReturnCanceled.Invoke();
		                          } );
	}


#region Location Items
	public static readonly BindableProperty LocationItemsProperty = BindableProperty.Create( nameof( LocationItems ),
	                                                                                         typeof( ObservableCollection<TripDisplayEntry> ),
	                                                                                         typeof( ReturnItems ),
	                                                                                         new ObservableCollection<TripDisplayEntry>(),
	                                                                                         propertyChanged: LocationItemsPropertyChanged );

	private static void LocationItemsPropertyChanged( BindableObject bindable, object oldValue, object newValue )
	{
		if( bindable is ReturnItems Rt && newValue is ObservableCollection<TripDisplayEntry> Items )
			Rt.Model.LocationItems = Items;
	}


	public ICollection<TripDisplayEntry>? LocationItems
	{
		get => (ICollection<TripDisplayEntry>?)GetValue( LocationItemsProperty );
		set => SetValue( LocationItemsProperty, value );
	}
#endregion
}