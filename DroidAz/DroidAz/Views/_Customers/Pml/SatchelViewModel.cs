﻿#nullable enable

namespace DroidAz.Views._Customers.Pml;

public class SatchelViewModel : ViewModelBase
{
#region Satchel
	public Action OnBadSatchel = null!;

	public delegate void OnAcceptEvent( string satchel );

	public OnAcceptEvent OnAccept = null!;


	public string Satchel
	{
		get { return Get( () => Satchel, "" ); }
		set { Set( () => Satchel, value ); }
	}

	[DependsUpon250( nameof( Satchel ) )]
	public void WhenSatchelChanges()
	{
		var S = Satchel.Trim();

		if( S.IsNotNullOrEmpty() )
		{
			var Enab = S.StartsWith( "AUDR" );

			if( !Enab )
				OnBadSatchel();
			else
				OnAccept( S );
		}
	}
#endregion

#region Cancel Button
	public Action   OnCancel = null!;
	public ICommand Cancel => Commands[ nameof( Cancel ) ];

	public void Execute_Cancel()
	{
		OnCancel();
	}
#endregion
}