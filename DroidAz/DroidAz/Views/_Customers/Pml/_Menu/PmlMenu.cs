﻿#nullable enable

namespace DroidAz.Views._Customers.Pml.Menu;

public static class PmlMenu
{
	private const string RETURNS = "PmlReturns",
	                     KIOSK   = "PmlKiosk",
	                     CLAIMS  = "PmlClaims";

	public static Dictionary<string, Applications.Program> Programs = new()
	                                                                  {
		                                                                  {
			                                                                  RETURNS,
			                                                                  new Applications.Program
			                                                                  {
				                                                                  PageType = typeof( Returns ),
				                                                                  Default  = true
			                                                                  }
		                                                                  },
		                                                                  {
			                                                                  CLAIMS,
			                                                                  new Applications.Program
			                                                                  {
				                                                                  PageType = typeof( DriverClaims )
			                                                                  }
		                                                                  },
		                                                                  {
			                                                                  KIOSK,
			                                                                  new Applications.Program
			                                                                  {
				                                                                  PageType = typeof( Kiosk.Kiosk )
			                                                                  }
		                                                                  }
	                                                                  };
}