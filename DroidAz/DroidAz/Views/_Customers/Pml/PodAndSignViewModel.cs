﻿#nullable enable

using DroidAz.Controls.Default;
using DroidAz.Modifications;
using DroidAz.Views.Common;
using Signature = Protocol.Data.Signature;

namespace DroidAz.Views._Customers.Pml;

public class PodAndSignViewModel : ViewModelBase
{
	private const string RETURN = "Return",
	                     UP     = "Up";

	public ObservableCollection<PmlTripItemsDisplayEntry> Items
	{
		get { return Get( () => Items, [] ); }
		set { Set( () => Items, value ); }
	}

	public int TotalScanned
	{
		get { return Get( () => TotalScanned, 0 ); }
		set { Set( () => TotalScanned, value ); }
	}


	public bool EnablePickup
	{
		get { return Get( () => EnablePickup, false ); }
		set { Set( () => EnablePickup, value ); }
	}

#region Satchel
	public string Satchel
	{
		get { return Get( () => Satchel, "" ); }
		set { Set( () => Satchel, value ); }
	}

	[DependsUpon( nameof( Satchel ) )]
	public void WhenSatchelChanges()
	{
		POP = "";
	}
#endregion

#region Trip
	public TripDisplayEntry Trip
	{
		get { return Get( () => Trip, new TripDisplayEntry() ); }
		set { Set( () => Trip, value ); }
	}

	[DependsUpon( nameof( Trip ) )]
	public void WhenTripChanges()
	{
		POP = "";
	}
#endregion


#region POP
	// ReSharper disable once InconsistentNaming
	public string POP
	{
		get { return Get( () => POP, "" ); }
		set { Set( () => POP, value ); }
	}

	[DependsUpon250( nameof( POP ) )]
	[DependsUpon250( nameof( Signature ) )]
	public void WhenPopOrSignatureChanges()
	{
		EnablePickup = PopPodAndSign.ValidateSignatureAndPop( Signature, POP );
	}
#endregion

#region Back Button
	public Action   OnBack = null!;
	public ICommand Back => Commands[ nameof( Back ) ];

	public void Execute_Back()
	{
		OnBack();
	}
#endregion

#region Sign Button
	public Action ClearSignature = null!;

	private Orientation.ORIENTATION SaveOrientation;

	public bool SignatureVisible
	{
		get { return Get( () => SignatureVisible, false ); }
		set { Set( () => SignatureVisible, value ); }
	}


	public bool DetailsVisible
	{
		get { return Get( () => DetailsVisible, true ); }
		set { Set( () => DetailsVisible, value ); }
	}

	public ICommand Sign => Commands[ nameof( Sign ) ];

	public void Execute_Sign()
	{
		SaveOrientation             = Orientation.LayoutDirection;
		Orientation.LayoutDirection = Orientation.ORIENTATION.LANDSCAPE;
		ClearSignature();
		SignatureVisible = true;
		DetailsVisible   = false;
	}

	public SignatureResult? Signature
	{
		get { return Get( () => Signature, (SignatureResult?)null ); }
		set { Set( () => Signature, value ); }
	}

	[DependsUpon( nameof( Signature ) )]
	public void WhenSignatureChanges()
	{
		Orientation.LayoutDirection = SaveOrientation;
		SignatureVisible            = false;
		DetailsVisible              = true;
		WhenPopOrSignatureChanges();
	}

	public void OnSignatureOk( SignatureResult? signature )
	{
		SignatureVisible            = false;
		DetailsVisible              = true;
		Orientation.LayoutDirection = SaveOrientation;
		Signature                   = signature;
	}

	public void OnSignatureCancel()
	{
		OnSignatureOk( null );
	}
#endregion

#region Pickup Button
	public Action<List<string>, Protocol.Data._Customers.Pml.Satchel> OnPickupComplete = null!;

	public ICommand Pickup => Commands[ nameof( Pickup ) ];

	public bool CanExecute_Pickup() => EnablePickup;

	public void Execute_Pickup()
	{
		var GpsPoint = Globals.Gps.CurrentGpsPoint;

		var TripIdSet = new HashSet<string>();

		foreach( var I in Items )
		{
			foreach( var Id in I.OriginalTripIds )
				TripIdSet.Add( Id );
		}

		var TripIds = TripIdSet.ToList();

		var Si = Signature;

		var UpdateSatchel = new Protocol.Data._Customers.Pml.Satchel
		                    {
			                    SatchelId       = Satchel,
			                    Driver          = Users.UserName,
			                    ProgramName     = CurrentVersion.APP_VERSION,
			                    TripIds         = TripIds,
			                    PickupTime      = DateTimeOffset.Now,
			                    PickupLatitude  = GpsPoint.Latitude,
			                    PickupLongitude = GpsPoint.Longitude,
			                    POP             = POP,

			                    Signature = new Signature
			                                {
				                                Status = STATUS.PICKED_UP,
				                                Date   = DateTimeOffset.Now,
				                                Height = Si?.Height ?? 0,
				                                Width  = Si?.Width ?? 0,
				                                Points = Si?.Points ?? ""
			                                }
		                    };

		Signature = null;

		var SatchelPackage = new TripPackage();

		UpdateSatchel.Packages.Add( SatchelPackage );

		var SatchelPackageItems = SatchelPackage.Items;

		var HasReturn = false;

		foreach( var I in Items )
		{
			HasReturn |= I.ItemCode.Compare( RETURN, StringComparison.OrdinalIgnoreCase ) == 0;

			var Scanned = I.Scanned;

			SatchelPackageItems.Add( new TripItem
			                         {
				                         Barcode     = I.Barcode,
				                         ItemCode    = I.ItemCode,
				                         Description = I.Description,
				                         Height      = I.Height,
				                         Length      = I.Length,
				                         Original    = I.Pieces,
				                         Pieces      = Scanned
			                         } );

			SatchelPackage.Pieces += Scanned;
		}

		SatchelPackage.PackageType = HasReturn ? RETURN : UP;

		POP = "";
		OnPickupComplete( TripIds, UpdateSatchel );
	}
#endregion
}