﻿#nullable enable

using DroidAz.Dialogs;

namespace DroidAz.Views._Customers.Pml;

public static class Common
{
	public const string SATCHEL = "Satchel";

	public enum MIXED_MODE_RESULT : byte
	{
		ERROR,
		OK
	}

	public static bool IsSatchel( this string str ) => string.Compare( str.Trim(), SATCHEL, StringComparison.OrdinalIgnoreCase ) == 0;
	public static bool IsSatchel( this TripDisplayEntry? t ) => t?.PackageType.IsSatchel() ?? false;

	public static async void ShowMixedModeError()
	{
		await ErrorDialog.Execute( "MixedMode" );
	}

	public static MIXED_MODE_RESULT CheckMixedMode( ObservableCollection<TripDisplayEntry>? entries, Action onError )
	{
		if( entries is not null )
		{
			var Locations = new Dictionary<string, List<TripDisplayEntry>>();

			foreach( var Entry in entries )
			{
				if( Entry.Status == STATUS.DISPATCHED )
				{
					var Location = Entry.PickupBarcode;

					if( !Locations.TryGetValue( Location, out var Entries ) )
						Locations[ Location ] = Entries = [];

					Entries.Add( Entry );
				}
			}

			foreach( var Location in Locations )
			{
				var ContainsO   = Location.Value.Any( t => t.TripId.StartsWith( "O" ) );
				var ContainsImp = Location.Value.Any( t => t.TripId.StartsWith( "IMP" ) );

				if( ContainsO && ContainsImp )
				{
					onError();
					return MIXED_MODE_RESULT.ERROR;
				}
			}
		}
		return MIXED_MODE_RESULT.OK;
	}
}