﻿#nullable enable

namespace DroidAz.Views._Customers.Pml;

public class ReturnsAcceptViewModel : ViewModelBase
{
	public int TotalScanned
	{
		get { return Get( () => TotalScanned, 0 ); }
		set { Set( () => TotalScanned, value ); }
	}

	public ObservableCollection<PmlTripItemsDisplayEntry> Items
	{
		get { return Get( () => Items, [] ); }
		set { Set( () => Items, SetShow( value ) ); }
	}

	private static ObservableCollection<PmlTripItemsDisplayEntry> SetShow( ObservableCollection<PmlTripItemsDisplayEntry> value )
	{
		foreach( var Entry in value )
			Entry.DontHide = true;
		return value;
	}


#region Selected Item
	public PmlTripItemsDisplayEntry? SelectedItem
	{
		get { return Get( () => SelectedItem, (PmlTripItemsDisplayEntry?)null ); }
		set { Set( () => SelectedItem, value ); }
	}

	[DependsUpon( nameof( SelectedItem ), Delay = 1000 )]
	public void WhenSelectedItemChanges()
	{
		SelectedItem = null; // Remove highlight
	}
#endregion

#region Reset Item
	public Func<Task<bool>>? OnResetItem;
	public ICommand          RestItem => Commands[ nameof( RestItem ) ];

	public async void Execute_ResetItem()
	{
		var Item = SelectedItem;

		if( Item is not null && OnResetItem is not null && await OnResetItem() )
		{
			Item.Scanned = 0;
			var P = Item.Pieces;
			Item.Balance =  P;
			TotalScanned -= (int)P;
		}
	}
#endregion

#region Futile Button
	public Action? OnFutile;

	public ICommand Futile => Commands[ nameof( Futile ) ];

	public void Execute_Futile()
	{
		OnFutile?.Invoke();
	}
#endregion

#region Accept Button
	public Action? OnAccept;

	public ICommand Accept => Commands[ nameof( Accept ) ];

	public void Execute_Accept()
	{
		OnAccept?.Invoke();
	}
#endregion

#region Back Button
	public Action? OnBack;

	public ICommand Back => Commands[ nameof( Back ) ];

	public void Execute_Back()
	{
		OnBack?.Invoke();
	}
#endregion
}