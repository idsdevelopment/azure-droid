﻿#nullable enable

using DroidAz.Modifications;
using ServiceLevelColours = DroidAz.Utils.ServiceLevelColours;

// ReSharper disable VirtualMemberCallInConstructor
// ReSharper disable InconsistentNaming

namespace DroidAz.Views.BaseViewModels;

internal abstract class TripsBaseViewModelInstance : UpdateViewModel, IDisposable
{
	protected const int ERROR_TIMEOUT_MS      = 10_000,
						TIME_TO_LIVE_IN_HOURS = 24 * 3;

	public bool LoadingTrips { get; private set; }

	internal ServiceLevelColours ServiceLevelColours { get; private set; } = new();

	private static readonly STATUS[] Filters = [STATUS.NEW, STATUS.ACTIVE, STATUS.DISPATCHED, STATUS.PICKED_UP, STATUS.DELETED, STATUS.FINALISED, STATUS.VERIFIED]; // Needs ACTIVE for bounced trips, NEW for Trip Moved to Another Driver

	protected override void OnInitialised()
	{
		base.OnInitialised();

		if( !IsInDesignMode )
		{
			OnPing += Ping;

			DontCreateListener = false;
			CreateListener();
		}
	}

	protected virtual void Dispose( bool disposing )
	{
		if( !Disposed )
		{
			Disposed = true;
			Azure.Client.CancelTripUpdateListen();
		}
	}

	internal TripsBaseViewModelInstance( string userName )
	{
		UserName = userName;
	}

	internal TripsBaseViewModelInstance() : this( Users.UserName )
	{
	}

	private bool Disposed;

	private bool LastPing,
				 HasListener,
				 DontCreateListener = true;

#region Trips
	internal readonly Dictionary<string, Trip> Trips = new();
#endregion

	public readonly string UserName,
						   TimeToLiveInHours = TIME_TO_LIVE_IN_HOURS.ToString();

	private void CreateListener()
	{
		LastPing = true;

		if( !HasListener && !DontCreateListener )
		{
			if( MainPageViewModel.Instance != null )
				Azure.Client.ListenTripUpdate( Messaging.DRIVERS, MainPageViewModel.Instance.DriverName.ToLower(), TimeToLiveInHours, ProcessMessage );

			HasListener = true;
		}
	}

	private async void ProcessMessage( List<TripUpdateMessage> tripUpdateMessages )
	{
		void UpdateStatus( string tripId, STATUS status, STATUS1 status1, bool receivedByDevice, bool readByDriver )
		{
			if( Trips.TryGetValue( tripId, out var Trip ) )
			{
				Trip.Status1          = status;
				Trip.Status2          = status1;
				Trip.ReceivedByDevice = receivedByDevice;
				Trip.ReadByDriver     = readByDriver;
				OnStatusUpdated( Trip );
			}
		}

		foreach( var UpdateMessage in tripUpdateMessages )
		{
		#if DEBUG
			DebugWrite( UpdateMessage );
		#endif
			if( !Filters.Contains( UpdateMessage.Status ) )
				continue;

			switch( UpdateMessage.Action )
			{
			case TripUpdateMessage.ACTION.UPDATE_STATUS:
				foreach( var TripId in UpdateMessage.TripIdList )
				{
					if( UpdateMessage.Status is STATUS.DELETED or STATUS.FINALISED or STATUS.LIMBO or STATUS.VERIFIED or STATUS.POSTED )
						RemoveTrip( TripId );
					else
						UpdateStatus( TripId, UpdateMessage.Status, UpdateMessage.Status1, UpdateMessage.ReceivedByDevice, UpdateMessage.ReadByDriver );
				}
				break;

			case TripUpdateMessage.ACTION.UPDATE_TRIP when UpdateMessage.Status is STATUS.ACTIVE or STATUS.DELETED: // Bounced Trip	 or Deleted
				foreach( var TripId in UpdateMessage.TripIdList )
					RemoveTrip( TripId );
				break;

			case TripUpdateMessage.ACTION.UPDATE_TRIP:
				while( true )
				{
					try
					{
						var Trps = await Azure.Client.RequestGetTrips( new TripIdList( UpdateMessage.TripIdList ) );

						if( Trps is not null )
						{
							var ToUpdate = new List<string>();

							foreach( var Trip in Trps )
							{
								Trip.Modified = true;

								var TripId = Trip.TripId;

								if( !Trip.ReceivedByDevice )
									ToUpdate.Add( TripId );

								switch( Trip.Status1 )
								{
								case STATUS.NEW:
								case STATUS.DELETED:
								case STATUS.FINALISED:
								case STATUS.VERIFIED:
								case STATUS.LIMBO:
								case STATUS.POSTED:
									RemoveTrip( TripId );
									break;

								default:
									Trips[ TripId ] = Trip;
									break;
								}
							}

							foreach( var Id in ToUpdate )
							{
								UpdateFromObject( new ReceivedByDevice
												  {
													  TripId = Id
												  } );
							}

							OnUpdateTrips( ( from T in Trps
											 where ToUpdate.Contains( T.TripId )
											 select T ).ToDictionary( trip => trip.TripId, trip => trip ) );
						}

						break;
					}
					catch
					{
						Thread.Sleep( ERROR_TIMEOUT_MS );
					}
				}
				break;
			}
		}
	}

	private void Ping( object sender, bool online )
	{
		if( online )
		{
			if( !LastPing ) // May have been offLine when trying to create listener
				CreateListener();

			LastPing = true;
		}
		else
			LastPing = false;
	}

	internal void PickupDeliverTrip( TripUpdate trip )
	{
		var T = trip.Trip;

		if( T is not null )
		{
			var TripId = T.TripId;
			var Now    = DateTimeOffset.Now;

			if( Trips.TryGetValue( TripId, out var Trip ) )
			{
				Trip.Pieces      = T.Pieces;
				Trip.Weight      = T.Weight;
				Trip.Reference   = T.Reference;
				Trip.DriverNotes = trip.DriverNotes;

				if( trip.IsUndeliverable )
				{
					Trip.Status1 = T.Status;
					Trip.Status2 = STATUS1.UNDELIVERED;

					string Note;
					var    DriverName = MainPageViewModel.Instance?.DriverName ?? "NULL DRIVER";

					var Time = Now.ToString( "ddd, dd MMM yyyy HH:mm:ss K" );

					if( T.Status <= STATUS.PICKED_UP )
						Trip.PickupNotes = Note = $"Could not pick-up : {Time}\r\nDriver: {DriverName}\r\n\r\n{Trip.PickupNotes}";
					else
						Trip.DeliveryNotes = Note = $"Could not deliver : {Time}\r\nDriver: {DriverName}\r\n\r\n{Trip.DeliveryNotes}";

					Trip.UndeliverableNotes = $"{Note}\r\n\r\n{Trip.UndeliverableNotes}";
				}

				else if( T.Status < STATUS.PICKED_UP )
				{
					Trip.Status1                 = STATUS.PICKED_UP;
					Trip.POP                     = trip.PopPod;
					Trip.PickupWaitTimeStart     = trip.WaitTimeStart;
					Trip.PickupWaitTimeInSeconds = trip.WaitTimeInSeconds;
					Trip.PickupArriveTime        = trip.ArriveTime;
				}
				else
				{
					Trip.Status1                   = STATUS.VERIFIED;
					Trip.POD                       = trip.PopPod;
					Trip.DeliveryWaitTimeStart     = trip.WaitTimeStart;
					Trip.DeliveryWaitTimeInSeconds = trip.WaitTimeInSeconds;
					Trip.DeliveryArriveTime        = trip.ArriveTime;
				}

				var S = trip.Signature;

				var Gps = Globals.Gps.CurrentGpsPoint;

				string Image64,
					   ImageExtension = trip.ImageExtension.Trim().TrimStart( '.' );

				if( trip.Trip is not null && ( trip.ImageBytes.Length > 0 ) && ( ImageExtension.Length > 0 ) )
				{
					try
					{
						Image64        = Convert.ToBase64String( trip.ImageBytes );
						ImageExtension = $"Trips\\{trip.Trip.TripId}.{ImageExtension}";
					}
					catch
					{
						ImageExtension = Image64 = "";
					}
				}
				else
					ImageExtension = Image64 = "";

				UpdateFromObject( new DeviceTripUpdate
								  {
									  TripUpdate = new Protocol.Data.DeviceTripUpdate( CurrentVersion.APP_VERSION )
												   {
													   TripId = TripId,

													   Status  = Trip.Status1,
													   Status1 = Trip.Status2,

													   UndeliverableNotes = Trip.UndeliverableNotes,

													   Pieces    = Trip.Pieces,
													   Weight    = Trip.Weight,
													   Reference = Trip.Reference,
													   PopPod    = trip.PopPod,
													   Signature = new Signature
																   {
																	   Status = Trip.Status1,
																	   Points = S.Points,
																	   Width  = S.Width,
																	   Height = S.Height,
																	   Date   = Now
																   },

													   UpdateLatitude  = Gps.Latitude,
													   UpdateLongitude = Gps.Longitude,
													   UpdateTime      = DateTimeOffset.Now,

													   DriverNotes       = Trip.DriverNotes,
													   WaitTimeStarTime  = trip.WaitTimeStart,
													   WaitTimeInSeconds = trip.WaitTimeInSeconds,

													   ArriveTime = trip.ArriveTime
												   },

									  ImageName   = ImageExtension,
									  ImageBase64 = Image64
								  } );

				if( Trip.Status1 is < STATUS.DISPATCHED or >= STATUS.DELIVERED || trip.IsUndeliverable )
					Trips.Remove( TripId );

				// Broadcast Update To All Views
				OnUpdateTrips( Trips );
			}
		}
	}

	internal void GetTripsForDriver()
	{
		if( !IsInDesignMode && !LoadingTrips )
		{
			Task.Run( () =>
					  {
						  LoadingTrips = true;

						  try
						  {
							  var Tps = new TripList();

							  Task.WaitAll( Task.Run( async () =>
													  {
														  while( true )
														  {
															  try
															  {
																  Tps = await Azure.Client.RequestGetTripsForDriver( UserName );
																  break;
															  }
															  catch
															  {
																  Thread.Sleep( 10_000 );
															  }
														  }
													  } ),
											Task.Run( async () =>
													  {
														  while( true )
														  {
															  try
															  {
																  ServiceLevelColours = new ServiceLevelColours( await Azure.Client.RequestGetServiceLevelColours() );
																  break;
															  }
															  catch
															  {
																  Thread.Sleep( 10_000 );
															  }
														  }
													  } )
										  );

							  // ReSharper disable once InlineTemporaryVariable
							  var T = Trips; // Don't inline

							  lock( T )
							  {
								  T.Clear();

								  foreach( var Tp in Tps )
								  {
									  try
									  {
										  T.Add( Tp.TripId, Tp );

										  if( !Tp.ReceivedByDevice )
											  UpdateFromObject( new ReceivedByDevice {TripId = Tp.TripId} );
									  }
									  catch
									  {
									  }
								  }
							  }

							  OnUpdateTrips( T );
						  }
						  finally
						  {
							  LoadingTrips = false;
						  }
					  } );
		}
	}

	public void RemoveTrip( string tripId )
	{
		if( Trips.Remove( tripId ) )
			OnRemoveTrip( tripId );
	}

	public void Dispose()
	{
		Dispose( true );
		GC.SuppressFinalize( this );
	}

#if DEBUG
	public static void DebugWrite( string txt )
	{
		Console.ForegroundColor = ConsoleColor.DarkYellow;
		Console.WriteLine( txt );
		Console.ResetColor();
	}

	public static void DebugWrite( TripUpdateMessage msg )
	{
		DebugWrite( JsonConvert.SerializeObject( msg, Formatting.Indented ) );
	}
#endif

#region Virtuals
	protected virtual void OnUpdateTrips( Dictionary<string, Trip> trips )
	{
	}

	protected virtual void OnRemoveTrip( string tripId )
	{
	}

	protected virtual void OnStatusUpdated( Trip trip )
	{
	}
#endregion
}