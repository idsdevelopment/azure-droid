﻿#nullable enable

using DroidAz.Controls.Default;
using DroidAz.Modifications;
using WeakEvent;
using Preferences = Globals.Preferences;
using ServiceLevelColours = DroidAz.Utils.ServiceLevelColours;

// ReSharper disable InconsistentNaming

namespace DroidAz.Views.BaseViewModels;

public abstract class TripsBaseViewModel : ViewModelBase
{
	internal class TripsBaseViewModelInstanceImplementation : TripsBaseViewModelInstance
	{
		protected internal TripsBaseViewModelInstanceImplementation( string userName ) : base( userName )
		{
		}

	#region Virtual Redirect Events
		private readonly WeakEventSource<string> OnRemoveTripEvent = new();

		public event EventHandler<string> OnOnRemoveTrip
		{
			add => OnRemoveTripEvent.Subscribe( value );
			remove => OnRemoveTripEvent.Unsubscribe( value );
		}

		private readonly WeakEventSource<Dictionary<string, Trip>> OnUpdateTripsEvent = new();

		public event EventHandler<Dictionary<string, Trip>> OnOnUpdateTrips
		{
			add => OnUpdateTripsEvent.Subscribe( value );
			remove => OnUpdateTripsEvent.Unsubscribe( value );
		}

		private readonly WeakEventSource<Trip> OnUpdateStatusEvent = new();

		public event EventHandler<Trip> OnOnUpdateStatus
		{
			add => OnUpdateStatusEvent.Subscribe( value );
			remove => OnUpdateStatusEvent.Unsubscribe( value );
		}

	#region Fire Events (Virtual Overrides)
		private readonly object UpdateLock = new();

		protected override void OnUpdateTrips( Dictionary<string, Trip> trips )
		{
			try
			{
				lock( UpdateLock )
					OnUpdateTripsEvent.Raise( this, trips );
			}
			catch
			{
			}
		}

		protected override void OnRemoveTrip( string tripId )
		{
			try
			{
				lock( UpdateLock )
					OnRemoveTripEvent.Raise( this, tripId );
			}
			catch
			{
			}
		}

		protected override void OnStatusUpdated( Trip trip )
		{
			try
			{
				lock( UpdateLock )
					OnUpdateStatusEvent.Raise( this, trip );
			}
			catch
			{
			}
		}
	#endregion
	#endregion
	}

	internal ServiceLevelColours ServiceLevelColours => Instance is null ? new ServiceLevelColours() : Instance.ServiceLevelColours;

	internal static TripsBaseViewModelInstanceImplementation? Instance { get; private set; }

	protected TripsBaseViewModel( string userName, bool hasDisplayedTrips = true )
	{
		TripsBaseViewModelInstanceImplementation I;

		lock( LockObject )
		{
			if( Instance is not null && ( UserName is null || ( UserName != userName ) ) )
			{
				Instance.Dispose();
				Instance = null;
			}

			I = Instance ??= new TripsBaseViewModelInstanceImplementation( UserName = userName );

			if( hasDisplayedTrips )
			{
				// Hookup to weak events
				I.OnOnUpdateStatus += UpdateStatus;
				I.OnOnUpdateTrips  += UpdateTrips;
				I.OnOnRemoveTrip   += RemoveTrip;
			}
		}

		if( hasDisplayedTrips )
		{
			if( MainPageViewModel.Instance is not null )
			{
				MainPageViewModel.Instance.OnSortFilterChange += ( _, tuple ) =>
																 {
																	 var (Visibility, PrimarySort, SecondarySort) = tuple;
																	 OnSortFilterChange( Visibility, PrimarySort, SecondarySort );
																 };
			}

			I.GetTripsForDriver(); // Calls back through UpdateTrips
		}

		Service.OnPing += ( _, b ) =>
						  {
							  OnPing?.Invoke( b );
						  };

		DeliveryGeoFence = Preferences.EnableDeliveryGeoFence;
		PickupGeoFence   = Preferences.EnablePickupGeoFence;
		UseFencing       = DeliveryGeoFence || PickupGeoFence;

		if( UseFencing )
		{
			GeoFenceRadius                =  Preferences.DeliveryGeoFenceRadius;
			Globals.Gps.OnPositionChanged += OnGpsOnOnPositionChanged;
		}
	}

	protected TripsBaseViewModel( bool hasDisplayedTrips ) : this( Users.UserName, hasDisplayedTrips )
	{
	}

	protected TripsBaseViewModel() : this( true )
	{
	}

	private readonly        int    GeoFenceRadius;
	private static readonly object LockObject = new();

	public    Action?       OnClose = null; // Used by Pml Kiosk
	protected Action<bool>? OnPing;

	private readonly bool PickupGeoFence;

	private readonly bool DeliveryGeoFence;

	private readonly bool    UseFencing;
	private static   string? UserName;

	private (bool Ok, TripDisplayEntry? Trip, int Index) GetTrip( string tripId )
	{
		var I = 0;

		foreach( var Trip in TripDisplayEntries )
		{
			if( Trip.TripId == tripId )
				return ( true, Trip, I );
			I++;
		}

		return ( false, null, -1 );
	}

#region Gps Position
	private void OnGpsOnOnPositionChanged()
	{
		OnGpsOnOnPositionChanged( this, Globals.Gps.CurrentGpsPoint );
	}

	private bool InPositionChange;

	protected virtual void OnGeoFenceChange( string tripId, bool insideFence )
	{
	}

	private void OnGpsOnOnPositionChanged( object _, GpsPoint point )
	{
		if( !InPositionChange )
		{
			if( UseFencing )
			{
				InPositionChange = false;

				try
				{
					var Changed = false;

					foreach( var DisplayEntry in TripDisplayEntries )
					{
						var Point = DisplayEntry.Status switch
									{
										< STATUS.PICKED_UP => PickupGeoFence ? DisplayEntry.PickupGpsPoint : null,
										STATUS.PICKED_UP   => DeliveryGeoFence ? DisplayEntry.DeliveryGpsPoint : null,
										_                  => new GpsPoint()
									};

						var (Inside, Diff) = Point?.WithinGeoLocationRadius( point, GeoFenceRadius ) ?? ( true, 0 );

						if( DisplayEntry.InsideFence != Inside )
						{
							Changed                  = true;
							DisplayEntry.InsideFence = Inside;
							OnGeoFenceChange( DisplayEntry.TripId, Inside );
						}

						DisplayEntry.DistanceDiff = Diff;
					}

					if( Changed )
					{
						TripDisplayEntries = new ObservableCollection<TripDisplayEntry>( from T in TripDisplayEntries
																						 orderby T.InsideFence descending
																						 select T );
					}
				}
				finally
				{
					InPositionChange = false;
				}
			}
		}
	}
#endregion

#region Trip Update
	public void UpdateFromObject( IdBase updateObject )
	{
		Instance?.UpdateFromObject( updateObject );
	}

	public void PickupDeliverTrip( TripUpdate trip )
	{
		Instance?.PickupDeliverTrip( trip );
	}

	public void PickupDeliverTrip( TripDisplayEntry trip, string popPod = "", SignatureResult? sig = null, byte[]? image = null, string imageExtension = "" )
	{
		if( Instance is { } I )
			I.PickupDeliverTrip( new TripUpdate( trip, popPod, sig, image, imageExtension ) );
	}

	public void RemoveTrip( string tripId )
	{
		Instance?.RemoveTrip( tripId );
	}

#region Sorting
	public static int Compare( MainPageViewModel.SORT sort, TripDisplayEntry t1, TripDisplayEntry t2 )
	{
		return sort switch
			   {
				   MainPageViewModel.SORT.ACCOUNT_ID => string.Compare( t1.AccountId, t2.AccountId, StringComparison.InvariantCulture ),
				   MainPageViewModel.SORT.COMPANY => t1.Status == STATUS.PICKED_UP ? string.Compare( t1.DeliveryCompany, t2.DeliveryCompany, StringComparison.InvariantCulture )
														 : string.Compare( t1.PickupCompany, t2.PickupCompany, StringComparison.InvariantCulture ),
				   MainPageViewModel.SORT.DUE_DATE_TIME => DateTimeOffset.Compare( t1.DueTime, t2.DueTime ),
				   MainPageViewModel.SORT.NEW           => !t1.ReadByDriver ? 1 : 0,
				   MainPageViewModel.SORT.REFERENCE     => string.Compare( t1.Reference, t2.Reference, StringComparison.InvariantCulture ),
				   MainPageViewModel.SORT.STATUS        => t1.Status - t2.Status,
				   _                                    => 0
			   };
	}

	protected virtual void OnSortFilterChange( MainPageViewModel.VISIBILITY visibility, MainPageViewModel.SORT primarySort, MainPageViewModel.SORT secondarySort )
	{
		if( Instance is not null )
		{
			ObservableCollection<TripDisplayEntry> SortTrips( List<TripDisplayEntry> trips )
			{
				trips.Sort( ( t1, t2 ) =>
							{
								var Retval = Compare( primarySort, t1, t2 );

								if( Retval == 0 )
									Retval = Compare( secondarySort, t1, t2 );

								return Retval;
							} );

				return new ObservableCollection<TripDisplayEntry>( trips );
			}

			var LevelColours = Instance.ServiceLevelColours;

			var Entries = SortTrips( ( from Kv in Trips
									   let T = Kv.Value
									   where ( visibility == MainPageViewModel.VISIBILITY.ALL ) || ( ( visibility == MainPageViewModel.VISIBILITY.DELIVERIES ) && ( T.Status1 == STATUS.PICKED_UP ) )
																								|| ( ( visibility == MainPageViewModel.VISIBILITY.NEW ) && !T.ReadByDriver )
																								|| ( ( visibility == MainPageViewModel.VISIBILITY.PICKUPS ) && ( ( T.Status1 == STATUS.ACTIVE ) || ( T.Status1 == STATUS.DISPATCHED ) ) )
									   select new TripDisplayEntry( T, LevelColours ) ).ToList() );
			TripDisplayEntries = Entries;
			TripCount          = Entries.Count;

			RaisePropertyChangedDelayed( nameof( TripDisplayEntries ) );
			OnGpsOnOnPositionChanged();
		}
	}

	protected void FilterAndSortTrips()
	{
		if( MainPageViewModel.Instance is not null )
		{
			var FilterSet = MainPageViewModel.Instance.Filters;
			OnSortFilterChange( FilterSet.Visibility, FilterSet.PrimarySort, FilterSet.SecondarySort );
		}
	}
#endregion

	private void UpdateStatus( object sender, Trip trip )
	{
		var (Ok, Trip, _) = GetTrip( trip.TripId );

		if( Ok && Trip is not null )
		{
			Trip.Status           = trip.Status1;
			Trip.Status3          = trip.Status3;
			Trip.ReceivedByDevice = Trip.ReceivedByDevice;
			Trip.ReadByDriver     = Trip.ReadByDriver;

			Trip.RecentlyUpdated = true;
			Trip.Removed         = false;

			FilterAndSortTrips();

			OnUpdateTripStatus( Trip );
			RaisePropertyChangedDelayed( nameof( TripDisplayEntries ) );
		}
	}

	private void RemoveTrip( object sender, string tripId )
	{
		var (Ok, Trip, Index) = GetTrip( tripId );

		if( Ok )
		{
			if( Trip is not null )
				Trip.Removed = true;

			var De = TripDisplayEntries;

			if( OnRemoveTrip( tripId ) )
			{
				De.RemoveAt( Index );
				TripCount = De.Count;
			}
			RaisePropertyChangedDelayed( nameof( TripDisplayEntries ) );
		}
	}

	protected void RemoveTrips( List<string> tripIds )
	{
		foreach( var TripId in tripIds )
			RemoveTrip( TripId );
	}

	private void UpdateTrips( object sender, Dictionary<string, Trip> trips )
	{
		if( Instance is not null )
		{
			var SColours = Instance.ServiceLevelColours;

			var De = new ObservableCollection<TripDisplayEntry>( from T in trips
																 select new TripDisplayEntry( T.Value, SColours ) );
			TripCount = De.Count;

			foreach( var Entry in De )
			{
				Entry.RecentlyUpdated = true;
				OnUpdateTrip( Entry );
			}
			FilterAndSortTrips();
			RaisePropertyChangedDelayed( nameof( TripDisplayEntries ) );
		}
	}
#endregion

#region Fields
	public ObservableCollection<TripDisplayEntry> TripDisplayEntries
	{
		get { return Get( () => TripDisplayEntries, [] ); }
		set { Set( () => TripDisplayEntries, value ); }
	}

	[DependsUpon( nameof( TripDisplayEntries ) )]
	public virtual void WhenTripDisplayEntriesChanges()
	{
	}

	public Dictionary<string, Trip> Trips => Instance is not null ? Instance.Trips : new Dictionary<string, Trip>();
#endregion

#region LocationFilter
	public string LocationFilter
	{
		get { return Get( () => LocationFilter, "" ); }
		set { Set( () => LocationFilter, value ); }
	}

	[DependsUpon( nameof( LocationFilter ) )]
	public void WhenLocationFilterChanges()
	{
		if( Instance is not null )
		{
			var Trps                = new ObservableCollection<TripDisplayEntry>();
			var Filter              = LocationFilter;
			var EmptyLocationFilter = Filter.IsNullOrEmpty();

			// ReSharper disable once ForeachCanBePartlyConvertedToQueryUsingAnotherGetEnumerator
			foreach( var Trip in Trips )
			{
				var Trp = new TripDisplayEntry( Trip.Value, Instance.ServiceLevelColours );

				if( EmptyLocationFilter || ( Trp.DisplayLocation == Filter ) )
					Trps.Add( Trp );
			}

			TripDisplayEntries = Trps;
		}
	}
#endregion

#region Counts
#region Trip Count
	private int _TripCount = -1;

	protected int TripCount
	{
		get => _TripCount;
		set
		{
			if( value != _TripCount )
			{
				_TripCount = value;

				if( MainPageViewModel.Instance != null )
					MainPageViewModel.Instance.TripCount = value;

				OnTripCountChange( value );
			}
		}
	}
#endregion

#region Scanned Counts
	public bool ShowTripCount
	{
		get => MainPageViewModel.Instance is {ShowTripCount: true};
		set
		{
			if( MainPageViewModel.Instance != null )
			{
				Dispatcher( () =>
							{
								MainPageViewModel.Instance.ShowTripCount = value;
							} );
			}
		}
	}

	public bool ShowScannedCount
	{
		get => MainPageViewModel.Instance is {ShowTripCount: true};
		set
		{
			if( MainPageViewModel.Instance != null )
			{
				Dispatcher( () =>
							{
								MainPageViewModel.Instance.ShowScannedCount = value;
							} );
			}
		}
	}

	protected int ScannedCount
	{
		get => MainPageViewModel.Instance is not null ? MainPageViewModel.Instance.TripCount : 0;
		set
		{
			if( MainPageViewModel.Instance != null )
			{
				Dispatcher( () =>
							{
								MainPageViewModel.Instance.ScannedCount = value;
							} );
			}
		}
	}
#endregion
#endregion

#region Virtuals
	protected virtual bool OnRemoveTrip( string tripId ) => !Preferences.EnhancedVisuals;

	protected virtual void OnUpdateTrip( TripDisplayEntry trip )
	{
	}

	protected virtual void OnUpdateTripStatus( TripDisplayEntry trip )
	{
	}

	protected virtual void OnTripCountChange( int count )
	{
	}
#endregion
}