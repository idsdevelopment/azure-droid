﻿#nullable enable

using WeakEvent;

namespace DroidAz.Views.BaseViewModels;

public class PingViewModel : ViewModelBase
{
	private readonly WeakEventSource<bool> _OnPing = new();

	public event EventHandler<bool> OnPing
	{
		add => _OnPing.Subscribe( value );
		remove => _OnPing.Unsubscribe( value );
	}


	~PingViewModel()
	{
		Service.OnPing -= MainPageViewModelOnOnPingChange;
	}

	public PingViewModel()
	{
		Service.OnPing += MainPageViewModelOnOnPingChange;
	}

	private void MainPageViewModelOnOnPingChange( object sender, bool online )
	{
		_OnPing.Raise( this, online );
	}
}