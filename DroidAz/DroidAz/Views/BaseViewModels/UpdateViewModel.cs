﻿#nullable enable

using Ids;
using Protocol.Data._Customers.Pml;
using File = System.IO.File;
using Image = Protocol.Data.Image;
using Preferences = Globals.Preferences;

namespace DroidAz.Views.BaseViewModels;

public abstract class IdBase
{
	public Guid Id { get; } = Guid.NewGuid();
}

public class ReceivedByDevice : IdBase
{
	public string TripId = "";
}

public class ReadByDriver : IdBase
{
	public string TripId = "";
}

public class DeviceTripUpdate : IdBase
{
	public string                          ImageBase64 = "";
	public string                          ImageName   = "";
	public Protocol.Data.DeviceTripUpdate? TripUpdate;
}

public class FutileUpdate : IdBase
{
	public Futile? Futile;
}

public class SatchelUpdate : IdBase
{
	public Satchel? Satchel;
}

public class PmlClaimSatchel : IdBase
{
	public ClaimSatchels? Satchels;
}

public class PmlClaimTrips : IdBase
{
	public ClaimTrips? Trips;
}

public class LocationDelivery : IdBase
{
	public LocationDeliveries? Deliveries;
}

public class PmlVerifySatchel : IdBase
{
	public DateTimeOffset DateTime;

	public double Latitude,
	              Longitude;

	public string ProgramName = "",
	              UserName    = "",
	              TripId      = "";
}

public abstract class UpdateViewModel : PingViewModel
{
	private static readonly List<IdBase> UpdatesList = [];

	private static string BaseFolder    = "",
	                      _SaveFileName = "";

	private static readonly object LockObject = new();

	// ReSharper disable once NotAccessedField.Local
#pragma warning disable
	private static Timer UpdateTimer;
#pragma warning restore

	private static volatile bool InUpdate;

	private static string SaveFileName
	{
		get
		{
			if( _SaveFileName.IsNullOrEmpty() )
			{
				var AppData = Environment.GetFolderPath( Environment.SpecialFolder.LocalApplicationData );
				BaseFolder = $"{AppData}\\{PRODUCT_NAME}";

				if( !Directory.Exists( BaseFolder ) )
					Directory.CreateDirectory( BaseFolder );

				_SaveFileName = $"{BaseFolder}\\UpdateList.{SETTINGS_FILE_EXTENSION}";
			}

			return _SaveFileName;
		}
	}

	public class SaveEntry
	{
		public Type?  Type        { get; set; }
		public string EntryAsJson { get; set; } = "";
	}

	public class SaveList : List<SaveEntry>
	{
	}

	public static void InitialiseUpdates()
	{
		LoadUpdateList();

		UpdateTimer = new Timer( _ =>
		                         {
			                         Update();
		                         }, null, 10_000, 10_000 );
	}

	public void UpdateFromObject( IdBase updateObject )
	{
		lock( LockObject )
		{
			UpdatesList.Add( updateObject );
			SaveUpdateList();
		}

		Update();
	}

	private static void SaveUpdateList()
	{
		lock( LockObject )
		{
			try
			{
				var SaveList = new SaveList();

				foreach( var Entry in UpdatesList )
				{
					SaveList.Add( new SaveEntry
					              {
						              Type        = Entry.GetType(),
						              EntryAsJson = JsonConvert.SerializeObject( Entry )
					              } );
				}

				var SerializedObject = JsonConvert.SerializeObject( SaveList );
				File.WriteAllText( SaveFileName, SerializedObject );
			}
			catch( Exception Exception )
			{
				Console.WriteLine( Exception );
			}
		}
	}


	private static void LoadUpdateList()
	{
		lock( LockObject )
		{
			var FileName = SaveFileName;
			var Temp     = new List<IdBase>();

			if( File.Exists( FileName ) )
			{
				var SerializedObject = File.ReadAllText( FileName );
				var Lst              = JsonConvert.DeserializeObject<SaveList>( SerializedObject );

				if( Lst is not null )
				{
					foreach( var Entry in Lst )
					{
						if( Entry.Type is not null )
						{
							var Obj = JsonConvert.DeserializeObject( Entry.EntryAsJson, Entry.Type );

							if( Obj is IdBase IdBase )
							{
								var Found = false;

								foreach( var Base in UpdatesList )
								{
									if( Base.Id == IdBase.Id )
									{
										Found = true;
										break;
									}
								}

								if( !Found )
									Temp.Add( IdBase );
							}
						}
					}
				}

				foreach( var IdBase in UpdatesList )
					Temp.Add( IdBase );

				UpdatesList.Clear();
				UpdatesList.AddRange( Temp );
			}
		}
	}

	private static Azure.InternalClient WhichPmlClient( string tripId ) => !Preferences.IsPml || tripId.StartsWith( "O" ) ? Azure.Client : Azure.SecondaryClient;


	// ReSharper disable once NotAccessedField.Local
	private static void Update()
	{
		if( InUpdate )
			return;

		InUpdate = true;

		Task.Run( () =>
		          {
			          Monitor.Enter( LockObject );

			          try
			          {
				          Azure.InternalClient WhichClient( IEnumerable<string> tripIds )
				          {
					          return tripIds.Any( t => t.StartsWith( "O" ) ) ? Azure.Client : Azure.SecondaryClient;
				          }

				          while( UpdatesList.Count > 0 )
				          {
					          try
					          {
						          IdsClient.RethrowCommunicationsException = true;

						          var UpdObj = UpdatesList[ 0 ];

						          Monitor.Exit( LockObject );

						          string TripId;

						          switch( UpdObj )
						          {
						          case ReceivedByDevice ReceivedByDevice:
							          TripId = ReceivedByDevice.TripId;
							          WhichPmlClient( TripId ).RequestTripReceivedByDevice( CurrentVersion.APP_VERSION, TripId ).Wait();
							          break;

						          case ReadByDriver ReadByDriver:
							          TripId = ReadByDriver.TripId;
							          WhichPmlClient( TripId ).RequestTripReadByDriver( CurrentVersion.APP_VERSION, TripId ).Wait();
							          break;

						          case DeviceTripUpdate DeviceTripUpdate:
							          {
								          Task[] Tasks;

								          if( ( DeviceTripUpdate.ImageBase64.Length > 0 ) && ( DeviceTripUpdate.ImageName.Length > 0 ) )
								          {
									          try
									          {
										          Tasks = new Task[ 2 ];

										          Tasks[ 1 ] = Azure.Client.RequestUploadImage(
										          [
											          new Image
											          {
												          Bytes    = Convert.FromBase64String( DeviceTripUpdate.ImageBase64 ),
												          Name     = DeviceTripUpdate.ImageName,
												          Status   = DeviceTripUpdate.TripUpdate!.Status,
												          DateTime = DeviceTripUpdate.TripUpdate.UpdateTime
											          }
										          ] );
									          }
									          catch
									          {
										          Tasks = new Task[ 1 ];
									          }
								          }
								          else
									          Tasks = new Task[ 1 ];

								          Tasks[ 0 ] = Azure.Client.RequestUpdateTripFromDevice( DeviceTripUpdate.TripUpdate );
								          Task.WaitAll( Tasks );
							          }
							          break;

						          case FutileUpdate FutileUpdate:
							          var Futile = FutileUpdate.Futile;

							          if( Futile is not null )
								          WhichClient( Futile.TripIds ).RequestPML_Futile( Futile ).Wait();
							          break;

						          case SatchelUpdate SatchelUpdate:
							          var Satchel = SatchelUpdate.Satchel;

							          if( Satchel is not null )
								          WhichClient( Satchel.TripIds ).RequestPML_UpdateSatchel( Satchel ).Wait();
							          break;

						          case PmlClaimSatchel Satchels:
							          Azure.SecondaryClient.RequestPML_ClaimSatchels( Satchels.Satchels ).Wait();
							          break;

						          case PmlClaimTrips Trips:
							          Azure.SecondaryClient.RequestPML_ClaimTrips( Trips.Trips ).Wait();
							          break;

						          case PmlVerifySatchel Vs:
							          Azure.SecondaryClient.RequestPML_VerifySatchel( new VerifySatchel
							                                                          {
								                                                          DateTime    = Vs.DateTime,
								                                                          Latitude    = Vs.Latitude,
								                                                          Longitude   = Vs.Longitude,
								                                                          ProgramName = Vs.ProgramName,
								                                                          TripId      = Vs.TripId,
								                                                          UserName    = Vs.UserName
							                                                          } ).Wait();
							          break;

						          case LocationDelivery Deliveries:
							          Azure.Client.RequestLocationDeliveries( Deliveries.Deliveries ).Wait();
							          break;
						          }
					          }
					          catch
					          {
						          return;
					          }
					          finally
					          {
						          IdsClient.RethrowCommunicationsException = false;
					          }

					          Monitor.Enter( LockObject );

					          UpdatesList.RemoveAt( 0 );
					          SaveUpdateList();
				          }

				          Monitor.Exit( LockObject );
			          }
			          finally
			          {
				          InUpdate = false;
			          }
		          } );
	}
}