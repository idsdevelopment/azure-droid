﻿#nullable enable

using DroidAz.Controls.Default;

namespace DroidAz.Views.BaseViewModels;

public class TripUpdate
{
	public byte[] ImageBytes;
	public string ImageExtension;

	public bool IsUndeliverable;

	public string PopPod;

	public SignatureResult   Signature;
	public TripDisplayEntry? Trip;

	public string DriverNotes;

	public DateTimeOffset WaitTimeStart;
	public int            WaitTimeInSeconds;

	public DateTimeOffset ArriveTime;

	public TripUpdate() : this( null )
	{
	}

	public TripUpdate( TripDisplayEntry? trip,
	                   string            popPod            = "",
	                   SignatureResult?  signature         = null,
	                   byte[]?           imageBytes        = null,
	                   string            imageExtension    = "",
	                   DateTimeOffset?   waitTimeStart     = null,
	                   int               waitTimeInSeconds = 0,
	                   string            driverNotes       = "",
	                   DateTimeOffset?   arriveTime        = null )
	{
		Trip              = trip;
		PopPod            = popPod;
		Signature         = signature ?? new SignatureResult();
		ImageBytes        = imageBytes ?? [];
		ImageExtension    = imageExtension.Trim();
		DriverNotes       = driverNotes.Trim();
		WaitTimeStart     = waitTimeStart ?? DateTimeOffset.MinValue;
		WaitTimeInSeconds = waitTimeInSeconds;
		ArriveTime        = arriveTime ?? DateTimeOffset.MinValue;
	}
}