﻿#nullable enable

using System.ComponentModel;
using System.Runtime.CompilerServices;
using ServiceLevelColours = DroidAz.Utils.ServiceLevelColours;

// ReSharper disable InconsistentNaming

namespace DroidAz.Views.BaseViewModels;

public static class TripDisplayEntryExtensions
{
	public static bool IsZero( this GpsPoint p ) => p is { Latitude: 0, Longitude: 0 };

	public static (bool Ok, decimal Distance) WithinGeoLocationRadius( this GpsPoint p1, GpsPoint p2, double radiusInMetres )
	{
		if( p1.IsZero() && p2.IsZero() )
			return ( false, 0 );

		var Dist   = Math.Abs( Measurement.SimpleGpsDistance( p1.Latitude, p1.Longitude, p2.Latitude, p2.Longitude ).Metres );
		var Radius = Math.Abs( (decimal)radiusInMetres );

		return ( Dist <= Radius, Dist );
	}
}

public class TripDisplayEntry : INotifyPropertyChanged
{
	public string TripId { get; set; } = "";

	public bool HasItems => Items.Count > 0;

	public STATUS Status
	{
		get => _Status;
		set
		{
			_Status = value;
			OnPropertyChanged();
			OnPropertyChanged( nameof( IsPickup ) );
			OnPropertyChanged( nameof( IsDelivery ) );
		}
	}

	public STATUS2 Status3 { get; set; }

	public string AccountId { get; set; } = "";

	public string Reference
	{
		get => _Reference;
		set
		{
			_Reference = value;
			OnPropertyChanged();
		}
	}

	public bool ReadByDriver
	{
		get => _ReadByDriver;
		set
		{
			_ReadByDriver = value;
			OnPropertyChanged();
		}
	}

	public bool           ReceivedByDevice { get; set; }
	public DateTimeOffset DueTime          { get; set; }
	public DateTimeOffset ReadyTime        { get; set; }

	public DateTimeOffset DisplayTime => IsDelivery ? DueTime : ReadyTime;

	public DateTimeOffset CallDate { get; set; }

	public GpsPoint PickupGpsPoint { get; set; } = new();
	public string   PickupBarcode  { get; set; } = "";
	public string   PickupCompany  { get; set; } = "";
	public string   PickupSuite    { get; set; } = "";
	public string   PickupAddress  { get; set; } = "";
	public string   PickupCity     { get; set; } = "";
	public string   PickupRegion   { get; set; } = "";

	public string DisplayPickupAddress => _DisplayPickupAddress.IsNotNullOrWhiteSpace() ? _DisplayPickupAddress
		                                      : ( _DisplayPickupAddress = $"{PickupSuite} {PickupAddress} {PickupCity} {PickupRegion}" ).Pack();

	public string PickupNotes   { get; set; } = "";
	public string PickupContact { get; set; } = "";
	public string PickupPhone   { get; set; } = "";

	public GpsPoint DeliveryGpsPoint { get; set; } = new();
	public string   DeliveryBarcode  { get; set; } = "";
	public string   DeliveryCompany  { get; set; } = "";
	public string   DeliverySuite    { get; set; } = "";
	public string   DeliveryAddress  { get; set; } = "";
	public string   DeliveryCity     { get; set; } = "";
	public string   DeliveryRegion   { get; set; } = "";

	public string DisplayDeliveryAddress => _DisplayDeliveryAddress.IsNotNullOrWhiteSpace() ? _DisplayDeliveryAddress
		                                        : ( _DisplayDeliveryAddress = $"{DeliverySuite} {DeliveryAddress} {DeliveryCity} {DeliveryRegion}" ).Pack();

	public string DeliveryNotes   { get; set; } = "";
	public string DeliveryContact { get; set; } = "";
	public string DeliveryPhone   { get; set; } = "";

	public bool IsPickup   => Status < STATUS.PICKED_UP;
	public bool IsDelivery => !IsPickup;

	public bool NotDelivered => ( Status >= STATUS.PICKED_UP ) && ( ( Status3 & STATUS2.WAS_UNDELIVERABLE ) != 0 );
	public bool NotPickuped  => ( Status < STATUS.PICKED_UP ) && ( ( Status3 & STATUS2.WAS_UNDELIVERABLE ) != 0 );

	public string ServiceLevel                { get; set; } = "";
	public Color  ServiceLevelForegroundColor { get; set; } = Color.White;
	public Color  ServiceLevelBackgroundColor { get; set; } = Color.Black;
	public string PackageType                 { get; set; } = "";

	public decimal Pieces             { get; set; }
	public decimal OriginalPieceCount { get; }

	public string POP { get; set; } = "";
	public string POD { get; set; } = "";

	public string DisplayCompany => IsPickup ? PickupCompany : DeliveryCompany;

	public string DisplayAddress => IsPickup ? DisplayPickupAddress : DisplayDeliveryAddress;

	public string DisplayNotes    => ( IsPickup ? PickupNotes : DeliveryNotes ).Pack();
	public bool   HasDisplayNotes => DisplayNotes.IsNotNullOrWhiteSpace();

	public string DriverNote { get; set; } = "";

	public string DisplayLocation
	{
		get
		{
			var Loc = IsPickup ? PickupLocation : DeliveryLocation;

			if( Loc.IsNullOrWhiteSpace() )
				Loc = DefaultLocation;

			return Loc;
		}
	}

	public List<TripItemsDisplayEntry> Items { get; set; } = [];

	private readonly string DefaultLocation = "";

	private readonly string DeliveryLocation = "";

	private readonly Protocol.Data.TripUpdate? OriginalTrip;

	private readonly string PickupLocation = "";

	private string _DisplayDeliveryAddress = "";

	private string _DisplayPickupAddress = "";

	private bool _ReadByDriver;

	private string _Reference = "";

	private STATUS _Status;

	public TripDisplayEntry()
	{
	}

	public TripDisplayEntry( TripDisplayEntry t )
	{
		OriginalTrip     = t.OriginalTrip;
		DefaultLocation  = t.DefaultLocation;
		PickupLocation   = t.PickupLocation;
		DeliveryLocation = t.DeliveryLocation;

		TripId  = t.TripId;
		Status  = t.Status;
		Status3 = t.Status3;

		AccountId = t.AccountId;
		Reference = t.Reference;

		ReadByDriver     = t.ReadByDriver;
		ReceivedByDevice = t.ReceivedByDevice;

		DueTime   = t.DueTime;
		ReadyTime = t.ReadyTime;
		CallDate  = t.CallDate;

		PickupGpsPoint.Latitude  = t.PickupGpsPoint.Latitude;
		PickupGpsPoint.Longitude = t.PickupGpsPoint.Longitude;
		PickupBarcode            = t.PickupBarcode;
		PickupCompany            = t.PickupCompany;
		PickupSuite              = t.PickupSuite;
		PickupAddress            = t.PickupAddress;
		PickupCity               = t.PickupCity;
		PickupRegion             = t.PickupRegion;

		PickupNotes   = t.PickupNotes;
		PickupContact = t.PickupContact;
		PickupPhone   = t.PickupPhone;

		DeliveryGpsPoint.Latitude  = t.DeliveryGpsPoint.Latitude;
		DeliveryGpsPoint.Longitude = t.DeliveryGpsPoint.Longitude;

		DeliveryBarcode = t.DeliveryBarcode;
		DeliveryCompany = t.DeliveryCompany;
		DeliverySuite   = t.DeliverySuite;
		DeliveryAddress = t.DeliveryAddress;
		DeliveryCity    = t.DeliveryCity;
		DeliveryRegion  = t.DeliveryRegion;

		DeliveryNotes   = t.DeliveryNotes;
		DeliveryContact = t.DeliveryContact;
		DeliveryPhone   = t.DeliveryPhone;

		ServiceLevel                = t.ServiceLevel;
		ServiceLevelForegroundColor = t.ServiceLevelForegroundColor;
		ServiceLevelBackgroundColor = t.ServiceLevelBackgroundColor;

		PackageType = t.PackageType;

		POP = t.POP;
		POD = t.POD;

		Weight             = t.Weight;
		Pieces             = t.Pieces;
		OriginalPieceCount = t.Pieces;

		Items = Items;

		InsideFence      = t.InsideFence;
		DistanceDiff     = t.DistanceDiff;
		DistanceAsString = t.DistanceAsString;

		DriverNote = t.DriverNote;

		_Removed         = t._Removed;
		_RecentlyUpdated = t._RecentlyUpdated;
	}

	public TripDisplayEntry( Protocol.Data.TripUpdate t, ServiceLevelColours serviceLevelColours )
	{
		OriginalTrip     = t;
		DefaultLocation  = t.Location.Trim();
		PickupLocation   = t.PickupAddressBarcode.Trim();
		DeliveryLocation = t.DeliveryAddressBarcode.Trim();

		TripId  = t.TripId;
		Status  = t.Status1;
		Status3 = t.Status3;

		AccountId = t.AccountId;
		Reference = t.Reference;

		ReadByDriver     = t.ReadByDriver;
		ReceivedByDevice = t.ReceivedByDevice;

		DueTime   = t.DueTime?.DateTime ?? DateTime.MinValue;
		ReadyTime = t.ReadyTime?.DateTime ?? DateTime.MinValue;
		CallDate  = t.CallTime?.DateTime ?? DateTime.MinValue;

		PickupGpsPoint.Latitude  = (double)t.PickupAddressLatitude;
		PickupGpsPoint.Longitude = (double)t.PickupAddressLongitude;

		PickupBarcode = t.PickupAddressBarcode;
		PickupCompany = t.PickupCompanyName;
		PickupSuite   = t.PickupAddressSuite;
		PickupAddress = t.PickupAddressAddressLine1;
		PickupCity    = t.PickupAddressCity;

		PickupNotes   = t.PickupNotes;
		PickupContact = t.PickupContact;
		PickupPhone   = t.PickupAddressPhone;

		DeliveryGpsPoint.Latitude  = (double)t.DeliveryAddressLatitude;
		DeliveryGpsPoint.Longitude = (double)t.DeliveryAddressLongitude;

		DeliveryBarcode = t.DeliveryAddressBarcode;
		DeliveryCompany = t.DeliveryCompanyName;
		DeliverySuite   = t.DeliveryAddressSuite;
		DeliveryAddress = t.DeliveryAddressAddressLine1;
		DeliveryCity    = t.DeliveryAddressCity;

		DeliveryNotes   = t.DeliveryNotes;
		DeliveryContact = t.DeliveryContact;
		DeliveryPhone   = t.DeliveryAddressPhone;

		var Sc = serviceLevelColours[ t.ServiceLevel ];
		ServiceLevel                = t.ServiceLevel;
		ServiceLevelForegroundColor = Sc.Foreground;
		ServiceLevelBackgroundColor = Sc.Background;

		PackageType = t.PackageType;

		POP = t.POP;
		POD = t.POD;

		Weight             = t.Weight;
		Pieces             = t.Pieces;
		OriginalPieceCount = t.Pieces;

		Items = InternalFlattenItems( TripId, t.Packages );

		DriverNote       = t.DriverNotes;
		_RecentlyUpdated = t.Modified;
	}

	protected virtual List<TripItem> FlattenItems( string tripId, List<TripPackage> packages )
	{
		var TripItems = new List<TripItem>();

		foreach( var Package in packages )
		{
			foreach( var Item in Package.Items )
				TripItems.Add( Item );
		}

		TripItems = ( from I in TripItems
		              orderby I.ItemCode, I.Barcode
		              select I ).ToList();

		for( var I = TripItems.Count; --I > 0; )
		{
			var I1 = TripItems[ I ];
			var I2 = TripItems[ I - 1 ];

			if( ( I1.ItemCode == I2.ItemCode ) && ( I1.Barcode == I2.Barcode ) )
			{
				I2.Pieces += I1.Pieces;
				I2.Weight += I1.Weight;
				TripItems.RemoveAt( I );
			}
		}

		return TripItems;
	}

	public static List<TripItemsDisplayEntry> ConvertItems( string tripId, List<TripItem> items ) =>
		( from I in items
		  select new TripItemsDisplayEntry( tripId, I ) ).ToList();

	public TripDisplayEntry Clone() => new( this );

	private List<TripItemsDisplayEntry> InternalFlattenItems( string tripId, List<TripPackage> packages ) => ConvertItems( tripId, FlattenItems( tripId, packages ) );

#region Weight
	private decimal _Weight;

	public decimal Weight
	{
		get => _Weight;
		set
		{
			_Weight      = value;
			WeightString = value.ToWeightString();
			OnPropertyChanged();
		}
	}

	public string WeightString { get; private set; } = "";
#endregion

#region OnPropertyChanged
	public event PropertyChangedEventHandler? PropertyChanged;

	protected virtual void OnPropertyChanged( [CallerMemberName] string? propertyName = null )
	{
		PropertyChanged?.Invoke( this, new PropertyChangedEventArgs( propertyName ) );
	}
#endregion

#region Recently Updated / Removed
	private bool _RecentlyUpdated,
	             _Removed;

	public bool RecentlyUpdated
	{
		get => _RecentlyUpdated;
		set
		{
			if( _RecentlyUpdated != value )
			{
				_RecentlyUpdated = value;
				_Removed         = false;

				if( !value && OriginalTrip is not null )
					OriginalTrip.Modified = false;

				OnPropertyChanged();
			}
		}
	}

	public bool Removed
	{
		get => _Removed;
		set
		{
			if( _Removed != value )
			{
				_Removed         = value;
				_RecentlyUpdated = false;
				OnPropertyChanged();
			}
		}
	}
#endregion

#region IsVisible
	private bool _IsVisible = true;

	public virtual bool IsVisible
	{
		get => _IsVisible;
		set
		{
			if( _IsVisible != value )
			{
				_IsVisible = value;
				OnPropertyChanged();
			}
		}
	}
#endregion

#region Geo Fence
	private bool _InsideFence = true;

	public bool InsideFence
	{
		get => _InsideFence;
		set
		{
			if( value != _InsideFence )
			{
				_InsideFence = value;
				OnPropertyChanged();
			}
		}
	}

	private decimal _DistanceDiff;

	public decimal DistanceDiff
	{
		get => _DistanceDiff;
		set
		{
			if( value != _DistanceDiff )
			{
				_DistanceDiff    = value;
				DistanceAsString = $"({value:N0})";
				OnPropertyChanged();
			}
		}
	}

	private string _DistanceAsString = "";

	public string DistanceAsString
	{
		get => _DistanceAsString;
		private set
		{
			if( value != _DistanceAsString )
			{
				_DistanceAsString = value;
				OnPropertyChanged();
			}
		}
	}
#endregion

#region Hide Show
	private bool _Hidden;

	public bool Hidden
	{
		get => _Hidden;
		set
		{
			if( value != _Hidden )
			{
				_Hidden = value;
				OnPropertyChanged();
				OnPropertyChanged( nameof( Showing ) );
			}
		}
	}

	public bool Showing
	{
		get => !_Hidden;
		set => Hidden = !value;
	}
#endregion
}