﻿#nullable enable

using System.ComponentModel;
using System.Runtime.CompilerServices;

// ReSharper disable InconsistentNaming

namespace DroidAz.Views.BaseViewModels;

public class TripItemsDisplayEntry : INotifyPropertyChanged
{
	public string  ItemCode       { get; set; } = "";
	public string  Barcode        { get; set; } = "";
	public string  Description    { get; set; } = "";
	public decimal Weight         { get; set; }
	public decimal Pieces         { get; set; }
	public decimal Length         { get; set; }
	public decimal Width          { get; set; }
	public decimal Height         { get; set; }
	public decimal Original       { get; set; }
	public decimal QH             { get; set; }
	public bool    IsCarton       { get; set; }
	public decimal CartonQuantity { get; set; }


#region Customisable Data
	public bool BinaryData1 { get; set; }
#endregion

	public virtual decimal DisplayScanned => Scanned;
	public virtual decimal DisplayBalance => Balance;

	public virtual decimal Scanned
	{
		get => _Scanned;
		set
		{
			if( SetField( ref _Scanned, value ) )
				OnPropertyChanged( nameof( DisplayScanned ) );
		}
	}

	public virtual decimal Balance
	{
		get => _Balance;
		set
		{
			if( SetField( ref _Balance, value ) )
				OnPropertyChanged( nameof( DisplayBalance ) );
		}
	}

	public virtual bool IsVisible
	{
		get => _IsVisible;
		set
		{
			if( SetField( ref _IsVisible, value ) )
				OnPropertyChanged( nameof( RowHeight ) );
		}
	}

	public GridLength RowHeight => IsVisible ? GridLength.Auto : 0;

	public virtual bool OverScanned { get; set; } = false;

	public string OriginalTripId { get; } = "";

	private decimal _Balance;

	private bool    _IsVisible = true;
	private decimal _Scanned;

	public TripItemsDisplayEntry()
	{
	}

	public TripItemsDisplayEntry( TripItemsDisplayEntry e )
	{
		OriginalTripId = e.OriginalTripId;
		ItemCode       = e.ItemCode;
		Barcode        = e.Barcode;
		Description    = e.Description;
		Weight         = e.Weight;
		Pieces         = e.Pieces;
		Balance        = e.Balance;
		Length         = e.Length;
		Width          = e.Width;
		Height         = e.Height;
		Original       = e.Original;
		QH             = e.QH;
		Scanned        = e.Scanned;
		IsVisible      = e.IsVisible;
		BinaryData1    = e.BinaryData1;
		IsCarton       = e.IsCarton;
		CartonQuantity = e.CartonQuantity;
	}

	public TripItemsDisplayEntry( string tripIds, TripItem i ) : this( tripIds )
	{
		ItemCode       = i.ItemCode;
		Barcode        = i.Barcode;
		Description    = i.Description;
		Height         = i.Height;
		Length         = i.Length;
		Original       = i.Original;
		Pieces         = i.Pieces;
		Balance        = i.Pieces;
		QH             = i.Qh;
		Weight         = i.Weight;
		Width          = i.Width;
		BinaryData1    = i.BinaryData1;
		IsCarton       = i.IsCarton;
		CartonQuantity = i.CartonQuantity;
	}

	public TripItemsDisplayEntry( string tripIds )
	{
		OriginalTripId = tripIds;
	}

	protected virtual void OnPropertyChanged( [CallerMemberName] string? propertyName = null )
	{
		PropertyChanged?.Invoke( this, new PropertyChangedEventArgs( propertyName ) );
	}

	protected bool SetField<T>( ref T field, T value, [CallerMemberName] string? propertyName = null )
	{
		if( EqualityComparer<T>.Default.Equals( field, value ) )
			return false;
		field = value;
		OnPropertyChanged( propertyName );
		return true;
	}

	public event PropertyChangedEventHandler? PropertyChanged;
}