﻿using DroidAz.Views._Standard.Menu;

namespace DroidAz.Views;

[XamlCompilation( XamlCompilationOptions.Compile )]
public partial class MainPage : ContentPage
{
	public static MainPage          Instance  { get; private set; }
	public static MainPageViewModel ViewModel { get; private set; }
	private       bool              FirstShow = true;

	public MainPage()
	{
		var IsDesignMode = DesignMode.IsDesignModeEnabled;
		Orientation.LayoutDirection = Orientation.ORIENTATION.PORTRAIT;

		Instance = this;
		InitializeComponent();

		if( !IsDesignMode )
		{
			MainMenuFrame.IsVisible = false;

			MenuBackground.IsVisible = false;

			if( BindingContext is MainPageViewModel Model )
			{
				ViewModel = Model;

				Model.DisplaySignOutAlert = async () => await DisplayAlert( "Exit IDS", "This will exit IDS, are you sure?", "Yes", "Cancel" );

				Model.OnShowMenu = async show =>
				                   {
					                   if( FirstShow )
					                   {
						                   FirstShow = false;
						                   await MainMenuFrame.TranslateTo( BaseLayout.Width, 0, 250, Easing.Linear );
					                   }

					                   if( show )
					                   {
						                   MenuBackground.Opacity   = 0;
						                   MenuBackground.IsVisible = true;
						                   BaseLayout.RaiseChild( MenuBackground );

						                   MainMenuFrame.Opacity   = 0;
						                   MainMenuFrame.IsVisible = true;
						                   BaseLayout.RaiseChild( MainMenuFrame );

						                   await Task.WhenAll(
						                                      MenuBackground.FadeTo( 1, 500, Easing.CubicOut ),
						                                      MainMenuFrame.FadeTo( 1, 500, Easing.CubicOut ),
						                                      MainMenuFrame.TranslateTo( 0.6, 0, 500, Easing.CubicOut )
						                                     );
					                   }
					                   else
					                   {
						                   await Task.WhenAll(
						                                      MenuBackground.FadeTo( 0, 500, Easing.CubicIn ),
						                                      MainMenuFrame.FadeTo( 0, 500, Easing.CubicIn )
						                                     );

						                   await MainMenuFrame.TranslateTo( BaseLayout.Width, 0, 1, Easing.Linear );

						                   BaseLayout.LowerChild( MainMenuFrame );

						                   BaseLayout.LowerChild( MenuBackground );
						                   MenuBackground.IsVisible = false;
					                   }
				                   };

				Model.IsMenuOpen        =  false;
				MainMenuFrame.Unfocused += ( _, _ ) => { Model.IsMenuOpen = false; };

				BaseLayout.Focused += ( _, _ ) => { Model.IsMenuOpen = false; };
			}

			Applications.MainContentPage = this;
			Applications.LoadArea        = LoadView;

			Applications.Programs = StandardMenu.Programs;

			Applications.RunProgram( Applications.SIGN_IN );
		}
	}
}