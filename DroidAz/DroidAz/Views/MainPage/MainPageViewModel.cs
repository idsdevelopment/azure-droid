﻿#nullable enable

using System.Timers;
using DroidAz.Controls;
using DroidAz.Gps;
using DroidAz.Modifications;
using IdsUpdater;
using WeakEvent;
using Timer = System.Timers.Timer;

namespace DroidAz.Views;

public class MainPageViewModel : ViewModelBase
{
	// ReSharper disable once InconsistentNaming
	public static MainPageViewModel? Instance { get; private set; }

	public static bool IsOnline { get; private set; }

	private readonly Timer PingTimer = new( 15000 );

	// For Debugging
	private bool InPing;

	private Updater? Updater;

	public Func<Task<bool>>? DisplaySignOutAlert;

	public Func<bool, Task>? OnShowMenu;

	public async Task DoPing()
	{
		if( !InPing )
		{
			InPing = true;

			try
			{
				var Ok = "";

				try
				{
					Ok = await Azure.Client.RequestPing();
				}
				catch
				{
				}

				var OLine = Ok == "OK";

				Dispatcher( () =>
				            {
					            IsOnline = OLine;
					            OnLine   = OLine;
					            OffLine  = !OLine;
					            Service.OnPingEvent.Raise( this, OLine );
				            } );
			}
			finally
			{
				InPing = false;
			}
		}
	}

	protected override void OnInitialised()
	{
		base.OnInitialised();

		if( UpdateNeeded )
		{
			UpdateNeeded = false;
			SaveSettings();
			CreateUpdater( 0 ).RunInstall();
		}
	}

	public MainPageViewModel()
	{
		Instance = this;

		if( !IsInDesignMode )
		{
			async void OnPingTimerOnElapsed( object o, ElapsedEventArgs elapsedEventArgs )
			{
				await DoPing();
			}

			PingTimer.Elapsed += OnPingTimerOnElapsed;

			PingTimer.Start();

			Task.Run( async () =>
			          {
				          await DoPing();
			          } );

			GpsViewModel.Instance.Start();
		}
	}

#region ToolBar
#region Gps
	public bool ShowGps
	{
		get { return Get( () => ShowGps, false ); }
		set { Set( () => ShowGps, value ); }
	}


	public string Latitude
	{
		get { return Get( () => Latitude, "0" ); }
		set { Set( () => Latitude, value ); }
	}


	public string Longitude
	{
		get { return Get( () => Longitude, "0" ); }
		set { Set( () => Longitude, value ); }
	}
#endregion

#region Update
	public bool UpdateAvailable
	{
		get { return Get( () => UpdateAvailable, IsInDesignMode ); }
		set { Set( () => UpdateAvailable, value ); }
	}

	[Setting]
	public bool UpdateNeeded
	{
		get { return Get( () => UpdateNeeded, false ); }
		set { Set( () => UpdateNeeded, value ); }
	}

	public ICommand RunInstall => Commands[ nameof( RunInstall ) ];

	public void Execute_RunInstall()
	{
		Updater?.RunInstall();
	}
#endregion

#region Debug
	public bool DebugServer
	{
		get { return Get( () => DebugServer, false ); }
		set { Set( () => DebugServer, value ); }
	}

	[DependsUpon( nameof( DebugServer ) )]
	public void WhenDebugServerChanges()
	{
		ShowGps = DebugServer;

		Globals.Gps.OnPositionChanged += ( _, point ) =>
		                                 {
			                                 Latitude  = point.Latitude.ToString( CultureInfo.InvariantCulture );
			                                 Longitude = point.Longitude.ToString( CultureInfo.InvariantCulture );
		                                 };
	}
#endregion

	public bool ProgramTitleVisible
	{
		get { return Get( () => ProgramTitleVisible, true ); }
		set { Set( () => ProgramTitleVisible, value ); }
	}

	public string DriverName
	{
		get { return Get( () => DriverName, "IDS Mobile" ); }
		set { Set( () => DriverName, value ); }
	}


	public bool ShowTripCount
	{
		get { return Get( () => ShowTripCount, false ); }
		set { Set( () => ShowTripCount, value ); }
	}


	public int TripCount
	{
		get { return Get( () => TripCount, 0 ); }
		set { Set( () => TripCount, value ); }
	}


	public bool ShowScannedCount
	{
		get { return Get( () => ShowScannedCount, false ); }
		set { Set( () => ShowScannedCount, value ); }
	}


	public int ScannedCount
	{
		get { return Get( () => ScannedCount, 0 ); }
		set { Set( () => ScannedCount, value ); }
	}


	public bool OnLine
	{
		get { return Get( () => OnLine, false ); }
		set { Set( () => OnLine, value ); }
	}

	public bool OffLine
	{
		get { return Get( () => OffLine, true ); }
		set { Set( () => OffLine, value ); }
	}

	private bool InCountChange;

	[DependsUpon( nameof( ShowTripCount ) )]
	public void WhenShowTripCountChanges()
	{
		if( ShowTripCount && !InCountChange )
		{
			InCountChange    = true;
			ShowScannedCount = false;
			InCountChange    = false;
		}
	}

	[DependsUpon( nameof( ShowScannedCount ) )]
	public void WhenShowScannedCountChanges()
	{
		if( ShowScannedCount && !InCountChange )
		{
			InCountChange = true;
			ShowTripCount = false;
			InCountChange = false;
			ScannedCount  = 0;
		}
	}
#endregion

#region Sorting / Visibility
	public enum VISIBILITY
	{
		ALL,
		PICKUPS,
		DELIVERIES,
		NEW
	}

	public enum SORT
	{
		STATUS,
		TRIP_ID,
		DUE_DATE_TIME,
		ACCOUNT_ID,
		COMPANY,
		REFERENCE,
		NEW
	}

	public struct FilterSet
	{
		public VISIBILITY Visibility;

		public SORT PrimarySort,
		            SecondarySort;
	}

	private readonly WeakEventSource<(VISIBILITY visibility, SORT primarySort, SORT secondarySort)> _OnSortFilterChange = new();

	public event EventHandler<(VISIBILITY visibility, SORT primarySort, SORT secondarySort)> OnSortFilterChange
	{
		add => _OnSortFilterChange.Subscribe( value );
		remove => _OnSortFilterChange.Unsubscribe( value );
	}

	public FilterSet Filters => new()
	                            {
		                            Visibility    = (VISIBILITY)Visibility,
		                            PrimarySort   = (SORT)PrimarySort,
		                            SecondarySort = (SORT)SecondarySort
	                            };

	[Setting]
	public int Visibility
	{
		get { return Get( () => Visibility, 0 ); }
		set { Set( () => Visibility, value ); }
	}


	[Setting]
	public int PrimarySort
	{
		get { return Get( () => PrimarySort, 0 ); }
		set { Set( () => PrimarySort, value ); }
	}


	[Setting]
	public int SecondarySort
	{
		get { return Get( () => SecondarySort, 0 ); }
		set { Set( () => SecondarySort, value ); }
	}

	[DependsUpon( nameof( Visibility ) )]
	[DependsUpon( nameof( PrimarySort ) )]
	[DependsUpon( nameof( SecondarySort ) )]
	public void SortSave()
	{
		SaveSettings();
		_OnSortFilterChange.Raise( this, ( (VISIBILITY)Visibility, (SORT)PrimarySort, (SORT)SecondarySort ) );
	}
#endregion

#region Menu
	public string VersionNumber
	{
		get { return Get( () => VersionNumber, CurrentVersion.VERSION ); }
		set { Set( () => VersionNumber, value ); }
	}


	public bool AllowManualBarcode
	{
		get { return Get( () => AllowManualBarcode, false ); }
		set { Set( () => AllowManualBarcode, value ); }
	}


	[Setting]
	public bool ManualBarcodeInput
	{
		get { return Get( () => ManualBarcodeInput, IsInDesignMode ); }
		set { Set( () => ManualBarcodeInput, value ); }
	}

	[DependsUpon500( nameof( ManualBarcodeInput ) )]
	public void WhenManualBarcodeInputChanges()
	{
		SaveSettings();
	}

	public ICommand ShowMenu => Commands[ nameof( ShowMenu ) ];

	public void Execute_ShowMenu()
	{
		IsMenuOpen = !IsMenuOpen;
	}

	public bool MenuVisible
	{
		get { return Get( () => MenuVisible, IsInDesignMode ); }
		set { Set( () => MenuVisible, value ); }
	}

#region Third Party Scanner
	protected override void AfterLoadSettings()
	{
		base.AfterLoadSettings();

		var Allow = AllowThirdPartyScanner;
		BarcodeScanners.AllowThirdPartyScanner = Allow;
		BarcodeScanners.UseThirdPartyScanner   = Allow && UseThirdPartyScanner;
	}

	public bool AllowThirdPartyScanner
	{
		get { return Get( () => AllowThirdPartyScanner, false ); }
		set { Set( () => AllowThirdPartyScanner, value ); }
	}

	[Setting]
	public bool UseThirdPartyScanner
	{
		get { return Get( () => UseThirdPartyScanner, false ); }
		set { Set( () => UseThirdPartyScanner, value ); }
	}

	[DependsUpon( nameof( UseThirdPartyScanner ) )]
	public void WhenUseThirdPartyScanner()
	{
		var Party3 = UseThirdPartyScanner;
		BarcodeScanners.UseThirdPartyScanner = Party3;
		SaveSettings();
		BarcodeScanner.RaiseScannerTypeChange();
	}
#endregion

	private Updater CreateUpdater( int pollingInterval = Updater.DEFAULT_POLLING_INTERVAL_IN_MINUTES )
	{
		return new Updater( CurrentVersion.CURRENT_VERSION, "App", "com.idsapp.DroidAz.apk",
		                    ( fileName, newVersionNumber, bytes ) =>
		                    {
			                    try
			                    {
				                    Updates.OnSaveUpdate?.Invoke( fileName, newVersionNumber, bytes );
				                    UpdateAvailable = true;
				                    UpdateNeeded    = true;
			                    }
			                    catch
			                    {
				                    UpdateAvailable = false;
				                    UpdateNeeded    = false;
			                    }
			                    finally
			                    {
				                    SaveSettings();
			                    }
		                    },
		                    Users.HasCarrierModifications ? Users.CarrierId.ToLower() : Updater.DEFAULT_RESELLER,
		                    pollingInterval,
		                    filename =>
		                    {
			                    Updates.OnRunUpdate?.Invoke( filename );
		                    }
		                  );
	}

	public Action<bool>? OnMenuVisible;

	[DependsUpon( nameof( MenuVisible ) )]
	public void WhenMenuVisibleChanges()
	{
		var Visible = MenuVisible;

		try
		{
			OnMenuVisible?.Invoke( Visible );
		}
		catch
		{
		}
		finally
		{
			if( Updater is null && Visible && !IsInDesignMode )
				Updater = CreateUpdater();
		}
	}


	public bool MenuEnabled
	{
		get { return Get( () => MenuEnabled, true ); }
		set { Set( () => MenuEnabled, value ); }
	}

	public bool IsMenuOpen
	{
		get { return Get( () => IsMenuOpen, true ); }
		set { Set( () => IsMenuOpen, value ); }
	}

	[DependsUpon( nameof( IsMenuOpen ), Delay = 250 )]
	public async void WhenMenuOpenChanges()
	{
		if( !IsInDesignMode )
		{
			MenuEnabled = false;

			try
			{
				if( OnShowMenu is not null )
					await OnShowMenu.Invoke( IsMenuOpen );
			}
			finally
			{
				MenuEnabled = true;
			}
		}
	}

	public string CurrentProgramDescription
	{
		get { return Get( () => CurrentProgramDescription, "" ); }
		set { Set( () => CurrentProgramDescription, value ); }
	}

	public ObservableCollection<Applications.MenuEntry> MenuItemsSource
	{
		get { return Get( () => MenuItemsSource, [] ); }
		set { Set( () => MenuItemsSource, value ); }
	}


	[Setting]
	public int MenuSelectedIndex
	{
		get { return Get( () => MenuSelectedIndex, -1 ); }
		set { Set( () => MenuSelectedIndex, value ); }
	}


	public Applications.MenuEntry SelectItemMenuEntry
	{
		get { return Get( () => SelectItemMenuEntry, new Applications.MenuEntry {Text = ""} ); }
		set { Set( () => SelectItemMenuEntry, value ); }
	}


	[DependsUpon( nameof( MenuSelectedIndex ) )]
	public void WhenMenuSelectedIndexChanges()
	{
		var Ndx = MenuSelectedIndex;

		if( Ndx >= 0 )
		{
			var Items = MenuItemsSource;

			if( Ndx < Items.Count )
				SelectItemMenuEntry = Items[ Ndx ];
		}
	}


	[DependsUpon( nameof( SelectItemMenuEntry ) )]
	public void WhenMenuSelectionChanges()
	{
		if( Users.CarrierId.IsNotNullOrWhiteSpace() && Users.UserName.IsNotNullOrWhiteSpace() ) // Signed In ?
		{
			var Entry = SelectItemMenuEntry;

			if( Entry.Id.IsNotNullOrWhiteSpace() )
			{
				var Txt    = Entry.Id;
				var Ndx    = 0;
				var SelNdx = -1;

				foreach( var MenuEntry in MenuItemsSource )
				{
					if( Txt == MenuEntry.Id )
					{
						SelNdx = Ndx;
						break;
					}
					++Ndx;
				}

				CurrentProgramDescription = Entry.Text;
				MenuSelectedIndex         = SelNdx;
				SaveSettings();
				IsMenuOpen = false;
				Applications.RunProgram( Entry.Id );
			}
		}
	}

	public ICommand SignOut => Commands[ nameof( SignOut ) ];

	public async void Execute_SignOut()
	{
		if( DisplaySignOutAlert is not null && await DisplaySignOutAlert() )
		{
			await Azure.Client.SignOut();
			Applications.Kill();
		}
	}
#endregion
}