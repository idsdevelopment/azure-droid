﻿#nullable enable

using DroidAz.Controls.Default;
using Preferences = Globals.Preferences;

namespace DroidAz.Views.Common;

public class PopPodAndSign
{
	public static bool ValidateSignatureAndPop( SignatureResult? sig, string pop )
	{
		var Enable = true;

		if( Preferences.RequirePopSignature )
			Enable = sig is not null && sig.Points.IsNotNullOrWhiteSpace();

		if( Enable && Preferences.RequirePop )
		{
			if( pop.IsNotNullOrWhiteSpace() )
			{
				var Len = Preferences.PopPodMinLength;

				if( Len > 0 )
					Enable = pop.Length >= Len;

				if( Enable && Preferences.PopPodMustBeAlpha )
				{
					foreach( var C in pop )
					{
						if( C is (>= 'A' and <= 'Z') or (>= 'a' and <= 'z') or ' ' )
							continue;

						Enable = false;
						break;
					}
				}
			}
			else
				Enable = false;
		}
		return Enable;
	}
}