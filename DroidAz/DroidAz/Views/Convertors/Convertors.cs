﻿#nullable enable

namespace DroidAz.Views.Convertors;

public class TripDisplayEntryToModifiedOrRemovedBackgroundConvertor : IValueConverter
{
	private static Color? BackgroundColor,
	                      ModifiedColor,
	                      RemovedColor;

	public object? Convert( object value, Type targetType, object parameter, CultureInfo culture )
	{
		if( BackgroundColor is null )
		{
			var Res = Application.Current.Resources;
			BackgroundColor = Res.TryGetValue( "BackgroundColor", out var ResValue ) ? (Color)ResValue : Color.Black;

			if( Globals.Preferences.EnhancedVisuals )
			{
				ModifiedColor = Res.TryGetValue( "ModifiedColor", out ResValue ) ? (Color)ResValue : Color.DarkMagenta;
				RemovedColor  = Res.TryGetValue( "DeletedColor", out ResValue ) ? (Color)ResValue : Color.Red;
			}
			else
			{
				ModifiedColor = BackgroundColor;
				RemovedColor  = BackgroundColor;
			}
		}

		if( value is TripDisplayEntry Trip )
		{
			if( Trip.Removed )
				return RemovedColor;

			if( Trip.RecentlyUpdated )
				return ModifiedColor;
		}
		return BackgroundColor;
	}

	public object ConvertBack( object value, Type targetType, object parameter, CultureInfo culture ) => throw new NotImplementedException();
}

public class OrientationLandscapeToBoolConvertor : IValueConverter
{
	public object Convert( object value, Type targetType, object parameter, CultureInfo culture ) => value is Orientation.ORIENTATION.LANDSCAPE;

	public object ConvertBack( object value, Type targetType, object parameter, CultureInfo culture ) => value is true ? Orientation.ORIENTATION.LANDSCAPE
		                                                                                                     : Orientation.ORIENTATION.PORTRAIT;
}

public class VisibilityToHideRowConvertor : IValueConverter
{
	public object Convert( object value, Type targetType, object parameter, CultureInfo culture ) => value is true ? -1 : 0; // -1 auto

	public object ConvertBack( object value, Type targetType, object parameter, CultureInfo culture ) => throw new NotImplementedException();
}

public class BoolReadColorConvertor : IValueConverter
{
	private static Color? UnReadColor,
	                      ReadColor;

	public object? Convert( object value, Type targetType, object parameter, CultureInfo culture )
	{
		if( ReadColor is null )
		{
			var Res = Application.Current.Resources;
			ReadColor   = Res.TryGetValue( "ReadColor", out var ResValue ) ? (Color)ResValue : Color.Red;
			UnReadColor = Res.TryGetValue( "UnReadColor", out var ResValue1 ) ? (Color)ResValue1 : Color.Purple;
		}

		return value is true ? ReadColor : UnReadColor;
	}

	public object ConvertBack( object value, Type targetType, object parameter, CultureInfo culture ) => throw new NotImplementedException();
}

public class DebugBackgroundColorConvertor : IValueConverter
{
	private static Color? BackgroundColor,
	                      DebugBackgroundColor;

	public object? Convert( object value, Type targetType, object parameter, CultureInfo culture )
	{
		if( BackgroundColor is null )
		{
			var Res = Application.Current.Resources;
			BackgroundColor      = Res.TryGetValue( "BackgroundColor", out var ResValue ) ? (Color)ResValue : Color.Red;
			DebugBackgroundColor = Res.TryGetValue( "DebugBackgroundColor", out var ResValue1 ) ? (Color)ResValue1 : Color.Purple;
		}

		return value is true ? DebugBackgroundColor : BackgroundColor;
	}

	public object ConvertBack( object value, Type targetType, object parameter, CultureInfo culture ) => throw new NotImplementedException();
}

public class BoolReadTextColorConvertor : IValueConverter
{
	private static Color? UnReadColor,
	                      ReadColor;

	public object? Convert( object value, Type targetType, object parameter, CultureInfo culture )
	{
		if( ReadColor is null )
		{
			var Res = Application.Current.Resources;
			ReadColor   = Res.TryGetValue( "ReadTextColor", out var ResValue ) ? (Color)ResValue : Color.Red;
			UnReadColor = Res.TryGetValue( "UnReadTextColor", out var ResValue1 ) ? (Color)ResValue1 : Color.Purple;
		}

		return value is true ? ReadColor : UnReadColor;
	}

	public object ConvertBack( object value, Type targetType, object parameter, CultureInfo culture ) => throw new NotImplementedException();
}

public class BoolToPickupDeliveryColorConvertor : IValueConverter
{
	private static Color? PickupColor,
	                      DeliveryColor;

	public object? Convert( object value, Type targetType, object parameter, CultureInfo culture )
	{
		if( DeliveryColor is null )
		{
			var Res = Application.Current.Resources;
			PickupColor   = Res.TryGetValue( "PickupColor", out var ResValue ) ? (Color)ResValue : Color.Red;
			DeliveryColor = Res.TryGetValue( "DeliveryColor", out var ResValue1 ) ? (Color)ResValue1 : Color.Purple;
		}

		if( value is bool IsPickup )
			return IsPickup ? PickupColor : DeliveryColor;

		return Color.Red;
	}

	public object ConvertBack( object value, Type targetType, object parameter, CultureInfo culture ) => throw new NotImplementedException();
}

public class BoolToPickupDeliveryButtonTexConvertor : IValueConverter
{
	private const string QM = "????";

	private static string? PickupText,
	                       DeliverText;

	public object? Convert( object value, Type targetType, object parameter, CultureInfo culture )
	{
		if( PickupText is null )
		{
			var Res = Application.Current.Resources;
			PickupText  = Res.TryGetValue( "DetailsPickup", out var ResValue ) ? (string)ResValue : QM;
			DeliverText = Res.TryGetValue( "DetailsDeliver", out var ResValue1 ) ? (string)ResValue1 : QM;
		}

		if( value is bool IsPickup )
			return IsPickup ? PickupText : DeliverText;

		return QM;
	}

	public object ConvertBack( object value, Type targetType, object parameter, CultureInfo culture ) => throw new NotImplementedException();
}

public class BoolToErrorBackgroundConvertor : IValueConverter
{
	private static Color? ErrorBackgroundColor;

	public object Convert( object value, Type targetType, object parameter, CultureInfo culture )
	{
		if( ErrorBackgroundColor is null )
		{
			var Res = Application.Current.Resources;
			ErrorBackgroundColor = Res.TryGetValue( "ErrorBackground", out var ResValue ) ? (Color)ResValue : Color.Red;
		}

		return value is true ? ErrorBackgroundColor : (object)Color.Transparent;
	}

	public object ConvertBack( object value, Type targetType, object parameter, CultureInfo culture ) => throw new NotImplementedException();
}

public class BoolToPiecesErrorTextConvertor : IValueConverter
{
	private static Color? ErrorTextColor,
	                      NormalTextColor;

	public object? Convert( object value, Type targetType, object parameter, CultureInfo culture )
	{
		if( ErrorTextColor is null )
		{
			var Res = Application.Current.Resources;
			ErrorTextColor  = Res.TryGetValue( "PiecesErrorTextColor", out var ResValue ) ? (Color)ResValue : Color.Red;
			NormalTextColor = Res.TryGetValue( "PiecesNormalTextColor", out ResValue ) ? (Color)ResValue : Color.Red;
		}

		return value is true ? ErrorTextColor : NormalTextColor;
	}

	public object ConvertBack( object value, Type targetType, object parameter, CultureInfo culture ) => throw new NotImplementedException();
}

public class BoolToScannedPiecesErrorTextConvertor : IValueConverter
{
	private static Color? ErrorTextColor,
	                      NormalTextColor;

	public object? Convert( object value, Type targetType, object parameter, CultureInfo culture )
	{
		if( ErrorTextColor is null )
		{
			var Res = Application.Current.Resources;
			ErrorTextColor  = Res.TryGetValue( "PiecesErrorTextColor", out var ResValue ) ? (Color)ResValue : Color.Red;
			NormalTextColor = Res.TryGetValue( "ScannedPiecesNormalText", out ResValue ) ? (Color)ResValue : Color.Red;
		}

		return value is true ? ErrorTextColor : NormalTextColor;
	}

	public object ConvertBack( object value, Type targetType, object parameter, CultureInfo culture ) => throw new NotImplementedException();
}

public class BoolToPopPodTextConvertor : IValueConverter
{
	private static string? PopText,
	                       PodText;

	public object? Convert( object value, Type targetType, object parameter, CultureInfo culture )
	{
		if( PopText is null )
		{
			var Res = Application.Current.Resources;
			PopText = Res.TryGetValue( "DetailsPop", out var ResValue ) ? (string)ResValue : "??";
			PodText = Res.TryGetValue( "DetailsPod", out ResValue ) ? (string)ResValue : "??";
		}

		return value is true ? PopText : PodText;
	}

	public object ConvertBack( object value, Type targetType, object parameter, CultureInfo culture ) => throw new NotImplementedException();
}

public class BoolToRowVisibilityConvertor : IValueConverter
{
	public object Convert( object value, Type targetType, object parameter, CultureInfo culture ) => value is true ? new GridLength( 1, GridUnitType.Auto ) : new GridLength( 0 );

	public object ConvertBack( object value, Type targetType, object parameter, CultureInfo culture ) => throw new NotImplementedException();
}