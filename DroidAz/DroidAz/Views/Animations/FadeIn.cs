﻿namespace DroidAz.Views;

public static class Animations
{
	public static async Task FadeIn( this Layout parent, View showingElement, View hidingElement )
	{
		parent.RaiseChild( showingElement );

		await Task.WhenAll(
						   showingElement.FadeTo( 1, 500, Easing.CubicOut ),
						   hidingElement.FadeTo( 0, 500, Easing.CubicIn )
						  );
	}
}