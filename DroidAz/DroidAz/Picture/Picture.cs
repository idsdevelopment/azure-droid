﻿#nullable enable

using Xamarin.Essentials;

namespace DroidAz;

internal class Picture
{
	public static async Task<(bool Ok, string ImageExtension, byte[] ImageBytes)> Get()
	{
		var Ok        = false;
		var Extension = "";
		var Bytes     = Array.Empty<byte>();

		try
		{
			var Photo = await MediaPicker.CapturePhotoAsync();

			// Canceled
			if( Photo is not null )
			{
				await using var Stream = await Photo.OpenReadAsync();

				var Length = (int)Stream.Length;

				if( Length > 0 )
				{
					Bytes = new byte[ Length ];

					// ReSharper disable once MustUseReturnValue
					await Stream.ReadAsync( Bytes, 0, Length );

					Extension = Path.GetExtension( Photo.FileName );
					Ok        = true;
				}
			}
		}
		catch
		{
		}
		finally
		{
			if( !Ok )
			{
				Extension = "";
				Bytes     = [];
			}
		}
		return ( Ok, Extension, Bytes );
	}
}