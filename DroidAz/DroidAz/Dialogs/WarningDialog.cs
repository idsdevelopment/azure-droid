﻿#nullable enable

namespace DroidAz.Dialogs;

public static class WarningDialog
{
	public static Task Execute( string messageKey ) => MainPage.Instance.DisplayAlert( Globals.Resources.GetStringResource( "Warning" ),
	                                                                                   Globals.Resources.GetStringResource( messageKey ),
	                                                                                   Globals.Resources.GetStringResource( "Ok" ) );
}