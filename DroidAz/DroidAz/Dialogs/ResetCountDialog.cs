﻿#nullable enable

namespace DroidAz.Dialogs;

public static class ResetCountDialog
{
	public static Task<bool> Execute() => ConfirmDialog.Execute( "ResetScannedCount" );
}