﻿#nullable enable

namespace DroidAz.Dialogs;

public static class CancelDialog
{
	public static Task<bool> Execute() => ConfirmDialog.Execute( "CancelMessage" );
}