﻿#nullable enable

namespace DroidAz.Dialogs;

public static class ConfirmDialog
{
	public static Task<bool> Execute( string messageKey, string okKey, string cancelKey ) => MainPage.Instance.DisplayAlert( Globals.Resources.GetStringResource( "Confirm" ),
	                                                                                                                         Globals.Resources.GetStringResource( messageKey ),
	                                                                                                                         Globals.Resources.GetStringResource( okKey ),
	                                                                                                                         Globals.Resources.GetStringResource( cancelKey )
	                                                                                                                       );

	public static Task<bool> Execute( string messageKey ) => Execute( messageKey, "Yes", "No" );
}