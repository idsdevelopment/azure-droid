﻿#nullable enable

namespace DroidAz.Dialogs;

public static class ShipmentRemoved
{
	private static bool InRemoved;

	public static async void Execute()
	{
		if( !InRemoved )
		{
			InRemoved = true;
			Sounds.PlayWarningSound();
			await WarningDialog.Execute( "Shipment Removed" );
			InRemoved = false;
		}
	}
}