﻿#nullable enable

namespace DroidAz.Dialogs;

public static class ErrorDialog
{
	public static Task Execute( string messageKey, bool errorSound = true )
	{
		if( errorSound )
			Sounds.PlayBadScanSound();

		return MainPage.Instance.DisplayAlert( Globals.Resources.GetStringResource( "Error" ),
		                                       Globals.Resources.GetStringResource( messageKey ),
		                                       Globals.Resources.GetStringResource( "Cancel" ) );
	}
}