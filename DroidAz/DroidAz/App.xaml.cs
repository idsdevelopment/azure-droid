﻿#nullable enable

using DroidAz.Modifications;

namespace DroidAz;

public partial class App : Application
{
	protected override void OnStart()
	{
	}

	protected override void OnSleep()
	{
	}

	protected override void OnResume()
	{
	}

	private static bool Initialised = false;

	public App()
	{
		if( !Initialised )
		{
			Initialised = true;

			UnhandledExceptions.OnHandledException = exception =>
													 {
														 var CrashText = new StringBuilder();

														 CrashText.Append( exception.Message )
																  .Append( exception.StackTrace );

														 var Ie = exception.InnerException;

														 if( Ie is not null )
														 {
															 if( Ie is AggregateException Ae )
															 {
																 foreach( var E in Ae.Flatten().InnerExceptions )
																	 CrashText.Append( E.Message );
															 }
															 else
																 CrashText.Append( Ie.Message );
														 }

#pragma warning disable 4014
														 try
														 {
															 Azure.Client.RequestClientCrashLog( new CrashReport
																								 {
																									 CrashData = CrashText.ToString(),
																									 TimeZone  = (sbyte)DateTimeOffset.Now.Offset.Hours,
																									 Account   = Users.CarrierId,
																									 User      = Users.UserName
																								 } );
														 }
														 catch( Exception Exception ) // Probably Offline
														 {
															 Console.WriteLine( Exception );
															 return;
														 }
#pragma warning restore 4014
														 Thread.Sleep( 5000 );
													 };

			InitializeComponent();
			MainPage = new MainPage();
		}
	}
}