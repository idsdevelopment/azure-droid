﻿#nullable enable

using Settings = Globals.Settings;

namespace DroidAz.Gps;

public class GpsViewModel : ViewModelBase
{
	public const int GPS_UPDATE_TIME_LIMIT_IN_MINUTES = 1;

	[Setting]
	public GpsPoints GpsPoints
	{
		get { return Get( () => GpsPoints, [] ); }
		set { Set( () => GpsPoints, value ); }
	}

	public static  GpsViewModel  Instance => _Instance ??= new GpsViewModel();
	private static GpsViewModel? _Instance;

	private static readonly GpsPoints EmptyGpsPoints = [];

	private static DateTimeOffset LastSend = DateTimeOffset.MinValue;

	private readonly object LockObject = new();

	public void Start()
	{
		Settings.GpsEnabled           =  true;
		Globals.Gps.OnPositionChanged += GpsOnOnPositionChanged;
	}


	[DependsUpon( nameof( GpsPoints ), Delay = 1000 )]
	public void WhenGpsPointsChange()
	{
		lock( LockObject )
			SaveSettings();
	}


	private void GpsOnOnPositionChanged( object sender, GpsPoint gPoint )
	{
		Globals.Gps.CurrentGpsPoint = gPoint;

		if( Settings.GpsEnabled && Distance.IsDistanceOk( gPoint.Latitude, gPoint.Longitude ) )
		{
			lock( LockObject )
			{
				var Temp = GpsPoints;
				GpsPoints = EmptyGpsPoints; // Start timer

				Temp.Add( gPoint );

				GpsPoints = Temp;

				var Now    = DateTimeOffset.Now;
				var DoSend = ( Now - LastSend ).TotalMinutes >= GPS_UPDATE_TIME_LIMIT_IN_MINUTES;

				if( !DoSend )
				{
					foreach( var GpsPoint in Temp )
					{
						if( ( Now - GpsPoint.LocalDateTime ).TotalMinutes >= GPS_UPDATE_TIME_LIMIT_IN_MINUTES )
						{
							DoSend = true;

							break;
						}
					}
				}

				if( DoSend )
				{
					Task.Run( async () =>
					          {
						          GpsPoints? Points = null;

						          try
						          {
							          lock( LockObject )
							          {
								          Points    = GpsPoints;
								          GpsPoints = [];
							          }

							          await Azure.Client.RequestGps( Points );

							          LastSend = Now;
						          }
						          catch
						          {
							          lock( LockObject )
							          {
								          if( Points is not null )
								          {
									          Points.AddRange( GpsPoints );
									          GpsPoints = Points;
								          }
							          }
						          }
					          }
					        );
				}
			}
		}
	}
}