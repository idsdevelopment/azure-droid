﻿#nullable enable

namespace DroidAz.Utils;

public static class Colours
{
	public static Color ArgbToColor( this uint argb ) =>
		/*			var B = (byte)( argb & 0xff );
			var G = (byte)( ( argb >> 8 ) & 0xff );
			var R = (byte)( ( argb >> 16 ) & 0xff );
			var A = (byte)( ( argb >> 24 ) & 0xff );
*/
		Color.FromUint( argb );

	public static Color ArgbToColor( this int argb ) => ( (uint)argb ).ArgbToColor();


	public static uint ToArgb( this Color c ) => ( (uint)c.A << 24 ) | ( (uint)c.R << 16 ) | ( (uint)c.G << 8 ) | (uint)c.B;
}