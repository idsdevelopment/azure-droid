﻿#nullable enable

namespace DroidAz.Utils;

public class ServiceLevelColours : Dictionary<string, ServiceLevelColours.Colours>
{
	public class Colours
	{
		public Color Foreground,
		             Background;
	}

	public Colours GetColour( string serviceLevel ) => !TryGetValue( serviceLevel, out var C ) ? new Colours
	                                                                                             {
		                                                                                             Background = Color.Transparent,
		                                                                                             Foreground = Color.White
	                                                                                             } : C;

	public ServiceLevelColours()
	{
	}

	public ServiceLevelColours( Protocol.Data.ServiceLevelColours serviceLevels )
	{
		foreach( var ServiceLevel in serviceLevels )
		{
			Add( ServiceLevel.ServiceLevel, new Colours
			                                {
				                                Foreground = ServiceLevel.Foreground_ARGB.ArgbToColor(),
				                                Background = ServiceLevel.Background_ARGB.ArgbToColor()
			                                } );
		}
	}

	public new Colours this[ string serviceLevel ] => GetColour( serviceLevel );
}