﻿#nullable enable

namespace DroidAz.Controls;

public class NoPieceItemDisplayEntry : TripItemsDisplayEntry
{
	public NoPieceItemDisplayEntry()
	{
	}

	public NoPieceItemDisplayEntry( TripItemsDisplayEntry item ) : base( item )
	{
	}
}

public class NoPiecesViewModel : ViewModelBase
{
	public NoPiecesList Owner = null!;

#region Selected Item
	public TripDisplayEntry? SelectedItem
	{
		get { return Get( () => SelectedItem, (TripDisplayEntry?)null ); }
		set { Set( () => SelectedItem, value ); }
	}

	private bool InSelectedItemChange;

	public void WhenSelectedItemChanges()
	{
		if( !InSelectedItemChange )
		{
			InSelectedItemChange = true;
			Owner.SelectedItem   = SelectedItem;
			InSelectedItemChange = false;
		}
	}
#endregion

#region Trips
	public ObservableCollection<TripDisplayEntry> TripDisplayEntries
	{
		get { return Get( () => TripDisplayEntries, [] ); }
		set { Set( () => TripDisplayEntries, value ); }
	}


	[DependsUpon( nameof( TripDisplayEntries ) )]
	public void WhenTripDisplayEntriesChanges()
	{
		EnableScanner = TripDisplayEntries.Count > 0;
		EnableNext    = false;
	}
#endregion

#region Enables
	public bool EnableScanner
	{
		get { return Get( () => EnableScanner, false ); }
		set { Set( () => EnableScanner, value ); }
	}


	public bool IsVisible
	{
		get { return Get( () => IsVisible, true ); }
		set { Set( () => IsVisible, value ); }
	}


	public bool IsScanning
	{
		get { return Get( () => IsScanning, false ); }
		set { Set( () => IsScanning, value ); }
	}


	public bool EnableNext
	{
		get { return Get( () => EnableNext, false ); }
		set { Set( () => EnableNext, value ); }
	}
#endregion

#region Barcodes
	public Action? OnNotForThisLocation = null;

	public string Barcode
	{
		get { return Get( () => Barcode, "" ); }
		set { Set( () => Barcode, value ); }
	}

	private bool DisableSetScanning;

	[DependsUpon( nameof( Barcode ) )]
	public void WhenBarcodeChanges()
	{
		var BCode = Barcode.Trim();

		if( BCode != "" )
		{
			Barcode = "";

			var AnyVisible = false;
			var Found      = false;

			foreach( var Entry in TripDisplayEntries )
			{
				if( Entry.TripId == BCode )
				{
					Found           = true;
					Entry.IsVisible = false;
				}

				AnyVisible |= Entry.IsVisible;
			}

			if( !Found )
				OnNotForThisLocation?.Invoke();
			else
			{
				if( !AnyVisible )
				{
					IsScanning         = false;
					EnableNext         = true;
					DisableSetScanning = true;
				}
				else
					EnableNext = false;
			}
		}
		else if( !DisableSetScanning )
			IsScanning = true;
		else
			DisableSetScanning = false;
	}
#endregion

#region Buttons
	public ICommand OnCancelButtonClick => Commands[ nameof( OnCancelButtonClick ) ];

	public ICommand OnNextButtonClick => Commands[ nameof( OnNextButtonClick ) ];

	public Action OnCancel = null!,
	              OnNext   = null!;

	public void Execute_OnCancelButtonClick()
	{
		OnCancel();
	}

	public void Execute_OnNextButtonClick()
	{
		OnNext();
	}
#endregion
}