﻿#nullable enable

using DroidAz.Dialogs;

namespace DroidAz.Controls;

[XamlCompilation( XamlCompilationOptions.Compile )]
public partial class NoPiecesList : ContentView
{
	private readonly NoPiecesViewModel Model;

	public void StartScanning()
	{
		Model.IsVisible     = true;
		Model.EnableScanner = true;
		Model.IsScanning    = true;
	}

	public void StopScanning()
	{
		Model.IsVisible     = false;
		Model.EnableScanner = false;
		Model.IsScanning    = false;
	}

	public NoPiecesList()
	{
		InitializeComponent();

		var M = Model = (NoPiecesViewModel)Root.BindingContext;
		M.Owner = this;

		M.OnCancel = () =>
		             {
			             if( OnCancel is not null )
			             {
				             if( OnCancel.CanExecute( this ) )
					             OnCancel.Execute( this );
			             }
		             };

		M.OnNext = () =>
		           {
			           if( OnNext is not null )
			           {
				           if( OnNext.CanExecute( this ) )
					           OnNext.Execute( this );
			           }
		           };

		M.OnNotForThisLocation = async () =>
		                         {
			                         await ErrorDialog.Execute( "NotForLocation" );
		                         };
	}

#region Trips
	public static readonly BindableProperty TripDisplayEntriesProperty = BindableProperty.Create( nameof( TripDisplayEntries ),
	                                                                                              typeof( ObservableCollection<TripDisplayEntry> ),
	                                                                                              typeof( NoPiecesList ),
	                                                                                              new ObservableCollection<TripDisplayEntry>(),
	                                                                                              BindingMode.OneWay,
	                                                                                              propertyChanged: TripDisplayEntriesChanged );


	private static void TripDisplayEntriesChanged( BindableObject bindable, object oldValue, object newValue )
	{
		if( bindable is NoPiecesList Pl && newValue is ObservableCollection<TripDisplayEntry> Trips )
			Pl.Model.TripDisplayEntries = Trips;
	}

	public IEnumerable<TripDisplayEntry> TripDisplayEntries
	{
		get => (IEnumerable<TripDisplayEntry>)GetValue( TripDisplayEntriesProperty );
		set => SetValue( TripDisplayEntriesProperty, value );
	}
#endregion


#region Selected Item
	public static readonly BindableProperty SelectedItemProperty = BindableProperty.Create( nameof( TripDisplayEntries ),
	                                                                                        typeof( TripDisplayEntry ),
	                                                                                        typeof( NoPiecesList ),
	                                                                                        null,
	                                                                                        BindingMode.TwoWay,
	                                                                                        propertyChanged: SelectedItemPropertyChanged );


	private static void SelectedItemPropertyChanged( BindableObject bindable, object oldValue, object newValue )
	{
		if( bindable is NoPiecesList Pl )
			Pl.Model.SelectedItem = newValue is TripDisplayEntry Item ? Item : null;
	}

	public TripDisplayEntry? SelectedItem
	{
		get => (TripDisplayEntry?)GetValue( SelectedItemProperty );
		set => SetValue( SelectedItemProperty, value );
	}
#endregion

#region Buttons
	public static readonly BindableProperty OnCancelProperty = BindableProperty.Create( nameof( OnCancel ),
	                                                                                    typeof( ICommand ),
	                                                                                    typeof( NoPiecesList )
	                                                                                  );

	public ICommand? OnCancel
	{
		get => (ICommand?)GetValue( OnCancelProperty );
		set => SetValue( OnCancelProperty, value );
	}

	public static readonly BindableProperty OnNextProperty = BindableProperty.Create( nameof( OnNext ),
	                                                                                  typeof( ICommand ),
	                                                                                  typeof( NoPiecesList )
	                                                                                );


	public ICommand? OnNext
	{
		get => (ICommand?)GetValue( OnNextProperty );
		set => SetValue( OnNextProperty, value );
	}
#endregion
}