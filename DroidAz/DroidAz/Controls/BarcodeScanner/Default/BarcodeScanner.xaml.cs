﻿#nullable enable

using BarcodeInterface;
using ZXing.Mobile;
using Keyboard = Globals.Keyboard;
using Timer = System.Timers.Timer;

namespace DroidAz.Controls.Default;

[XamlCompilation( XamlCompilationOptions.Compile )]
public partial class BarcodeScanner : ContentView, IBarcodeScanner
{
#region ManualBarcode
	public bool ManualBarcodeInput
	{
		get => Model.ManualBarcodeInput;
		set => Model.ManualBarcodeInput = value;
	}
#endregion

	private static MobileBarcodeScanner? _Scanner;

	private static MobileBarcodeScanner Scanner => _Scanner ??= new MobileBarcodeScanner();

	public BarcodeScanner()
	{
		InitializeComponent();

		var M = (BarcodeScannerViewModel)Root.BindingContext;
		Model            = M;
		M.OwnerInterface = this;

		M.OnBarcode = barcode =>
		              {
			              Keyboard.Show = false;
			              OnBarcode?.Invoke( barcode );
		              };

		M.ExecuteZXingScanner = async () =>
		                        {
			                        var Tm = new Timer( 2_000 )
			                                 {
				                                 AutoReset = true
			                                 };

			                        Tm.Elapsed += ( _, _ ) =>
			                                      {
				                                      ViewModelBase.Dispatcher( () =>
				                                                                {
					                                                                Scanner.AutoFocus();
				                                                                } );
			                                      };

			                        try
			                        {
				                        Tm.Start();

				                        return ( await Scanner.Scan( new MobileBarcodeScanningOptions
				                                                     {
					                                                     AutoRotate = true
				                                                     } ) )?.Text ?? "";
			                        }
			                        catch
			                        {
				                        return "";
			                        }
			                        finally
			                        {
				                        Tm.Dispose();
			                        }
		                        };

		BarcodeEntry.Unfocused += ( _, _ ) =>
		                          {
			                          FocusDelayed();
		                          };

		BarcodeEntry.Focused += ( _, _ ) =>
		                        {
			                        Keyboard.Show = false;
		                        };

		Keyboard.OnKeyPress += ( _, keyData ) =>
		                       {
			                       if( keyData.Key is (char)0x9 or (char)0xd ) // Tab or Cr
			                       {
				                       keyData.Handled = true;
				                       Model.CompleteScan();
			                       }
		                       };
	}

	private readonly BarcodeScannerViewModel Model;

#region Focus
	private void FocusEntry()
	{
		if( IsEnabled && !BarcodeEntry.IsFocused )
		{
			BarcodeEntry.Focus();
			Keyboard.Show = false;
		}
	}

	private void FocusDelayed( int delay = 500 )
	{
		Tasks.RunVoid( delay, () =>
		                      {
			                      ViewModelBase.Dispatcher( FocusEntry );
		                      } );
	}

	bool IBarcodeScanner.Focus()
	{
		FocusEntry();
		return true;
	}
#endregion

#region Enabled
	private static void IsEnabledPropertyChanged( BindableObject bindable, object oldValue, object newValue )
	{
		if( bindable is BarcodeScanner Bs && newValue is bool IsEnabled )
		{
			Bs.BarcodeEntry.IsEnabled     = IsEnabled;
			( (ContentView)Bs ).IsEnabled = IsEnabled;

			if( IsEnabled )
				Bs.FocusDelayed();
		}
	}

	public new static readonly BindableProperty IsEnabledProperty = BindableProperty.Create( nameof( IsEnabled ),
	                                                                                         typeof( bool ),
	                                                                                         typeof( BarcodeScanner ),
	                                                                                         false,
	                                                                                         propertyChanged: IsEnabledPropertyChanged );

	public new bool IsEnabled
	{
		get => (bool)GetValue( IsEnabledProperty );
		set => SetValue( IsEnabledProperty, value );
	}
#endregion

#region Interface
#region Scanning
	private bool _Scanning;

	public bool Scanning
	{
		get => _Scanning;
		set
		{
			_Scanning = value;
			Model.OnIsScanningChange( value );
		}
	}
#endregion

	public string Barcode => Model.Barcode;

	public string Placeholder
	{
		get => BarcodeEntry.Placeholder;
		set => BarcodeEntry.Placeholder = value;
	}

	public Color PlaceholderTextColor
	{
		get => BarcodeEntry.PlaceholderColor;
		set => BarcodeEntry.PlaceholderColor = value;
	}

	public Action<string>? OnBarcode { get; set; }

	public ContentView ScannerView
	{
		get => this;
		set { }
	}

	public void Dispose()
	{
	}
#endregion
}