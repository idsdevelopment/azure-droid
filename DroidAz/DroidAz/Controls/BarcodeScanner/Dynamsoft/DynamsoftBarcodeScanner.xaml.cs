﻿#nullable enable

using BarcodeInterface;

namespace DroidAz.Controls.Dynamsoft;

[XamlCompilation( XamlCompilationOptions.Compile )]
public partial class DynamsoftBarcodeScanner : ContentView, IBarcodeScanner
{
	public bool Scanning
	{
		get => AndroidScanner.Scanning;
		set => AndroidScanner.Scanning = value;
	}

	public string Barcode              => AndroidScanner.Barcode;
	public string Placeholder          { get; set; } = "";
	public Color  PlaceholderTextColor { get; set; } = Color.Black;

	public Action<string>? OnBarcode
	{
		get => AndroidScanner.OnBarcode;
		set => AndroidScanner.OnBarcode = value;
	}

	public ContentView ScannerView
	{
		get => this;
		set { }
	}

	bool IBarcodeScanner.IsEnabled
	{
		get => _IsEnabled;
		set
		{
			if( _IsEnabled != value )
			{
				_IsEnabled = value;

				if( AndroidScanner is { } Scanner )
					Scanner.IsEnabled = value;
			}
		}
	}

	private bool _IsEnabled;

	private readonly IBarcodeScanner AndroidScanner;

	public DynamsoftBarcodeScanner()
	{
		InitializeComponent();
		AndroidScanner = BarcodeScanners.DynamsoftBarcodeScanner;
	}
}