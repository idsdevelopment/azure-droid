﻿#nullable enable

using BarcodeInterface;
using WeakEvent;
using Keyboard = Globals.Keyboard;

namespace DroidAz.Controls;

[XamlCompilation( XamlCompilationOptions.Compile )]
public partial class BarcodeScanner : ContentView, IBarcodeScanner
{
	~BarcodeScanner()
	{
		OnScannerEnableChange   -= OnOnScannerEnableChange;
		OnScannerTypeChange     -= OnOnScannerTypeChange;
		ApplicationArea.Content =  null;
		BarcodeLoadArea.Content =  null;
		Scanner                 =  null;
	}

	public BarcodeScanner()
	{
		InitializeComponent();

		OnScannerEnableChange += OnOnScannerEnableChange;
		OnScannerTypeChange   += OnOnScannerTypeChange;
		OnOnScannerTypeChange();
	}

	private bool             ButtonVisible;
	private IBarcodeScanner? Scanner;

	private void OnOnScannerEnableChange( object sender, bool e )
	{
		if( sender != this )
			IsEnabled = false;
	}

	private void OnOnScannerTypeChange()
	{
		var Main = MainPageViewModel.Instance!;
		OnOnScannerTypeChange( this, ( Main.AllowThirdPartyScanner, Main.UseThirdPartyScanner ) );
	}

	private void InitScanner( bool typeChange = false )
	{
		if( IsVisible && Scanner is { } Sc )
		{
			Sc.PlaceholderTextColor = PlaceholderTextColor;
			Sc.Placeholder          = Placeholder;

			BarcodeLoadArea.Content   = Sc.ScannerView;
			BarcodeLoadArea.IsVisible = true;

			Sc.ScannerView.IsVisible = true;

			ScanButton.IsVisible = ButtonVisible;
			ScanButton.IsEnabled = false;

			if( IsEnabled )
			{
				var SaveScanning = Scanning;

				Sc.Scanning = false;

				Sc.OnBarcode = barcode =>
							   {
								   ViewModelBase.Dispatcher( () =>
															 {
																 Barcode = "";
																 Barcode = barcode;
																 OnBarcode?.Invoke( barcode );
															 } );
							   };

				Sc.IsEnabled = true;

				if( typeChange )
					Scanning = SaveScanning;
			}
			Focus();
		}
	}

	private void OnOnScannerTypeChange( object sender, (bool AllowThirdPartyScanner, bool UseThirdPartyScanner) e )
	{
		Keyboard.Show = false;

		var (AllowThirdPartyScanner, UseThirdPartyScanner) = e;

		if( AllowThirdPartyScanner && UseThirdPartyScanner )
		{
			Scanner       = BarcodeScanners.DynamsoftBarcodeScanner;
			ButtonVisible = true;
		}
		else
		{
			Scanner       = new Default.BarcodeScanner();
			ButtonVisible = false;
		}
		InitScanner( true );
	}

	private void ScanButton_OnClicked( object sender, EventArgs e )
	{
		if( Scanner is { } Sc )
		{
			Sc.Scanning = false;
			Sc.Scanning = true;
		}
	}

	public new bool Focus() => Scanner?.Focus() ?? false;

#region Property Overrides
#region IsVisible
	private static void IsVisiblePropertyChanged( BindableObject bindable, object oldValue, object newValue )
	{
		if( bindable is BarcodeScanner Bs && newValue is bool IsVisible )
		{
			Bs.BarcodeLoadArea.IsVisible = IsVisible;
			Bs.ScanButton.IsVisible      = Bs.ButtonVisible && IsVisible;
			Bs.InitScanner();
		}
	}

	public new static readonly BindableProperty IsVisibleProperty = BindableProperty.Create( nameof( IsVisible ),
																							 typeof( bool ),
																							 typeof( BarcodeScanner ),
																							 true,
																							 propertyChanged: IsVisiblePropertyChanged );

	public new bool IsVisible
	{
		get => (bool)GetValue( IsVisibleProperty );
		set => SetValue( IsVisibleProperty, value );
	}
#endregion

#region IsEnabled
	private static void IsEnabledPropertyChanged( BindableObject bindable, object oldValue, object newValue )
	{
		if( bindable is BarcodeScanner Bs && newValue is bool IsEnabled )
		{
			if( Bs.Scanner is { } Scanner )
			{
				Scanner.IsEnabled = IsEnabled;

				if( !IsEnabled )
					Scanner.Scanning = false;
			}

			RaiseScannerEnableChange( Bs, IsEnabled );
			( (ContentView)Bs ).IsEnabled = IsEnabled;
			Bs.InitScanner();
		}
	}

	public new static readonly BindableProperty IsEnabledProperty = BindableProperty.Create( nameof( IsEnabled ),
																							 typeof( bool ),
																							 typeof( BarcodeScanner ),
																							 false,
																							 BindingMode.TwoWay,
																							 propertyChanged: IsEnabledPropertyChanged );

	public new bool IsEnabled
	{
		get => (bool)GetValue( IsEnabledProperty );
		set => SetValue( IsEnabledProperty, value );
	}

#region Global Enable Change Event
	private static readonly WeakEventSource<bool> _OnScannerEnableChangeEventSource = new();

	private static event EventHandler<bool> OnScannerEnableChange
	{
		add => _OnScannerEnableChangeEventSource.Subscribe( value );
		remove => _OnScannerEnableChangeEventSource.Unsubscribe( value );
	}

	private static bool InRaiseEvent;

	private static void RaiseScannerEnableChange( BarcodeScanner sender, bool enabled )
	{
		if( !InRaiseEvent )
		{
			InRaiseEvent = true;

			try
			{
				_OnScannerEnableChangeEventSource.Raise( sender, enabled );
			}
			finally
			{
				InRaiseEvent = false;
			}
		}
	}
#endregion
#endregion
#endregion

#region Interface  (Interface properties need to be bindable)
#region Scanning
	private static void ScanningPropertyChanged( BindableObject bindable, object oldValue, object newValue )
	{
		if( bindable is BarcodeScanner {Scanner: { } Scanner} Bs && newValue is bool Scanning )
		{
			if( Scanning && !Bs.IsEnabled )
				Bs.IsEnabled = true;

			if( Bs.ButtonVisible )
				Bs.ScanButton.IsEnabled = Scanning;
			else
			{
				Scanner.Scanning = Scanning;

				if( Scanning )
				{
					Bs.Barcode = "";
					Scanner.Focus();
				}
			}
		}
	}

	public static readonly BindableProperty ScanningProperty = BindableProperty.Create( nameof( Scanning ),
																						typeof( bool ),
																						typeof( BarcodeScanner ),
																						false,
																						propertyChanged: ScanningPropertyChanged );

	public bool Scanning
	{
		get => (bool)GetValue( ScanningProperty );
		set => SetValue( ScanningProperty, value );
	}
#endregion

#region Barcode
	public static readonly BindableProperty BarcodeProperty = BindableProperty.Create( nameof( Barcode ),
																					   typeof( string ),
																					   typeof( BarcodeScanner ),
																					   "",
																					   BindingMode.OneWayToSource
																					 );

	public string Barcode
	{
		get => (string)GetValue( BarcodeProperty );
		set => SetValue( BarcodeProperty, value );
	}
#endregion

#region Placeholder
	public static readonly BindableProperty PlaceholderProperty = BindableProperty.Create( nameof( Placeholder ),
																						   typeof( string ),
																						   typeof( BarcodeScanner ),
																						   "",
																						   propertyChanged: PlaceholderPropertyChanged
																						 );

	private static void PlaceholderPropertyChanged( BindableObject bindable, object oldValue, object newValue )
	{
		if( bindable is BarcodeScanner {Scanner: { } Scanner} && newValue is string Holder )
			Scanner.Placeholder = Holder;
	}

	public string Placeholder
	{
		get => (string)GetValue( PlaceholderProperty );
		set => SetValue( PlaceholderProperty, value );
	}
#endregion

#region PlaceholderTextColor
	public static readonly BindableProperty PlaceholderTextColorProperty = BindableProperty.Create( nameof( PlaceholderTextColor ),
																									typeof( Color ),
																									typeof( BarcodeScanner ),
																									Color.Black,
																									propertyChanged: PlaceholderTextColorPropertyChanged
																								  );

	private static void PlaceholderTextColorPropertyChanged( BindableObject bindable, object oldValue, object newValue )
	{
		if( bindable is BarcodeScanner {Scanner: { } Scanner} && newValue is Color TextColor )
			Scanner.PlaceholderTextColor = TextColor;
	}

	public Color PlaceholderTextColor
	{
		get => (Color)GetValue( PlaceholderTextColorProperty );
		set => SetValue( PlaceholderTextColorProperty, value );
	}
#endregion

	public Action<string>? OnBarcode { get; set; }

	public ContentView ScannerView
	{
		get => this;
		set { }
	}
#endregion

#region Application Content
	public static readonly BindableProperty ApplicationProperty = BindableProperty.Create( nameof( Application ),
																						   typeof( View ),
																						   typeof( BarcodeScanner ),
																						   propertyChanged: ApplicationPropertyChanged );

	private static void ApplicationPropertyChanged( BindableObject bindable, object oldValue, object newValue )
	{
		if( bindable is BarcodeScanner Bs && newValue is View View )
		{
			var App = Bs.ApplicationArea;
			App.Content            = View;
			View.HorizontalOptions = LayoutOptions.FillAndExpand;
			View.VerticalOptions   = LayoutOptions.FillAndExpand;
			App.ForceLayout();
		}
	}

	public View Application
	{
		get => (View)GetValue( ApplicationProperty );
		set => SetValue( ApplicationProperty, value );
	}
#endregion

#region Global Change Event
	private static readonly WeakEventSource<(bool AllowThirdPartyScanner, bool UseThirdPartyScanner)> _OnScannerTypeChangeEventSource = new();

	public static event EventHandler<(bool AllowThirdPartyScanner, bool UseThirdPartyScanner)> OnScannerTypeChange
	{
		add => _OnScannerTypeChangeEventSource.Subscribe( value );
		remove => _OnScannerTypeChangeEventSource.Unsubscribe( value );
	}

	public static void RaiseScannerTypeChange( (bool AllowThirdPartyScanner, bool UseThirdPartyScanner) e )
	{
		_OnScannerTypeChangeEventSource.Raise( null, e );
	}

	public static void RaiseScannerTypeChange()
	{
		var Main = MainPageViewModel.Instance!;
		RaiseScannerTypeChange( ( Main.AllowThirdPartyScanner, Main.UseThirdPartyScanner ) );
	}
#endregion
}