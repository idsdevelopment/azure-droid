﻿#nullable enable

using DroidAz.Controls.Default;
using Preferences = Globals.Preferences;

namespace DroidAz.Controls.PodAndSign;

public class PodAndSignViewModel : ViewModelBase
{
	public string CompanyName
	{
		get { return Get( () => CompanyName, "" ); }
		set { Set( () => CompanyName, value ); }
	}

	public bool IsPickup
	{
		get { return Get( () => IsPickup, true ); }
		set { Set( () => IsPickup, value ); }
	}


	public bool FinaliseEnabled
	{
		get { return Get( () => FinaliseEnabled, false ); }
		set { Set( () => FinaliseEnabled, value ); }
	}


	public string PopOrPod
	{
		get { return Get( () => PopOrPod, "" ); }
		set { Set( () => PopOrPod, value ); }
	}


#region Disclaimer
	public Action<bool> OnShowDisclaimer = null!;
#endregion

	public PodAndSign Owner = null!;

#region Picture
	public bool ShowPictureButton
	{
		get { return Get( () => ShowPictureButton, false ); }
		set { Set( () => ShowPictureButton, value ); }
	}

	public ICommand OnPictureClick => Commands[ nameof( OnPictureClick ) ];

	public async void Execute_OnPictureClick()
	{
		( ImageOk, ImageExtension, ImageBytes ) = await OnGetImage();
		EnableEnabledPickupDelivery();
	}

	private string                                                          ImageExtension = "";
	private byte[]                                                          ImageBytes     = [];
	private bool                                                            ImageOk;
	public  Func<Task<(bool Ok, string ImageExtension, byte[] ImageBytes)>> OnGetImage = null!;
#endregion

#region Initialisation
	private bool RequirePop,
	             RequirePopSignature,
	             RequirePopPicture,
	             RequirePod,
	             RequirePodSignature,
	             RequirePodPicture,
	             PopPodMustBeAlpha,
	             HasDisclaimer;

	private int PopPodMinLength;

	protected override void OnInitialised()
	{
		base.OnInitialised();
		HasDisclaimer = Preferences.HasDisclaimer;

		RequirePop          = Preferences.RequirePop;
		RequirePopSignature = Preferences.RequirePopSignature;
		RequirePopPicture   = Preferences.RequirePopPicture;

		RequirePod          = Preferences.RequirePod;
		RequirePodSignature = Preferences.RequirePodSignature;
		RequirePodPicture   = Preferences.RequirePodPicture;

		PopPodMustBeAlpha = Preferences.PopPodMustBeAlpha;
		PopPodMinLength   = Preferences.PopPodMinLength;
		HasDisclaimer     = Preferences.HasDisclaimer;

		EnableEnabledPickupDelivery();
	}
#endregion

#region Signature
	public Action<bool> OnShowSignature = null!;

	public void OnSignatureCancel()
	{
		Signature = null;
		OnShowSignature( false );
		SignButtonEnabled = true;
	}


	public SignatureResult? Signature
	{
		get { return Get( () => Signature, (SignatureResult?)null ); }
		set { Set( () => Signature, value ); }
	}


	public bool ShowSignatureButton
	{
		get { return Get( () => ShowSignatureButton, false ); }
		set { Set( () => ShowSignatureButton, value ); }
	}


	[DependsUpon( nameof( Signature ) )]
	public void WhenSignatureChanges()
	{
		OnShowSignature( false );
		SignButtonEnabled = true;
	}

	public bool SignButtonEnabled
	{
		get { return Get( () => SignButtonEnabled, true ); }
		set { Set( () => SignButtonEnabled, value ); }
	}


	public ICommand OnSignButtonClick => Commands[ nameof( OnSignButtonClick ) ];

	public void Execute_OnSignButtonClick()
	{
		if( HasDisclaimer )
			OnShowDisclaimer( true );

		SignButtonEnabled = false;
		OnShowSignature( true );
	}
#endregion

#region Buttons
	[DependsUpon250( nameof( Signature ) )]
	[DependsUpon250( nameof( PopOrPod ) )]
	public void EnableEnabledPickupDelivery()
	{
		var Pickup   = IsPickup;
		var Delivery = !Pickup;

		ShowSignatureButton = ( RequirePopSignature && Pickup ) || ( RequirePodSignature && Delivery );
		ShowPictureButton   = ( RequirePopPicture && Pickup ) || ( RequirePodPicture && Delivery );

		bool Enable;
		var  Pp = PopOrPod;

		bool AlphaOk()
		{
			if( PopPodMustBeAlpha )
			{
				if( ( Pp.IndexOf( "  ", StringComparison.Ordinal ) >= 0 ) || ( Pp.IndexOf( "..", StringComparison.Ordinal ) >= 0 ) || ( Pp.IndexOf( "--", StringComparison.Ordinal ) > 0 ) )
					return false;

				foreach( var C in Pp )
				{
					if( C is not (>= 'A' and <= 'Z' or >= 'a' and <= 'z' or ' ' or '.' or '-') )
						return false;
				}
			}
			return true;
		}

		var PopPodOk    = Pp.IsNotNullOrWhiteSpace() && ( Pp.Length >= PopPodMinLength ) && AlphaOk();
		var SignatureOk = Signature is {Points.Length: > 2};

		if( Pickup )
		{
			Enable = ( !RequirePop || ( RequirePop && PopPodOk ) )
			         && ( !RequirePopSignature || ( RequirePopSignature && SignatureOk ) )
			         && ( !RequirePopPicture || ( RequirePopPicture && ImageOk ) );
		}
		else
		{
			Enable = ( !RequirePod || ( RequirePod && PopPodOk ) )
			         && ( !RequirePodSignature || ( RequirePodSignature && SignatureOk ) )
			         && ( !RequirePodPicture || ( RequirePodPicture && ImageOk ) );
		}

		FinaliseEnabled = Enable;
	}

	public Action<PodAndSign.Result> SetResult = null!;

	private void DoExecute( ICommand? command, PodAndSign.Result result )
	{
		try
		{
			if( command is not null && command.CanExecute( this ) )
			{
				SetResult( result );
				command.Execute( this );
			}
		}
		catch
		{
		}
		finally
		{
			Signature = null;
			PopOrPod  = "";
		}
	}

	public ICommand OnFinaliseButtonClick => Commands[ nameof( OnFinaliseButtonClick ) ];

	public void Execute_OnFinaliseButtonClick()
	{
		DoExecute( Owner.OnFinalise, new PodAndSign.Result
		                             {
			                             Signature      = Signature,
			                             PopPod         = PopOrPod,
			                             ImageBytes     = ImageBytes,
			                             ImageExtension = ImageExtension
		                             } );
	}

	public ICommand OnCancelButtonClick => Commands[ nameof( OnCancelButtonClick ) ];

	public void Execute_OnCancelButtonClick()
	{
		DoExecute( Owner.OnCancel, new PodAndSign.Result() );
	}
#endregion
}