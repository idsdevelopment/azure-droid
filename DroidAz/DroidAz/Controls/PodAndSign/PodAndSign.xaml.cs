﻿#nullable enable

using DroidAz.Controls.Default;
using ViewModelsBase;

namespace DroidAz.Controls.PodAndSign;

[XamlCompilation( XamlCompilationOptions.Compile )]
public partial class PodAndSign : ContentView
{
	private readonly PodAndSignViewModel Model;

	protected override void OnPropertyChanged( string? propertyName = null )
	{
		base.OnPropertyChanged( propertyName );

		if( propertyName is not null && ( propertyName == "IsVisible" ) && IsPickup )
		{
			Tasks.RunVoid( 100, () =>
			                    {
				                    ( (AViewModelBase)Model ).Dispatcher?.Invoke( () =>
				                                                                  {
					                                                                  Model.EnableEnabledPickupDelivery();
				                                                                  } );
			                    } );
		}
	}

	public PodAndSign()
	{
		InitializeComponent();

		var Sig = Signature;
		var M   = (PodAndSignViewModel)Root.BindingContext;
		Model   = M;
		M.Owner = this;

		M.OnGetImage = async () => await Picture.Get();

		M.OnShowSignature = async show =>
		                    {
			                    if( show )
			                    {
				                    Sig.Clear();
				                    Orientation.LayoutDirection = Orientation.ORIENTATION.LANDSCAPE;
				                    await this.RaiseAndFade( Root, Sig );
			                    }
			                    else
			                    {
				                    Orientation.LayoutDirection = Orientation.ORIENTATION.PORTRAIT;
				                    await this.RaiseAndFade( Signature, Root );
			                    }
		                    };

		Sig.OnCancel = () =>
		               {
			               M.OnSignatureCancel();
		               };

		Sig.OnOk = signature =>
		           {
			           M.Signature = signature;
		           };

		M.OnShowDisclaimer = async show =>
		                     {
			                     if( show )
				                     await this.RaiseAndFade( Root, Disclaimer );
			                     else
				                     await this.RaiseAndFade( Disclaimer, Root );
		                     };

		M.SetResult = result =>
		              {
			              Values = result;
		              };
	}

#region Values
	public class Result
	{
		public string           PopPod         { get; init; } = "";
		public SignatureResult? Signature      { get; init; }
		public byte[]?          ImageBytes     { get; init; }
		public string           ImageExtension { get; init; } = "";
	}

	public static readonly BindableProperty ValuesProperty = BindableProperty.Create( nameof( Values ),
	                                                                                  typeof( Result ),
	                                                                                  typeof( PodAndSign ),
	                                                                                  new Result(),
	                                                                                  BindingMode.OneWayToSource
	                                                                                );

	public Result Values
	{
		get => (Result)GetValue( ValuesProperty );
		private set => SetValue( ValuesProperty, value );
	}
#endregion

#region IsPickup
	public static readonly BindableProperty IsPickupProperty = BindableProperty.Create( nameof( IsPickup ),
	                                                                                    typeof( bool ),
	                                                                                    typeof( PodAndSign ),
	                                                                                    true,
	                                                                                    propertyChanged: IsPickupPropertyChanged
	                                                                                  );

	private static void IsPickupPropertyChanged( BindableObject bindable, object oldValue, object newValue )
	{
		if( bindable is PodAndSign Ps && newValue is bool Pu )
			Ps.Model.IsPickup = Pu;
	}


	public bool IsPickup
	{
		get => (bool)GetValue( IsPickupProperty );
		set => SetValue( IsPickupProperty, value );
	}
#endregion

#region Captions
	public static readonly BindableProperty FinaliseCaptionProperty = BindableProperty.Create( nameof( FinaliseCaption ),
	                                                                                           typeof( string ),
	                                                                                           typeof( PodAndSign ),
	                                                                                           "",
	                                                                                           propertyChanged: OnFinalisedCaptionPropertyChanged
	                                                                                         );

	private static void OnFinalisedCaptionPropertyChanged( BindableObject bindable, object oldValue, object newValue )
	{
		if( bindable is PodAndSign Ps && newValue is string Caption )
			Ps.FinaliseButton.Text = Caption;
	}


	public string FinaliseCaption
	{
		get => (string)GetValue( FinaliseCaptionProperty );
		set => SetValue( FinaliseCaptionProperty, value );
	}
#endregion

#region Buttons
	public static readonly BindableProperty OnCancelProperty = BindableProperty.Create( nameof( OnCancel ),
	                                                                                    typeof( ICommand ),
	                                                                                    typeof( PodAndSign )
	                                                                                  );

	public ICommand? OnCancel
	{
		get => (ICommand?)GetValue( OnCancelProperty );
		set => SetValue( OnCancelProperty, value );
	}

	public static readonly BindableProperty OnFinaliseProperty = BindableProperty.Create( nameof( OnFinalise ),
	                                                                                      typeof( ICommand ),
	                                                                                      typeof( PodAndSign )
	                                                                                    );


	public ICommand? OnFinalise
	{
		get => (ICommand?)GetValue( OnFinaliseProperty );
		set => SetValue( OnFinaliseProperty, value );
	}
#endregion
}