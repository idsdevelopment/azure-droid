﻿#nullable enable

using System.Diagnostics.CodeAnalysis;

namespace XamarinAndroidEntry;

public class ExtendedEntry : Entry
{
	/// <summary>
	///     The font property
	/// </summary>
	[SuppressMessage( "Microsoft.Security", "CA2104:DoNotDeclareReadOnlyMutableReferenceTypes" )]
	public static readonly BindableProperty FontProperty =
		BindableProperty.Create( nameof( Font ), typeof( Font ), typeof( ExtendedEntry ), new Font() );

	/// <summary>
	///     The XAlign property
	/// </summary>
	[SuppressMessage( "Microsoft.Security", "CA2104:DoNotDeclareReadOnlyMutableReferenceTypes" )]
	public static readonly BindableProperty XAlignProperty =
		BindableProperty.Create( nameof( XAlign ), typeof( TextAlignment ), typeof( ExtendedEntry ), TextAlignment.Start );

	/// <summary>
	///     The HasBorder property
	/// </summary>
	[SuppressMessage( "Microsoft.Security", "CA2104:DoNotDeclareReadOnlyMutableReferenceTypes" )]
	public static readonly BindableProperty HasBorderProperty =
		BindableProperty.Create( nameof( HasBorder ), typeof( bool ), typeof( ExtendedEntry ), true );

	/// <summary>
	///     The PlaceholderTextColor property
	/// </summary>
	[SuppressMessage( "Microsoft.Security", "CA2104:DoNotDeclareReadOnlyMutableReferenceTypes" )]
	public static readonly BindableProperty PlaceholderTextColorProperty =
		BindableProperty.Create( nameof( PlaceholderTextColor ), typeof( Color ), typeof( ExtendedEntry ), Color.Default );

	/// <summary>
	///     The ShowVirtualKeyboardOnFocus property
	/// </summary>
	[SuppressMessage( "Microsoft.Security", "CA2104:DoNotDeclareReadOnlyMutableReferenceTypes" )]
	public static readonly BindableProperty ShowVirtualKeyboardOnFocusProperty =
		BindableProperty.Create( nameof( ShowVirtualKeyboardOnFocus ), typeof( bool ), typeof( ExtendedEntry ), true, propertyChanged: ShowVirtualKeyboardOnFocusPropertyChanged );

	/// <summary>
	///     The ShowVirtualKeyboardOnFocus property
	/// </summary>
	[SuppressMessage( "Microsoft.Security", "CA2104:DoNotDeclareReadOnlyMutableReferenceTypes" )]
	public static readonly BindableProperty PaddingProperty =
		BindableProperty.Create( nameof( Padding ), typeof( Thickness ), typeof( ExtendedEntry ), new Thickness( 10, 0, 0, 0 ) );


	/// <summary>
	///     The KeyboardVisible property
	/// </summary>
	[SuppressMessage( "Microsoft.Security", "CA2104:DoNotDeclareReadOnlyMutableReferenceTypes" )]
	public static readonly BindableProperty KeyboardVisibleProperty =
		BindableProperty.Create( nameof( KeyboardVisible ), typeof( bool ), typeof( ExtendedEntry ), false, BindingMode.Default, propertyChanged: KeyboardVisiblePropertyChanged );


	/// <summary>
	///     KeyboardVisible property
	/// </summary>
	public bool KeyboardVisible
	{
		get => (bool)GetValue( KeyboardVisibleProperty );
		set => SetValue( KeyboardVisibleProperty, value );
	}


	/// <summary>
	///     Text Padding property
	/// </summary>
	public Thickness Padding
	{
		get => (Thickness)GetValue( PaddingProperty );
		set => SetValue( PaddingProperty, value );
	}


	public IVirtualKeyboard? VirtualKeyboardHandler { get; set; }

	public bool ShowVirtualKeyboardOnFocus
	{
		get => (bool)GetValue( ShowVirtualKeyboardOnFocusProperty );
		set => SetValue( ShowVirtualKeyboardOnFocusProperty, value );
	}

	/// <summary>
	///     Gets or sets the Font
	/// </summary>
	public Font Font
	{
		get => (Font)GetValue( FontProperty );
		set => SetValue( FontProperty, value );
	}

	/// <summary>
	///     Gets or sets the X alignment of the text
	/// </summary>
	public TextAlignment XAlign
	{
		get => (TextAlignment)GetValue( XAlignProperty );
		set => SetValue( XAlignProperty, value );
	}

	/// <summary>
	///     Gets or sets if the border should be shown or not
	/// </summary>
	public bool HasBorder
	{
		get => (bool)GetValue( HasBorderProperty );
		set => SetValue( HasBorderProperty, value );
	}

	/// <summary>
	///     Sets color for placeholder text
	/// </summary>
	public Color PlaceholderTextColor
	{
		get => (Color)GetValue( PlaceholderTextColorProperty );
		set => SetValue( PlaceholderTextColorProperty, value );
	}

	public void OnFocused( object sender, FocusEventArgs e )
	{
		if( e.IsFocused )
		{
			if( !ShowVirtualKeyboardOnFocus )
				HideKeyboard();
		}
	}

	public new bool Focus()
	{
		if( ShowVirtualKeyboardOnFocus )
			ShowKeyboard();
		else
			HideKeyboard();

		return true;
	}

	public void ShowKeyboard()
	{
		VirtualKeyboardHandler?.ShowKeyboard();
	}

	public void HideKeyboard()
	{
		VirtualKeyboardHandler?.HideKeyboard();
	}

	public event EventHandler<EventArgs>? LeftSwipe;
	public event EventHandler<EventArgs>? RightSwipe;

	public void OnLeftSwipe( object sender, EventArgs e )
	{
		LeftSwipe?.Invoke( this, e );
	}

	public void OnRightSwipe( object sender, EventArgs e )
	{
		RightSwipe?.Invoke( this, e );
	}

	public ExtendedEntry()
	{
		Focused += OnFocused;
	}

	private static void ShowVirtualKeyboardOnFocusPropertyChanged( BindableObject bindable, object oldValue, object newValue )
	{
		if( bindable is ExtendedEntry E && newValue is bool Show )
		{
			if( E.IsFocused && Show )
				E.ShowKeyboard();
			else
				E.HideKeyboard();
		}
	}

	private static void KeyboardVisiblePropertyChanged( BindableObject bindable, object oldValue, object newValue )
	{
		if( bindable is ExtendedEntry E && newValue is bool Visible )
		{
			if( Visible )
				E.ShowKeyboard();
			else
				E.HideKeyboard();
		}
	}
}