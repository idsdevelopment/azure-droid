﻿#nullable enable

namespace XamarinAndroidEntry;

public class SoftwareKeyboardEventArgs : EventArgs
{
	public int KeyboardHeight { get; }

	public SoftwareKeyboardEventArgs( int keyboardHeight )
	{
		KeyboardHeight = keyboardHeight;
	}
}