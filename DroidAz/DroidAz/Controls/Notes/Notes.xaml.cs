﻿#nullable enable

namespace DroidAz.Controls;

[XamlCompilation( XamlCompilationOptions.Compile )]
public partial class Notes : ContentView
{
	public Notes()
	{
		InitializeComponent();
	}

#region Buttons
	private bool InButtonClick;

	private void OkButton_OnClicked( object sender, EventArgs e )
	{
		if( !InButtonClick )
		{
			InButtonClick = true;

			try
			{
				Text = Editor.Text;

				if( OnEditComplete is { } Oc )
				{
					if( Oc.CanExecute( this ) )
						Oc.Execute( this );
				}
			}
			finally
			{
				InButtonClick = false;
			}
		}
	}

	private void CancelButton_OnClicked( object sender, EventArgs e )
	{
		if( !InButtonClick )
		{
			InButtonClick = true;

			try
			{
				if( OnEditCanceled is { } Oc )
				{
					if( Oc.CanExecute( this ) )
						Oc.Execute( this );
				}
			}
			finally
			{
				InButtonClick = false;
			}
		}
	}
#endregion

#region Title
	public static readonly BindableProperty TitleProperty = BindableProperty.Create( nameof( Title ),
	                                                                                 typeof( string ),
	                                                                                 typeof( Notes ),
	                                                                                 "Notes",
	                                                                                 propertyChanged: TitleChanged
	                                                                               );

	private static void TitleChanged( BindableObject bindable, object oldValue, object newValue )
	{
		if( bindable is Notes N && newValue is string T )
			N.TitleLabel.Text = T;
	}

	public string Title
	{
		get => (string)GetValue( TitleProperty );
		set => SetValue( TitleProperty, value );
	}
#endregion

#region Text
	public static readonly BindableProperty TextProperty = BindableProperty.Create( nameof( Text ),
	                                                                                typeof( string ),
	                                                                                typeof( Notes ),
	                                                                                "",
	                                                                                BindingMode.TwoWay,
	                                                                                propertyChanged: TextChanged
	                                                                              );

	private static void TextChanged( BindableObject bindable, object oldValue, object newValue )
	{
		if( bindable is Notes {InButtonClick: false} N && newValue is string T )
			N.Editor.Text = T;
	}

	public string Text
	{
		get => (string)GetValue( TextProperty );
		set => SetValue( TextProperty, value );
	}
#endregion

#region OnEditCanceled
	public static readonly BindableProperty OnEditCanceledProperty = BindableProperty.Create( nameof( OnEditCanceled ),
	                                                                                          typeof( ICommand ),
	                                                                                          typeof( Notes )
	                                                                                        );

	public ICommand? OnEditCanceled
	{
		get => (ICommand?)GetValue( OnEditCanceledProperty );
		set => SetValue( OnEditCanceledProperty, value );
	}
#endregion

#region OnEditComplete
	public static readonly BindableProperty OnEditCompleteProperty = BindableProperty.Create( nameof( OnEditComplete ),
	                                                                                          typeof( ICommand ),
	                                                                                          typeof( WaitTimer )
	                                                                                        );

	public ICommand? OnEditComplete
	{
		get => (ICommand?)GetValue( OnEditCompleteProperty );
		set => SetValue( OnEditCompleteProperty, value );
	}
#endregion
}