﻿#nullable enable

using DroidAz.Dialogs;

namespace DroidAz.Controls.Default.PiecesList;

[XamlCompilation( XamlCompilationOptions.Compile )]
public partial class PiecesList : ContentView
{
	private readonly PiecesListViewModel Model;

	public Action<string>? OnBarcodeNotFound;

	public PiecesList()
	{
		InitializeComponent();

		var M = Model = (PiecesListViewModel)Root.BindingContext;

		M.OnBarcodeNotFound = barcode =>
							  {
								  if( OnBarcodeNotFound is not null )
									  OnBarcodeNotFound( barcode );
								  else
									  Sounds.PlayBadScanSound();
							  };
	}

	private async void Cell_OnTapped( object sender, EventArgs e )
	{
		if( ListView.SelectedItem is PieceItemDisplayEntry {Scanned: > 0} )
		{
			if( await ResetCountDialog.Execute() )
				Model.ResetSelectedItem();
		}
	}

#region Counts
	public static readonly BindableProperty TotalScannedProperty = BindableProperty.Create( nameof( TotalScanned ),
																							typeof( decimal ),
																							typeof( PiecesList ),
																							(decimal)0,
																							BindingMode.TwoWay );

	public decimal TotalScanned
	{
		get => (decimal)GetValue( TotalScannedProperty );
		set => SetValue( TotalScannedProperty, value );
	}
#endregion

#region Trips
	public static readonly BindableProperty TripDisplayEntriesProperty = BindableProperty.Create( nameof( TripDisplayEntries ),
																								  typeof( IEnumerable<TripDisplayEntry> ),
																								  typeof( PiecesList ),
																								  new ObservableCollection<TripDisplayEntry>(),
																								  propertyChanged: TripDisplayEntriesChanged );

	public IEnumerable<TripDisplayEntry> TripDisplayEntries
	{
		get => (IEnumerable<TripDisplayEntry>)GetValue( TripDisplayEntriesProperty );
		set => SetValue( TripDisplayEntriesProperty, value );
	}

	private static void TripDisplayEntriesChanged( BindableObject bindable, object oldValue, object newValue )
	{
		if( bindable is PiecesList Pl && newValue is IEnumerable<TripDisplayEntry> Trips )
			Pl.Model.TripDisplayEntries = Trips is ObservableCollection<TripDisplayEntry> T ? T : new ObservableCollection<TripDisplayEntry>( Trips );
	}

	public static readonly BindableProperty IsPickupProperty = BindableProperty.Create( nameof( IsPickup ),
																						typeof( bool ),
																						typeof( PiecesList ),
																						true,
																						propertyChanged: IsPickupPropertyChanged );

	public bool IsPickup
	{
		get => (bool)GetValue( IsPickupProperty );
		set => SetValue( IsPickupProperty, value );
	}

	private static void IsPickupPropertyChanged( BindableObject bindable, object oldValue, object newValue )
	{
		if( bindable is PiecesList Pl && newValue is bool IsPickup )
			Pl.Model.IsPickup = IsPickup;
	}
#endregion

#region Events
	public static readonly BindableProperty OnCancelProperty = BindableProperty.Create( nameof( OnCancel ),
																						typeof( ICommand ),
																						typeof( PiecesList ) );

	public ICommand OnCancel
	{
		get => (ICommand)GetValue( OnCancelProperty );
		set => SetValue( OnCancelProperty, value );
	}

	private async void CancelButton_OnClicked( object sender, EventArgs e )
	{
		Model.IsScanning = false;

		if( OnCancel.CanExecute( this ) && await CancelDialog.Execute() )
			OnCancel.Execute( this );
		Model.SelectedItem     = null;
		Model.ShowCancelButton = false;
	}

	public static readonly BindableProperty OnFinaliseProperty = BindableProperty.Create( nameof( OnFinalise ),
																						  typeof( ICommand ),
																						  typeof( PiecesList ) );

	public ICommand OnFinalise
	{
		get => (ICommand)GetValue( OnFinaliseProperty );
		set => SetValue( OnFinaliseProperty, value );
	}

	private void FinaliseButton_OnClicked( object sender, EventArgs e )
	{
		Model.IsScanning = false;
		TotalScanned     = Model.TotalScanned;

		if( OnFinalise.CanExecute( this ) )
			OnFinalise.Execute( this );
	}
#endregion
}