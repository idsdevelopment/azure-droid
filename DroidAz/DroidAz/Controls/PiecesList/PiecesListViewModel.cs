﻿#nullable enable

using Preferences = Globals.Preferences;

namespace DroidAz.Controls.Default.PiecesList;

public class PieceItemDisplayEntry : TripItemsDisplayEntry
{
	public override decimal Balance
	{
		get => base.Balance;

		set
		{
			IsVisible    = value != 0;
			InError      = value < 0;
			base.Balance = value;
			OnPropertyChanged( nameof( InError ) );
		}
	}

	public bool InError
	{
		get => _InError;
		set
		{
			_InError = value;
			OnPropertyChanged( nameof( InError ) );
			OnPropertyChanged( nameof( NotInError ) );
		}
	}

	public bool NotInError => !InError;

	private bool _InError;

	public PieceItemDisplayEntry()
	{
	}

	public PieceItemDisplayEntry( TripItemsDisplayEntry item ) : base( item )
	{
	}
}

public class PiecesListViewModel : ViewModelBase
{
	private bool AllowOverScans,
	             AllowUnderScans;

	public Action<string>? OnBarcodeNotFound;

#region Counts
	public decimal TotalScanned,
	               TotalPieces;
#endregion


	protected override void OnInitialised()
	{
		base.OnInitialised();

		AllowUnderScans = Preferences.AllowUnderScans;
		AllowOverScans  = Preferences.AllowOverScans;
	}

#region Barcode
	public string Barcode
	{
		get { return Get( () => Barcode, "" ); }
		set { Set( () => Barcode, value ); }
	}


	public bool IsScanning
	{
		get { return Get( () => IsScanning, false ); }
		set { Set( () => IsScanning, value ); }
	}

	[DependsUpon50( nameof( Barcode ) )]
	public void WhenBarcodeChanges()
	{
		try
		{
			var BCode = Barcode;
			Barcode = "";

			if( BCode.IsNotNullOrWhiteSpace() )
			{
				foreach( var Entry in Items )
				{
					if( Entry.Barcode == BCode )
					{
						Entry.Scanned++;
						Entry.Balance--;

						TotalScanned++;

						bool Enab;

						if( TotalPieces > 0 )
						{
							var OverScanned = ( TotalScanned > TotalPieces ) || Entry.InError;

							if( OverScanned )
								Enab = AllowOverScans;
							else if( AllowUnderScans && ( TotalScanned > 0 ) )
								Enab = true;
							else
								Enab = TotalScanned == TotalPieces;
						}
						else
							Enab = IsPickup;

						EnableFinaliseButton = Enab;
						return;
					}
				}
				OnBarcodeNotFound?.Invoke( BCode );
			}
		}
		finally
		{
			IsScanning = true;
		}
	}
#endregion

#region Buttons
	public bool ShowCancelButton
	{
		get { return Get( () => ShowCancelButton, true ); }
		set { Set(
				  () => ShowCancelButton, value ); }
	}


	public bool EnableFinaliseButton
	{
		get { return Get( () => EnableFinaliseButton, false ); }
		set { Set( () => EnableFinaliseButton, value ); }
	}
#endregion

#region Trip Items
	public ObservableCollection<PieceItemDisplayEntry> Items
	{
		get { return Get( () => Items, [] ); }
		set { Set( () => Items, value ); }
	}

	public PieceItemDisplayEntry? SelectedItem
	{
		get { return Get( () => SelectedItem, (PieceItemDisplayEntry?)null ); }
		set { Set( () => SelectedItem, value ); }
	}

	public void ResetSelectedItem()
	{
		var Item = SelectedItem;

		if( Item is not null )
		{
			SelectedItem = null;

			var Scanned = Item.Scanned;
			Item.Scanned =  0;
			Item.Balance =  Item.Pieces;
			TotalScanned -= Scanned;
		}
	}
#endregion


#region Trips
	public bool IsPickup
	{
		get { return Get( () => IsPickup, true ); }
		set { Set( () => IsPickup, value ); }
	}

	public ObservableCollection<TripDisplayEntry> TripDisplayEntries
	{
		get { return Get( () => TripDisplayEntries, [] ); }
		set { Set( () => TripDisplayEntries, value ); }
	}

	public static string GetCode( TripItemsDisplayEntry entry ) => ( entry.Barcode.IsNotNullOrEmpty() ? entry.Barcode : entry.ItemCode ).Trim();


	[DependsUpon( nameof( TripDisplayEntries ) )]
	public void WhenTripDisplayEntriesChanges()
	{
		TotalPieces          = TotalScanned = 0;
		EnableFinaliseButton = IsPickup && Preferences.AllowUnderScans;

		var Entries = TripDisplayEntries;

		if( Entries.Count > 0 )
		{
			var Itms = new Dictionary<string, PieceItemDisplayEntry>();

			void Add( PieceItemDisplayEntry entry )
			{
				var Code = GetCode( entry );

				if( entry.Pieces <= 0 )
					entry.Pieces = 1;

				var Pieces = entry.Pieces;
				TotalPieces += Pieces;

				if( !Itms.TryGetValue( Code, out var Entry ) )
					Itms.Add( Code, Entry = entry );
				else
					Entry.Pieces += Pieces;

				Entry.Balance += Pieces;
			}

			// Consolidate Trip Items
			foreach( var TripDisplayEntry in Entries )
			{
				if( TripDisplayEntry.HasItems )
				{
					foreach( var DisplayEntry in TripDisplayEntry.Items )
					{
						DisplayEntry.Balance = 0;
						Add( new PieceItemDisplayEntry( DisplayEntry ) );
					}
				}
				else
				{
					Add( new PieceItemDisplayEntry
					     {
						     Barcode = TripDisplayEntry.TripId,
						     Pieces  = TripDisplayEntry.Pieces
					     } );
				}
			}

			var TripItems = new ObservableCollection<PieceItemDisplayEntry>();

			foreach( var DisplayEntry in Itms )
			{
				var Item = DisplayEntry.Value;
				Item.Barcode = DisplayEntry.Key;
				TripItems.Add( Item );
			}

			Items      = TripItems;
			IsScanning = true;
		}
	}
#endregion
}