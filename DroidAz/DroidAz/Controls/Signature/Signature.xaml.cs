﻿#nullable enable

namespace DroidAz.Controls.Default;

[XamlCompilation( XamlCompilationOptions.Compile )]
public partial class Signature : ContentView
{
	private readonly SignatureViewModel Model;
	public           OnCancelEvent?     OnCancel;
	public           OnOkEvent?         OnOk;

	public Signature()
	{
		InitializeComponent();

		var M = Model = (SignatureViewModel)Root.BindingContext;

		M.OnGetPoints = () => SignaturePad.Points;

		M.OnOk = signature =>
		         {
			         OnOk?.Invoke( signature );
		         };

		M.OnCancel = () =>
		             {
			             OnCancel?.Invoke();
		             };
	}

	private void SignaturePad_OnStrokeCompleted( object sender, EventArgs e )
	{
		Model.OnEnableOk();
	}

	private void SignaturePad_OnCleared( object sender, EventArgs e )
	{
		Model.OnEnableOk();
	}

	public void Clear()
	{
		Model.EnableOk = false;
		SignaturePad.Clear();
	}

	public delegate void OnCancelEvent();

	public delegate void OnOkEvent( SignatureResult signature );
}