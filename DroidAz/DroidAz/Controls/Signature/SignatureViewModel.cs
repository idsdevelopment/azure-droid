﻿#nullable enable

namespace DroidAz.Controls.Default;

public class SignatureResult
{
	public int Height,
	           Width;

	public string Points = "";
}

public class SignatureViewModel : ViewModelBase
{
	private const int MARGIN  = 5,
	                  QUALITY = 500;


	public bool EnableOk
	{
		get { return Get( () => EnableOk, false ); }
		set { Set( () => EnableOk, value ); }
	}

#region Events
	public delegate void OnOkEvent( SignatureResult signature );

	public OnOkEvent? OnOk;

	public delegate void OnCancelEvent();

	public OnCancelEvent? OnCancel;

	public delegate IEnumerable<Point> GetPointsEvent();

	public GetPointsEvent? OnGetPoints;

	public void OnEnableOk()
	{
		if( OnGetPoints is not null )
		{
			var Points = OnGetPoints();
			EnableOk = Points.Count() > 20;
		}
		else
			EnableOk = false;
	}

	public ICommand OkButtonClick => Commands[ nameof( OkButtonClick ) ];

	public bool CanExecute_OkButtonClick() => EnableOk;

	public void Execute_OkButtonClick()
	{
		if( OnOk is not null && OnGetPoints is not null )
		{
			var Points = OnGetPoints();

			double MaxX = float.NegativeInfinity,
			       MaxY = float.NegativeInfinity,
			       MinX = float.PositiveInfinity,
			       MinY = float.PositiveInfinity;

			var Enumerable = Points as Point[] ?? Points.ToArray();

			foreach( var (X, Y) in Enumerable )
			{
				MaxX = Math.Max( MaxX, X );
				MaxY = Math.Max( MaxY, Y );

				MinX = Math.Min( MinX, X );
				MinY = Math.Min( MinY, Y );
			}

			var Width  = (int)( MaxX - MinX );
			var Height = (int)( MaxY - MinY );

			var Longest = Math.Max( Width, Height );
			var Scale   = Longest >= QUALITY ? (float)QUALITY / Longest : 1;

			var Result = new SignatureResult
			             {
				             Width  = (int)( Width * Scale ) + ( 2 * MARGIN ),
				             Height = (int)( Height * Scale ) + ( 2 * MARGIN )
			             };

			var XAdj = (int)Math.Round( -MinX * Scale, MidpointRounding.AwayFromZero ) + MARGIN;
			var YAdj = (int)Math.Round( -MinY * Scale, MidpointRounding.AwayFromZero ) + MARGIN;

			var SigPoints = new StringBuilder();
			var First     = true;

			void PointString( bool mouseDown, Point point )
			{
				if( !First )
					SigPoints.Append( ',' );
				else
					First = false;

				var StrX = ( (int)point.X + XAdj ).ToString();
				var StrY = ( (int)point.Y + YAdj ).ToString();
				SigPoints.Append( mouseDown ? "1:" : "0:" ).Append( StrX ).Append( ':' ).Append( StrY );
			}

			var NeedMoveTo = true;
			var Last       = Point.Zero;

			foreach( var Point in Enumerable )
			{
				var P = Point;

				// If empty discontinuous line
				if( P.IsEmpty )
					NeedMoveTo = true;
				else
				{
					P.X = (float)Math.Round( P.X * Scale, MidpointRounding.AwayFromZero );
					P.Y = (float)Math.Round( P.Y * Scale, MidpointRounding.AwayFromZero );

					if( NeedMoveTo )
					{
						NeedMoveTo = false;

						if( !Last.IsEmpty )
							PointString( false, Last ); // Move from
						PointString( false, P );        // Move To
						PointString( true, P );
					}
					else if( ( (int)Last.X != P.X ) || ( (int)Last.Y != P.Y ) ) // Skip Duplicates
						PointString( true, P );
				}

				Last = P;
			}

			if( !Last.IsEmpty )
				PointString( false, Last ); // Force Mouse Up At End

			Result.Points = SigPoints.ToString();
			OnOk( Result );
		}
	}

	public ICommand CancelButtonClick => Commands[ nameof( CancelButtonClick ) ];

	public void Execute_CancelButtonClick()
	{
		OnCancel?.Invoke();
	}
#endregion
}