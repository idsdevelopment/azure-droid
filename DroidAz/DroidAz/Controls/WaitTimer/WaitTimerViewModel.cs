﻿#nullable enable

using Timer = System.Timers.Timer;

namespace DroidAz.Controls;

public class WaitTimerViewModel : ViewModelBase
{
	private const string START_TIMER = "Start Timer",
	                     STOP_TIMER  = "Stop Timer";

	public int TotalSeconds { get; private set; }


	public string ButtonText
	{
		get { return Get( () => ButtonText, START_TIMER ); }
		set { Set( () => ButtonText, value ); }
	}

#region Timer
	private Timer Timer = null!;
#endregion

	protected override void OnInitialised()
	{
		base.OnInitialised();

		Timer = new Timer( 1_000 )
		        {
			        AutoReset = true
		        };

		Timer.Elapsed += ( _, _ ) =>
		                 {
			                 var T = new TimeSpan( 0, 0, 0, ++TotalSeconds );
			                 Hours   = $"{T.Hours:00}";
			                 Minutes = $"{T.Minutes:00}";
			                 Seconds = $"{T.Seconds:00}";
		                 };
	}

#region Clear
	private void Clear()
	{
		TotalSeconds = 0;
		Hours        = "00";
		Minutes      = "00";
		Seconds      = "00";
	}
#endregion

#region Active
	public bool Active
	{
		get { return Get( () => Active, false ); }
		set { Set( () => Active, value ); }
	}

	[DependsUpon( nameof( Active ) )]
	public void WhenActiveChanges()
	{
	}
#endregion

#region Timer Text
	public string Hours
	{
		get { return Get( () => Hours, "00" ); }
		set { Set( () => Hours, value ); }
	}


	public string Minutes
	{
		get { return Get( () => Minutes, "00" ); }
		set { Set( () => Minutes, value ); }
	}


	public string Seconds
	{
		get { return Get( () => Seconds, "00" ); }
		set { Set( () => Seconds, value ); }
	}
#endregion

#region On Start
	public Action? OnTimerStopped = null;

	public ICommand OnStart => Commands[ nameof( OnStart ) ];

	public bool CanExecute_OnStart() => true;

	public void Execute_OnStart()
	{
		var T = Timer;

		if( T.Enabled ) // Running
		{
			T.Stop();
			ButtonText = START_TIMER;
			OnTimerStopped?.Invoke();
			Clear();
		}
		else
		{
			ButtonText   = STOP_TIMER;
			TotalSeconds = 0;
			T.Start();
		}
	}
#endregion


#region On Cancel
	public Action?  OnTimerCanceled = null;
	public ICommand OnCancel => Commands[ nameof( OnCancel ) ];

	public bool CanExecute_OnCancel() => true;

	public void Execute_OnCancel()
	{
		Timer.Stop();
		ButtonText = START_TIMER;
		Clear();
		OnTimerCanceled?.Invoke();
	}
#endregion

#region Title
	public string Title
	{
		get { return Get( () => Title, "" ); }
		set { Set( () => Title, value ); }
	}


	public bool TitleVisible
	{
		get { return Get( () => TitleVisible, false ); }
		set { Set( () => TitleVisible, value ); }
	}

	[DependsUpon( nameof( Title ) )]
	public void WhenTitleChanges()
	{
		TitleVisible = Title.IsNotNullOrWhiteSpace();
	}
#endregion
}