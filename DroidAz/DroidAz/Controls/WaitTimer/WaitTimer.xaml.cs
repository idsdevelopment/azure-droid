﻿#nullable enable

namespace DroidAz.Controls;

[XamlCompilation( XamlCompilationOptions.Compile )]
public partial class WaitTimer : ContentView
{
	private readonly WaitTimerViewModel Model;

	public WaitTimer()
	{
		InitializeComponent();
		var M = Model = (WaitTimerViewModel)Root.BindingContext;

		M.OnTimerStopped = () =>
		                   {
			                   var Ts = M.TotalSeconds;
			                   ValueInSeconds = Ts;

			                   if( OnWaitTimer is { } Owt && Owt.CanExecute( Ts ) )
				                   Owt.Execute( Ts );
		                   };

		M.OnTimerCanceled = () =>
		                    {
			                    if( OnOnWaitTimerCanceled is { } Owc && Owc.CanExecute( 0 ) )
				                    Owc.Execute( 0 );
		                    };
	}

#region Visibility
	public new static readonly BindableProperty IsVisibleProperty = BindableProperty.Create( nameof( IsVisible ),
	                                                                                         typeof( bool ),
	                                                                                         typeof( WaitTimer ),
	                                                                                         true,
	                                                                                         propertyChanged: IsVisiblePropertyChanged
	                                                                                       );

	private static void IsVisiblePropertyChanged( BindableObject bindable, object oldValue, object newValue )
	{
		if( bindable is WaitTimer W and ContentView Cv && newValue is bool Vis )
		{
			Cv.IsVisible = Vis;

			if( Vis && W.StartTimerWhenVisible )
				W.Model.Execute_OnStart();
		}
	}

	public new bool IsVisible
	{
		get => (bool)GetValue( IsVisibleProperty );
		set => SetValue( IsVisibleProperty, value );
	}


	public static readonly BindableProperty StartTimerWhenVisibleProperty = BindableProperty.Create( nameof( StartTimerWhenVisible ),
	                                                                                                 typeof( bool ),
	                                                                                                 typeof( WaitTimer ),
	                                                                                                 true
	                                                                                               );

	public bool StartTimerWhenVisible
	{
		get => (bool)GetValue( StartTimerWhenVisibleProperty );
		set => SetValue( StartTimerWhenVisibleProperty, value );
	}
#endregion

#region Title
	public static readonly BindableProperty TitleProperty = BindableProperty.Create( nameof( Title ),
	                                                                                 typeof( string ),
	                                                                                 typeof( WaitTimer ),
	                                                                                 "",
	                                                                                 propertyChanged: TitleChanged
	                                                                               );

	private static void TitleChanged( BindableObject bindable, object oldValue, object newValue )
	{
		if( bindable is WaitTimer W && newValue is string T )
			W.Model.Title = T;
	}

	public string Title
	{
		get => (string)GetValue( TitleProperty );
		set => SetValue( TitleProperty, value );
	}
#endregion

#region OnWaitWimer
	public static readonly BindableProperty OnOnWaitTimerProperty = BindableProperty.Create( nameof( OnWaitTimer ),
	                                                                                         typeof( ICommand ),
	                                                                                         typeof( WaitTimer )
	                                                                                       );

	public ICommand? OnWaitTimer
	{
		get => (ICommand?)GetValue( OnOnWaitTimerProperty );
		set => SetValue( OnOnWaitTimerProperty, value );
	}
#endregion

#region OnWaitWimerCanceled
	public static readonly BindableProperty OnOnWaitTimerCanceledProperty = BindableProperty.Create( nameof( OnOnWaitTimerCanceled ),
	                                                                                                 typeof( ICommand ),
	                                                                                                 typeof( WaitTimer )
	                                                                                               );

	public ICommand? OnOnWaitTimerCanceled
	{
		get => (ICommand?)GetValue( OnOnWaitTimerCanceledProperty );
		set => SetValue( OnOnWaitTimerCanceledProperty, value );
	}
#endregion

#region ValueInSeconds
	public static readonly BindableProperty ValueInSecondsProperty = BindableProperty.Create( nameof( ValueInSeconds ),
	                                                                                          typeof( int ),
	                                                                                          typeof( WaitTimer ),
	                                                                                          0,
	                                                                                          BindingMode.OneWayToSource
	                                                                                        );

	public int ValueInSeconds
	{
		get => (int)GetValue( ValueInSecondsProperty );
		set => SetValue( ValueInSecondsProperty, value );
	}
#endregion
}