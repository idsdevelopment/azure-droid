﻿#nullable enable

using Preferences = Globals.Preferences;

namespace DroidAz.Controls.Default.Location;

public class LocationViewModel : TripsBaseViewModel
{
	public Location Owner = null!;

#region Actions
	public Action? OnSingleTap,
				   OnDoubleTap;

	public ICommand OnTripListSingleTap => Commands[ nameof( OnTripListSingleTap ) ];

	public ICommand OnTripListDoubleTap => Commands[ nameof( OnTripListDoubleTap ) ];

	public void Execute_OnTripListSingleTap()
	{
		OnSingleTap?.Invoke();
	}

	public void Execute_OnTripListDoubleTap()
	{
		OnDoubleTap?.Invoke();
	}
#endregion

#region Batch Mode
	private void HideAll()
	{
		AllDeliveriesVisible = false;
		AllPickupsVisible    = false;
	}

	private void BatchModeShowAll()
	{
		var Items   = LocationItems;
		var AllPVis = false;
		var AllDVis = false;

		if( Preferences.LocationBatchMode && Items is not null && ( Items.Count > 1 ) )
		{
			var Loc = Items.First().DisplayLocation;

			var LocTrips = ( from I in Items
							 where I.DisplayLocation == Loc
							 select I ).ToList();

			if( LocTrips.Count > 1 )
			{
				foreach( var LocTrip in LocTrips )
				{
					switch( LocTrip.Status )
					{
					case STATUS.DISPATCHED:
						AllPVis = true;
						break;

					case STATUS.PICKED_UP:
						AllDVis = true;
						break;
					}
				}
			}
		}

		AllDeliveriesVisible = AllDVis;
		AllPickupsVisible    = AllPVis;
	}

	public bool AllPickupsVisible
	{
		get { return Get( () => AllPickupsVisible, false ); }
		set { Set( () => AllPickupsVisible, value ); }
	}

	[DelayChangeNotification( Delay = 200 )]
	public bool AllDeliveriesVisible
	{
		get { return Get( () => AllDeliveriesVisible, false ); }
		set { Set( () => AllDeliveriesVisible, value ); }
	}

	public ICommand OnBatchPickupMode => Commands[ nameof( OnBatchPickupMode ) ];

	public void Execute_OnBatchPickupMode()
	{
		var Bm = Owner.OnBatchPickupMode;

		if( Bm is not null && Bm.CanExecute( Owner ) )
			Bm.Execute( Owner );
	}

	public ICommand OnBatchDeliveryMode => Commands[ nameof( OnBatchDeliveryMode ) ];

	public void Execute_OnBatchDeliveryMode()
	{
		var Bm = Owner.OnBatchDeliveryMode;

		if( Bm is not null && Bm.CanExecute( Owner ) )
			Bm.Execute( Owner );
	}

	public ObservableCollection<TripDisplayEntry>? LocationItems
	{
		get { return Get( () => LocationItems, (ObservableCollection<TripDisplayEntry>?)null ); }
		set { Set( () => LocationItems, value ); }
	}

	[DependsUpon( nameof( LocationItems ) )]
	public void WhenLocationItemsChanges()
	{
		Owner.LocationItems = LocationItems;
		BatchModeShowAll();
	}
#endregion

#region Cancel
	public bool CancelVisible
	{
		get { return Get( () => CancelVisible, false ); }
		set { Set( () => CancelVisible, value ); }
	}

	public ICommand Cancel => Commands[ nameof( Cancel ) ];

	public void Execute_Cancel()
	{
		Barcode                   = "";
		Owner.LocationCompanyName = "";
		SelectedItem              = null;
		LocationItems             = null;
		CancelVisible             = false;
		LocationTripIds           = [];
		HideAll();
		Owner.IsScanning = false;
	}
#endregion

#region Trips
	public STATUS[] StatusFilter
	{
		get { return Get( () => StatusFilter, [STATUS.DISPATCHED, STATUS.PICKED_UP] ); }
		set { Set( () => StatusFilter, value ); }
	}

	public new ObservableCollection<TripDisplayEntry> TripDisplayEntries
	{
		get { return Get( () => TripDisplayEntries, [] ); }
		set { Set( () => TripDisplayEntries, value ); }
	}

	[DependsUpon( nameof( TripDisplayEntries ) )]
	public void WhenTripDisplayEntriesChange()
	{
		Owner.TripDisplayEntries = TripDisplayEntries;
	}

	public TripDisplayEntry? SelectedItem
	{
		get { return Get( () => SelectedItem, (TripDisplayEntry?)null ); }
		set { Set( () => SelectedItem, value ); }
	}

	[DependsUpon( nameof( SelectedItem ) )]
	public void WhenSelectedItemChanges()
	{
		var Item = SelectedItem;

		Owner.SelectedItem = Item;

		if( Item is null )
		{
			if( LocationItems is not null && ( LocationItems.Count > 0 ) )
			{
				BatchModeShowAll();
				return;
			}
		}
		else
			LocationItems = null;

		HideAll();
	}

	public List<string> LocationTripIds
	{
		get { return Get( () => LocationTripIds, [] ); }
		set { Set( () => LocationTripIds, value ); }
	}

	[DependsUpon( nameof( LocationTripIds ) )]
	public void WhenLocationTripIdsChange()
	{
		Owner.LocationTripIds = LocationTripIds;
	}

	public LocationViewModel() : base( false )
	{
	}

	public bool WaitingForTrips
	{
		get { return Get( () => WaitingForTrips, true ); }
		set { Set( () => WaitingForTrips, value ); }
	}

	[DependsUpon( nameof( WaitingForTrips ) )]
	public void WhenWaitingForTripsChanges()
	{
		if( !WaitingForTrips )
		{
			BarcodeScannerEnabled = true;
			IsScanning            = true;
		}
	}
#endregion

#region Barcode
	public bool BarcodeScannerEnabled
	{
		get { return Get( () => BarcodeScannerEnabled, false ); }
		set { Set( () => BarcodeScannerEnabled, value ); }
	}

	public bool IsScanning
	{
		get { return Get( () => IsScanning, false ); }
		set { Set( () => IsScanning, value ); }
	}

	public string Barcode
	{
		get { return Get( () => Barcode, "" ); }
		set { Set( () => Barcode, value ); }
	}

	public Func<Task> NoTripsAtThisLocationError = null!,
					  OutSideFence               = null!;

	[DependsUpon( nameof( Barcode ) )]
	public async Task WhenBarcodeChanges()
	{
		var Entries = TripDisplayEntries;

		var BCode = Barcode.Trim();
		Barcode = "";

		if( BCode != "" )
		{
			BCode = Owner.OnFormatBarcode( BCode, Entries );

			var Items = Owner.OnFilterByBarcode( Entries, BCode );

			if( Items.Count > 0 )
			{
				Items = new ObservableCollection<TripDisplayEntry>( from E in Items
																	where E.InsideFence
																	select E );

				if( Items.Count > 0 )
				{
					var LocationItem = Items[ 0 ];

					Owner.LocationCompanyName = LocationItem.Status == STATUS.PICKED_UP ? LocationItem.DeliveryCompany : LocationItem.PickupCompany;

					IsScanning    = false;
					LocationItems = Items;

					LocationTripIds = ( from T in Items
										select T.TripId ).ToList();

					CancelVisible = true;
					BatchModeShowAll();
					return;
				}
				await OutSideFence.Invoke();
			}
			else
				await NoTripsAtThisLocationError();
		}
		IsScanning = true;
	}
#endregion
}