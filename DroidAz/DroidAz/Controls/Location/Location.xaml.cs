﻿#nullable enable

using DroidAz.Dialogs;

namespace DroidAz.Controls.Default.Location;

[XamlCompilation( XamlCompilationOptions.Compile )]
public partial class Location : ContentView
{
	public Location()
	{
		InitializeComponent();

		var M = (LocationViewModel)LocScanner.BindingContext;
		Model = M;

		M.Owner = this;

		M.NoTripsAtThisLocationError = async () =>
									   {
										   await ShowError( "NoItemsAtLocation" );
									   };

		M.OutSideFence = async () =>
						 {
							 await ShowError( "OutsideGeoFence" );
						 };

		void DoCommand( ICommand? command )
		{
			if( command is not null && command.CanExecute( this ) )
				command.Execute( this );
		}

		M.OnSingleTap = () =>
						{
							DoCommand( ItemTapped );
						};

		M.OnDoubleTap = () =>
						{
							DoCommand( ItemDoubleTapped );
						};
	}

	public readonly LocationViewModel Model;

	private static async Task ShowError( string messageKey )
	{
		Sounds.PlayBadScanSound();
		await ErrorDialog.Execute( messageKey );
	}

	public void Cancel( bool barcodeScannerEnabled = true )
	{
		var C = Model.Cancel;

		if( C.CanExecute( this ) )
		{
			Model.BarcodeScannerEnabled = barcodeScannerEnabled;
			Model.Execute_Cancel();

			C.Execute( this );
		}
	}

#region Scanning
	public void StartScanning()
	{
		IsScanning = false;
		IsScanning = true;
	}

	public static readonly BindableProperty IsScanningProperty = BindableProperty.Create( nameof( IsScanning ),
																						  typeof( bool ),
																						  typeof( Location ),
																						  false,
																						  BindingMode.TwoWay,
																						  propertyChanged: IsScanningPropertyChanged );

	private static void IsScanningPropertyChanged( BindableObject bindable, object oldValue, object newValue )
	{
		if( bindable is Location L && newValue is bool Value )
			L.Model.IsScanning = Value;
	}

	public bool IsScanning
	{
		get => (bool)GetValue( IsScanningProperty );
		set => SetValue( IsScanningProperty, value );
	}
#endregion

#region Virtuals
	public virtual string OnFormatBarcode( string barcode, ObservableCollection<TripDisplayEntry> displayTrips ) => barcode.Trim();

	public virtual ObservableCollection<TripDisplayEntry> OnFilterByBarcode( ObservableCollection<TripDisplayEntry> displayTrips, string barcode ) => new( from T in displayTrips
																																						   where T.DisplayLocation == barcode
																																						   select T );
#endregion

#region Visibility
	public new static readonly BindableProperty IsVisibleProperty = BindableProperty.Create( nameof( IsVisible ),
																							 typeof( bool ),
																							 typeof( Location ),
																							 true,
																							 BindingMode.Default,
																							 propertyChanged: IsVisiblePropertyChanged );

	private static void IsVisiblePropertyChanged( BindableObject bindable, object oldValue, object newValue )
	{
		if( bindable is Location Location && newValue is bool IsVisible )
		{
			( (ContentView)Location ).IsVisible = IsVisible;

			if( !IsVisible )
				Location.LocScanner.IsEnabled = false;
		}
	}

	public new bool IsVisible
	{
		get => (bool)GetValue( IsVisibleProperty );
		set => SetValue( IsVisibleProperty, value );
	}
#endregion

#region Selected Item
	public static readonly BindableProperty LocationItemsProperty = BindableProperty.Create( nameof( LocationItems ),
																							 typeof( ObservableCollection<TripDisplayEntry> ),
																							 typeof( Location ),
																							 new ObservableCollection<TripDisplayEntry>(),
																							 BindingMode.OneWayToSource );

	public ObservableCollection<TripDisplayEntry>? LocationItems
	{
		get => (ObservableCollection<TripDisplayEntry>?)GetValue( LocationItemsProperty );
		set => SetValue( LocationItemsProperty, value );
	}

	public static readonly BindableProperty SelectedItemProperty = BindableProperty.Create( nameof( SelectedItem ),
																							typeof( TripDisplayEntry ),
																							typeof( Location ),
																							null,
																							BindingMode.TwoWay,
																							propertyChanged: SelectedItemPropertyChanged );

	private static void SelectedItemPropertyChanged( BindableObject bindable, object oldValue, object newValue )
	{
		if( bindable is Location T )
		{
			var Item = newValue as TripDisplayEntry; // Item or null
			T.Model.SelectedItem = Item;

			if( Item is not null && T.ItemTapped is { } It && It.CanExecute( T ) )
				It.Execute( T );
		}
	}

	public TripDisplayEntry? SelectedItem
	{
		get => (TripDisplayEntry?)GetValue( SelectedItemProperty );
		set => SetValue( SelectedItemProperty, value );
	}

	public static readonly BindableProperty ItemTappedProperty = BindableProperty.Create( nameof( ItemTapped ),
																						  typeof( ICommand ),
																						  typeof( Location ) );

	public ICommand? ItemTapped
	{
		get => (ICommand?)GetValue( ItemTappedProperty );
		set => SetValue( ItemTappedProperty, value );
	}

	public static readonly BindableProperty ItemDoubleTappedProperty = BindableProperty.Create( nameof( ItemDoubleTapped ),
																								typeof( ICommand ),
																								typeof( Location ),
																								propertyChanged: ItemDoubleTappedPropertyChanged );

	private static void ItemDoubleTappedPropertyChanged( BindableObject bindable, object oldValue, object newValue )
	{
		if( bindable is Location {ItemDoubleTapped: { } DoubleTapped} T )
		{
			if( DoubleTapped.CanExecute( T ) )
				DoubleTapped.Execute( T );
		}
	}

	public ICommand? ItemDoubleTapped
	{
		get => (ICommand?)GetValue( ItemDoubleTappedProperty );
		set => SetValue( ItemDoubleTappedProperty, value );
	}

	public static readonly BindableProperty FilterOnEmptyProperty = BindableProperty.Create( nameof( FilterOnEmpty ),
																							 typeof( bool ),
																							 typeof( Location ),
																							 true,
																							 propertyChanged: FilterOnEmptyPropertyChanged );

	private static void FilterOnEmptyPropertyChanged( BindableObject bindable, object oldValue, object newValue )
	{
		//if( bindable is Location L && newValue is bool Filter )
		//	L.TripList.FilterOnEmpty = Filter;
	}

	public bool FilterOnEmpty
	{
		get => (bool)GetValue( FilterOnEmptyProperty );
		set => SetValue( FilterOnEmptyProperty, value );
	}

	public static readonly BindableProperty LocationTripIdsProperty = BindableProperty.Create( nameof( LocationTripIds ),
																							   typeof( IList<string> ),
																							   typeof( Location ),
																							   new List<string>(),
																							   BindingMode.OneWayToSource
																							 );

	public IList<string> LocationTripIds
	{
		get => (IList<string>)GetValue( LocationTripIdsProperty );
		set => SetValue( LocationTripIdsProperty, value );
	}

	public STATUS[] TripStatusFilter
	{
		get => (STATUS[])GetValue( TripStatusFilterProperty );
		set => SetValue( TripStatusFilterProperty, value );
	}

	public static readonly BindableProperty TripStatusFilterProperty = BindableProperty.Create( nameof( TripStatusFilter ),
																								typeof( STATUS[] ),
																								typeof( Location ),
																								new[] {STATUS.DISPATCHED, STATUS.PICKED_UP},
																								propertyChanged: TripStatusFilterPropertyChanged );

	private static void TripStatusFilterPropertyChanged( BindableObject bindable, object oldValue, object newValue )
	{
		if( bindable is Location L && newValue is STATUS[] Filter )
			L.Model.StatusFilter = Filter;
	}
#endregion

#region Trips
	public static readonly BindableProperty TripDisplayEntriesProperty = BindableProperty.Create( nameof( TripDisplayEntries ),
																								  typeof( ObservableCollection<TripDisplayEntry> ),
																								  typeof( Location ),
																								  new ObservableCollection<TripDisplayEntry>(),
																								  BindingMode.OneWayToSource
																								);

	public ObservableCollection<TripDisplayEntry> TripDisplayEntries
	{
		get => (ObservableCollection<TripDisplayEntry>)GetValue( TripDisplayEntriesProperty );
		set => SetValue( TripDisplayEntriesProperty, value );
	}
#endregion

#region Batch Mode
	public static readonly BindableProperty OnBatchPickupModeProperty = BindableProperty.Create( nameof( OnBatchPickupMode ),
																								 typeof( ICommand ),
																								 typeof( Location ) );

	public ICommand? OnBatchPickupMode
	{
		get => (ICommand?)GetValue( OnBatchPickupModeProperty );
		set => SetValue( OnBatchPickupModeProperty, value );
	}

	public static readonly BindableProperty OnBatchDeliveryModeProperty = BindableProperty.Create( nameof( OnBatchDeliveryMode ),
																								   typeof( ICommand ),
																								   typeof( Location ) );

	public ICommand? OnBatchDeliveryMode
	{
		get => (ICommand?)GetValue( OnBatchDeliveryModeProperty );
		set => SetValue( OnBatchDeliveryModeProperty, value );
	}
#endregion

#region Location
	public static readonly BindableProperty LocationCompanyNameProperty = BindableProperty.Create( nameof( LocationCompanyName ),
																								   typeof( string ),
																								   typeof( Location ),
																								   "",
																								   BindingMode.OneWayToSource
																								 );

	public string LocationCompanyName
	{
		get => (string)GetValue( LocationCompanyNameProperty );
		set => SetValue( LocationCompanyNameProperty, value );
	}
#endregion

#region Placeholder
	public static readonly BindableProperty PlaceholderProperty = BindableProperty.Create( nameof( Placeholder ),
																						   typeof( string ),
																						   typeof( Location ),
																						   "Location",
																						   propertyChanged: PlaceholderPropertyChanged );

	private static void PlaceholderPropertyChanged( BindableObject bindable, object oldValue, object newValue )
	{
		if( bindable is Location Location && newValue is string Pl )
			Location.LocScanner.Placeholder = Pl;
	}

	public string Placeholder
	{
		get => (string)GetValue( PlaceholderProperty );
		set => SetValue( PlaceholderProperty, value );
	}

	public static readonly BindableProperty PlaceholderTextColorProperty = BindableProperty.Create( nameof( Placeholder ),
																									typeof( Color ),
																									typeof( Location ),
																									Color.SlateGray,
																									BindingMode.Default,
																									propertyChanged: PlaceholderTextPropertyChanged );

	private static void PlaceholderTextPropertyChanged( BindableObject bindable, object oldValue, object newValue )
	{
		if( bindable is Location Location && newValue is Color C )
			Location.LocScanner.PlaceholderTextColor = C;
	}

	public Color PlaceholderTextColor
	{
		get => (Color)GetValue( PlaceholderTextColorProperty );
		set => SetValue( PlaceholderTextColorProperty, value );
	}
#endregion
}