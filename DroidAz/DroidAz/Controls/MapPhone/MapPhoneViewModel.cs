﻿#nullable enable

using Xamarin.Essentials;

namespace DroidAz.Controls.MapPhone;

internal class MapPhoneViewModel : ViewModelBase
{
	public string PhoneNumber
	{
		get { return Get( () => PhoneNumber, "" ); }
		set { Set( () => PhoneNumber, value ); }
	}

	public string Address
	{
		get { return Get( () => Address, "" ); }
		set { Set( () => Address, value ); }
	}

	public string City
	{
		get { return Get( () => City, "" ); }
		set { Set( () => City, value ); }
	}

	public string Region
	{
		get { return Get( () => Region, "" ); }
		set { Set( () => Region, value ); }
	}

	public string CompanyName
	{
		get { return Get( () => CompanyName, "" ); }
		set { Set( () => CompanyName, value ); }
	}

	public string Contact
	{
		get { return Get( () => Contact, "" ); }
		set { Set( () => Contact, value ); }
	}

	public double Latitude, Longitude;

#region Actions
	public Action? OnBackButton;

#region Map
	public ICommand OnShowMapClick => Commands[ nameof( OnShowMapClick ) ];

	public async Task Execute_OnShowMapClick()
	{
		var Placemark = new Placemark
						{
							CountryName  = "",
							FeatureName  = Address,
							Thoroughfare = Address,
							AdminArea    = Region,
							Locality     = City
						};
		var Options = new MapLaunchOptions {Name = CompanyName};

		try
		{
			await Map.OpenAsync( Placemark, Options );
		}
		catch
		{
			// No map application available to open or placemark can not be located try Browser
			await Launcher.OpenAsync( new Uri( $"geo:0,0,?q={Latitude},{Longitude}({CompanyName}\r\n{Address})" ) );
		}

		OnBackButton?.Invoke();
	}
#endregion

	public ICommand OnDialPhoneClick => Commands[ nameof( OnDialPhoneClick ) ];

	public void Execute_OnDialPhoneClick()
	{
		try
		{
			PhoneDialer.Open( PhoneNumber );
		}
		catch
		{
		}
		OnBackButton?.Invoke();
	}
#endregion

#region Buttons
	[DependsUpon( nameof( PhoneNumber ) )]
	public void WhenPhoneNumberChanges()
	{
		var Text = $"Dial {PhoneNumber} ({Contact})";
		PhoneButtonText    = Text;
		PhoneButtonVisible = Text.IsNotNullOrWhiteSpace();
	}

	public string PhoneButtonText
	{
		get { return Get( () => PhoneButtonText, "" ); }
		set { Set( () => PhoneButtonText, value ); }
	}

	public bool PhoneButtonVisible
	{
		get { return Get( () => PhoneButtonVisible, false ); }
		set { Set( () => PhoneButtonVisible, value ); }
	}
#endregion
}