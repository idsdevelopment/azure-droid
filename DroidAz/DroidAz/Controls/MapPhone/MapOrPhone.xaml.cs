﻿#nullable enable

namespace DroidAz.Controls.MapPhone;

[XamlCompilation( XamlCompilationOptions.Compile )]
public partial class MapOrPhone : ContentView
{
	public MapOrPhone()
	{
		InitializeComponent();

		Model              = (MapPhoneViewModel)Root.BindingContext;
		Model.OnBackButton = DoOnBackButton;
	}

	private readonly MapPhoneViewModel Model;

#region Company Name
	public string CompanyName
	{
		get => (string)GetValue( CompanyNameProperty );
		set => SetValue( CompanyNameProperty, value );
	}

	public static readonly BindableProperty CompanyNameProperty = BindableProperty.Create( nameof( CompanyName ),
																						   typeof( string ),
																						   typeof( MapOrPhone ),
																						   "",
																						   propertyChanged: CompanyNamePropertyChanged
																						 );

	private static void CompanyNamePropertyChanged( BindableObject bindable, object oldValue, object newValue )
	{
		if( bindable is MapOrPhone M && newValue is string CoName )
			M.Model.CompanyName = CoName;
	}
#endregion

#region Contact
	public string Contact
	{
		get => (string)GetValue( ContactProperty );
		set => SetValue( ContactProperty, value );
	}

	public static readonly BindableProperty ContactProperty = BindableProperty.Create( nameof( Contact ),
																					   typeof( string ),
																					   typeof( MapOrPhone ),
																					   "",
																					   propertyChanged: ContactPropertyChanged
																					 );

	private static void ContactPropertyChanged( BindableObject bindable, object oldValue, object newValue )
	{
		if( bindable is MapOrPhone M && newValue is string Contact )
			M.Model.Contact = Contact;
	}
#endregion

#region Phone Number
	public string PhoneNumber
	{
		get => (string)GetValue( PhoneNumberProperty );
		set => SetValue( PhoneNumberProperty, value );
	}

	public static readonly BindableProperty PhoneNumberProperty = BindableProperty.Create( nameof( PhoneNumber ),
																						   typeof( string ),
																						   typeof( MapOrPhone ),
																						   "",
																						   propertyChanged: PhoneNumberPropertyChanged
																						 );

	private static void PhoneNumberPropertyChanged( BindableObject bindable, object oldValue, object newValue )
	{
		if( bindable is MapOrPhone M && newValue is string PhoneNumber )
			M.Model.PhoneNumber = PhoneNumber;
	}
#endregion

#region Address
	public string Address
	{
		get => (string)GetValue( AddressProperty );
		set => SetValue( AddressProperty, value );
	}

	public static readonly BindableProperty AddressProperty = BindableProperty.Create( nameof( Address ),
																					   typeof( string ),
																					   typeof( MapOrPhone ),
																					   "",
																					   propertyChanged: AddressPropertyChanged
																					 );

	private static void AddressPropertyChanged( BindableObject bindable, object oldValue, object newValue )
	{
		if( bindable is MapOrPhone M && newValue is string Address )
			M.Model.Address = Address;
	}
#endregion

#region City
	public string City
	{
		get => (string)GetValue( CityProperty );
		set => SetValue( CityProperty, value );
	}

	public static readonly BindableProperty CityProperty = BindableProperty.Create( nameof( City ),
																				    typeof( string ),
																				    typeof( MapOrPhone ),
																				    "",
																				    propertyChanged: CityPropertyChanged
																				  );

	private static void CityPropertyChanged( BindableObject bindable, object oldValue, object newValue )
	{
		if( bindable is MapOrPhone M && newValue is string City )
			M.Model.City = City;
	}
#endregion

#region Region
	public string Region
	{
		get => (string)GetValue( RegionProperty );
		set => SetValue( RegionProperty, value );
	}

	public static readonly BindableProperty RegionProperty = BindableProperty.Create( nameof( Region ),
																					  typeof( string ),
																					  typeof( MapOrPhone ),
																					  "",
																					  propertyChanged: RegionPropertyChanged
																					);

	private static void RegionPropertyChanged( BindableObject bindable, object oldValue, object newValue )
	{
		if( bindable is MapOrPhone M && newValue is string Region )
			M.Model.Region = Region;
	}
#endregion

#region Cancel
	public ICommand? OnBackButton
	{
		get => (ICommand?)GetValue( OnBackButtonProperty );
		set => SetValue( OnBackButtonProperty, value );
	}

	public static readonly BindableProperty OnBackButtonProperty = BindableProperty.Create( nameof( OnBackButton ),
																						    typeof( ICommand ),
																						    typeof( MapOrPhone )
																						  );

	private void DoOnBackButton()
	{
		var Cmd = OnBackButton;

		if( Cmd is not null && Cmd.CanExecute( null ) )
			Cmd.Execute( null );
	}

	private void BackButton_OnClicked( object sender, EventArgs e )
	{
		DoOnBackButton();
	}
#endregion
}