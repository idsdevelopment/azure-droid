﻿#nullable enable

using Preferences = Globals.Preferences;

namespace DroidAz.Controls.ArriveTime;

[XamlCompilation( XamlCompilationOptions.Compile )]
public partial class ArriveWaitTime : ContentView
{
	private void Button_OnArriveAndContinue( object? sender, EventArgs? e )
	{
		ArriveTime        = DateTimeOffset.Now;
		WaitTimeInSeconds = 0;
		Completed();
	}

#region Execute
	private void Show()
	{
		Buttons.IsVisible = true;
		Root.RaiseChild( Buttons );
		Root?.FadeIn( Buttons, WaitTimer );
		WaitTimer.IsVisible = false;
		DoCommand( OnExecute );
	}

	private void Finished( ICommand? command )
	{
		Buttons.IsVisible   = false;
		WaitTimer.IsVisible = false;
		DoCommand( command );
	}

	private void Completed()
	{
		Finished( OnCompleted );
	}

	private void Canceled()
	{
		Finished( OnCancel );
	}

	private void WaitTimerCompleted()
	{
		WaitTimeInSeconds = WaitTimer.ValueInSeconds;
		Completed();
	}

	public ArriveWaitTime()
	{
		InitializeComponent();

		WaitTimer.OnOnWaitTimerCanceled = new Command( Canceled );
		WaitTimer.OnWaitTimer           = new Command( WaitTimerCompleted );

		SetAndContinue.Command = new Command( () =>
											  {
												  Button_OnArriveAndContinue( null, null );
											  } );

		SetAndWait.Command = new Command( () =>
										  {
											  Button_OnArriveAndWait( null, null );
										  } );

		Cancel.Command = new Command( () =>
									  {
										  Button_OnCancel( null, null );
									  } );

		CompulsoryArriveTime = Preferences.CompulsoryArriveTime;
	}

	private void DoCommand( ICommand? command )
	{
		if( command is not null && command.CanExecute( this ) )
			command.Execute( this );
	}
#endregion

#region IsVisible
	public new static readonly BindableProperty IsVisibleProperty = BindableProperty.Create( nameof( IsVisible ),
																							 typeof( bool ),
																							 typeof( ArriveWaitTime ),
																							 false,
																							 propertyChanged: IsVisiblePropertyChanged );

	private static void IsVisiblePropertyChanged( BindableObject bindable, object oldValue, object newValue )
	{
		if( bindable is ArriveWaitTime Aw && newValue is bool Vis )
		{
			( (ContentView)Aw ).IsVisible = Vis;

			if( Vis )
				Aw.Show();
			else
				Aw.Close();
		}
	}

	public new bool IsVisible
	{
		get => (bool)GetValue( IsVisibleProperty );
		set => SetValue( IsVisibleProperty, value );
	}
#endregion


#region ArriveTime
	public static readonly BindableProperty ArriveTimeProperty = BindableProperty.Create( nameof( ArriveTime ),
																						  typeof( DateTimeOffset ),
																						  typeof( ArriveWaitTime ),
																						  DateTimeOffset.MinValue,
																						  BindingMode.OneWayToSource );

	public DateTimeOffset ArriveTime
	{
		get => (DateTimeOffset)GetValue( ArriveTimeProperty );
		set => SetValue( ArriveTimeProperty, value );
	}

	public static readonly BindableProperty CompulsoryArriveTimeProperty = BindableProperty.Create( nameof( CompulsoryArriveTime ),
																									typeof( bool ),
																									typeof( ArriveWaitTime ),
																									false,
																									BindingMode.OneWayToSource );

	public bool CompulsoryArriveTime
	{
		get => (bool)GetValue( CompulsoryArriveTimeProperty );
		set => SetValue( CompulsoryArriveTimeProperty, value );
	}


	public static readonly BindableProperty OnCompletedProperty = BindableProperty.Create( nameof( OnCompleted ),
																						   typeof( ICommand ),
																						   typeof( ArriveWaitTime )
																						 );

	public ICommand? OnCompleted
	{
		get => (ICommand?)GetValue( OnCompletedProperty );
		set => SetValue( OnCompletedProperty, value );
	}
#endregion


#region WaitTime
#region OnWaiting
	public static readonly BindableProperty OnWaitingProperty = BindableProperty.Create( nameof( OnWaiting ),
																						 typeof( ICommand ),
																						 typeof( ArriveWaitTime ) );

	public ICommand? OnWaiting
	{
		get => (ICommand?)GetValue( OnWaitingProperty );
		set => SetValue( OnWaitingProperty, value );
	}
#endregion

	public static readonly BindableProperty WaitTimeInSecondsProperty = BindableProperty.Create( nameof( WaitTimeInSeconds ),
																								 typeof( int ),
																								 typeof( ArriveWaitTime ),
																								 0,
																								 BindingMode.OneWayToSource );

	public int WaitTimeInSeconds
	{
		get => (int)GetValue( WaitTimeInSecondsProperty );
		set => SetValue( WaitTimeInSecondsProperty, value );
	}

	private async void Button_OnArriveAndWait( object? sender, EventArgs? e )
	{
		ArriveTime = DateTimeOffset.Now;

		WaitTimer.IsVisible = true;
		await Root.FadeIn( WaitTimer, Buttons );
		Buttons.IsVisible = false;
		DoCommand( OnWaiting );
	}
#endregion

#region OnExecute
	public static readonly BindableProperty OnExecuteProperty = BindableProperty.Create( nameof( OnExecute ),
																						 typeof( ICommand ),
																						 typeof( ArriveWaitTime ) );

	public ICommand? OnExecute
	{
		get => (ICommand?)GetValue( OnExecuteProperty );
		set => SetValue( OnExecuteProperty, value );
	}
#endregion

#region OnCancel
	public static readonly BindableProperty OnCancelProperty = BindableProperty.Create( nameof( OnCancel ),
																						typeof( ICommand ),
																						typeof( ArriveWaitTime ) );

	public ICommand? OnCancel
	{
		get => (ICommand?)GetValue( OnCancelProperty );
		set => SetValue( OnCancelProperty, value );
	}

	private void Button_OnCancel( object? sender, EventArgs? e )
	{
		DoCommand( OnCancel );
		Close();
	}
#endregion

#region OnClose
	public static readonly BindableProperty OnCloseProperty = BindableProperty.Create( nameof( OnClose ),
																					   typeof( ICommand ),
																					   typeof( ArriveWaitTime ) );

	public ICommand? OnClose
	{
		get => (ICommand?)GetValue( OnCloseProperty );
		set => SetValue( OnCloseProperty, value );
	}

	private void Close()
	{
		Finished( OnClose );
	}
#endregion
}