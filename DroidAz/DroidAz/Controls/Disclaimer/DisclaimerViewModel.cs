﻿#nullable enable

using Preferences = Globals.Preferences;

namespace DroidAz.Controls.Disclaimer;

internal class DisclaimerViewModel : ViewModelBase
{
	public string Disclaimer
	{
		get { return Get( () => Disclaimer, "" ); }
		set { Set( () => Disclaimer, value ); }
	}

	public ICommand OkButtonClick => Commands[ nameof( OkButtonClick ) ];

	public ICommand CancelButtonClick => Commands[ nameof( CancelButtonClick ) ];

	public Action OnOkButton     = null!,
	              OnCancelButton = null!;

	protected override void OnInitialised()
	{
		base.OnInitialised();
		Disclaimer = Preferences.Disclaimer;
	}

	public bool CanExecute_OkButtonClick() => true;

	public void Execute_OkButtonClick()
	{
		OnOkButton();
	}

	public bool CanExecute_CancelButtonClick() => true;


	public void Execute_CancelButtonClick()
	{
		OnCancelButton();
	}
}