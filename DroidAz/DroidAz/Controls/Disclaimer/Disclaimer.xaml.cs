﻿#nullable enable

namespace DroidAz.Controls.Disclaimer;

[XamlCompilation( XamlCompilationOptions.Compile )]
public partial class Disclaimer : ContentView
{
	public Action? OnOkButtonClick,
	               OnCancelButtonClick;

	public Disclaimer()
	{
		InitializeComponent();
		var M = (DisclaimerViewModel)Root.BindingContext;

		M.OnCancelButton = () =>
		                   {
			                   OnCancelButtonClick?.Invoke();
		                   };

		M.OnOkButton = () =>
		               {
			               OnOkButtonClick?.Invoke();
		               };
	}
}