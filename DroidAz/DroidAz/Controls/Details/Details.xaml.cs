﻿#nullable enable

using DroidAz.Dialogs;
using Keyboard = Globals.Keyboard;
using TripUpdate = DroidAz.Views.BaseViewModels.TripUpdate;
// ReSharper disable InconsistentNaming

namespace DroidAz.Controls.Default;

[XamlCompilation( XamlCompilationOptions.Compile )]
public partial class Details : ContentView
{
	public Details()
	{
		InitializeComponent();
		var SaveOrientation = Orientation.LayoutDirection;

		var Sig = new Signature();
		SignatureView.Content = Sig;

		var M = (DetailsViewModel)Root.BindingContext;
		Model   = M;
		M.Owner = this;

		var D = Disclaimer;

		async Task KillDisclaimer( bool ok )
		{
			await this.RaiseAndFade( Disclaimer, DetailsView );
			M.OnDisclaimer( ok );
		}

		D.OnCancelButtonClick = async () =>
								{
									await KillDisclaimer( false );
								};

		D.OnOkButtonClick = async () =>
							{
								await KillDisclaimer( true );
							};

		M.OnShowMapOrPhone = async show =>
							 {
								 if( show )
									 await this.RaiseAndFade( DetailsView, MapOrPhone );
								 else
									 await this.RaiseAndFade( MapOrPhone, DetailsView );
							 };

		M.OnShowDisclaimer = async () =>
							 {
								 await this.RaiseAndFade( DetailsView, Disclaimer );
							 };

		M.OnShowSignature = async show =>
							{
								if( show )
								{
									SaveOrientation             = Orientation.LayoutDirection;
									Orientation.LayoutDirection = Orientation.ORIENTATION.LANDSCAPE;
									await this.RaiseAndFade( DetailsView, SignatureView );
								}
								else
								{
									Orientation.LayoutDirection = SaveOrientation;
									await this.RaiseAndFade( SignatureView, DetailsView );
								}
							};

		M.OnClearSignature = () =>
							 {
								 Sig.Clear();
							 };

		M.OnConfirmUndeliverable = async tripId => await MainPage.Instance.DisplayAlert( "Unable To Deliver", $"Cannot deliver shipment {tripId}?", "Yes", "Cancel" );
		M.OnConfirmUnPickupAble  = async tripId => await MainPage.Instance.DisplayAlert( "Unable To Pick-up", $"Cannot pick-up shipment {tripId}?", "Yes", "Cancel" );

		M.OnShowSignature( false );

		M.OnShowSubMenu = async show =>
						  {
							  if( show )
							  {
								  await Root.RaiseAndFade( DetailsView, SubMenu );
								  Root.RaiseChild( SubMenuButton );
							  }
							  else
								  await Root.RaiseAndFade( SubMenu, DetailsView );
						  };

		M.OnShowWaitTimer = async show =>
							{
								if( show )
								{
									Orientation.LayoutDirection = Orientation.ORIENTATION.BOTH;
									WaitTimer.IsVisible         = true;
									await Root.RaiseAndFade( DetailsView, WaitTimer );
								}
								else
								{
									Orientation.LayoutDirection = Orientation.ORIENTATION.PORTRAIT;
									await Root.RaiseAndFade( WaitTimer, DetailsView );
									WaitTimer.IsVisible = false;
								}
							};

		M.OnShowNotes = async show =>
						{
							if( show )
							{
								Notes.IsVisible = true;
								await Root.RaiseAndFade( DetailsView, Notes );
							}
							else
							{
								await Root.RaiseAndFade( Notes, DetailsView );
								Notes.IsVisible = false;
							}
						};

		M.OnConfirmOnSetArriveTime = async () => await ConfirmDialog.Execute( "Set Arrive Time" );

		Sig.OnOk = signature =>
				   {
					   Model.Signature = signature;
				   };

		Sig.OnCancel = () =>
					   {
						   Model.Signature = new SignatureResult();
					   };

		M.OnGetImage = async () => await Picture.Get();

		M.OnConfirmCancel = async () => await ConfirmDialog.Execute( "Are you sure you wish to cancel?" );
	}

	private readonly DetailsViewModel Model;

	private static void FocusEntry( Entry sender )
	{
		Tasks.RunVoid( 20, () =>
						   {
							   ViewModelBase.Dispatcher( () =>
													     {
														     sender.CursorPosition  = 0;
														     sender.SelectionLength = sender.Text.Length;
													     } );
						   } );
	}

	private void Pieces_OnFocused( object sender, FocusEventArgs e )
	{
		FocusEntry( Pieces );
	}

	private void Weight_OnFocused( object sender, FocusEventArgs e )
	{
		FocusEntry( Weight );
	}

	private void MapOrPhoneDoubleTap( object sender, EventArgs e )
	{
		if( sender is StackLayout Layout )
		{
			if( Layout == Pickups )
				Model.ShowMapOrPhone( true );
			else if( Layout == Deliveries )
				Model.ShowMapOrPhone( false );
		}
	}

	public void Clear()
	{
		Model.Clear();
	}

#region Application Content
	public static readonly BindableProperty ApplicationProperty = BindableProperty.Create( nameof( Application ),
																						   typeof( View ),
																						   typeof( Details ),
																						   propertyChanged: ApplicationPropertyChanged );

	private static void ApplicationPropertyChanged( BindableObject bindable, object oldValue, object newValue )
	{
		if( bindable is Details Detail && newValue is View View )
		{
			var App = Detail.ApplicationArea;
			App.Content            = View;
			View.HorizontalOptions = LayoutOptions.FillAndExpand;
			View.VerticalOptions   = LayoutOptions.FillAndExpand;
			App.ForceLayout();
		}
	}

	public View Application
	{
		get => (View)GetValue( ApplicationProperty );
		set => SetValue( ApplicationProperty, value );
	}
#endregion

#region Selected Item
	public static readonly BindableProperty SelectedItemProperty = BindableProperty.Create( nameof( SelectedItem ),
																						    typeof( TripDisplayEntry ),
																						    typeof( Details ),
																						    propertyChanged: SelectItemPropertyChanged );

	public TripDisplayEntry? SelectedItem
	{
		get => (TripDisplayEntry?)GetValue( SelectedItemProperty );
		set => SetValue( SelectedItemProperty, value );
	}

	private static void SelectItemPropertyChanged( BindableObject bindable, object oldValue, object newValue )
	{
		if( bindable is Details D )
		{
			var Trip = newValue as TripDisplayEntry;
			D.Model.Trip = Trip ?? new TripDisplayEntry();
		}
	}

	public static readonly BindableProperty UpdateItemProperty = BindableProperty.Create( nameof( UpdateItem ),
																						  typeof( TripUpdate ),
																						  typeof( Details ),
																						  null,
																						  BindingMode.TwoWay );

	public TripUpdate? UpdateItem
	{
		get => (TripUpdate?)GetValue( UpdateItemProperty );
		set => SetValue( UpdateItemProperty, value );
	}
#endregion

#region ArriveTime
	public static readonly BindableProperty IgnoreArriveTimeProperty = BindableProperty.Create( nameof( IgnoreArriveTime ),
																							    typeof( bool ),
																							    typeof( Details ),
																							    false,
																							    propertyChanged: IgnoreArriveTimeChanged );

	public bool IgnoreArriveTime
	{
		get => (bool)GetValue( IgnoreArriveTimeProperty );
		set => SetValue( IgnoreArriveTimeProperty, value );
	}

	private static void IgnoreArriveTimeChanged( BindableObject bindable, object oldValue, object newValue )
	{
		if( bindable is Details D && newValue is bool Ignore )
			D.Model.IgnoreArriveTime = Ignore;
	}
#endregion

#region Buttons
	public static readonly BindableProperty EnablePickupDeliveryProperty = BindableProperty.Create( nameof( EnablePickupDelivery ),
																								    typeof( bool ),
																								    typeof( Details ),
																								    false,
																								    propertyChanged: EnablePickupDeliveryChanged );

	public bool EnablePickupDelivery
	{
		get => (bool)GetValue( EnablePickupDeliveryProperty );
		set => SetValue( EnablePickupDeliveryProperty, value );
	}

	private static void EnablePickupDeliveryChanged( BindableObject bindable, object oldValue, object newValue )
	{
		if( bindable is Details D && newValue is bool Enable )
			D.Model.EnablePickupDelivery = Enable;
	}

#region Individual Overrides
	public static readonly BindableProperty EnablePickupProperty = BindableProperty.Create( nameof( EnablePickup ),
																						    typeof( bool ),
																						    typeof( Details ),
																						    true,
																						    propertyChanged: EnablePickupChanged );

	public bool EnablePickup
	{
		get => (bool)GetValue( EnablePickupProperty );
		set => SetValue( EnablePickupProperty, value );
	}

	private static void EnablePickupChanged( BindableObject bindable, object oldValue, object newValue )
	{
		if( bindable is Details D && newValue is bool Enable )
		{
			var Model = D.Model;
			Model.EnablePickup      = Enable;
			Model.ShowPickupCompany = Enable && Model.ShowPickupCompany;
		}
	}

	public static readonly BindableProperty EnableDeliveryProperty = BindableProperty.Create( nameof( EnableDelivery ),
																							  typeof( bool ),
																							  typeof( Details ),
																							  true,
																							  propertyChanged: EnableDeliveryChanged );

	public bool EnableDelivery
	{
		get => (bool)GetValue( EnableDeliveryProperty );
		set => SetValue( EnableDeliveryProperty, value );
	}

	private static void EnableDeliveryChanged( BindableObject bindable, object oldValue, object newValue )
	{
		if( bindable is Details D && newValue is bool Enable )
		{
			var Model = D.Model;
			Model.EnableDelivery      = Enable;
			Model.ShowDeliveryCompany = Enable && Model.ShowDeliveryCompany;
		}
	}
#endregion

#region Undeliverable Button
	public static readonly BindableProperty ShowUndeliverableProperty = BindableProperty.Create( nameof( ShowUndeliverable ),
																							     typeof( bool ),
																							     typeof( Details ),
																							     true,
																							     propertyChanged: ShowUndeliverableChanged );

	public bool ShowUndeliverable
	{
		get => (bool)GetValue( ShowUndeliverableProperty );
		set => SetValue( ShowUndeliverableProperty, value );
	}

	private static void ShowUndeliverableChanged( BindableObject bindable, object oldValue, object newValue )
	{
		if( bindable is Details D && newValue is bool Enable )
			D.Model.AllowUndeliverable = Enable;
	}
#endregion

#region DoCommand
	private void DoCommand( ICommand? command )
	{
		Keyboard.Show = false;

		if( command is not null && command.CanExecute( this ) )
			command.Execute( this );
	}
#endregion

#region BackButton
	public ICommand? OnBackButton
	{
		get => (ICommand?)GetValue( OnBackButtonProperty );
		set => SetValue( OnBackButtonProperty, value );
	}

	public static readonly BindableProperty OnBackButtonProperty = BindableProperty.Create( nameof( OnBackButton ),
																						    typeof( ICommand ),
																						    typeof( Details )
																						  );

	public void BackButton_OnClicked( object? sender, EventArgs? e )
	{
		Clear();
		DoCommand( OnBackButton );
	}
#endregion

#region Cancel
	public ICommand? OnCancelButton
	{
		get => (ICommand?)GetValue( OnCancelButtonProperty );
		set => SetValue( OnCancelButtonProperty, value );
	}

	public static readonly BindableProperty OnCancelButtonProperty = BindableProperty.Create( nameof( OnCancelButton ),
																							  typeof( ICommand ),
																							  typeof( Details ),
																							  propertyChanged: OnCancelButtonPropertyChanged
																							);

	private static void OnCancelButtonPropertyChanged( BindableObject bindable, object oldValue, object newValue )
	{
		if( bindable is Details D )
			D.Model.HasCancelButton = newValue is ICommand;
	}

	public void CancelButton_OnClicked( object? sender, EventArgs? e )
	{
		Clear();
		DoCommand( OnCancelButton );
	}

	public bool IsCancelButtonVisible
	{
		get => (bool)GetValue( IsCancelButtonVisibleProperty );
		set => SetValue( IsCancelButtonVisibleProperty, value );
	}

	public static readonly BindableProperty IsCancelButtonVisibleProperty = BindableProperty.Create( nameof( IsCancelButtonVisible ),
																								     typeof( bool ),
																								     typeof( Details ),
																								     false,
																								     propertyChanged: IsCancelButtonVisiblePropertyChanged
																								   );

	private static void IsCancelButtonVisiblePropertyChanged( BindableObject bindable, object oldValue, object newValue )
	{
		if( bindable is Details D && newValue is bool Vis )
			D.Model.IsCancelButtonVisible = Vis;
	}
#endregion

#region Finaiised
	public ICommand? OnFinalised
	{
		get => (ICommand?)GetValue( OnFinalisedProperty );
		set => SetValue( OnFinalisedProperty, value );
	}

	public static readonly BindableProperty OnFinalisedProperty = BindableProperty.Create( nameof( OnFinalised ),
																						   typeof( ICommand ),
																						   typeof( Details )
																						 );

	public void Finalise()
	{
		if( OnFinalised is not null && OnFinalised.CanExecute( this ) )
			OnFinalised.Execute( this );
	}
#endregion

#region Hide/SHow
	public bool ShowPickupCompany
	{
		get => (bool)GetValue( ShowPickupCompanyProperty );
		set => SetValue( ShowPickupCompanyProperty, value );
	}

	public static readonly BindableProperty ShowPickupCompanyProperty = BindableProperty.Create( nameof( ShowPickupCompany ),
																							     typeof( bool ),
																							     typeof( Details ),
																							     true,
																							     BindingMode.TwoWay,
																							     propertyChanged: ShowPickupCompanyPropertyChanged
																							   );

	private static void ShowPickupCompanyPropertyChanged( BindableObject bindable, object oldValue, object newValue )
	{
		if( bindable is Details D && newValue is bool Hide )
		{
			var Model = D.Model;
			Model.ShowPickupCompany = Hide && Model.EnablePickup;
		}
	}

	public bool ShowDeliveryCompany
	{
		get => (bool)GetValue( ShowDeliveryCompanyProperty );
		set => SetValue( ShowDeliveryCompanyProperty, value );
	}

	public static readonly BindableProperty ShowDeliveryCompanyProperty = BindableProperty.Create( nameof( ShowDeliveryCompany ),
																								   typeof( bool ),
																								   typeof( Details ),
																								   true,
																								   BindingMode.TwoWay,
																								   propertyChanged: SHowDeliveryCompanyPropertyChanged
																								 );

	private static void SHowDeliveryCompanyPropertyChanged( BindableObject bindable, object oldValue, object newValue )
	{
		if( bindable is Details D && newValue is bool Hide )
		{
			var Model = D.Model;
			Model.ShowDeliveryCompany = Hide && Model.EnableDelivery;
		}
	}
#endregion
#endregion
}