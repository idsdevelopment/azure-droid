﻿#nullable enable

using Preferences = Globals.Preferences;
using TripUpdate = DroidAz.Views.BaseViewModels.TripUpdate;

// ReSharper disable InconsistentNaming

namespace DroidAz.Controls.Default;

public class DetailsViewModel : ViewModelBase
{
	public bool HasCancelButton
	{
		get { return Get( () => HasCancelButton, false ); }
		set { Set( () => HasCancelButton, value ); }
	}

	public bool IsCancelButtonVisible
	{
		get { return Get( () => IsCancelButtonVisible, false ); }
		set { Set( () => IsCancelButtonVisible, value ); }
	}

	public bool HasPieces
	{
		get { return Get( () => HasPieces, false ); }
		set { Set( () => HasPieces, value ); }
	}

	public bool EnablePickup
	{
		get { return Get( () => EnablePickup, true ); }
		set { Set( () => EnablePickup, value ); }
	}

	public bool EnableDelivery
	{
		get { return Get( () => EnableDelivery, true ); }
		set { Set( () => EnableDelivery, value ); }
	}

	public bool ShowPopPod
	{
		get { return Get( () => ShowPopPod, true ); }
		set { Set( () => ShowPopPod, value ); }
	}

	public string PopOrPod
	{
		get { return Get( () => PopOrPod, "" ); }
		set { Set( () => PopOrPod, value ); }
	}

	public string PopOrPodText
	{
		get { return Get( () => PopOrPodText, "" ); }
		set { Set( () => PopOrPodText, value ); }
	}

	public bool DisableEditReference
	{
		get { return Get( () => DisableEditReference, !Preferences.EditReference ); }
	}

	public bool DisableEditPieces
	{
		get { return Get( () => DisableEditPieces, !Preferences.EditPiecesWeight ); }
	}

	public bool DisableEditWeight
	{
		get { return Get( () => DisableEditWeight, !Preferences.EditPiecesWeight ); }
	}

	public string EditPieces
	{
		get { return Get( () => EditPieces, "0" ); }
		set { Set( () => EditPieces, value ); }
	}

	public string EditWeight
	{
		get { return Get( () => EditWeight, "0" ); }
		set { Set( () => EditWeight, value ); }
	}

#region Tab Control
	public bool DetailsTabVisible
	{
		get { return Get( () => DetailsTabVisible, true ); }
		set { Set( () => DetailsTabVisible, value ); }
	}
#endregion

	private readonly bool HasDisclaimer;

	private readonly int PopPodMinLength;

	private readonly bool PopPodMustBeAlpha;

	private readonly bool RequireDeliveryArriveTime;

	private readonly bool RequireDeliveryWaitTime;

	private readonly bool RequirePickupArriveTime;

	private readonly bool RequirePickupWaitTime;

	private readonly bool RequirePod;

	private readonly bool RequirePodPicture;

	private readonly bool RequirePodSignature;

	private readonly bool RequirePop;

	private readonly bool RequirePopPicture;

	private readonly bool RequirePopSignature;

	private bool CompulsoryArriveTime,
	             HasCompulsoryArriveTime,
	             RequireNeedsArriveTime;

	public Details? Owner;

	public DetailsViewModel()
	{
		if( MainPageViewModel.Instance is not null )
			MainPageViewModel.Instance.ProgramTitleVisible = false;

		RequirePop          = Preferences.RequirePop;
		RequirePopSignature = Preferences.RequirePopSignature;
		RequirePopPicture   = Preferences.RequirePopPicture;

		RequirePod          = Preferences.RequirePod;
		RequirePodSignature = Preferences.RequirePodSignature;
		RequirePodPicture   = Preferences.RequirePodPicture;

		PopPodMustBeAlpha = Preferences.PopPodMustBeAlpha;
		PopPodMinLength   = Preferences.PopPodMinLength;
		HasDisclaimer     = Preferences.HasDisclaimer;

		RequirePickupArriveTime   = Preferences.NeedPickupArriveTime;
		RequireDeliveryArriveTime = Preferences.NeedDeliveryArriveTime;
		RequireNeedsArriveTime    = Preferences.NeedArriveTime;
		CompulsoryArriveTime      = Preferences.CompulsoryArriveTime;
		RequirePickupWaitTime     = Preferences.PickupWaitTime;
		RequireDeliveryWaitTime   = Preferences.DeliveryWaitTime;

		WaitTimeVisible = RequirePickupWaitTime || RequireDeliveryWaitTime;
	}

	protected override void OnInitialised()
	{
		base.OnInitialised();
		WhenIgnoreArriveTimeChanges();
	}

	[DependsUpon250( nameof( EditPieces ) )]
	public void WhenEditPiecesChanges()
	{
		if( Trip is { } T )
		{
			if( !decimal.TryParse( EditPieces.ToNumeric( true ), out var Value ) )
				Value = 0;
			T.Pieces = Value;
		}
	}

	[DependsUpon250( nameof( EditWeight ) )]
	public void WhenEditWeightChanges()
	{
		if( Trip is { } T )
		{
			if( !decimal.TryParse( EditWeight.ToNumeric( true ), out var Value ) )
				Value = 0;
			T.Weight = Value;
		}
	}

#region Enable Pickup/Delivery
	public bool EnablePickupDelivery
	{
		get { return Get( () => EnablePickupDelivery, false ); }
		set { Set( () => EnablePickupDelivery, value ); }
	}

	[DependsUpon( nameof( EnablePickupDelivery ) )]
	public void WhenEnablePickupDeliveryChanges()
	{
		DoPreferenceEnables();
	}
#endregion

#region Ignore ArriveTime
	public bool IgnoreArriveTime
	{
		get { return Get( () => IgnoreArriveTime, false ); }
		set { Set( () => IgnoreArriveTime, value ); }
	}

	[DependsUpon( nameof( IgnoreArriveTime ) )]
	public void WhenIgnoreArriveTimeChanges()
	{
		var NotIgnoreArriveTime = !IgnoreArriveTime;

		CompulsoryArriveTime   = NotIgnoreArriveTime && Preferences.CompulsoryArriveTime;
		RequireNeedsArriveTime = NotIgnoreArriveTime && Preferences.NeedArriveTime;
	}
#endregion

#region Sub Menu
#region Wait Timer
	public bool WaitTimeVisible
	{
		get { return Get( () => WaitTimeVisible, false ); }
		set { Set( () => WaitTimeVisible, value ); }
	}

	public bool WaitTimeEnable
	{
		get { return Get( () => WaitTimeEnable, false ); }
		set { Set( () => WaitTimeEnable, value ); }
	}

	public  Action<bool>?  OnShowWaitTimer;
	private DateTimeOffset WaitTimeStart = DateTimeOffset.MinValue;

	public ICommand OnWaitTimerCancel => Commands[ nameof( OnWaitTimerCancel ) ];

	public bool CanExecute_OnWaitTimerCancel() => true;

	public void Execute_OnWaitTimerCancel()
	{
		WaitTimerValueInSeconds = 0;
		OnShowWaitTimer?.Invoke( false );
	}

	public string WaitTimeText
	{
		get { return Get( () => WaitTimeText, "" ); }
		set { Set( () => WaitTimeText, value ); }
	}

	public ICommand OnWaitTimeClick => Commands[ nameof( OnWaitTimeClick ) ];

	public bool CanExecute_OnWaitTimeClick() => true;

	public void Execute_OnWaitTimeClick()
	{
		SubMenuVisible = false;
		WaitTimeStart  = DateTimeOffset.Now;
		OnShowWaitTimer?.Invoke( true );
	}

	public string WaitTimerTitle
	{
		get { return Get( () => WaitTimerTitle, "" ); }
		set { Set( () => WaitTimerTitle, value ); }
	}

	public int WaitTimerValueInSeconds
	{
		get { return Get( () => WaitTimerValueInSeconds, 0 ); }
		set { Set( () => WaitTimerValueInSeconds, value ); }
	}

	[DependsUpon( nameof( WaitTimerValueInSeconds ) )]
	public void WhenWaitTimerValueInSecondsChanges()
	{
		OnShowWaitTimer?.Invoke( false );
	}
#endregion

	public Func<bool, Task>? OnShowSubMenu;
	public ICommand          OnMenuButtonClick => Commands[ nameof( OnMenuButtonClick ) ];

	public bool CanExecute_OnMenuButtonClick() => true;

	private async Task ShowSubMenu( bool show )
	{
		SubMenuVisible = show;

		if( OnShowSubMenu is not null )
			await OnShowSubMenu.Invoke( show );
	}

	public async Task Execute_OnMenuButtonClick()
	{
		if( Trip is { } T )
		{
			var Vis = !SubMenuVisible;
			SubMenuVisible = Vis;

			if( Vis )
			{
				var Enab = !CompulsoryArriveTimeOk();

				if( T.Status < STATUS.PICKED_UP )
				{
					WaitTimeText      = "Pickup Wait Time";
					WaitTimerTitle    = "Pickup Wait Time";
					ArriveTimeText    = "Pickup Arrive Time";
					ArriveTimeEnabled = Enab && RequirePickupArriveTime;
					WaitTimeEnable    = RequirePickupWaitTime;
				}
				else
				{
					WaitTimeText      = "Delivery Wait Time";
					WaitTimerTitle    = "Delivery Wait Time";
					ArriveTimeText    = "Delivery Arrive Time";
					ArriveTimeEnabled = Enab && RequireDeliveryArriveTime;
					WaitTimeEnable    = RequireDeliveryWaitTime;
				}
			}
			await ShowSubMenu( Vis );
		}
	}

	public bool SubMenuVisible
	{
		get { return Get( () => SubMenuVisible, false ); }
		set { Set( () => SubMenuVisible, value ); }
	}

	public bool MainMenuVisible
	{
		get { return Get( () => MainMenuVisible, true ); }
		set { Set( () => MainMenuVisible, value ); }
	}
#endregion

#region Driver Notes
	public Action<bool>? OnShowNotes;

	public ICommand OnDriverNoteCanceled => Commands[ nameof( OnDriverNoteCanceled ) ];

	public async Task Execute_OnDriverNoteCanceled()
	{
		await ShowSubMenu( false );
		OnShowNotes?.Invoke( false );
		DriverNote = PreviousDriverNote;
	}

	public ICommand OnDriverNoteClick => Commands[ nameof( OnDriverNoteClick ) ];

	private string PreviousDriverNote = "";

	public async Task Execute_OnDriverNoteClick()
	{
		SubMenuVisible     = false;
		PreviousDriverNote = DriverNote;
		await ShowSubMenu( false );
		OnShowNotes?.Invoke( true );
	}

	public bool CanExecute_OnDriverNoteEdited() => true;

	public ICommand OnDriverNoteEdited => Commands[ nameof( OnDriverNoteEdited ) ];

	public async Task Execute_OnDriverNoteEdited()
	{
		await ShowSubMenu( false );
		OnShowNotes?.Invoke( false );
	}

	public string DriverNote
	{
		get { return Get( () => DriverNote, "" ); }
		set { Set( () => DriverNote, value ); }
	}
#endregion

#region Arrive Time
	public bool ArriveTimeVisible
	{
		get { return Get( () => ArriveTimeVisible, false ); }
		set { Set( () => ArriveTimeVisible, value ); }
	}

	public bool ArriveTimeEnabled
	{
		get { return Get( () => ArriveTimeEnabled, false ); }
		set { Set( () => ArriveTimeEnabled, value ); }
	}

	public string ArriveTimeText
	{
		get { return Get( () => ArriveTimeText, "" ); }
		set { Set( () => ArriveTimeText, value ); }
	}

	public Func<Task<bool>>? OnConfirmOnSetArriveTime;

	private DateTimeOffset ArriveTime = DateTimeOffset.MinValue;

	public ICommand OnArriveTimeClick => Commands[ nameof( OnArriveTimeClick ) ];

	public async void Execute_OnArriveTimeClick()
	{
		if( OnConfirmOnSetArriveTime is null || await OnConfirmOnSetArriveTime.Invoke() )
		{
			ArriveTime              = DateTimeOffset.Now;
			HasCompulsoryArriveTime = true;
			var Enab = CompulsoryArriveTimeOk();
			EnableSubMenuItem = Enab;
			ArriveTimeEnabled = !Enab;
		}
		await ShowSubMenu( false );
	}
#endregion

#region Map Or Phone
	public ICommand OnMapCancel => Commands[ nameof( OnMapCancel ) ];

	public async Task Execute_OnMapCancel()
	{
		MainMenuVisible = true;
		await OnShowMapOrPhone( false );
	}

	public Func<bool, Task> OnShowMapOrPhone = null!;

	public async void ShowMapOrPhone( bool isPickup )
	{
		if( Trip is { } T )
		{
			string Phone, Addr, Co, Ci, Re;

			if( isPickup )
			{
				Phone = T.PickupPhone;
				Addr  = T.PickupAddress;
				Co    = T.PickupCompany;
				Ci    = T.PickupCity;
				Re    = T.PickupRegion;
			}
			else
			{
				Phone = T.DeliveryPhone;
				Addr  = T.DeliveryAddress;
				Co    = T.DeliveryCompany;
				Ci    = T.DeliveryCity;
				Re    = T.DeliveryRegion;
			}

			CompanyName = Co;
			Address     = Addr;
			City        = Ci;
			Region      = Re;
			PhoneNumber = Phone;

			MainMenuVisible = false;
			await OnShowMapOrPhone( true );
		}
	}

	public string PhoneNumber
	{
		get { return Get( () => PhoneNumber, "" ); }
		set { Set( () => PhoneNumber, value ); }
	}

	public string Address
	{
		get { return Get( () => Address, "" ); }
		set { Set( () => Address, value ); }
	}

	public string CompanyName
	{
		get { return Get( () => CompanyName, "" ); }
		set { Set( () => CompanyName, value ); }
	}

	public string City
	{
		get { return Get( () => City, "" ); }
		set { Set( () => City, value ); }
	}

	public string Region
	{
		get { return Get( () => Region, "" ); }
		set { Set( () => Region, value ); }
	}
#endregion

#region Buttons
#region Enables
	private void DoPreferenceEnables()
	{
		var Enab     = EnablePickupDelivery;
		var Pickup   = Enab && IsPickup;
		var Delivery = Enab && IsDelivery;

		EnablePopPod = ( Pickup && RequirePop ) || ( Delivery && RequirePod );
		EnableSign   = ( Pickup && RequirePopSignature ) || ( Delivery && RequirePodSignature );
	}

	public bool EnableSign
	{
		get { return Get( () => EnableSign, false ); }
		set { Set( () => EnableSign, value ); }
	}

	public bool EnablePopPod
	{
		get { return Get( () => EnablePopPod, false ); }
		set { Set( () => EnablePopPod, value ); }
	}

	public bool EnableSubMenuItem
	{
		get { return Get( () => EnableSubMenuItem, true ); }
		set { Set( () => EnableSubMenuItem, value ); }
	}

	public bool EnableArriveTime
	{
		get { return Get( () => EnableArriveTime, true ); }
		set { Set( () => EnableArriveTime, value ); }
	}
#endregion

#region Disclaimer
	public Action OnShowDisclaimer = null!;

	public void OnDisclaimer( bool ok )
	{
		if( ok )
			DoSignature();
		else
			Execute_OnBack();
	}
#endregion

#region Signature
	public bool ShowSignButton
	{
		get { return Get( () => ShowSignButton, false ); }
		set { Set( () => ShowSignButton, value ); }
	}

	public ICommand OnSignatureClick => Commands[ nameof( OnSignatureClick ) ];

	private void DoSignature()
	{
		OnClearSignature?.Invoke();
		OnShowSignature?.Invoke( true );
	}

	public bool CanExecute_OnSignatureClick() => EnableSubMenuItem;

	public void Execute_OnSignatureClick()
	{
		if( HasDisclaimer )
			OnShowDisclaimer();
		else
			DoSignature();
	}
#endregion

#region Picture
	private string ImageExtension = "";
	private byte[] ImageBytes     = [];
	private bool   ImageOk;

	public bool ShowTakePictureButton
	{
		get { return Get( () => ShowTakePictureButton, false ); }
		set { Set( () => ShowTakePictureButton, value ); }
	}

	public ICommand OnPictureClick => Commands[ nameof( OnPictureClick ) ];

	public async void Execute_OnPictureClick()
	{
		SubMenuVisible                          = false;
		( ImageOk, ImageExtension, ImageBytes ) = await OnGetImage();
		EnableEnabledPickupDelivery();
	}

	public Func<Task<(bool Ok, string ImageExtension, byte[] ImageBytes)>> OnGetImage = null!;
#endregion
#endregion

#region Trip
	public void Clear()
	{
		PopOrPod                = "";
		Signature               = new SignatureResult();
		ImageBytes              = [];
		ImageExtension          = "";
		ImageOk                 = false;
		DriverNote              = "";
		WaitTimerValueInSeconds = 0;
		WaitTimeStart           = DateTimeOffset.MinValue;

		HasCompulsoryArriveTime = false;
		EnableSubMenuItem       = false;
		EnableArriveTime        = true;
	}

	public TripDisplayEntry Trip
	{
		get { return Get( () => Trip, new TripDisplayEntry() ); }
		set { Set( () => Trip, value ); }
	}

	private bool CompulsoryArriveTimeOk() => !CompulsoryArriveTime || HasCompulsoryArriveTime;

	[DependsUpon( nameof( Trip ) )]
	public void WhenTripChanges()
	{
		Clear();

		if( Trip is { } T )
		{
			T.RecentlyUpdated = false;

			if( RequireNeedsArriveTime )
			{
				ArriveTimeVisible = true;

				ArriveTime = Trip.Status switch
				             {
					             < STATUS.PICKED_UP => RequirePickupArriveTime,
					             _                  => RequireDeliveryArriveTime
				             } ? DateTimeOffset.Now : DateTimeOffset.MinValue;
			}
			else
			{
				ArriveTime        = DateTimeOffset.MinValue;
				ArriveTimeVisible = false;
			}

			HasPieces = T.Pieces > 0;

			EditPieces = $"{T.Pieces:N}";
			EditWeight = T.Weight.ToWeightString();

			PopOrPodText = Globals.Resources.GetStringResource( T.IsPickup ? "DetailsPop" : "DetailsPod" );

			var Items = new ObservableCollection<TripItemsDisplayEntry>( T.Items );
			HasItems          = Items.Count > 0;
			TripDisplayItems  = Items;
			DetailsTabVisible = true;

			var InsideFence = T.InsideFence;
			OutsideGeoFence = !InsideFence;

			var Enable = EnablePickupDelivery && InsideFence;
			ShowPopPod = Enable;
			var Pickup   = T.IsPickup;
			var Delivery = T.IsDelivery;
			DriverNote               = T.DriverNote;
			EnableSubMenuItem        = CompulsoryArriveTimeOk();
			ShowPickupDeliveryButton = Enable;

			ShowSignButton        = Enable && ( ( RequirePopSignature && Pickup ) || ( RequirePodSignature && Delivery ) );
			ShowTakePictureButton = Enable && ( ( RequirePopPicture && Pickup ) || ( RequirePodPicture && Delivery ) );
		}
	}
#endregion

#region Geo Fence
	public bool PickupDeliveryEnable
	{
		get { return Get( () => PickupDeliveryEnable, false ); }
		set { Set( () => PickupDeliveryEnable, value ); }
	}

	public bool OutsideGeoFence
	{
		get { return Get( () => OutsideGeoFence, false ); }
		set { Set( () => OutsideGeoFence, value ); }
	}
#endregion

#region Items
	public ICommand OnShowItems => Commands[ nameof( OnShowItems ) ];

	public void Execute_OnShowItems()
	{
		ItemsTabVisible   = true;
		DetailsTabVisible = false;
	}

	public ICommand OnShowDetails => Commands[ nameof( OnShowDetails ) ];

	public void Execute_OnShowDetails()
	{
		ItemsTabVisible   = false;
		DetailsTabVisible = true;
	}

	public ObservableCollection<TripItemsDisplayEntry> TripDisplayItems
	{
		get { return Get( () => TripDisplayItems, [] ); }
		set { Set( () => TripDisplayItems, value ); }
	}

	public bool ItemsTabVisible
	{
		get { return Get( () => ItemsTabVisible, false ); }
		set { Set( () => ItemsTabVisible, value ); }
	}

	public bool HasItems
	{
		get { return Get( () => HasItems, false ); }
		set { Set( () => HasItems, value ); }
	}
#endregion

#region Signature
	public Action?       OnClearSignature;
	public Action<bool>? OnShowSignature;

	public SignatureResult Signature
	{
		get { return Get( () => Signature, new SignatureResult() ); }
		set { Set( () => Signature, value ); }
	}

	[DependsUpon( nameof( Signature ) )]
	public void WhenSignatureChanges()
	{
		OnShowSignature?.Invoke( false );
	}
#endregion

#region Back Button
	public bool ShowPickupDeliveryButton
	{
		get { return Get( () => ShowPickupDeliveryButton, true ); }
		set { Set( () => ShowPickupDeliveryButton, value ); }
	}

	public ICommand OnBack => Commands[ nameof( OnBack ) ];

	public void Execute_OnBack()
	{
		if( !HasCancelButton )
			DoCancel();

		Owner?.BackButton_OnClicked( null, null );
	}
#endregion

#region Cancel Button
	public Func<Task<bool>>? OnConfirmCancel;

	private void DoCancel()
	{
		ShowSignButton           = false;
		ShowPickupDeliveryButton = false;
		ShowPopPod               = false;
		Trip                     = new TripDisplayEntry();
	}

	public ICommand OnCancel => Commands[ nameof( OnCancel ) ];

	public async void Execute_OnCancel()
	{
		if( OnConfirmCancel is null || await OnConfirmCancel() )
		{
			DoCancel();
			Owner?.CancelButton_OnClicked( null, null );
		}
	}
#endregion

#region Pickup / Delivery Button
	public ICommand PickupDeliveryButtonClick => Commands[ nameof( PickupDeliveryButtonClick ) ];

	public bool EnabledPickupDeliveryButton
	{
		get { return Get( () => EnabledPickupDeliveryButton, false ); }
		set { Set( () => EnabledPickupDeliveryButton, value ); }
	}

	[DependsUpon250( nameof( Signature ) )]
	[DependsUpon250( nameof( PopOrPod ) )]
	public void EnableEnabledPickupDelivery()
	{
		if( Trip is { } T )
		{
			bool Enable;

			if( T.Removed )
				Enable = false;
			else
			{
				var Pp = PopOrPod;

				bool AlphaOk()
				{
					if( PopPodMustBeAlpha )
					{
						if( ( Pp.IndexOf( "  ", StringComparison.Ordinal ) >= 0 ) || ( Pp.IndexOf( "..", StringComparison.Ordinal ) >= 0 ) || ( Pp.IndexOf( "--", StringComparison.Ordinal ) > 0 ) )
							return false;

						foreach( var C in Pp )
						{
							if( C is not (>= 'A' and <= 'Z' or >= 'a' and <= 'z' or ' ' or '.' or '-') )
								return false;
						}
					}
					return true;
				}

				var PopPodOk    = Pp.IsNotNullOrWhiteSpace() && ( Pp.Length >= PopPodMinLength ) && AlphaOk();
				var SignatureOk = Signature.Points.Length > 2;

				if( IsPickup )
				{
					Enable = ( !RequirePop || ( RequirePop && PopPodOk ) )
					         && ( !RequirePopSignature || ( RequirePopSignature && SignatureOk ) )
					         && ( !RequirePopPicture || ( RequirePopPicture && ImageOk ) );
				}
				else
				{
					Enable = ( !RequirePod || ( RequirePod && PopPodOk ) )
					         && ( !RequirePodSignature || ( RequirePodSignature && SignatureOk ) )
					         && ( !RequirePodPicture || ( RequirePodPicture && ImageOk ) );
				}
			}
			EnabledPickupDeliveryButton = Enable;
		}
	}

	public bool CanExecute_PickupDeliveryButtonClick() => EnabledPickupDeliveryButton;

	public void Execute_PickupDeliveryButtonClick()
	{
		if( Owner is not null )
		{
			var T = Trip;

			Owner.UpdateItem = new TripUpdate
			                   {
				                   Trip      = T,
				                   PopPod    = PopOrPod,
				                   Signature = Signature,

				                   ImageBytes     = ImageBytes,
				                   ImageExtension = ImageExtension,

				                   DriverNotes       = DriverNote,
				                   WaitTimeStart     = WaitTimeStart,
				                   WaitTimeInSeconds = WaitTimerValueInSeconds,
				                   ArriveTime        = ArriveTime
			                   };
		}

		Trip = new TripDisplayEntry();

		Dispatcher( () =>
		            {
			            Owner?.Finalise();
		            } );
	}
#endregion

#region Undeliverable
	public bool IsPickup
	{
		get { return Get( () => IsPickup, true ); }
		set { Set( () => IsPickup, value ); }
	}

	public bool IsDelivery
	{
		get { return Get( () => IsDelivery, false ); }
		set { Set( () => IsDelivery, value ); }
	}

	[DependsUpon( nameof( Trip ) )]
	public void SetPickupDelivery()
	{
		if( Trip is { } T )
		{
			var Pu = T.Status < STATUS.PICKED_UP;
			IsPickup   = Pu;
			IsDelivery = !Pu;
		}
	}

	public Func<string, Task<bool>> OnConfirmUndeliverable = TrueDefault,
	                                OnConfirmUnPickupAble  = TrueDefault;

	private static Task<bool> TrueDefault( string tripId ) => Task.FromResult( true );

	public bool AllowUndeliverable
	{
		get { return Get( () => AllowUndeliverable, Preferences.AllowUndeliverable ); }
		set { Set( () => AllowUndeliverable, value ); }
	}

	public ICommand UndeliverableClick => Commands[ nameof( UndeliverableClick ) ];

	private void DoUndeliverable( STATUS status )
	{
		if( Trip is { } T )
		{
			T.Status = status;

			if( Owner is not null )
			{
				Owner.UpdateItem = new TripUpdate
				                   {
					                   IsUndeliverable = true,
					                   Trip            = T
				                   };
			}

			Dispatcher( () =>
			            {
				            Owner?.Finalise();
			            } );
		}
	}

	public async void Execute_UndeliverableClick()
	{
		if( Trip is { } T )
		{
			if( await OnConfirmUndeliverable( T.TripId ) )
				DoUndeliverable( STATUS.PICKED_UP );
		}
	}

	public ICommand UnpickupableClick => Commands[ nameof( UnpickupableClick ) ];

	public async void Execute_UnpickupableClick()
	{
		if( Trip is { } T )
		{
			if( await OnConfirmUnPickupAble( T.TripId ) )
				DoUndeliverable( STATUS.DISPATCHED );
		}
	}
#endregion

#region Hide/Show
	public bool ShowPickupCompany
	{
		get { return Get( () => ShowPickupCompany, true ); }
		set { Set( () => ShowPickupCompany, value ); }
	}

	public void WhenShowPickupCompanyChanges()
	{
		if( Owner is not null )
			Owner.ShowPickupCompany = ShowPickupCompany;
	}

	public bool ShowDeliveryCompany
	{
		get { return Get( () => ShowDeliveryCompany, true ); }
		set { Set( () => ShowDeliveryCompany, value ); }
	}

	public void WhenShowDeliveryCompanyChanges()
	{
		if( Owner is not null )
			Owner.ShowDeliveryCompany = ShowDeliveryCompany;
	}
#endregion
}