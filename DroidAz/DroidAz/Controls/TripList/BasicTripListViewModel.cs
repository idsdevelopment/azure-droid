﻿#nullable enable

using Preferences = Globals.Preferences;

namespace DroidAz.Controls.Default;

public class BasicTripListViewModel : ViewModelBase
{
	public BasicTripList? Owner { get; set; } = null!;

#region Gps
	public bool ShowDistance
	{
		get { return Get( () => ShowDistance, false ); }
		set { Set( () => ShowDistance, value ); }
	}
#endregion

	public ObservableCollection<TripDisplayEntry> TripDisplayEntries
	{
		get { return Get( () => TripDisplayEntries, [] ); }
		set { Set( () => TripDisplayEntries, value ); }
	}

	protected override void OnInitialised()
	{
		base.OnInitialised();
		ShowDistance = Preferences.EnableDeliveryGeoFence || Preferences.EnablePickupGeoFence;
	}

#region Selected Item
	public TripDisplayEntry? SelectedItem // Bound To ListView
	{
		get { return Get( () => SelectedItem, (TripDisplayEntry?)null ); }
		set { Set( () => SelectedItem, value ); }
	}

	[DependsUpon( nameof( SelectedItem ) )]
	public void WhenSelectedItemChanges()
	{
		if( Owner is { } O )
			O.SelectedItem = SelectedItem;
	}
#endregion
}