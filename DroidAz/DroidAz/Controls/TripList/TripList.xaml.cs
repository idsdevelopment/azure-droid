﻿#nullable enable

using DroidAz.Dialogs;

namespace DroidAz.Controls.Default;

[XamlCompilation( XamlCompilationOptions.Compile )]
public partial class TripList : ContentView
{
#region Trips
	public Dictionary<string, Trip> Trips => Model.Trips;
#endregion

	public TripList()
	{
		InitializeComponent();
		var M = (TripListViewModel)Root.BindingContext;
		M.Owner = this;
		Model   = M;

		void DoTap( ICommand? tap )
		{
			SelectedItem = M.SelectedItem;

			if( tap is not null && tap.CanExecute( this ) )
				tap.Execute( this );
		}

		M.OnSingleTap = () =>
						{
							DoTap( ItemTapped );
						};

		M.OnDoubleTap = () =>
						{
							DoTap( ItemDoubleTapped );
						};

		M.OnOutsideGeoFence = () =>
							  {
								  ErrorDialog.Execute( "Outside Geo Fence" );
							  };

		if( EnableWaitingForTrips )
			M.SetWaitingForTrips();
	}

	private readonly TripListViewModel Model;

#region Selected Item
	public static readonly BindableProperty SelectedItemProperty = BindableProperty.Create( nameof( SelectedItem ),
																							typeof( TripDisplayEntry ),
																							typeof( TripList ),
																							null,
																							BindingMode.TwoWay,
																							propertyChanged: SelectedItemPropertyChanged );

	private static void SelectedItemPropertyChanged( BindableObject bindable, object oldValue, object newValue )
	{
		if( bindable is TripList T )
		{
			var Entry = newValue as TripDisplayEntry;
			T.Model.SelectedItem = Entry; // Item or null
		}
	}

	public TripDisplayEntry? SelectedItem
	{
		get => (TripDisplayEntry?)GetValue( SelectedItemProperty );
		set => SetValue( SelectedItemProperty, value );
	}
#endregion

#region Item Tapped
	public static readonly BindableProperty ItemTappedProperty = BindableProperty.Create( nameof( ItemTapped ),
																						  typeof( ICommand ),
																						  typeof( TripList ) );

	public ICommand? ItemTapped
	{
		get => (ICommand?)GetValue( ItemTappedProperty );
		set => SetValue( ItemTappedProperty, value );
	}
#endregion

#region Item Double Tapped
	public static readonly BindableProperty ItemDoubleTappedProperty = BindableProperty.Create( nameof( ItemDoubleTapped ),
																								typeof( ICommand ),
																								typeof( TripList ) );

	public ICommand? ItemDoubleTapped
	{
		get => (ICommand?)GetValue( ItemDoubleTappedProperty );
		set => SetValue( ItemDoubleTappedProperty, value );
	}
#endregion

#region Geo Fence
#region Single Tap
	public static readonly BindableProperty SingleTapCheckGeoFenceProperty = BindableProperty.Create( nameof( SingleTapCheckGeoFence ),
																									  typeof( bool ),
																									  typeof( TripList ),
																									  false,
																									  propertyChanged: SingleTapCheckGeoFencePropertyChanged
																									);

	private static void SingleTapCheckGeoFencePropertyChanged( BindableObject bindable, object oldValue, object newValue )
	{
		if( bindable is TripList Tl && newValue is bool Enable )
			Tl.Model.SingleTapCheckGeoFence = Enable;
	}

	public bool SingleTapCheckGeoFence
	{
		get => (bool)GetValue( SingleTapCheckGeoFenceProperty );
		set => SetValue( SingleTapCheckGeoFenceProperty, value );
	}
#endregion

#region Double Tap
	public static readonly BindableProperty DoubleTapCheckGeoFenceProperty = BindableProperty.Create( nameof( DoubleTapCheckGeoFence ),
																									  typeof( bool ),
																									  typeof( TripList ),
																									  false,
																									  propertyChanged: DoubleTapCheckGeoFencePropertyChanged
																									);

	private static void DoubleTapCheckGeoFencePropertyChanged( BindableObject bindable, object oldValue, object newValue )
	{
		if( bindable is TripList Tl && newValue is bool Enable )
			Tl.Model.DoubleTapCheckGeoFence = Enable;
	}

	public bool DoubleTapCheckGeoFence
	{
		get => (bool)GetValue( DoubleTapCheckGeoFenceProperty );
		set => SetValue( DoubleTapCheckGeoFenceProperty, value );
	}
#endregion
#endregion

#region Waiting For Trips
	public static readonly BindableProperty EnableWaitingForTripsProperty = BindableProperty.Create( nameof( EnableWaitingForTrips ),
																									 typeof( bool ),
																									 typeof( TripList ),
																									 true
																								   );

	public bool EnableWaitingForTrips
	{
		get => (bool)GetValue( EnableWaitingForTripsProperty );
		set => SetValue( EnableWaitingForTripsProperty, value );
	}

	public static readonly BindableProperty WaitingForTripsProperty = BindableProperty.Create( nameof( WaitingForTrips ),
																							   typeof( bool ),
																							   typeof( TripList ),
																							   true,
																							   BindingMode.OneWayToSource
																							 );

	public bool WaitingForTrips
	{
		get => (bool)GetValue( WaitingForTripsProperty );
		set => SetValue( WaitingForTripsProperty, value );
	}
#endregion

#region Display Trips
	public static readonly BindableProperty TripDisplayEntriesProperty = BindableProperty.Create( nameof( TripDisplayEntries ),
																								  typeof( ObservableCollection<TripDisplayEntry> ),
																								  typeof( TripList ),
																								  new ObservableCollection<TripDisplayEntry>(),
																								  BindingMode.TwoWay,
																								  propertyChanged: TripDisplayEntriesPropertyChanged );

	private static void TripDisplayEntriesPropertyChanged( BindableObject bindable, object oldValue, object newValue )
	{
		if( bindable is TripList T )
			T.TripIdFilter = [..T.TripIdFilter];
	}

	public ObservableCollection<TripDisplayEntry> TripDisplayEntries
	{
		get => (ObservableCollection<TripDisplayEntry>)GetValue( TripDisplayEntriesProperty );
		set => SetValue( TripDisplayEntriesProperty, value );
	}

	//----------------------------------------

	public static readonly BindableProperty FilteredTripDisplayEntriesProperty = BindableProperty.Create( nameof( FilteredTripDisplayEntries ),
																										  typeof( ObservableCollection<TripDisplayEntry> ),
																										  typeof( TripList ),
																										  new ObservableCollection<TripDisplayEntry>(),
																										  BindingMode.TwoWay );

	public ObservableCollection<TripDisplayEntry> FilteredTripDisplayEntries
	{
		get => (ObservableCollection<TripDisplayEntry>)GetValue( FilteredTripDisplayEntriesProperty );
		set => SetValue( FilteredTripDisplayEntriesProperty, value );
	}

	//----------------------------------------
	public static readonly BindableProperty FilterOnEmptyProperty = BindableProperty.Create( nameof( FilterOnEmpty ),
																							 typeof( bool ),
																							 typeof( TripList ),
																							 true,
																							 propertyChanged: FilterOnEmptyPropertyChanged );

	private static void FilterOnEmptyPropertyChanged( BindableObject bindable, object oldValue, object newValue )
	{
		if( bindable is TripList T && newValue is bool OnEmpty )
			T.Model.FilterOnEmpty = OnEmpty;
	}

	public bool FilterOnEmpty
	{
		get => (bool)GetValue( FilterOnEmptyProperty );
		set => SetValue( FilterOnEmptyProperty, value );
	}

	public List<string> TripIdFilter
	{
		get => (List<string>)GetValue( TripIdFilterProperty );
		set => SetValue( TripIdFilterProperty, value );
	}

	public static readonly BindableProperty TripIdFilterProperty = BindableProperty.Create( nameof( TripIdFilter ),
																							typeof( List<string> ),
																							typeof( TripList ),
																							new List<string>(),
																							propertyChanged: TripIdFilterPropertyChanged );

	private static void TripIdFilterPropertyChanged( BindableObject bindable, object oldValue, object newValue )
	{
		if( bindable is TripList T && newValue is List<string> Filter )
			T.Model.TripIdFilter = Filter;
	}

	//-----------------------------------------------------------------------

	public STATUS[] TripStatusFilter
	{
		get => (STATUS[])GetValue( TripStatusFilterProperty );
		set => SetValue( TripStatusFilterProperty, value );
	}

	public static readonly BindableProperty TripStatusFilterProperty = BindableProperty.Create( nameof( TripStatusFilter ),
																								typeof( STATUS[] ),
																								typeof( TripList ),
																								new[] {STATUS.DISPATCHED, STATUS.PICKED_UP},
																								propertyChanged: TripStatusFilterPropertyChanged );

	private static void TripStatusFilterPropertyChanged( BindableObject bindable, object oldValue, object newValue )
	{
		if( bindable is TripList T && newValue is STATUS[] Filter )
			T.Model.StatusFilter = Filter;
	}
#endregion

#region IsVisible
	public new static readonly BindableProperty IsVisibleProperty = BindableProperty.Create( nameof( IsVisible ),
																							 typeof( bool ),
																							 typeof( TripList ),
																							 false,
																							 propertyChanged: IsVisiblePropertyChanged );

	private static void IsVisiblePropertyChanged( BindableObject bindable, object oldValue, object newValue )
	{
		if( bindable is TripList T && newValue is bool Vis )
		{
			if( Vis )
			{
				//Fudge to force refresh
				var View = T.CollectionView;
				var Bc   = View.BindingContext;
				View.BindingContext = null;
				View.BindingContext = Bc;
			}

			var Cv = (ContentView)T;
			Cv.IsVisible = Vis;
		}
	}

	public new bool IsVisible
	{
		get => (bool)GetValue( IsVisibleProperty );
		set => SetValue( IsVisibleProperty, value );
	}
#endregion
}