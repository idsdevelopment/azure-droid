﻿#nullable enable

using DroidAz.Dialogs;
using Preferences = Globals.Preferences;

namespace DroidAz.Controls.Default;

public class TripListViewModel : TripsBaseViewModel
{
	public TripList? Owner
	{
		get => _Owner;
		set
		{
			_Owner = value;
			WhenTripIdFilterChanges();
		}
	}

	private TripList? _Owner;

	protected override void OnInitialised()
	{
		base.OnInitialised();
		ShowDistance = Preferences.EnableDeliveryGeoFence || Preferences.EnablePickupGeoFence;
	}

#region Overrides
	protected override bool OnRemoveTrip( string tripId )
	{
		if( TripOptions.ConfirmTripRemove )
		{
			Tasks.RunVoid( () =>
			               {
				               Dispatcher( ShipmentRemoved.Execute );
			               }
			             );
		}
		return base.OnRemoveTrip( tripId );
	}

	protected bool LoadingTrips => Instance?.LoadingTrips ?? true;

	private void PlayUpdateSound( TripDisplayEntry trip )
	{
		if( !LoadingTrips && ( trip.Status < STATUS.VERIFIED ) )
		{
/*
			var Status = ( from T in TripDisplayEntries
						   where T.TripId == trip.TripId
						   select T.Status ).FirstOrDefault();

			if( Status != trip.Status )
*/
			Sounds.PlayTripNotificationSound();
		}
	}

	protected override void OnUpdateTrip( TripDisplayEntry trip )
	{
		PlayUpdateSound( trip );
	}

	protected override void OnUpdateTripStatus( TripDisplayEntry trip )
	{
		PlayUpdateSound( trip );
	}
#endregion

#region Trips
#region Gps
	public bool ShowDistance
	{
		get { return Get( () => ShowDistance, false ); }
		set { Set( () => ShowDistance, value ); }
	}

	public Action? OnOutsideGeoFence;

	public bool SingleTapCheckGeoFence
	{
		get { return Get( () => SingleTapCheckGeoFence, false ); }
		set { Set( () => SingleTapCheckGeoFence, value ); }
	}

	public bool DoubleTapCheckGeoFence
	{
		get { return Get( () => DoubleTapCheckGeoFence, false ); }
		set { Set( () => DoubleTapCheckGeoFence, value ); }
	}
#endregion

#region Filters
	public ObservableCollection<TripDisplayEntry> FilteredTripDisplayEntries
	{
		get { return Get( () => FilteredTripDisplayEntries, [] ); }
		set { Set( () => FilteredTripDisplayEntries, value ); }
	}

	[DependsUpon( nameof( FilteredTripDisplayEntries ) )]
	public void WhenFilteredTripDisplayEntriesChange()
	{
		SetWaitingForTrips();
	}

	public bool FilterOnEmpty
	{
		get { return Get( () => FilterOnEmpty, true ); }
		set { Set( () => FilterOnEmpty, value ); }
	}

	public List<string> TripIdFilter
	{
		get { return Get( () => TripIdFilter, [] ); }
		set { Set( () => TripIdFilter, value ); }
	}

	public STATUS[] StatusFilter
	{
		get { return Get( () => StatusFilter, [STATUS.DISPATCHED, STATUS.PICKED_UP] ); }
		set { Set( () => StatusFilter, value ); }
	}

	[DependsUpon( nameof( StatusFilter ) )]
	[DependsUpon( nameof( TripIdFilter ) )]
	[DependsUpon( nameof( FilterOnEmpty ) )]
	public void WhenTripIdFilterChanges()
	{
		IEnumerable<TripDisplayEntry> Entries;

		if( TripIdFilter.Count == 0 )
			Entries = FilterOnEmpty ? TripDisplayEntries : new List<TripDisplayEntry>();
		else
		{
			var Filter = TripIdFilter;

			Entries = from De in TripDisplayEntries
			          where Filter.Contains( De.TripId )
			          select De;

			var StatFilter = StatusFilter;

			Entries = from E in Entries
			          where StatFilter.Contains( E.Status )
			          select E;
		}

		var OEntries = new ObservableCollection<TripDisplayEntry>( Entries );
		FilteredTripDisplayEntries = OEntries;

		if( Owner is not null )
			Owner.FilteredTripDisplayEntries = OEntries;
	}
#endregion

#region Geo Fence
	protected override void OnGeoFenceChange( string tripId, bool insideFence )
	{
		var Entry = ( from Fe in FilteredTripDisplayEntries
		              where Fe.TripId == tripId
		              select Fe ).FirstOrDefault();

		if( Entry is not null )
			Entry.InsideFence = insideFence;
	}
#endregion

#region Waiting For Trips
	public void SetWaitingForTrips()
	{
		var Waiting = ( FilteredTripDisplayEntries.Count == 0 ) && ( Owner?.EnableWaitingForTrips ?? true );
		WaitingForTrips    = Waiting;
		NotWaitingForTrips = !Waiting;
	}

	public bool WaitingForTrips
	{
		get { return Get( () => WaitingForTrips, false ); }
		set { Set( () => WaitingForTrips, value ); }
	}

	public bool NotWaitingForTrips
	{
		get { return Get( () => NotWaitingForTrips, true ); }
		set { Set( () => NotWaitingForTrips, value ); }
	}

	[DependsUpon( nameof( WaitingForTrips ) )]
	public void WhenWaitingFroTripsChanges()
	{
		if( Owner is not null )
			Owner.WaitingForTrips = WaitingForTrips;
	}
#endregion

	protected override void OnTripCountChange( int count )
	{
		SetWaitingForTrips();
	}

	public override void WhenTripDisplayEntriesChanges()
	{
		if( Owner is not null )
			Owner.TripDisplayEntries = TripDisplayEntries;
		WhenTripIdFilterChanges();
		SetWaitingForTrips();
	}
#endregion

#region Tapped Events
	public Action?  OnSingleTap, OnDoubleTap;
	public ICommand OnTapped => Commands[ nameof( OnTapped ) ];

	public void Execute_OnTapped( object item )
	{
		if( item is TripDisplayEntry Entry )
		{
			if( SingleTapCheckGeoFence && !Entry.InsideFence )
				OnOutsideGeoFence?.Invoke();
			else
			{
				Entry.RecentlyUpdated = false;
				SelectedItem          = Entry;
				WhenTripIdFilterChanges();
				OnSingleTap?.Invoke();
			}
		}
	}

	public ICommand OnDoubleTapped => Commands[ nameof( OnDoubleTapped ) ];

	public void Execute_OnDoubleTapped( object item )
	{
		if( item is TripDisplayEntry Entry )
		{
			if( DoubleTapCheckGeoFence && !Entry.InsideFence )
				OnOutsideGeoFence?.Invoke();
			else
			{
				Entry.RecentlyUpdated = false;
				SelectedItem          = Entry;
				WhenTripIdFilterChanges();
				OnDoubleTap?.Invoke();
			}
		}
	}
#endregion

#region Selected Item
	public TripDisplayEntry? SelectedItem // Bound To ListView
	{
		get { return Get( () => SelectedItem, (TripDisplayEntry?)null ); }
		set { Set( () => SelectedItem, value ); }
	}

	[DependsUpon( nameof( SelectedItem ) )]
	public void WhenSelectedItemChanges()
	{
		if( SelectedItem is { } Entry )
		{
			if( Entry.Removed )
			{
				Entry.Removed = false;
				var Tid = Entry.TripId;

				Trips.Remove( Tid );

				for( int I = 0, C = TripDisplayEntries.Count; I < C; I++ )
				{
					if( TripDisplayEntries[ I ].TripId == Tid )
					{
						TripDisplayEntries.RemoveAt( I );
						break;
					}
				}

				SelectedItem = null;
				WhenTripIdFilterChanges();
			}

			if( Owner is not null )
				Owner.SelectedItem = SelectedItem;
		}
	}
#endregion
}