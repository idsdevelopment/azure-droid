﻿#nullable enable

namespace DroidAz.Controls.Default;

[XamlCompilation( XamlCompilationOptions.Compile )]
public partial class BasicTripList : ContentView
{
	public BasicTripList()
	{
		InitializeComponent();
		var M = Model = (BasicTripListViewModel)Root.BindingContext;
		M.Owner = this;
	}

	private readonly BasicTripListViewModel Model;

#region Selected Item
	public static readonly BindableProperty SelectedItemProperty = BindableProperty.Create( nameof( SelectedItem ),
																							typeof( TripDisplayEntry ),
																							typeof( BasicTripList ),
																							null,
																							BindingMode.TwoWay,
																							propertyChanged: SelectedItemPropertyChanged );

	private static void SelectedItemPropertyChanged( BindableObject bindable, object oldValue, object newValue )
	{
		if( bindable is BasicTripList T )
		{
			var Item = newValue as TripDisplayEntry; // Item or null
			T.Model.SelectedItem = Item;

			if( Item is not null )
			{
				var Command = T.ItemTapped;

				if( Command is not null && Command.CanExecute( T ) )
					Command.Execute( T );
			}
		}
	}

	public TripDisplayEntry? SelectedItem
	{
		get => (TripDisplayEntry?)GetValue( SelectedItemProperty );
		set => SetValue( SelectedItemProperty, value );
	}


	public static readonly BindableProperty ItemTappedProperty = BindableProperty.Create( nameof( ItemTapped ),
																						  typeof( ICommand ),
																						  typeof( BasicTripList ) );

	public ICommand? ItemTapped
	{
		get => (ICommand?)GetValue( ItemTappedProperty );
		set => SetValue( ItemTappedProperty, value );
	}
#endregion

#region Display Trips
	public static readonly BindableProperty TripDisplayEntriesProperty = BindableProperty.Create( nameof( TripDisplayEntries ),
																								  typeof( ObservableCollection<TripDisplayEntry> ),
																								  typeof( BasicTripList ),
																								  new ObservableCollection<TripDisplayEntry>(),
																								  BindingMode.TwoWay,
																								  propertyChanged: TripDisplayEntriesPropertyChanged );

	private static void TripDisplayEntriesPropertyChanged( BindableObject bindable, object oldValue, object newValue )
	{
		if( bindable is BasicTripList T && newValue is ObservableCollection<TripDisplayEntry> Entries )
			T.Model.TripDisplayEntries = Entries;
	}

	public ObservableCollection<TripDisplayEntry> TripDisplayEntries
	{
		get => (ObservableCollection<TripDisplayEntry>)GetValue( TripDisplayEntriesProperty );
		set => SetValue( TripDisplayEntriesProperty, value );
	}
#endregion
}