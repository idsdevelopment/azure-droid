﻿namespace DroidAz;

public static partial class Extensions
{
	public static async Task DoFade( this VisualElement element, bool fadeIn )
	{
		await element.FadeTo( fadeIn ? 1 : 0, 500, fadeIn ? Easing.CubicIn : Easing.CubicOut );
	}

	public static async Task RaiseAndFade( this Layout layout, View view, bool fadeIn )
	{
		if( fadeIn )
		{
			view.IsVisible = true;
			layout.RaiseChild( view );
		}

		await view.DoFade( fadeIn );

		if( !fadeIn )
			view.IsVisible = false;
	}

	public static async Task RaiseAndFade( this Layout layout, View[] fromViews, View toView )
	{
		var L = fromViews.Length;

		if( L > 0 )
		{
			var Tasks = new Task[ fromViews.Length + 1 ];
			Tasks[ 0 ] = layout.RaiseAndFade( toView, true );

			for( var I = 0; I < L; )
			{
				var V = fromViews[ I++ ];
				Tasks[ I ] = layout.RaiseAndFade( V, false );
			}

			await Task.WhenAll( Tasks );
		}
	}

	public static async Task RaiseAndFade( this Layout layout, View fromView, View toView )
	{
		await layout.RaiseAndFade( [fromView], toView );
	}
}