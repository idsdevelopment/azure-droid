﻿using Preferences = Globals.Preferences;

namespace DroidAz;

public static partial class Extensions
{
	public static string ToWeightString( this decimal weight ) => weight.ToString( Preferences.DecimalWeight ? "0.00" : "0" );
}