﻿#nullable enable

using DroidAz.Views._Customers.Phoenix.Menu;
using DroidAz.Views._Customers.Pml.Menu;
using DroidAz.Views._Customers.Priority.Menu;
using DroidAz.Views._Standard.Menu;
using CarrierPrograms = System.Collections.Generic.Dictionary<string, DroidAz.Applications.Program>;
using Preferences = Globals.Preferences;

// ReSharper disable ArrangeObjectCreationWhenTypeEvident

namespace DroidAz.Modifications;

internal struct Menus
{
	public Func<bool>? IsCarrier;

	public CarrierPrograms Programs;
}

public static class Users
{
	public static string UserName = "";

	public static bool HasCarrierModifications { get; private set; }

	public static string CarrierId
	{
		get => _CarrierId;
		set
		{
			_CarrierId              = value.Trim().ToUpper();
			HasCarrierModifications = false;

			foreach( var Menu in CarrierMenus )
			{
				if( Menu.IsCarrier is not null )
				{
					if( Menu.IsCarrier() )
					{
						_Programs               = Menu.Programs;
						HasCarrierModifications = true;
						break;
					}
				}
				else
					_Programs = Menu.Programs;
			}
		}
	}

	public static CarrierPrograms Programs => _Programs ?? CarrierMenus[ 0 ].Programs;

	internal static Menus[] CarrierMenus =
	[
		//Default Menu
		new Menus {IsCarrier = null, Programs = StandardMenu.Programs},
		new Menus
		{
			IsCarrier = () => CarrierId.Compare( "Coral", StringComparison.OrdinalIgnoreCase ) == 0,
			Programs  = StandardMenu.Programs
		},

		new Menus {IsCarrier = () => Preferences.IsPml, Programs      = PmlMenu.Programs},
		new Menus {IsCarrier = () => Preferences.IsPriority, Programs = PriorityMenu.Programs},
		new Menus {IsCarrier = IsPhoenix, Programs                    = PhoenixMenu.Programs},
		new Menus {IsCarrier = IsIntExp, Programs                     = StandardMenu.Programs}
	];

	private static CarrierPrograms? _Programs;

	private static string _CarrierId = "";

	private static bool IsPhoenix() => CarrierId.Contains( "phoenix", StringComparison.OrdinalIgnoreCase );
	private static bool IsIntExp()  => CarrierId.Contains( "intexp", StringComparison.OrdinalIgnoreCase );
}