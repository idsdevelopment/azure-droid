﻿#nullable enable

namespace Globals;

public static class Service
{
	public static readonly WeakEventSource<bool> OnPingEvent = new();

	// Invoked from ManPage
	public static event EventHandler<bool> OnPing
	{
		add => OnPingEvent.Subscribe( value );
		remove => OnPingEvent.Unsubscribe( value );
	}
}