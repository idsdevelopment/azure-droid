﻿#nullable enable

using Protocol.Data;

namespace Globals;

public static class Gps
{
	public static GpsPoint CurrentGpsPoint
	{
		get
		{
			lock( LockObject )
				return _CurrentGpsPoint;
		}

		set
		{
			lock( LockObject )
				_CurrentGpsPoint = value;

			RaisePositionChanged( value );
		}
	}

	private static readonly WeakEventSource<GpsPoint> _PositionChangeEventSource = new();

	private static readonly object LockObject = new();

	private static GpsPoint _CurrentGpsPoint = new();

	private static bool InRaisePosition;

	public static event EventHandler<GpsPoint> OnPositionChanged
	{
		add => _PositionChangeEventSource.Subscribe( value );
		remove => _PositionChangeEventSource.Unsubscribe( value );
	}

	public static void RaisePositionChanged( GpsPoint e )
	{
		if( !InRaisePosition )
		{
			InRaisePosition = true;
			_PositionChangeEventSource.Raise( null, e );
			InRaisePosition = false;
		}
	}
}