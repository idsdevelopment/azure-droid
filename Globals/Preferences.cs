﻿#nullable enable

using AzureRemoteService;
using Protocol.Data;

namespace Globals;

public static class Preferences
{
	public static DevicePreferences DevicePreferences
	{
		get
		{
			if( !Started )
				Prefs ??= new DevicePreferences();

			else if( !HavePreference )
			{
				WaitingPrefs = true;

				static async void GetPreferences()
				{
					Prefs = await Azure.Client.RequestDevicePreferences();

					HavePreference = Prefs is not null;

					if( !HavePreference )
						Prefs = new DevicePreferences();

					WaitingPrefs = false;
				}

				Tasks.RunVoid( GetPreferences );

				while( WaitingPrefs )
					Thread.Sleep( 100 );
			}
			return Prefs!;
		}
	}

	// Companies
	public static bool IsPml      => DevicePreferences.IsPml;
	public static bool IsPriority => DevicePreferences.IsPriority;

	public static bool EditPiecesWeight             => DevicePreferences.EditPiecesWeight;
	public static bool DecimalWeight                => DevicePreferences.DecimalWeight;
	public static bool EditReference                => DevicePreferences.EditReference;
	public static bool ByLocation                   => DevicePreferences.ByLocation && !EnableDeliveryGeoFence;
	public static bool LocationBatchMode            => DevicePreferences.BatchLocation;
	public static bool BatchUndeliverableAtLocation => DevicePreferences.BatchUndeliverableAtLocation;
	public static bool ByPiece                      => DevicePreferences.ByPiece;
	public static bool ScanShipments                => DevicePreferences.ScanShipments;

	public static bool AllowManualBarcode => DevicePreferences.AllowManualBarcodeInput;
	public static bool AllowUndeliverable => DevicePreferences.AllowUndeliverable;
	public static bool AllowOverScans     => DevicePreferences.AllowOverScans;
	public static bool AllowUnderScans    => DevicePreferences.AllowUnderScans;

	public static bool   AllowDynamsoftBarcodeReader => DevicePreferences.DynamsoftBarcodeReader;
	public static bool   EnablePickupGeoFence        => DevicePreferences.EnablePickupGeoFence;
	public static bool   EnableDeliveryGeoFence      => DevicePreferences.EnableDeliveryGeoFence;
	public static int    DeliveryGeoFenceRadius      => DevicePreferences.DeliveryGeoFenceRadius;
	public static bool   HasDisclaimer               => Disclaimer.IsNotNullOrWhiteSpace();
	public static string Disclaimer                  => DevicePreferences.DeviceDisclaimer.NullTrim();

	public static bool NeedPickupArriveTime   => DevicePreferences.PickupArriveTime;
	public static bool NeedDeliveryArriveTime => DevicePreferences.DeliveryArriveTime;

	public static bool NeedArriveTime       => NeedPickupArriveTime || NeedDeliveryArriveTime;
	public static bool CompulsoryArriveTime => DevicePreferences.CompulsoryArriveTime;

	public static bool PickupWaitTime   => DevicePreferences.PickupWaitTime;
	public static bool DeliveryWaitTime => DevicePreferences.DeliveryWaitTime;

	public static bool EnhancedVisuals => DevicePreferences.EnhancedVisuals;

	private static bool HavePreference;
	private static bool Started;

	private static DevicePreferences? Prefs;
	private static bool               WaitingPrefs;

	public static void ResetPreferences()
	{
		HavePreference = false;
		Started        = true;
	}

#region POP / POD
	public static bool RequirePop          => DevicePreferences.RequirePop;
	public static bool RequirePopSignature => DevicePreferences.RequirePopSignature;
	public static bool RequirePopPicture   => DevicePreferences.RequirePopPicture;
	public static bool RequirePod          => DevicePreferences.RequirePod;
	public static bool RequirePodSignature => DevicePreferences.RequirePodSignature;
	public static bool RequirePodPicture   => DevicePreferences.RequirePodPicture;

	public static bool PopPodMustBeAlpha => DevicePreferences.PopPodMustBeAlpha;
	public static int  PopPodMinLength   => DevicePreferences.PopPodMinLength;
#endregion
}