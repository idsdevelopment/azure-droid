﻿#nullable enable

namespace Globals;

public static class UnhandledExceptions
{
	public static Action<Exception>? OnHandledException;
}