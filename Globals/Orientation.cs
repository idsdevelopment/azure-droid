﻿namespace Globals;

public static class Orientation
{
	public enum ORIENTATION
	{
		BOTH,
		PORTRAIT,
		LANDSCAPE
	}

	public static Action<ORIENTATION> ChangeOrientation;

	public static ORIENTATION LayoutDirection
	{
		get => _LayoutDirection;
		set
		{
			if( value != _LayoutDirection )
			{
				_LayoutDirection = value;
				ChangeOrientation?.Invoke( value );
			}
		}
	}

	private static ORIENTATION _LayoutDirection = ORIENTATION.BOTH;
}