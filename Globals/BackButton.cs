﻿#nullable enable

namespace Globals;

public static class BackButton
{
	public static bool        AllowBackButton = false;
	public static Func<Task>? OnBackKey;
}