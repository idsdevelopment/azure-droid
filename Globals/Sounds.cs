﻿#nullable enable

namespace Globals;

public static class Sounds
{
	public static Action PlayBadScanSound = DoNothing;

	public static Action PlayAlert1Sound = DoNothing;

	public static Action PlayWarningSound = DoNothing;

	public static Action PlayTripNotificationSound = DoNothing;

	private static void DoNothing()
	{
	}
}