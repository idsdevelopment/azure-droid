﻿#nullable enable

namespace Globals;

public class Points
{
	public enum MEASUREMENT
	{
		KILOMETRES,
		MILES,
		NAUTICAL_MILES,
		METRES
	}

	public class Point
	{
		public double Latitude { get; set; }

		public double Longitude { get; set; }

		public bool IsZero => ( Latitude == 0 ) && ( Longitude == 0 );

		public override string ToString() => Latitude + "," + Longitude;
	}

	public bool IsZero => From.IsZero && To.IsZero;

	public Point From = new(),
				 To   = new();

	public double DistanceTo( MEASUREMENT measurement = MEASUREMENT.KILOMETRES )
	{
		var Rlat1  = ( Math.PI * From.Latitude ) / 180;
		var Rlat2  = ( Math.PI * To.Latitude ) / 180;
		var Theta  = From.Longitude - To.Longitude;
		var Rtheta = ( Math.PI * Theta ) / 180;
		var Dist   = ( Math.Sin( Rlat1 ) * Math.Sin( Rlat2 ) ) + ( Math.Cos( Rlat1 ) * Math.Cos( Rlat2 ) * Math.Cos( Rtheta ) );
		Dist = Math.Acos( Dist );
		Dist = ( Dist * 180 ) / Math.PI;
		Dist = Dist * 60 * 1.1515;

		if( double.IsNaN( Dist ) )
			Dist = 0;

		return measurement switch
			   {
				   MEASUREMENT.KILOMETRES     => Dist * 1.609344,
				   MEASUREMENT.METRES         => Dist * 1.609344 * 1000,
				   MEASUREMENT.NAUTICAL_MILES => Dist * 0.8684,
				   _                          => Dist
			   };
	}
}

public static class Distance
{
#if DEBUG
	public const double MIN_DISTANCE = 5; // 5 Metres
	public const int    MAX_STATIONARY_MINUTES = 1;
#else
	public const double MIN_DISTANCE           = 20; // 20 Metres
	public const int    MAX_STATIONARY_MINUTES = 2;
#endif
	public const int MAX_UPDATE_POINTS = 100;

	private static readonly Points   LastPoints = new();
	private static          DateTime LastTime;
	private static readonly object   LockObject = new();

	public static bool IsDistanceOk( double latitude, double longitude )
	{
		lock( LockObject )
		{
			if( LastPoints.IsZero )
			{
				var F = LastPoints.From;
				F.Latitude  = latitude;
				F.Longitude = longitude;
				return true;
			}

			var T = LastPoints.To;
			T.Latitude  = latitude;
			T.Longitude = longitude;

			var Dist = Math.Abs( LastPoints.DistanceTo( Points.MEASUREMENT.METRES ) );

			var Now = DateTime.Now;

			if( ( Dist >= MIN_DISTANCE ) || ( ( Now - LastTime ).TotalMinutes >= MAX_STATIONARY_MINUTES ) )
			{
				LastTime = Now;

				// Make this to next from
				var Temp = LastPoints.From;
				LastPoints.From = T;
				LastPoints.To   = Temp;
				return true;
			}

			return false;
		}
	}
}