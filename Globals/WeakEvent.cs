﻿#nullable enable

namespace Globals;

public class WeakEvent<TEventArg>
{
	protected virtual void RaiseEvent( object? sender, TEventArg b )
	{
		EventSource.Raise( sender, b );
	}

	public delegate void Action( object? sender, TEventArg b );

#region Events
	private readonly WeakEventSource<TEventArg> EventSource = new();

	private event EventHandler<TEventArg> OnEvent
	{
		add => EventSource.Subscribe( value );
		remove => EventSource.Unsubscribe( value );
	}

	public WeakEvent<TEventArg> Add( EventHandler<TEventArg> eventAction )
	{
		OnEvent += eventAction;
		return this;
	}

	public WeakEvent<TEventArg> Remove( EventHandler<TEventArg> eventAction )
	{
		OnEvent -= eventAction;
		return this;
	}

	public static WeakEvent<TEventArg> operator +( WeakEvent<TEventArg> weakEvent, EventHandler<TEventArg> handler ) => weakEvent.Add( handler );
	public static WeakEvent<TEventArg> operator -( WeakEvent<TEventArg> weakEvent, EventHandler<TEventArg> handler ) => weakEvent.Remove( handler );
#endregion

#region Value
	private TEventArg? _Value;

	public TEventArg? Value
	{
		get => _Value;
		set
		{
			_Value = value;

			if( value is not null )
				RaiseEvent( this, value );
		}
	}
#endregion
}