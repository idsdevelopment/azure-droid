﻿#nullable enable

namespace Globals;

public static class Updates
{
	public static SaveUpdateAction? OnSaveUpdate;

	public static RunUpdateAction? OnRunUpdate;

	public delegate void RunUpdateAction( string fileName );

	public delegate void SaveUpdateAction( string fileName, uint version, byte[] bytes );
}