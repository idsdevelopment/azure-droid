﻿#nullable enable

namespace Globals;

public static class BarcodeScanners
{
	public static bool AllowThirdPartyScanner,
	                   UseThirdPartyScanner;


	public static Func<bool, IBarcodeScanner> InitDynamSoftScanner = null!;

	public static IBarcodeScanner DynamsoftBarcodeScanner => AndroidDynamsoftBarcodeScanner ??= InitDynamSoftScanner( Debug.Testing );

	private static IBarcodeScanner? AndroidDynamsoftBarcodeScanner;
}