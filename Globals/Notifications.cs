﻿using Xamarin.Forms;

namespace Globals;

public static class Notifications<T>
{
	private static WeakEvent<INotification> EventHandler = new();

	public interface INotification
	{
		public T Message { get; protected set; }
	}
}