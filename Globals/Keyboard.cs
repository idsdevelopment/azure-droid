﻿#nullable enable

using Timer = System.Timers.Timer;

namespace Globals;

public static class Keyboard
{
	public static Action<bool>? ShowHide;

	public static bool Show
	{
		get => _Show;
		set
		{
			_Show = value;
			ShowHide?.Invoke( value );
		}
	}

	private static bool _Show = true;

#region KeyPress
	public class OnKeyPressData
	{
		public bool Handled;
		public char Key;
	}

	// ReSharper disable once InconsistentNaming
	public static readonly WeakEventSource<OnKeyPressData> _OnKeyPress = new();

	public static event EventHandler<OnKeyPressData> OnKeyPress
	{
		add => _OnKeyPress.Subscribe( value );
		remove => _OnKeyPress.Unsubscribe( value );
	}

	public enum BUTTON : byte
	{
		UNKNOWN,
		LEFT_BUTTON_1,
		RIGHT_BUTTON_1
	}

	public class OnButtonPressData
	{
		public BUTTON Button = BUTTON.UNKNOWN;
		public bool   Handled;
	}

	// ReSharper disable once InconsistentNaming
	public static readonly WeakEventSource<OnButtonPressData> _OnButtonPress = new();

	public static event EventHandler<OnButtonPressData> OnButtonPress
	{
		add => _OnButtonPress.Subscribe( value );
		remove => _OnButtonPress.Unsubscribe( value );
	}
#endregion

#region Double Tap
	private static bool EventSet;

	private static readonly Timer TappedTimer = new( 400 )
	                                            {
		                                            AutoReset = false
	                                            };

	public static void DoubleTap( Action doubleTapped )
	{
		if( !EventSet )
		{
			EventSet = true;

			TappedTimer.Elapsed += ( _, _ ) =>
			                       {
				                       TappedTimer.Stop();
			                       };
		}

		if( !TappedTimer.Enabled )
			TappedTimer.Start();
		else // Second tap if timer is enabled
		{
			TappedTimer.Stop();
			doubleTapped();
		}
	}
#endregion
}